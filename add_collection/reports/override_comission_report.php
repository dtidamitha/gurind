<?php

$from = $_REQUEST['from'];
$to = $_REQUEST['to'];

require_once ('../../config.php');
require_once ('../../reporting_part/report_header_oa.php');

if($from == "" && $to==""){
	echo "<script type='text/javascript'>alert('please select filters')</script>";
}else{
	
	$agent_add_count ="SELECT
	                        Count(collection_adds.agentid) AS COUNT,
	                        collection_agents.fname,
	                        collection_agents.agent_contactno,
	                        collection_agents.agent_code,
	                        collection_team.team_code,
	                        collection_team.team_name
                         FROM
	                       collection_adds
                        INNER JOIN collection_agents ON collection_adds.agentid = collection_agents.id
                        INNER JOIN collection_team ON collection_adds.team_id = collection_team.id
                        WHERE
                             collection_adds.add_collect_date BETWEEN '$from' AND '$to'
                        GROUP BY
	                         collection_adds.agentid";
?>

					<table style="border-collapse:collapse; margin: 0 auto;" border="1" id="report" width="98%">
						<thead>
						<tr>
							<th>Agent Name</th>
							<th>Agent Code</th>
							<th>Contact No</th>
							<th>Team Code</th>
							<th>Team Name</th>
							<th>No Of Adds</th>	
							<th>Override Comission</th>							
						</tr>
						</thead>

						<tbody>
		<?php
			$count_result = mysqli_query ($con_main, $agent_add_count);
		
			while ($row = mysqli_fetch_assoc ($count_result)){
				$count = $row['COUNT'];

				 $override_commission_select = "SELECT
	                                    collection_commission.id,
	                                    collection_commission.team_leader,
	                                    collection_commission.agent,
	                                    collection_commission.min,
	                                    collection_commission.max
                                      FROM
	                                    collection_commission
                                      WHERE
	                                    collection_commission.min <= '$count'
                                      AND collection_commission.max >= '$count'";

				$override_commission_result = mysqli_query ($con_main, $override_commission_select);
				$commission_row = mysqli_fetch_assoc($override_commission_result);
		?>
							<tr>
								<td><?php echo ($row['fname']); ?></td>
								<td><?php echo ($row['agent_code']); ?></td>
								<td><?php echo ($row['agent_contactno']); ?></td>
								<td><?php echo ($row['team_code']); ?></td>
								<td><?php echo ($row['team_name']); ?></td>
								<td><?php echo ($row['COUNT']); ?></td>
								<td><?php echo ($commission_row['agent']); ?></td>
								
							</tr>
	<?php
							}
	}
	?>
					</tbody>	
		        </table>
