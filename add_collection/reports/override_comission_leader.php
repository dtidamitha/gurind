<?php

$from = $_REQUEST['from'];
$to = $_REQUEST['to'];

require_once ('../../config.php');
require_once ('../../reporting_part/report_header_oa.php');
	
	$leader_details ="SELECT
						Count(collection_adds.team_id) AS C,
						collection_agents.fname AS NAME,
						collection_team.team_code,
						collection_team.team_name,
						collection_agents.agent_contactno,
						collection_agents.agent_code
					FROM
						collection_adds
					INNER JOIN collection_agents ON collection_adds.team_id = collection_agents.team_id
					INNER JOIN collection_team ON collection_agents.team_id = collection_team.id
					WHERE
						collection_adds.team_id != '10'
					AND collection_adds.team_id != '13'
					AND collection_agents.agent_status = '1'
					AND collection_adds.add_collect_date BETWEEN '$from'
					AND '$to'
					GROUP BY
						collection_adds.team_id";

$count_result = mysqli_query ($con_main, $leader_details);


?>

					<table style="border-collapse:collapse; margin: 0 auto;" border="1" id="report" width="98%">
						<thead>
						<tr>
							<th>Leader Name</th>
							<th>Leader Code</th>
							<th>Contact No</th>
							<th>Team Code</th>
							<th>Team Name</th>	
							<th>No Of Adds</th>
							<th>Allowance</th>	
							<th>Petrol</th>	
							<th>Extra Ads</th>
							<th>Extra Payment</th>						
						</tr>
						</thead>

						<tbody>
		<?php

             $tot_adds = 0;
		     $tot_allow = 0;
		     $tot_petrol = 0;
             $extra_ads = 0;
             $extra_payment = 0;
             $ex_adds = 0;
             $ex_payment = 0;
		      
				while ($row = mysqli_fetch_assoc ($count_result)) {
				$count = $row['C'];

				if($count>100){                      
                      $extra_ads = $count - 100;
                      $extra_payment = $extra_ads * 125;

                      $ex_adds = $ex_adds + $extra_ads;
                      $ex_payment = $ex_payment + $extra_payment;
				  }
				 $override_commission_select = "SELECT
													comission_bpe.allowance,
													comission_bpe.petrol
												FROM
													comission_bpe
												WHERE
													comission_bpe.min <= '$count'
												AND comission_bpe.max >= '$count'";
				

				$override_commission_result = mysqli_query ($con_main, $override_commission_select);
				$commission_row = mysqli_fetch_assoc($override_commission_result);

				$tot_allow = $tot_allow + (double)$commission_row['allowance'];
				$tot_petrol = $tot_petrol + (double)$commission_row['petrol'];
				$tot_adds = $tot_adds + $count;
		?>
							<tr>
								<td><?php echo ($row['name']); ?></td>
								<td><?php echo ($row['agent_code']); ?></td>
								<td><?php echo ($row['agent_contactno']); ?></td>
								<td><?php echo ($row['team_code']); ?></td>
								<td><?php echo ($row['team_name']); ?></td>
								<td><?php echo ($row['C']); ?></td>
								<td><?php echo (number_format($commission_row['allowance'],2)); ?></td>
								<td><?php echo (number_format($commission_row['petrol'],2)); ?></td>
								<td><?php echo ($extra_ads);?></td>
								<td><?php echo ($extra_payment); ?></td>
							</tr>
	<?php
	$extra_ads = 0;
	$extra_payment = 0;
		}
	?>
	<tr>
		<td colspan="5"><strong><center>TOTAL</center></strong></td>
			<td><strong><?php echo ($tot_adds); ?></strong></td>
			<td><strong><?php echo (number_format($tot_allow,2)); ?></strong></td>
			<td><strong><?php echo (number_format($tot_petrol,2)); ?></strong></td>
			<td><strong><?php echo ($ex_adds); ?></strong></td>
			<td><strong><?php echo ($ex_payment); ?></strong></td>
		</td>
	</tr>
					</tbody>	
		        </table>
