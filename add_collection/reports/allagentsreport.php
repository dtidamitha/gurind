<?php

$from = $_REQUEST['from'];
$to = $_REQUEST['to'];
$team = $_REQUEST['team'];
$agent = $_REQUEST['agent'];
$method = $_REQUEST['method'];


require_once ('../../config.php');
if($agent!=""){
$query2 = "SELECT
	collection_agents.fname,
	collection_agents.agent_code,
	collection_agents.bank,
	collection_agents.bank_accountno
FROM
	`collection_agents`
WHERE
	collection_agents.id = '$agent'";

$sql2 = mysqli_query ($con_main, $query2);
while ($row2 = mysqli_fetch_array ($sql2)){
	$name = $row2['fname'];
	$code = $row2['agent_code'];
	$bank = $row2['bank'];
	$account = $row2['bank_accountno'];

}
}

require_once ('../../reporting_part/report_header_oa1.php');

if($agent != "")
{

	$query = "SELECT
	collection_agents.fname,
	collection_agents.agent_code,
	collection_adds.add_collect_date,	
	collection_adds.amount,
	((collection_adds.amount*$method)/100) AS COMIS,	
	collection_adds.customer_name,
	collection_team.team_name,
	collection_category.category,
	collection_adds.receipt_no
FROM
	collection_adds
INNER JOIN collection_agents ON collection_adds.agentid = collection_agents.id
INNER JOIN collection_team ON collection_adds.team_id = collection_team.id
INNER JOIN collection_category ON collection_adds.category = collection_category.category_id
WHERE
	collection_adds.agentid = '$agent'
	HAVING
collection_adds.add_collect_date BETWEEN '$from' AND '$to'
ORDER BY
collection_adds.add_collect_date ASC";

}
else if($agent==""){

	$query = "SELECT
	collection_agents.fname,
	collection_agents.agent_code,
	collection_adds.add_collect_date,
	collection_adds.amount,
	((collection_adds.amount*$method)/100) AS COMIS,
	collection_adds.customer_name,
	collection_team.team_name,
	collection_category.category,
	collection_adds.receipt_no
FROM
	collection_adds
INNER JOIN collection_agents ON collection_adds.agentid = collection_agents.id
INNER JOIN collection_team ON collection_adds.team_id = collection_team.id
INNER JOIN collection_category ON collection_adds.category = collection_category.category_id
WHERE
	collection_adds.team_id = '$team'
	HAVING
collection_adds.add_collect_date BETWEEN '$from' AND '$to'
ORDER BY
collection_adds.add_collect_date ASC";
}  

?>

					<table style="border-collapse:collapse; margin: 0 auto;" border="1" id="report" width="98%">
						<thead>
						<tr>
							<th>Add Collect Date</th>
							<th>Category</th>														
							<th>Receipt No</th>
							<th>Customer Name</th>	
							<th>Agent Name</th>	
							<th>Amount</th>	
							<th>Comission</th>										
						</tr>
						</thead>

						<tbody>
		<?php
			$sql = mysqli_query ($con_main, $query);
		
		    $tot_amt = 0;
		    $tot_com = 0;

			while ($row = mysqli_fetch_array ($sql)){

				$tot_amt = $tot_amt + (double)$row['amount'];
				$tot_com = $tot_com + (double)$row['COMIS'];
		?>
							<tr>
								<td><?php echo ($row['add_collect_date']); ?></td>
								<td><?php echo ($row['category']); ?></td>
								<td><?php echo ($row['receipt_no']); ?></td>							
								<td><?php echo ($row['customer_name']); ?></td>
								<td><?php echo ($row['fname']); ?></td>	
								<td><?php echo (number_format($row['amount'],2)); ?></td>  
								<td><?php echo (number_format($row['COMIS'],2)); ?></td>																								
							</tr>
	<?php
							}
	?>
	<tr>
		<td colspan="5"><strong><center>TOTAL</center></strong></td>
		    <td><strong><?php echo (number_format($tot_amt,2)); ?></strong></td>
			<td><strong><?php echo (number_format($tot_com,2)); ?></strong></td>
		</td>
	</tr>
					</tbody>	
		        </table>                               
