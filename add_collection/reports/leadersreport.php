<?php

$from = $_REQUEST['from'];
$to = $_REQUEST['to'];

require_once ('../../config.php');
require_once ('../../reporting_part/report_header_oa.php');

if($from == "" && $to==""){
	echo "<script type='text/javascript'>alert('please select filters')</script>";
}else{
	$select_team_leader = "SELECT
	collection_agents.fname AS `name`,
	collection_team.team_code,
	collection_agents.agent_contactno,
	collection_team.id
	FROM
	collection_agents
	INNER JOIN collection_team ON collection_agents.team_id = collection_team.id
	WHERE
	collection_agents.agent_status = '1'";

?>

					<table style="border-collapse:collapse; margin: 0 auto;" border="1" id="report" width="98%">
						<thead>
						<tr>
							<th>Agent Name</th>
							<th>Team Code</th>
							<th>Contact No</th>
							<th>No Of Ads</th>
							<th>Comission</th>							
						</tr>
						</thead>

						<tbody>
		<?php
			$team_leader_result = mysqli_query($con_main, $select_team_leader);
		
			while ($row = mysqli_fetch_assoc ($team_leader_result)){
				$team_id = $row['id'];

				$commission_select = "SELECT
				Sum(collection_adds.amount) AS AC,
				FORMAT(
					(
						Sum(collection_adds.amount) / 100
					) * 5,
					0
				) AS COM_OF_5PRESE,
				Count(collection_adds.team_id) AS NO_OF_ADDS,
				collection_adds.team_id
				FROM
				collection_adds
				WHERE
				collection_adds.team_id != '10'
				AND collection_adds.team_id = $team_id
				AND collection_adds.add_collect_date BETWEEN '$from' AND '$to'
				GROUP BY
				collection_adds.team_id";

				$commission_result = mysqli_query ($con_main, $commission_select);
				$commission_row = mysqli_fetch_assoc($commission_result);
		?>
							<tr>
								<td><?php echo ($row['name']); ?></td>
								<td><?php echo ($row['team_code']); ?></td>
								<td><?php echo ($row['agent_contactno']); ?></td>
								<td><?php echo ($commission_row['NO_OF_ADDS']); ?></td>
								<td><?php echo ($commission_row['COM_OF_5PRESE']); ?></td>
							</tr>
	<?php
							}
	}
	?>
					</tbody>	
		        </table>
