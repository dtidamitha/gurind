<?php

$from = $_REQUEST['from'];
$to = $_REQUEST['to'];
$team = $_REQUEST['team'];
$agent = $_REQUEST['agent'];
require_once ('../../config.php');
require_once ('../../reporting_part/report_header_oa1.php');
 
if($agent != "")
{

	$query = "SELECT
	collection_agents.fname,
	collection_agents.agent_code,
	collection_agents.bank_accountno,
	collection_adds.add_collect_date,
	collection_adds.receipt_no,	
	collection_adds.amount,
	(collection_adds.amount*25)/100 AS COMIS,
	collection_adds.customer_name,
	collection_team.team_name,
	collection_category.category
FROM
	collection_adds
INNER JOIN collection_agents ON collection_adds.agentid = collection_agents.id
INNER JOIN collection_team ON collection_adds.team_id = collection_team.id
INNER JOIN collection_category ON collection_adds.category = collection_category.category_id
WHERE
	collection_adds.agentid = '$agent'
	HAVING
collection_adds.add_collect_date BETWEEN '$from' AND '$to'";

}

?>

<table style="border-collapse:collapse; margin: 0 auto;" border="1" id="report" width="98%">
						<thead>
						<tr>
							<!-- <th>Agent Name</th>
							<th>Agent Code</th>
							<th>Team Name</th> -->
							<th>Add Collect Date</th>							
							<th>Category</th>
							<th>Amount</th>
							<th>Comission</th>
							<th>Receipt No</th>
							<th>Customer Name</th>
													
						</tr>
						</thead>

						<tbody>
		<?php
			$sql = mysqli_query ($con_main, $query);
		
			while ($row = mysqli_fetch_array ($sql)){
		?>
							<tr>
								<!-- <td><?php echo ($row['fname']); ?></td>
								<td><?php echo ($row['agent_code']); ?></td>
								<td><?php echo ($row['team_name']); ?></td> -->
								<td><?php echo ($row['add_collect_date']); ?></td>
								<td><?php echo ($row['category']); ?></td>
								<td><?php echo ($row['amount']); ?></td>
								<td><?php echo ($row['COMIS']); ?></td>
								<td><?php echo ($row['receipt_no']); ?></td>
								<td><?php echo ($row['customer_name']); ?></td>
															
							</tr>
	<?php
							}
							$name = $row['fname'];
	?>
					</tbody>	
		        </table>                               
