<?php

$from = $_REQUEST['from'];
$to = $_REQUEST['to'];
$method = $_REQUEST['method'];

require_once ('../../config.php');
require_once ('../../reporting_part/report_header_oa.php');

if($from == "" && $to==""){
	echo "<script type='text/javascript'>alert('please select filters')</script>";
}else
{

	$query = "SELECT
					Count(collection_adds.agentid) AS COUNT,
					collection_agents.fname,
					collection_agents.agent_code,
					collection_team.team_code,
					collection_agents.agent_contactno
				FROM
					collection_adds
				INNER JOIN collection_agents ON collection_adds.agentid = collection_agents.id
				INNER JOIN collection_team ON collection_agents.team_id = collection_team.id
				WHERE
					collection_adds.add_collect_date BETWEEN '$from'
				AND '$to'
				AND collection_agents.agent_code NOT LIKE '%IND%'
				GROUP BY
					collection_adds.agentid
				ORDER BY
					collection_adds.add_collect_date ASC";
							 }  

?>

					<table style="border-collapse:collapse; margin: 0 auto;" border="1" id="report" width="98%">
						<thead>
						<tr>
							<th>Agent Name</th>
							<th>Agent Code</th>
							<th>Team Code</th>
							<th>Contact No</th>
							<th>No Of Ads</th>
							<th>Allowance</th>
							<th>Petrol</th>
							<th>No Of Extra Ads</th>
							<th>Additional Payment</th>
							<!-- <th>Add Collect Date</th>	 -->						
						</tr>
						</thead>

						<tbody>
		<?php
			$sql = mysqli_query ($con_main, $query);
		
		     $tot_adds = 0;
		     $tot_com = 0;
		     $tot_petrol = 0;
             $extra_ads = 0;
             $extra_payment = 0;
             $ex_adds = 0;
             $ex_payment = 0;
			while ($row = mysqli_fetch_array ($sql)){

				$add_count = $row['COUNT'];

				  if($add_count>30){                      
                      $extra_ads = $add_count - 30;
                      $extra_payment = $extra_ads * 400;

                      $ex_adds = $ex_adds + $extra_ads;
                      $ex_payment = $ex_payment + $extra_payment;
				  }

				$query2 =  "SELECT
								comission_bdo.min,
								comission_bdo.max,
								comission_bdo.allowance,
								comission_bdo.petrol
							FROM
								`comission_bdo`
							WHERE
								comission_bdo.min <= '$add_count'
							AND comission_bdo.max >= '$add_count'";
				$query2_result = mysqli_query ($con_main, $query2);
				$query2_row = mysqli_fetch_assoc($query2_result);

				 $tot_adds = $tot_adds + (double)$row['COUNT'];
				 $tot_com = $tot_com + (double)$query2_row['allowance'];
				 $tot_petrol = $tot_petrol + (double)$query2_row['petrol'];
		?>
							<tr>
								<td><?php echo ($row['fname']); ?></td>
								<td><?php echo ($row['agent_code']); ?></td>
								<td><?php echo ($row['team_code']); ?></td>
								<td><?php echo ($row['agent_contactno']); ?></td>
								<td><?php echo ($row['COUNT']); ?></td>
								<td><?php echo (number_format($query2_row['allowance'],2));?></td>
								<td><?php echo (number_format($query2_row['petrol'],2));?></td>	
								<td><?php echo ($extra_ads);?></td>
                                <td><?php echo (number_format($extra_payment,2));?></td>
							</tr>
	<?php
	$extra_ads = 0;
	$extra_payment = 0;
	}

	?>

	<tr>
		<td colspan="4"><strong><center>TOTAL</center></strong></td>
		    <td><strong><?php echo ($tot_adds); ?></strong></td>
			<td><strong><?php echo (number_format($tot_com,2)); ?></strong></td>
			<td><strong><?php echo (number_format($tot_petrol,2)); ?></strong></td>
			<td><strong><?php echo ($ex_adds); ?></strong></td>
			<td><strong><?php echo (number_format($ex_payment,2)); ?></strong></td>
		</td>
	</tr>
					</tbody>	
		        </table>
