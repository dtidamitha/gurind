<?php
	session_start();
	
	if (empty($_SESSION['ACCESS_CODE']) || $_SESSION['ACCESS_CODE'] == NULL){
		header ('Location: login.php');
		exit;
	}
	
	$folder_depth = "";
	$prefix = "";
	
	$folder_depth = substr_count($_SERVER["PHP_SELF"] , "/");
	$folder_depth = ($folder_depth == false) ? 2 : (int)$folder_depth;
	
    $prefix = str_repeat("../", $folder_depth - 2);
    
    $title_suffix = " Designation Master";
?>
<?php include $prefix.'config.php'; ?>
<?php include $prefix.'menu.php'; ?>
<?php include $prefix.'template_start.php'; ?>
<?php include $prefix.'page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-briefcase"></i>Assigning Teams For Sales Managers<br>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="../home.php">Home</a></li>
        <li>Comission Master</li>
        <li>sales managers team</li>
    </ul>
    <!-- END Blank Header -->
	
    <div class="row">
        <div class="col-md-12">
            <div class="block">               
               <!-- Basic Form Elements Content -->
                <form id="form-main" name="form-main" action="designation_crud.php" method="post"  class="form-horizontal form-bordered">
					<div class="form-group">
                        <label class="col-md-2 control-label" for="salesmanager">Sales Manager</label>
                            <div class="col-md-4">
                               <select id="salesmanager" name="salesmanager" class="select-chosen" data-placeholder="Choose sales manager"> 
                                   <option value="" selected disabled>Select Sales Manager Code</option>
                                   <?php
                                    $query="SELECT
                                                collection_agents.id,
                                                collection_agents.agent_code,
                                                collection_agents.fname
                                            FROM
                                                collection_agents
                                            WHERE
                                                collection_agents.agent_code LIKE '%SM%'";

                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['id']."\">".$type['agent_code']." - ".$type['fname']."</option>");
                                    }
                                ?>
                                </select>
                            </div>

                        <label class="col-md-2 control-label" for="teamcode">Team Code</label>
                            <div class="col-md-4">
                               <select id="teamcode" name="teamcode" class="select-chosen" data-placeholder="Choose team code"> 
                                   <option value="" selected disabled>Select Team Code</option>
                                   <?php
                                    $query="SELECT
                                                collection_team.team_code,
                                                collection_team.`status`,
                                                collection_team.id,
                                                collection_team.team_name
                                            FROM
                                                `collection_team`
                                            WHERE
                                                collection_team.`status` = '1'
                                            AND collection_team.id != '10'
                                            AND collection_team.id != '13'";

                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['id']."\">".$type['team_code']." - ".$type['team_name']."</option>");
                                    }
                                ?>


                               </select>
                            </div>             
                    </div>
                    <div class="form-group form-actions">
                        <input type="hidden" name="id" id="id" value="0"/>

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success primary-btn pull-right"><i class="fa fa-angle-right"></i> Assing</button>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-plus"></i> New</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Block -->
            </div>
		</div>
        <!-- END Example Content -->
    </div>
    <!-- END Example Block -->

    <!-- Table Block -->
    <div class="block full">
        <!-- Table Title -->
        <div class="block-title">
            <small>currently assigned teams for sales managers</small>
        </div>
        
        <div class="table-responsive"><table id="table-data" class="table table-condensed table-striped table-hover"></table></div>        
    </div>    
</div>


<?php include $prefix.'page_footer.php'; ?>
<?php include $prefix.'template_scripts.php'; ?>
<?php include $prefix.'template_end.php'; ?>

<script type="text/javascript">

	/*********** Data-table Initialize ***********/	 	
	App.datatables();

    var dt = $('#table-data').DataTable({
        "processing": true,
        "serverSide": true,
        "select": true,
        "columns": [
            {"data": "salesmanager", "name": "salesmanager", "title": "Sales Manager" },
            {"data": "smcode", "name": "smcode", "title": "Sales Manager Code" },
            {"data": "team", "name": "team", "title": "Team" },
            {"data": "teamcode", "name": "teamcode", "title": "Team Code" },
            {"data": "actions", "name": "actions","title":"Actions", "searchable": false, "orderable": false, 
                mRender: function (data, type, row) {
                    return '<div class="btn-group btn-group-xs"><button id="btn-row-edit" class="btn btn-primary" title="Edit"><i class="fa fa-pencil"></i></button></div>'
                }
            }
        ],
        "columnDefs": [
            {"className": "dt-center", "targets": [1,2]}
        ],
        "language": {
            "emptyTable": "No details to show..."
        },
        "ajax": "data/grid_data_sales_managers_team.php"
    });

	$('.dataTables_filter input').attr('placeholder', 'Search');

    $("#table-data tbody").on('click', '#btn-row-edit', function() {
        var str_id = $(this).closest('tr').attr('id');
        var arr_id = str_id.split("_");

        var row_id = arr_id[1];

        $.ajax({
            url: 'data/data_sales_managers_team.php',
            data: {
                id: row_id
            },
            method: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $('#table-data tbody #'+str_id+' #btn-row-edit').button('loading');
                NProgress.start();
            },
            error: function (e) {
                $.bootstrapGrowl('<h4>Error!</h4> <p>Error retrieving data</p>', {
                    type: 'danger',
                    delay: 2500,
                    allow_dismiss: true
                });

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            },
            success: function (r) {
                if (!r.result) {
                    $.bootstrapGrowl('<h4>Error!</h4> <p>'+r.message+'</p>', {
                        type: 'danger',
                        delay: 2500,
                        allow_dismiss: true
                    });
                }else{
                    $('#id').val(r.data[0].id);
                    $('#teamcode').val(r.data[0].team_id);
                    $('#teamcode').trigger("chosen:updated");

                    $('#salesmanager').val(r.data[0].sm_id);
                    $('#salesmanager').trigger("chosen:updated");
                }

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            }
        });
    });
    /*********** Table Control End ***********/	

    $('#form-main').on('submit', function (e){
		e.preventDefault();
		
		var id = $('#id').val();
		var op = (id == 0) ? "insert" : "update";
		
		var formdata = $('#form-main').serializeArray();
		formdata.push({'name':'operation','value':op});
		
		$.ajax({
			url: 'sales_managers_team_crud.php',
			data: formdata,
			success: function(r){
				var msg_typ = "info";
                var msg_txt = "";

                if (r.result){
                    msg_typ = 'success';
                    msg_txt = '<h4>Success!</h4> <p>Team saved</p>';

                    $('#form-main').trigger('reset');
                }else{
                    msg_typ = 'danger';
                    msg_txt = '<h4>Error!</h4> <p>'+r.message+'</p>';
                }

                $.bootstrapGrowl(msg_txt, {
                    type: msg_typ,
                    delay: 2500,
                    allow_dismiss: true
                });

                dt.ajax.reload();
                dt.draw();
			}
		});
	});

    $('#form-main').on('reset', function (e){
        $('#id').val("0");
        $('#teamcode').val("");
        $('#teamcode').trigger("chosen:updated");
        
        $('#salesmanager').val("");
        $('#salesmanager').trigger("chosen:updated");
    });	
</script>	
<?php mysqli_close($con_main); ?>