<?php
	header('Content-Type: application/json');
	
	session_start();
	
	require_once ('../config.php');

	$user = $_SESSION['USER_CODE'];
	$op = $_REQUEST['operation']; 
	$id = $_REQUEST['id'];  
	$teamid = $_REQUEST['teamid']; 
	$receiptno = $_REQUEST['receiptno']; 
	$cusname = $_REQUEST['cusname']; 
	$category = $_REQUEST['category'];
	$amount = $_REQUEST['amount'];
	$agentcode = $_REQUEST['acode'];
	$catid = $_REQUEST['category'];
	$teamid = $_REQUEST['teamid'];
	$collectdate = $_REQUEST['collectdate'];
	$today = date("Y-m-d");

	$query = "";
	$success = true;
	$message = "";
	$responce = array();

	$flag=0;
	
	if ($op == "insert"){
$no_of_adds = "SELECT
	              Max(collection_adds.no_of_adds) AS COUNT
               FROM
	             `collection_adds`
               WHERE
	              collection_adds.agentid = '$agentcode'";

$no_of_adds_result = mysqli_query ($con_main, $no_of_adds);
while ($row = mysqli_fetch_array ($no_of_adds_result)){
	$count = $row['COUNT'];
    
    if($count==NULL){
        $flag = 1;
    }else{
           $flag = $count+1;
    }
	
}


$query = "INSERT INTO `mobiman_main`.`collection_adds` (
    `agentid`,
	`receipt_no`,
	`team_id`,
	`customer_name`,
	`category`,
	`amount`,
	`add_collect_date`,
	`entered_date`,
	`entered_by`,
	`no_of_adds`
)	
VALUES
	(
	    '$agentcode',
		'$receiptno',
		'$teamid',
		'$cusname',
		'$catid',
		'$amount',
		'$collectdate',
		'$today',
		'$user',
		'$flag'
	);";
	}
	else if ($op == "update"){
		$query = "UPDATE `mobiman_main`.`collection_adds`
SET 
 `agentid` = '$agentcode',
 `receipt_no` = '$receiptno',
 `team_id` = '$teamid',
 `customer_name` = '$cusname',
 `category` = '$catid',
 `amount` = '$amount',
 `add_collect_date` = '$collectdate',
 `entered_date` = '$today',
 `entered_by` = '$user'

WHERE
	(`id` = '$id');";
	}
	
	$sql = mysqli_query ($con_main, $query);
	
	$id = ($op == "insert") ? mysqli_insert_id($con_main) : $id;
	
	if ($sql){
		$success = true;
		$message = "Success";
	}else{
		$success = false;
		$message = "Error SQL: (".mysqli_errno($con_main).") ".mysqli_error($con_main);
	}
	
	$responce['operation'] = $op;
	$responce['result'] = $success;
	$responce['id'] = $id;
	$responce['message'] = $message;
	$responce['debug'] = $query;

	
	echo (json_encode($responce));

	
	mysqli_close($con_main);
?>