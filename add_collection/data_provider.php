<?php
header('Content-Type: application/json');

include ('../config.php');

$where = "";
$responce = array();
$data = "";
$result = true;
$message = "";

$team = (isset($_REQUEST['team']) && $_REQUEST['team'] != NULL && !empty($_REQUEST['team'])) ? $_REQUEST['team'] : 0;

$query = "SELECT
collection_agents.id,
collection_agents.agent_code,
collection_agents.team_id
FROM
collection_agents
WHERE
collection_agents.team_id = '$team'";

$sql = mysqli_query ($con_main, $query);

if (!$sql){
    $result = false;
    $message .= " Error Sql : (".mysqli_errno($con_main).") ".mysqli_error($con_main).". ";
}else{
    $num_rows = mysqli_num_rows($sql);

    if ($num_rows > 0){
        $i = 0;

        while ($rows = mysqli_fetch_assoc ($sql)){
            $data[$i] = $rows;
            $i++;
        }

        $result = true;
    }else{
        $result = true;
        $message .= "Empty results ";
    }
}

mysqli_close($con_main);

$responce['data'] = $data;
$responce['result'] = $result;
$responce['message'] = $message;

echo (json_encode($responce));
?>