<?php
	session_start();
	
	if (empty($_SESSION['ACCESS_CODE']) || $_SESSION['ACCESS_CODE'] == NULL){
		header ('Location: login.php');
		exit;
	}
	
	$folder_depth = "";
	$prefix = "";
	
	$folder_depth = substr_count($_SERVER["PHP_SELF"] , "/");
	$folder_depth = ($folder_depth == false) ? 2 : (int)$folder_depth;
	
    $prefix = str_repeat("../", $folder_depth - 2);
    
    $title_suffix = " Department Master";
?>
<?php include $prefix.'config.php'; ?>
<?php include $prefix.'menu.php'; ?>
<?php include $prefix.'template_start.php'; ?>
<?php include $prefix.'page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-podium"></i>Advertisements Collection<br>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="../home.php">Home</a></li>
        <li>Adds</li>
    </ul>
    <!-- END Blank Header -->
		
	
    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
					<h2>Adds</h2>
				</div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <form id="form-main" name="form-main" action="provider_crud.php" method="post"  class="form-horizontal form-bordered">

                <div class="form-group">
                    <label class="col-md-2 control-label" for="collectdate">Select Add Collect Date</label>
                    <div class="col-md-4">
                      <input type="text" id="collectdate" name="collectdate" class="form-control input-datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">  
                      </div> 

                        <label class="col-md-2 control-label" for="acode">Agent Code</label>
                        <div class="col-md-4">
                            <select id="acode" name="acode" class="select-chosen" data-placeholder="Choose Agent Code"> 
                                   <option></option>
                                   <?php
                                    $query="SELECT
                                             collection_agents.id,
                                             collection_agents.agent_code,
                                             collection_agents.fname
                                            FROM
                                             `collection_agents`
                                            WHERE
                                              collection_agents.agent_code NOT LIKE '%SM%'";

                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['id']."\">".$type['agent_code']." - ".$type['fname']."</option>");
                                    }
                                ?>


                               </select>
                        </div>
                </div>  

                <div class="form-group">                      
                        <label class="col-md-2 control-label" for="aname">Agent Name</label>
                        <div class="col-md-4">
                                <input type="text" id="aname" name="aname" class="form-control" placeholder="Agent Name" readonly>
                        </div> 

                        <label class="col-md-2 control-label" for="teamcode">Team Code</label>
                        <div class="col-md-4">
                                <input type="text" id="teamcode" name="teamcode" class="form-control" placeholder="Team Code" readonly>
                                <input type="hidden" name="teamid" id="teamid" value="0" />

                        </div>
                        
                </div>
                       
                <div class="form-group">  
                           <label class="col-md-2 control-label" for="receiptno">Receipt No</label>        
                        <div class="col-md-4">                         
                            <input type="text" id="receiptno" name="receiptno" class="form-control" placeholder="Enter Receipt No">
                        </div>

                        <label class="col-md-2 control-label" for="cusname">Customer Name</label>
                        <div class="col-md-4">                           
                            <input type="text" id="cusname" name="cusname" class="form-control" placeholder="Enter Customer Name">
                        </div>
                </div>
                    

                    <div class="form-group">  
                           <label class="col-md-2 control-label" for="category">Category</label>        
                        <div class="col-md-4">                         
                            <select id="category" name="category" class="select-chosen" data-placeholder="Choose Category"> 
                                   <option></option>
                                   <?php
                                    $query="SELECT
                                           collection_category.category_id,
                                           collection_category.category,
                                           collection_category.`status`
                                            FROM
                                              `collection_category`
                                            WHERE
                                               collection_category.`status` = '1'";

                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['category_id']."\">".$type['category']."</option>");
                                    }
                                ?>
                               </select>
                        </div>

                         <label class="col-md-2 control-label" for="amount">Amount</label>
                        <div class="col-md-4">                           
                            <input type="text" id="amount" name="amount" class="form-control" placeholder="Enter Amount" value="2000">
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <input type="hidden" name="id" id="id" value="0" />

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success primary-btn pull-right"><i class="fa fa-angle-right"></i> Submit</button>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Block -->
		    </div>
            <!-- END Example Content -->
        </div>
        <!-- END Example Block -->
    </div>

    <!-- Table Block -->
    <div class="block full">
        <!-- Table Title -->
        <div class="block-title">
            <h2>Adds</h2><small>Adds currently exist in the system</small>
        </div>
        <!-- END Table Title -->

        <!-- Table Content -->
        <div class="table-responsive"><table id="table-data" class="table table-condensed table-striped table-hover"></table></div>
        <!-- END Table Content -->
    </div>
    <!-- END Table Block -->
<!-- END Page Content -->
</div>

<?php include $prefix.'page_footer.php'; ?>
<?php include $prefix.'template_scripts.php'; ?>
<?php include $prefix.'template_end.php'; ?>

<script type="text/javascript">

	/*********** Data-table Initialize ***********/
    $('#acode').on('change', function(){
        var cato = $('#acode').val();
        
        $.ajax({
            url: 'data/agent_code_generator.php',
            data: {
                agent_code : cato
            },
            method: 'post',
            error: function(e){
                alert ('Error requesting provider data');
            },
            success: function(r){
                $('#teamcode').val(r.data[0].team_code);
                $('#aname').val(r.data[0].fname);
                $('#teamid').val(r.data[0].id);

            }
        });
    });


    $('#cusname').on('click',function(){

        var cato = $('#receiptno').val();
        alert(cato);

         $.ajax({
            url: 'data/receipt_validation.php',
            data: {
                receipt_no : cato
            },
            method: 'post',
            error: function(e){
                alert ('Error requesting data');
            },
            success: function(r){
                if(r.result==false){
                    alert('Receipt no already exist!!');
                    $('#receiptno').val('');
                }

            }
        });

    });

    App.datatables();

    var dt = $('#table-data').DataTable({
        "processing": true,
        "serverSide": true,
        "select": true,
        "columns": [
            { "data": "collectdate", "name": "collectdate", "title": "Add Collect Date" },
            { "data": "entereddate", "name": "entereddate", "title": "Entered Date" },
			{ "data": "teamname", "name": "teamname", "title": "Team Name" },
            { "data": "agentname", "name": "agentname", "title": "Agent Name" },
			{ "data": "teamcode", "name": "teamcode", "title": "Team Code" },
            { "data": "receiptno", "name": "receiptno", "title": "Receipt No"}, 
            { "data": "customername", "name": "customername", "title": "Customer Name"},
            { "data": "category", "name": "category", "title": "Category"},
            { "data": "amount", "name": "amount", "title": "Amount"},
            {"data": "actions", "name": "actions","title":"Actions", "searchable": false, "orderable": false, 
                mRender: function (data, type, row) {
                    return '<div class="btn-group btn-group-xs"><button id="btn-row-edit" class="btn btn-primary" title="Edit"><i class="fa fa-pencil"></i></button></div>'
                }
            }
        ],
        "columnDefs":[
            {"className": "dt-center", "targets": [1,2,3]}
        ],
        "language": {
            "emptyTable": "No providers to show..."
        },
        "ajax": "data/grid_data_adcollection.php"
    });
	
    $('.dataTables_filter input').attr('placeholder', 'Search');

    $("#table-data tbody").on('click', '#btn-row-edit', function() {
        var str_id = $(this).closest('tr').attr('id');
        var arr_id = str_id.split("_");

        var row_id = arr_id[1];
       // alert (row_id);

        $.ajax({
            url: 'data/data_adcollection.php',
            data: {
                id: row_id
            },
            method: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $('#table-data tbody #'+str_id+' #btn-row-edit').button('loading');
                NProgress.start();
            },
            error: function (e) {
                $.bootstrapGrowl('<h4>Error!</h4> <p>Error retrieving records data</p>', {
                    type: 'danger',
                    delay: 2500,
                    allow_dismiss: true
                });

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            },
            success: function (r) {
                if (!r.result) {
                    $.bootstrapGrowl('<h4>Error!</h4> <p>'+r.message+'</p>', {
                        type: 'danger',
                        delay: 2500,
                        allow_dismiss: true
                    });
                }else{
                    
                    $('#receiptno').val(r.data[0].receipt_no);
                    $('#cusname').val(r.data[0].customer_name);
                    $('#amount').val(r.data[0].amount);
                    $('#acode').val(r.data[0].aid);                        
                    $('#acode').trigger("chosen:updated");                      
                    $('#aname').val(r.data[0].fname);
                    $('#teamcode').val(r.data[0].team_code);
                      
                      $('#category').val(r.data[0].category_id);  
                      $('#category').trigger("chosen:updated"); 
                      $('#id').val(r.data[0].id); 
                      $('#teamid').val(r.data[0].team_id); 
                      $('#collectdate').val(r.data[0].add_collect_date);

                                                                                
                }

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            }
        });
    });
    /*********** Table Control End ***********/

    /*********** Form Validation and Submission ***********/
	$('#form-main').on('submit', function (e){
		e.preventDefault();
		
		var id = $('#id').val();
		var op = (id == 0) ? "insert" : "update";
		
		var formdata = $('#form-main').serializeArray();
		formdata.push({'name':'operation','value':op});
		
		$.ajax({
			url: 'adcollection_crud.php',
			data: formdata,
			success: function(r){
				var msg_typ = "info";
                var msg_txt = "";

                if (r.result){
                    msg_typ = 'success';
                    msg_txt = '<h4>Success!</h4> <p>Add saved</p>';

                    $('#form-main').trigger('reset');
                }else{
                    msg_typ = 'danger';
                    msg_txt = '<h4>Error!</h4> <p>'+r.message+'</p>';
                }

                $.bootstrapGrowl(msg_txt, {
                    type: msg_typ,
                    delay: 2500,
                    allow_dismiss: true
                });

                dt.ajax.reload();
                dt.draw();
			}
		});
	});

    $('#form-main').on('reset', function (e){
        $('#id').val("0");

        $('#acode').val("");
        $('#acode').trigger("chosen:updated");

        $('#main_provider').val("");
        $('#status').trigger("chosen:updated");

        $('#collectdate').val("");
        $('#aname').val("");
        $('#teamcode').val("");
        $('#receiptno').val("");      
        $('#cusname').val("");


        $('#category').val("");
        $('#category').trigger("chosen:updated");


        $('#amount').val("");






            
    });
    /*********** Form Control End ***********/
		
	</script>
	
	<?php mysqli_close($con_main); ?>