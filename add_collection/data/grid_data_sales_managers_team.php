<?php
	header('Content-Type: application/json');
	
	include ('../../config.php');
	
	$result_array = array();
	
	$draw = $_REQUEST['draw'];
	$length = $_REQUEST['length'];
	$start = $_REQUEST['start'];
	$columns = $_REQUEST['columns'];
	$order_cols = $_REQUEST['order'];
	$search = $_REQUEST['search'];
	$data = array();
	
	 
	$col_set = array('T0.collection_team');
	
	 
	$order_by = "";
	$where = "";
	
	 
	foreach ($order_cols as $order_col){
		 
		$order_by .= (empty($order_by)) ? " ORDER BY " : ", ";
		
		$order_col_index = $order_col['column'];  
		$order_col_dir = $order_col['dir'];  
		
		 
		$order_by .= $col_set[$order_col_index]." ".$order_col_dir." ";
	}
	
	 
	if (!empty($search['value'])){
		 
		$term = $search['value'];
		
		$i = 0;
		
		 
		foreach ($columns as $column){
			 
			$col_name = $column['name'];
			$col_searchable = $column['searchable'];
			
			 
			if ($col_searchable == "true"){
				 
				$where .= (empty($where)) ? " WHERE " : " OR ";

				 
				$where .= $col_set[$i]." LIKE '%$term%' ";
			}
			
			$i = $i + 1;
		}
	}
	
	$query = "SELECT
				collection_smteams.id,
				collection_smteams.sm_id,
				collection_smteams.team_id,
				collection_agents.fname,
				collection_team.team_name,
				collection_team.team_code,
                collection_agents.agent_code
			FROM
				collection_smteams
			INNER JOIN collection_agents ON collection_smteams.sm_id = collection_agents.id
			INNER JOIN collection_team ON collection_smteams.team_id = collection_team.id";
	
	$sql = mysqli_query($con_main, $query);
	
	$i = 0;

	$result_array['data'] = "";
	
	while ($row = mysqli_fetch_assoc($sql)){

		$data['DT_RowId'] = "row_".$row['id'];
		$data['salesmanager'] = $row['fname'];
		$data['team'] = $row['team_name'];
		$data['smcode'] = $row['agent_code'];
		$data['teamcode'] = $row['team_code'];
		$result_array['data'][$i] = $data;
		
		$i = $i + 1;
	}	
	$result_array['draw'] = $draw;  
	$result_array['recordsTotal'] = $total_records;  
	$result_array['recordsFiltered'] = (!empty($search['value'])) ? $filtered_records : $total_records;  

	mysqli_close($con_main);
	echo (json_encode($result_array));
?>