<?php
header('Content-Type: application/json');

include ('../../config.php');

$where = "";
$responce = array();
$data = "";
$result = true;
$message = "";

$id = (isset($_REQUEST['id']) && $_REQUEST['id'] != NULL && !empty($_REQUEST['id'])) ? $_REQUEST['id'] : 0;
$team_code = (isset($_REQUEST['team_code']) && $_REQUEST['team_code'] != NULL && !empty($_REQUEST['team_code'])) ? $_REQUEST['team_code'] : 0;

$query = "SELECT
    collection_team.id,
    collection_team.team_code,
    collection_team.team_name,
    collection_team.`status`
FROM
    `collection_team`";

if ($id > 0){
    $where .= (empty($where)) ? " WHERE " : " AND ";
    $where .= " collection_team.id = '$id' ";
}

if($team_code > 0){
     $where .= (empty($where)) ? " WHERE " : " AND ";
    $where .= " collection_team.id = '$team_code' ";
}

$query = $query.$where;

$sql = mysqli_query ($con_main, $query);

if (!$sql){
    $result = false;
    $message .= " Error Sql : (".mysqli_errno($con_main).") ".mysqli_error($con_main).". ";
}

$num_rows = mysqli_num_rows($sql);

if ($num_rows > 0){
    $i = 0;

    while ($rows = mysqli_fetch_assoc ($sql)){
        $data[$i] = $rows;

        $i++;
    }
}else{
    $result = false;
    $message .= " Empty results ";
}

$responce['data'] = $data;
//$responce['debug'] = $query;
$responce['result'] = $result;
$responce['message'] = $message;

echo (json_encode($responce));

mysqli_close($con_main);
?>