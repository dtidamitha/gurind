<?php
header('Content-Type: application/json');

include ('../../config.php');

$where = "";
$responce = array();
$data = "";
$result = true;
$message = "";

$id = (isset($_REQUEST['id']) && $_REQUEST['id'] != NULL && !empty($_REQUEST['id'])) ? $_REQUEST['id'] : 0;

$query = "SELECT
    collection_adds.id,
    collection_adds.team_id,
    collection_adds.receipt_no,
    collection_adds.customer_name,
    collection_adds.amount,
    collection_adds.add_collect_date,
    collection_agents.id AS aid,
    collection_category.category_id,
    collection_agents.fname,
    collection_team.team_code
FROM
    collection_adds
LEFT JOIN collection_agents ON collection_adds.agentid = collection_agents.id
LEFT JOIN collection_category ON collection_adds.category = collection_category.category_id
LEFT JOIN collection_team ON collection_adds.team_id = collection_team.id
WHERE
    collection_adds.id = '$id'";
$sql = mysqli_query ($con_main, $query);

if (!$sql){
    $result = false;
    $message .= " Error Sql : (".mysqli_errno($con_main).") ".mysqli_error($con_main).". ";
}

$num_rows = mysqli_num_rows($sql);

if ($num_rows > 0){
    $i = 0;

    while ($rows = mysqli_fetch_assoc ($sql)){
        $data[$i] = $rows;
        $i++;
    }
}else{
    $result = false;
    $message .= " Empty results ";
}

$responce['data'] = $data;
$responce['result'] = $result;
$responce['message'] = $message;

echo (json_encode($responce));

mysqli_close($con_main);
?>