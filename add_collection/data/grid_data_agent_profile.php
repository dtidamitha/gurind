<?php
	header('Content-Type: application/json');
	
	include ('../../config.php');
	
	$result_array = array();
	
	$draw = $_REQUEST['draw'];
	$length = $_REQUEST['length'];
	$start = $_REQUEST['start'];
	$columns = $_REQUEST['columns'];
	$order_cols = $_REQUEST['order'];
	$search = $_REQUEST['search'];
	$data = array();
	
	
	$col_set = array('T0.collection_agents');
	
	// Where and Order By Clause string
	$order_by = "";
	$where = "";
	
	// Generate order by string
	// Loop over all ordered fields (inside array) received from datatable client
	foreach ($order_cols as $order_col){
		// if order by string is empty start order by clause with ORDER BY else append order by with comma
		$order_by .= (empty($order_by)) ? " ORDER BY " : ", ";
		
		$order_col_index = $order_col['column']; // Order column index no
		$order_col_dir = $order_col['dir']; // Order direction (asc/desc)
		
		// Generate order by string (convert index to table column + direction)
		$order_by .= $col_set[$order_col_index]." ".$order_col_dir." ";
	}
	
	// Generate where clause if search term is not empty (received with client's request)
	if (!empty($search['value'])){
		// get search term
		$term = $search['value'];
		
		$i = 0;
		
		// Loop over available columns
		foreach ($columns as $column){
			// Get current column name and searchable (true/false)
			$col_name = $column['name'];
			$col_searchable = $column['searchable'];
			
			// If current column is searchable
			if ($col_searchable == "true"){
				// if where string is empty start where clause with WHERE else append where string with OR
				$where .= (empty($where)) ? " WHERE " : " OR ";

				$where .= $col_set[$i]." LIKE '%$term%' ";
			}
			
			$i = $i + 1;
		}
	}
	
	$query = "SELECT
	collection_agents.id,
	collection_agents.fname,
	collection_agents.lname,
	collection_agents.gender,
	collection_agents.dob,
	collection_agents.nic,
	collection_agents.address,
	collection_agents.agent_code,
	collection_agents.agent_contactno,
	collection_agents.bank,
	collection_agents.bank_accountno,
	collection_agents.date_joined,
	collection_agents.previous_jobs,
	collection_agents.agent_status,
	collection_team_comission.comid,
	collection_team.team_code
FROM
	collection_agents
INNER JOIN collection_team ON collection_agents.team_id = collection_team.id
INNER JOIN collection_team_comission ON collection_agents.com_method = collection_team_comission.id";
	
	$sql = mysqli_query($con_main, $query);
	
	$i = 0;

	$result_array['data'] = "";
	
	while ($row = mysqli_fetch_assoc($sql)){

		$agentstatus = ($row['agent_status'] == 1) ? "Leader" : "Agent";
		
		$data['DT_RowId'] = "row_".$row['id'];
		$data['fname'] = $row['fname'];
		$data['lname'] = $row['lname'];
		$data['gender'] = $row['gender'];
		$data['dob'] = $row['dob'];
		$data['nic'] = $row['nic'];
		$data['address'] = $row['address'];
		$data['agentcode'] = $row['agent_code'];
		$data['contactno'] = $row['agent_contactno'];
		$data['bank'] = $row['bank'];
		$data['accountno'] = $row['bank_accountno'];
		$data['teamcode'] = $row['team_code'];
		$data['datejoined'] = $row['date_joined'];
		$data['previous'] = $row['previous_jobs'];
		$data['astatus'] = $agentstatus;
		$data['commethod'] = $row['comid'];
		$result_array['data'][$i] = $data;
		
		$i = $i + 1;
	}

	$result_array['draw'] = $draw; // Return same draw id received from datatable client request
	$result_array['recordsTotal'] = $total_records; // Return total record count in table
	$result_array['recordsFiltered'] = (!empty($search['value'])) ? $filtered_records : $total_records; // If search term is available return filtered records count or else total record count
	//$result_array['query'] = $query; //For debugging
	
	mysqli_close($con_main);
	
	echo (json_encode($result_array));
?>