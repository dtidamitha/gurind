<?php
header('Content-Type: application/json');

include ('../../config.php');

$where = "";
$responce = array();
$data = "";
$result = true;
$message = "";

// $id = (isset($_REQUEST['id']) && $_REQUEST['id'] != NULL && !empty($_REQUEST['id'])) ? $_REQUEST['id'] : 0;
$astatus = (isset($_REQUEST['astatus']) && $_REQUEST['astatus'] != NULL && !empty($_REQUEST['astatus'])) ? $_REQUEST['astatus'] :0;

$query = "SELECT
                collection_agents.fname,
                collection_agents.agent_code
          FROM
                collection_agents
          WHERE
                collection_agents.agent_status = '$astatus'
          ORDER BY
                collection_agents.agent_code DESC
          LIMIT 1";

$sql = mysqli_query ($con_main, $query);

if (!$sql){
    $result = false;
    $message .= " Error Sql : (".mysqli_errno($con_main).") ".mysqli_error($con_main).". ";
}

$num_rows = mysqli_num_rows($sql);

if ($num_rows > 0){
    $i = 0;

    while ($rows = mysqli_fetch_assoc ($sql)){
        $acode = $rows['agent_code'];
        $acode++;
       // $data['agentcode'] = $acode;
    }
}else{
    // $result = false;
    // $message .= " Empty results ";
     if($astatus == '0'){
        $acode = 'BDO0001';
     }
     else if($astatus == '1'){
      $acode = 'BPE0001';
     }
     else if($astatus == '2'){
      $acode = 'SM001';
     }
     else if($astatus == '3'){
      $acode = 'IND0001';
     }
}

$responce['data'] =$acode;
$responce['result'] = $result;
$responce['message'] = $message;

echo (json_encode($responce));

mysqli_close($con_main);
?>