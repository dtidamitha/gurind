/*
Navicat MySQL Data Transfer

Source Server         : LOCALHOST
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : gurind_main

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2018-10-17 17:10:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for mas_countries
-- ----------------------------
DROP TABLE IF EXISTS `mas_countries`;
CREATE TABLE `mas_countries` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `CountryCode` varchar(25) NOT NULL,
  `CountryName` varchar(150) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mas_countries
-- ----------------------------
INSERT INTO `mas_countries` VALUES ('1', 'LK', 'Sri Lanka');
INSERT INTO `mas_countries` VALUES ('2', 'US', 'United States of America');
