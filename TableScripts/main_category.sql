/*
Navicat MySQL Data Transfer

Source Server         : LOCALHOST
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : gurind_main

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2018-10-17 17:12:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for main_category
-- ----------------------------
DROP TABLE IF EXISTS `main_category`;
CREATE TABLE `main_category` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `CategoryCode` varchar(20) DEFAULT NULL,
  `CategoryName` varchar(150) DEFAULT NULL,
  `ParentCategory` int(20) DEFAULT NULL,
  `EnteredBy` int(20) DEFAULT NULL,
  `EnteredDate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of main_category
-- ----------------------------
INSERT INTO `main_category` VALUES ('1', 'RM/AG', 'Annealed Glass', null, '823', '2018-10-17 09:10:57');
INSERT INTO `main_category` VALUES ('2', 'RM/IM', 'Insulating Material', null, '823', '2018-10-17 09:11:42');
INSERT INTO `main_category` VALUES ('3', 'FG/SG', 'Single Glaze', null, '823', '2018-10-17 09:12:42');
INSERT INTO `main_category` VALUES ('4', 'FG/DG', 'Double Glaze', null, '823', '2018-10-17 09:13:23');
INSERT INTO `main_category` VALUES ('5', 'FG/TG', 'Triple Glaze', null, '823', '2018-10-17 09:13:56');
INSERT INTO `main_category` VALUES ('6', 'FG/LG', 'Laminated Glass', null, '823', '2018-10-17 09:14:30');
INSERT INTO `main_category` VALUES ('7', 'FG/HS', 'Heat Strengthened', null, '823', '2018-10-17 09:15:17');
INSERT INTO `main_category` VALUES ('8', 'FG/FT', 'Frosted Tempered Single Glaze', null, '823', '2018-10-17 09:16:18');
INSERT INTO `main_category` VALUES ('9', 'FG/FA', 'Frosted Annealed Glass', null, '823', '2018-10-17 09:17:07');
INSERT INTO `main_category` VALUES ('10', 'OC/AG', 'Off cut', null, '823', '2018-10-17 09:17:40');
INSERT INTO `main_category` VALUES ('11', 'PM', 'Packing Material', null, '823', '2018-10-17 09:19:47');
INSERT INTO `main_category` VALUES ('12', 'PC', 'Production Consumable', null, '823', '2018-10-17 09:20:21');
INSERT INTO `main_category` VALUES ('13', 'S', 'Stationery', null, '823', '2018-10-17 09:21:36');
INSERT INTO `main_category` VALUES ('14', 'MP', 'Machinery Spares & Parts', null, '823', '2018-10-17 09:22:01');
INSERT INTO `main_category` VALUES ('15', 'BM', 'Building Material Consumables & Items', null, '823', '2018-10-17 09:24:27');
INSERT INTO `main_category` VALUES ('16', 'GF', 'Groceries & Foods', null, '823', '2018-10-17 09:27:47');
