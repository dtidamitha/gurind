/*
Navicat MySQL Data Transfer

Source Server         : LOCALHOST
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : gurind_main

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2018-10-17 17:10:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for mas_customer_type
-- ----------------------------
DROP TABLE IF EXISTS `mas_customer_type`;
CREATE TABLE `mas_customer_type` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `CustomerType` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mas_customer_type
-- ----------------------------
INSERT INTO `mas_customer_type` VALUES ('1', 'Client');
INSERT INTO `mas_customer_type` VALUES ('2', 'Installer');
INSERT INTO `mas_customer_type` VALUES ('3', 'Fabricator');
INSERT INTO `mas_customer_type` VALUES ('4', 'Re-seller');
INSERT INTO `mas_customer_type` VALUES ('5', 'Civil Contractor');
