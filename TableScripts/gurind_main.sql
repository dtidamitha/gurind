/*
Navicat MySQL Data Transfer

Source Server         : LOCALHOST
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : gurind_main

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2018-10-17 17:13:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for main_category
-- ----------------------------
DROP TABLE IF EXISTS `main_category`;
CREATE TABLE `main_category` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `CategoryCode` varchar(20) DEFAULT NULL,
  `CategoryName` varchar(150) DEFAULT NULL,
  `ParentCategory` int(20) DEFAULT NULL,
  `EnteredBy` int(20) DEFAULT NULL,
  `EnteredDate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of main_category
-- ----------------------------
INSERT INTO `main_category` VALUES ('1', 'RM/AG', 'Annealed Glass', null, '823', '2018-10-17 09:10:57');
INSERT INTO `main_category` VALUES ('2', 'RM/IM', 'Insulating Material', null, '823', '2018-10-17 09:11:42');
INSERT INTO `main_category` VALUES ('3', 'FG/SG', 'Single Glaze', null, '823', '2018-10-17 09:12:42');
INSERT INTO `main_category` VALUES ('4', 'FG/DG', 'Double Glaze', null, '823', '2018-10-17 09:13:23');
INSERT INTO `main_category` VALUES ('5', 'FG/TG', 'Triple Glaze', null, '823', '2018-10-17 09:13:56');
INSERT INTO `main_category` VALUES ('6', 'FG/LG', 'Laminated Glass', null, '823', '2018-10-17 09:14:30');
INSERT INTO `main_category` VALUES ('7', 'FG/HS', 'Heat Strengthened', null, '823', '2018-10-17 09:15:17');
INSERT INTO `main_category` VALUES ('8', 'FG/FT', 'Frosted Tempered Single Glaze', null, '823', '2018-10-17 09:16:18');
INSERT INTO `main_category` VALUES ('9', 'FG/FA', 'Frosted Annealed Glass', null, '823', '2018-10-17 09:17:07');
INSERT INTO `main_category` VALUES ('10', 'OC/AG', 'Off cut', null, '823', '2018-10-17 09:17:40');
INSERT INTO `main_category` VALUES ('11', 'PM', 'Packing Material', null, '823', '2018-10-17 09:19:47');
INSERT INTO `main_category` VALUES ('12', 'PC', 'Production Consumable', null, '823', '2018-10-17 09:20:21');
INSERT INTO `main_category` VALUES ('13', 'S', 'Stationery', null, '823', '2018-10-17 09:21:36');
INSERT INTO `main_category` VALUES ('14', 'MP', 'Machinery Spares & Parts', null, '823', '2018-10-17 09:22:01');
INSERT INTO `main_category` VALUES ('15', 'BM', 'Building Material Consumables & Items', null, '823', '2018-10-17 09:24:27');
INSERT INTO `main_category` VALUES ('16', 'GF', 'Groceries & Foods', null, '823', '2018-10-17 09:27:47');

-- ----------------------------
-- Table structure for mas_access
-- ----------------------------
DROP TABLE IF EXISTS `mas_access`;
CREATE TABLE `mas_access` (
  `ACCESS_CODE` int(11) NOT NULL AUTO_INCREMENT,
  `USER_CODE` int(11) NOT NULL DEFAULT '0',
  `USERNAME` varchar(60) NOT NULL DEFAULT '',
  `PASSWORD` varchar(60) NOT NULL DEFAULT '',
  `GROUP_ALLOCATED` int(11) NOT NULL DEFAULT '0',
  `OVERRIDE_LOC` int(11) DEFAULT NULL,
  `OVERRIDE_DEP` int(11) DEFAULT NULL,
  `OVERRIDE_DES` int(11) DEFAULT NULL,
  `DATE_CREATED` date DEFAULT NULL,
  `USER_CREATED` int(11) NOT NULL DEFAULT '0',
  `STATUS` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ACCESS_CODE`),
  KEY `indx_access_ucode` (`USER_CODE`) USING BTREE,
  KEY `indx_access_usrnm` (`USERNAME`) USING BTREE,
  KEY `indx_access_passw` (`PASSWORD`) USING BTREE,
  KEY `indx_access_grpal` (`GROUP_ALLOCATED`) USING BTREE,
  KEY `indx_access_status` (`STATUS`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1 COMMENT='Store system access username password details of users.';

-- ----------------------------
-- Records of mas_access
-- ----------------------------
INSERT INTO `mas_access` VALUES ('26', '823', 'tharakanp', 'ebc572eb901054118d2ad2df9659266bcabc1cc8', '0', '0', '0', '0', '2018-10-01', '1', '1');

-- ----------------------------
-- Table structure for mas_countries
-- ----------------------------
DROP TABLE IF EXISTS `mas_countries`;
CREATE TABLE `mas_countries` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `CountryCode` varchar(25) NOT NULL,
  `CountryName` varchar(150) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mas_countries
-- ----------------------------
INSERT INTO `mas_countries` VALUES ('1', 'LK', 'Sri Lanka');
INSERT INTO `mas_countries` VALUES ('2', 'US', 'United States of America');

-- ----------------------------
-- Table structure for mas_customer
-- ----------------------------
DROP TABLE IF EXISTS `mas_customer`;
CREATE TABLE `mas_customer` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `CustomerCode` varchar(10) NOT NULL,
  `CustomerName` varchar(150) NOT NULL,
  `BillingName` varchar(150) NOT NULL,
  `CustomerAddress` varchar(500) NOT NULL,
  `City` varchar(150) NOT NULL,
  `Country` varchar(150) NOT NULL,
  `GeneralLineNo` varchar(500) DEFAULT NULL,
  `BusinessRegistrationNo` varchar(100) DEFAULT NULL,
  `VATRegNo` varchar(100) DEFAULT NULL,
  `SVATRegNo` varchar(100) DEFAULT NULL,
  `BOIRegNo` varchar(100) DEFAULT NULL,
  `CordinationContact1Name` varchar(150) DEFAULT NULL,
  `CordinationContact1Designation` varchar(100) DEFAULT NULL,
  `CordinationContact1Mobile` varchar(25) DEFAULT NULL,
  `CordinationContact1Email` varchar(150) DEFAULT NULL,
  `CordinationContact2Name` varchar(150) DEFAULT NULL,
  `CordinationContact2Designation` varchar(100) DEFAULT NULL,
  `CordinationContact2Mobile` varchar(25) DEFAULT NULL,
  `CordinationContact2Email` varchar(150) DEFAULT NULL,
  `PaymentsContact1Name` varchar(150) DEFAULT NULL,
  `PaymentsContact1Designation` varchar(100) DEFAULT NULL,
  `PaymentsContact1Mobile` varchar(25) DEFAULT NULL,
  `PaymentsContact1Email` varchar(150) DEFAULT NULL,
  `PaymentsManagerName` varchar(150) DEFAULT NULL,
  `PaymentsManagerDesignation` varchar(100) DEFAULT NULL,
  `PaymentsManagerMobile` varchar(25) DEFAULT NULL,
  `PaymentsManagerEmail` varchar(150) DEFAULT NULL,
  `LogisticsBoiCusDecContactName` varchar(150) DEFAULT NULL,
  `LogisticsBoiCusDecContactDesignation` varchar(100) DEFAULT NULL,
  `LogisticsBoiCusDecContactMobile` varchar(25) DEFAULT NULL,
  `LogisticsBoiCusDecContactEmail` varchar(150) DEFAULT NULL,
  `ProprietorName` varchar(150) DEFAULT NULL,
  `ProprietorDesignation` varchar(100) DEFAULT NULL,
  `ProprietorMobile` varchar(25) DEFAULT NULL,
  `ProprietorEmail` varchar(150) DEFAULT NULL,
  `Bank1Name` varchar(150) DEFAULT NULL,
  `Bank1Branch` varchar(150) DEFAULT NULL,
  `Bank1LkrFcbuBoth` tinyint(1) DEFAULT NULL,
  `Bank2Name` varchar(150) DEFAULT NULL,
  `Bank2Branch` varchar(150) DEFAULT NULL,
  `Bank2LkrFcbuBoth` tinyint(1) DEFAULT NULL,
  `CustomerLocality` bit(1) DEFAULT NULL,
  `IsWalkInCustomer` bit(1) DEFAULT NULL,
  `Status` bit(1) NOT NULL,
  `EnteredBy` int(10) DEFAULT NULL,
  `EnteredDate` date DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mas_customer
-- ----------------------------

-- ----------------------------
-- Table structure for mas_customer_type
-- ----------------------------
DROP TABLE IF EXISTS `mas_customer_type`;
CREATE TABLE `mas_customer_type` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `CustomerType` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mas_customer_type
-- ----------------------------
INSERT INTO `mas_customer_type` VALUES ('1', 'Client');
INSERT INTO `mas_customer_type` VALUES ('2', 'Installer');
INSERT INTO `mas_customer_type` VALUES ('3', 'Fabricator');
INSERT INTO `mas_customer_type` VALUES ('4', 'Re-seller');
INSERT INTO `mas_customer_type` VALUES ('5', 'Civil Contractor');

-- ----------------------------
-- Table structure for mas_module
-- ----------------------------
DROP TABLE IF EXISTS `mas_module`;
CREATE TABLE `mas_module` (
  `MOD_CODE` int(11) NOT NULL AUTO_INCREMENT,
  `MOD_NAME` varchar(100) NOT NULL DEFAULT '',
  `CHECK_CODE` varchar(40) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `ICON` varchar(60) DEFAULT NULL,
  `MAIN_MODULE` int(1) NOT NULL DEFAULT '1',
  `PARENT_MODULE_CODE` int(11) NOT NULL DEFAULT '0',
  `IN_MENU` int(1) NOT NULL DEFAULT '0',
  `MENU_LEVEL` int(11) NOT NULL DEFAULT '0',
  `INTERNAL_MENU_URL` varchar(255) DEFAULT NULL,
  `OPENABLE` int(1) NOT NULL DEFAULT '0',
  `ADDED_BY` int(11) NOT NULL DEFAULT '0',
  `ADDED_ON` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `STATUS` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`MOD_CODE`),
  UNIQUE KEY `indx_mod_uni_check_code` (`CHECK_CODE`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=184 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mas_module
-- ----------------------------
INSERT INTO `mas_module` VALUES ('73', 'Administrator', 'ADMIN-MAIN', 'admin/index.php', 'fa fa-user', '1', '73', '1', '1', '', '1', '1', '2018-03-23 15:25:56', '1');
INSERT INTO `mas_module` VALUES ('77', 'User Profiles', 'ADMIN-MAS-USER', 'admin/user_profile.php', 'fa fa-users', '0', '73', '1', '2', '', '0', '1', '2018-03-23 15:33:47', '1');
INSERT INTO `mas_module` VALUES ('78', 'Modules', 'ADMIN-MAS-MODULE', 'admin/module.php', 'fa fa-cogs', '0', '73', '1', '2', '', '0', '1', '2018-03-23 15:35:21', '1');
INSERT INTO `mas_module` VALUES ('79', 'Permissions', 'ADMIN-MAS-PERMISSION', 'admin/permission.php', 'fa fa-check-square', '0', '73', '1', '2', '', '0', '1', '2018-03-23 15:36:59', '1');
INSERT INTO `mas_module` VALUES ('95', 'Dashboard', 'DASHBOARD-MAIN', 'dashboard/dashboard.php', 'fa fa-dashboard', '1', '95', '1', '1', '', '1', '1', '2018-08-30 13:37:24', '1');
INSERT INTO `mas_module` VALUES ('156', 'Master Files', 'GURIND_MASTER_FILES', 'gurind/', 'fa fa-support', '1', '156', '1', '1', '', '1', '1', '2018-10-01 16:51:45', '1');
INSERT INTO `mas_module` VALUES ('157', 'Supplier Master', 'GURIND_SUPPLIER', 'gurind/supplier.php', 'fa fa-address-book', '0', '156', '1', '2', '', '0', '823', '2018-10-02 08:30:27', '1');
INSERT INTO `mas_module` VALUES ('158', 'Size Master', 'GURIND_SIZE', 'gurind/size.php', 'fa fa-cubes', '0', '156', '1', '2', '', '0', '823', '2018-10-02 09:30:39', '1');
INSERT INTO `mas_module` VALUES ('159', 'Colors Master', 'GURIND_COLORS', 'gurind/color.php', 'fa fa-barcode', '0', '156', '1', '2', '', '0', '823', '2018-10-02 09:42:45', '1');
INSERT INTO `mas_module` VALUES ('160', 'Unit Master', 'GURIND_UNIT', 'gurind/unit.php', 'fa fa-area-chart', '0', '156', '1', '2', '', '0', '823', '2018-10-02 09:55:56', '1');
INSERT INTO `mas_module` VALUES ('161', 'Rack Master', 'GURIND_RACK', 'gurind/rack.php', 'fa fa-eraser', '0', '156', '1', '2', '', '0', '823', '2018-10-02 10:06:28', '1');
INSERT INTO `mas_module` VALUES ('162', 'Item Master', 'GURIND_ITEM', 'gurind/item.php', 'fa fa-sitemap', '0', '156', '1', '2', '', '0', '823', '2018-10-02 10:16:04', '1');
INSERT INTO `mas_module` VALUES ('163', 'Bin Master', 'GURIND_BIN', 'gurind/bin.php', 'fa fa-binoculars', '0', '156', '1', '2', '', '0', '823', '2018-10-02 11:34:57', '1');
INSERT INTO `mas_module` VALUES ('164', 'Invoices', 'GURIND_INVOICES', 'gurind/', 'fa fa-building-o', '1', '164', '1', '1', '', '1', '823', '2018-10-02 12:01:38', '1');
INSERT INTO `mas_module` VALUES ('165', 'Price Inquiry', 'GURIND_PI', 'gurind/pi.php', 'fa fa-product-hunt', '0', '164', '1', '2', '', '0', '823', '2018-10-02 12:07:28', '1');
INSERT INTO `mas_module` VALUES ('166', 'GRN', 'GURIND_GRN', 'gurind/grn.php', 'fa fa-building-o', '0', '164', '1', '2', '', '0', '823', '2018-10-02 13:35:25', '1');
INSERT INTO `mas_module` VALUES ('167', 'Issue Note', 'GURIND_ISSUE_NOTE', 'gurind/issue.php', 'fa fa-sticky-note-o', '0', '164', '1', '2', '', '0', '823', '2018-10-02 14:55:55', '1');
INSERT INTO `mas_module` VALUES ('171', 'Bin', 'GURIND_BIN_OPERATIONS', 'gurind/', 'fa fa-binoculars', '1', '171', '1', '1', '', '1', '823', '2018-10-03 09:34:40', '1');
INSERT INTO `mas_module` VALUES ('172', 'Bin Allocation', 'GURIND_BIN_ALLOCATION', 'gurind/bin_allocation.php', 'fa fa-cart-plus', '0', '171', '1', '2', '', '0', '823', '2018-10-03 09:36:53', '1');
INSERT INTO `mas_module` VALUES ('173', 'Bin Reduction', 'GURIND_BIN_REDUCTION', 'gurind/bin_reduction.php', 'fa fa-shopping-cart', '0', '171', '1', '2', '', '0', '823', '2018-10-03 10:20:57', '1');
INSERT INTO `mas_module` VALUES ('174', 'Returns', 'GURIND_RETURNS', 'gurind/', 'fa fa-caret-square-o-left', '1', '174', '1', '1', '', '1', '823', '2018-10-03 10:43:27', '1');
INSERT INTO `mas_module` VALUES ('175', 'Return To Supplier ', 'GURIND_RETURN_SUPPLIER', 'gurind/return_supplier.php', '', '0', '174', '1', '2', '', '0', '823', '2018-10-03 10:58:55', '1');
INSERT INTO `mas_module` VALUES ('176', 'Return To Stores', 'GURIND_RETURN_STORES', 'gurind/return_stores.php', '', '0', '174', '1', '2', '', '0', '823', '2018-10-03 11:25:25', '1');
INSERT INTO `mas_module` VALUES ('181', 'Customer Master', 'GURIND_CUSTOMER', 'Gurind/customer.php', 'fa fa-address-card-o', '0', '156', '1', '2', '', '0', '823', '2018-10-15 11:29:28', '1');
INSERT INTO `mas_module` VALUES ('182', '', '', 'Gurind/gurind/customer.php', 'fa fa-address-card-o', '0', '156', '1', '2', '', '0', '823', '2018-10-15 11:26:54', '1');
INSERT INTO `mas_module` VALUES ('183', 'Main Category', 'MAINCATEGORY', 'Gurind/category.php', 'fa fa-bathtub', '0', '156', '1', '2', '', '0', '823', '2018-10-15 14:26:36', '1');

-- ----------------------------
-- Table structure for mas_permission
-- ----------------------------
DROP TABLE IF EXISTS `mas_permission`;
CREATE TABLE `mas_permission` (
  `ACCESS_CODE` int(11) NOT NULL DEFAULT '0',
  `MOD_CODE` int(11) NOT NULL DEFAULT '0',
  `ADDED_BY` int(11) NOT NULL DEFAULT '0',
  `ADDED_ON` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mas_permission
-- ----------------------------
INSERT INTO `mas_permission` VALUES ('14', '73', '1', '2018-03-23 15:54:50');
INSERT INTO `mas_permission` VALUES ('14', '75', '1', '2018-03-23 15:54:50');
INSERT INTO `mas_permission` VALUES ('14', '76', '1', '2018-03-23 15:54:50');
INSERT INTO `mas_permission` VALUES ('14', '74', '1', '2018-03-23 15:54:50');
INSERT INTO `mas_permission` VALUES ('14', '78', '1', '2018-03-23 15:54:50');
INSERT INTO `mas_permission` VALUES ('14', '79', '1', '2018-03-23 15:54:50');
INSERT INTO `mas_permission` VALUES ('14', '77', '1', '2018-03-23 15:54:50');
INSERT INTO `mas_permission` VALUES ('14', '81', '1', '2018-03-23 15:54:50');
INSERT INTO `mas_permission` VALUES ('14', '85', '1', '2018-03-23 15:54:50');
INSERT INTO `mas_permission` VALUES ('14', '83', '1', '2018-03-23 15:54:50');
INSERT INTO `mas_permission` VALUES ('14', '86', '1', '2018-03-23 15:54:50');
INSERT INTO `mas_permission` VALUES ('14', '88', '1', '2018-03-23 15:54:50');
INSERT INTO `mas_permission` VALUES ('14', '84', '1', '2018-03-23 15:54:50');
INSERT INTO `mas_permission` VALUES ('14', '82', '1', '2018-03-23 15:54:50');
INSERT INTO `mas_permission` VALUES ('14', '87', '1', '2018-03-23 15:54:50');
INSERT INTO `mas_permission` VALUES ('6', '179', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '177', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '82', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '88', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '83', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '81', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '104', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '103', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '102', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '101', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '130', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '129', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '131', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '128', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '123', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('16', '83', '1', '2018-03-23 20:08:19');
INSERT INTO `mas_permission` VALUES ('16', '85', '1', '2018-03-23 20:08:19');
INSERT INTO `mas_permission` VALUES ('16', '81', '1', '2018-03-23 20:08:19');
INSERT INTO `mas_permission` VALUES ('16', '91', '1', '2018-03-23 20:08:19');
INSERT INTO `mas_permission` VALUES ('16', '92', '1', '2018-03-23 20:08:19');
INSERT INTO `mas_permission` VALUES ('16', '93', '1', '2018-03-23 20:08:19');
INSERT INTO `mas_permission` VALUES ('16', '94', '1', '2018-03-23 20:08:19');
INSERT INTO `mas_permission` VALUES ('16', '90', '1', '2018-03-23 20:08:19');
INSERT INTO `mas_permission` VALUES ('16', '77', '1', '2018-03-23 20:08:19');
INSERT INTO `mas_permission` VALUES ('16', '73', '1', '2018-03-23 20:08:19');
INSERT INTO `mas_permission` VALUES ('6', '126', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '124', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '125', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '127', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '139', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '137', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('16', '86', '1', '2018-03-23 20:08:19');
INSERT INTO `mas_permission` VALUES ('16', '89', '1', '2018-03-23 20:08:19');
INSERT INTO `mas_permission` VALUES ('16', '88', '1', '2018-03-23 20:08:19');
INSERT INTO `mas_permission` VALUES ('16', '84', '1', '2018-03-23 20:08:19');
INSERT INTO `mas_permission` VALUES ('16', '82', '1', '2018-03-23 20:08:19');
INSERT INTO `mas_permission` VALUES ('16', '87', '1', '2018-03-23 20:08:19');
INSERT INTO `mas_permission` VALUES ('6', '135', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '138', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '136', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '134', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '122', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '100', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '97', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '120', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '117', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '118', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '105', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '121', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '95', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '116', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '112', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '133', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '115', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '113', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '132', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '114', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '111', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '77', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '79', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '78', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '74', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '76', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('25', '73', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('17', '129', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('17', '131', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('17', '155', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('17', '128', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('17', '123', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('17', '126', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('17', '124', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('17', '127', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('17', '139', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('17', '153', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('17', '154', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('17', '137', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('17', '135', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('17', '138', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('17', '136', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('17', '134', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('17', '122', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('17', '95', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('17', '77', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('17', '79', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('18', '73', '1', '2018-09-13 10:02:57');
INSERT INTO `mas_permission` VALUES ('18', '78', '1', '2018-09-13 10:02:57');
INSERT INTO `mas_permission` VALUES ('18', '79', '1', '2018-09-13 10:02:57');
INSERT INTO `mas_permission` VALUES ('18', '77', '1', '2018-09-13 10:02:57');
INSERT INTO `mas_permission` VALUES ('18', '111', '1', '2018-09-13 10:02:57');
INSERT INTO `mas_permission` VALUES ('18', '114', '1', '2018-09-13 10:02:57');
INSERT INTO `mas_permission` VALUES ('18', '132', '1', '2018-09-13 10:02:57');
INSERT INTO `mas_permission` VALUES ('18', '113', '1', '2018-09-13 10:02:57');
INSERT INTO `mas_permission` VALUES ('18', '115', '1', '2018-09-13 10:02:57');
INSERT INTO `mas_permission` VALUES ('18', '133', '1', '2018-09-13 10:02:57');
INSERT INTO `mas_permission` VALUES ('18', '112', '1', '2018-09-13 10:02:57');
INSERT INTO `mas_permission` VALUES ('18', '116', '1', '2018-09-13 10:02:57');
INSERT INTO `mas_permission` VALUES ('18', '95', '1', '2018-09-13 10:02:57');
INSERT INTO `mas_permission` VALUES ('19', '149', '820', '2018-09-21 10:06:51');
INSERT INTO `mas_permission` VALUES ('19', '148', '820', '2018-09-21 10:06:51');
INSERT INTO `mas_permission` VALUES ('19', '147', '820', '2018-09-21 10:06:51');
INSERT INTO `mas_permission` VALUES ('19', '151', '820', '2018-09-21 10:06:51');
INSERT INTO `mas_permission` VALUES ('19', '146', '820', '2018-09-21 10:06:51');
INSERT INTO `mas_permission` VALUES ('19', '152', '820', '2018-09-21 10:06:51');
INSERT INTO `mas_permission` VALUES ('19', '143', '820', '2018-09-21 10:06:51');
INSERT INTO `mas_permission` VALUES ('19', '141', '820', '2018-09-21 10:06:51');
INSERT INTO `mas_permission` VALUES ('19', '144', '820', '2018-09-21 10:06:51');
INSERT INTO `mas_permission` VALUES ('19', '142', '820', '2018-09-21 10:06:51');
INSERT INTO `mas_permission` VALUES ('19', '145', '820', '2018-09-21 10:06:51');
INSERT INTO `mas_permission` VALUES ('19', '140', '820', '2018-09-21 10:06:51');
INSERT INTO `mas_permission` VALUES ('19', '95', '820', '2018-09-21 10:06:51');
INSERT INTO `mas_permission` VALUES ('19', '77', '820', '2018-09-21 10:06:51');
INSERT INTO `mas_permission` VALUES ('19', '79', '820', '2018-09-21 10:06:51');
INSERT INTO `mas_permission` VALUES ('19', '78', '820', '2018-09-21 10:06:51');
INSERT INTO `mas_permission` VALUES ('19', '73', '820', '2018-09-21 10:06:51');
INSERT INTO `mas_permission` VALUES ('17', '78', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('17', '73', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('25', '79', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('25', '77', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('25', '95', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('25', '122', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('25', '134', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('25', '136', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('25', '138', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('25', '135', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('25', '137', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('25', '154', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('25', '153', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('25', '139', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('25', '127', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('25', '125', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('25', '124', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('25', '126', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('25', '123', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('25', '128', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('25', '155', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('25', '131', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('25', '129', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('25', '130', '818', '2018-09-22 20:38:05');
INSERT INTO `mas_permission` VALUES ('17', '130', '818', '2018-09-25 13:31:53');
INSERT INTO `mas_permission` VALUES ('26', '176', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '174', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '160', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '157', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '158', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '161', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '183', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '162', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '181', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '159', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '163', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '156', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '165', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '167', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '166', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '164', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '95', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '173', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '172', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '171', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '77', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '79', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '78', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('6', '75', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '73', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('6', '178', '1', '2018-10-11 10:14:25');
INSERT INTO `mas_permission` VALUES ('27', '79', '824', '2018-10-13 09:59:57');
INSERT INTO `mas_permission` VALUES ('27', '180', '824', '2018-10-13 09:59:57');
INSERT INTO `mas_permission` VALUES ('27', '78', '824', '2018-10-13 09:59:57');
INSERT INTO `mas_permission` VALUES ('27', '73', '824', '2018-10-13 09:59:57');
INSERT INTO `mas_permission` VALUES ('27', '77', '824', '2018-10-13 09:59:57');
INSERT INTO `mas_permission` VALUES ('26', '73', '823', '2018-10-15 14:27:07');
INSERT INTO `mas_permission` VALUES ('26', '175', '823', '2018-10-15 14:27:07');

-- ----------------------------
-- Table structure for mas_user
-- ----------------------------
DROP TABLE IF EXISTS `mas_user`;
CREATE TABLE `mas_user` (
  `USER_CODE` int(11) NOT NULL AUTO_INCREMENT,
  `EMP_NO` varchar(10) NOT NULL DEFAULT '0',
  `FIRST_NAME` varchar(60) NOT NULL DEFAULT '',
  `LAST_NAME` varchar(60) NOT NULL DEFAULT '',
  `GENDER` varchar(10) NOT NULL DEFAULT 'Male',
  `DOB` date DEFAULT NULL,
  `NIC` varchar(15) NOT NULL DEFAULT '',
  `LOCATION` int(11) NOT NULL DEFAULT '0',
  `COST_CENTER` int(11) NOT NULL DEFAULT '0',
  `DEPARTMENT` int(11) NOT NULL DEFAULT '0',
  `DESIGNATION` int(11) NOT NULL DEFAULT '0',
  `MOBILE_NO` varchar(20) NOT NULL DEFAULT '',
  `EMAIL` varchar(40) DEFAULT NULL,
  `PHOTO` varchar(200) DEFAULT NULL,
  `DEP_HEAD` int(1) NOT NULL DEFAULT '0',
  `IMMEDIATE` int(11) NOT NULL DEFAULT '0',
  `HIGH` int(11) NOT NULL DEFAULT '0',
  `ALTER1` int(11) NOT NULL DEFAULT '0',
  `ALTER2` int(11) NOT NULL DEFAULT '0',
  `STATUS` int(1) NOT NULL DEFAULT '1',
  `DATE_JOINED` date DEFAULT NULL,
  `DATE_LEFT` date DEFAULT NULL,
  `DATE_CREATED` date DEFAULT NULL,
  `USER_CREATED` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`USER_CODE`),
  KEY `indx_user_emp` (`EMP_NO`) USING BTREE,
  KEY `indx_user_fn` (`FIRST_NAME`) USING BTREE,
  KEY `indx_user_ln` (`LAST_NAME`) USING BTREE,
  KEY `indx_user_gn` (`GENDER`) USING BTREE,
  KEY `indx_user_lc` (`LOCATION`) USING BTREE,
  KEY `indx_user_dp` (`DEPARTMENT`) USING BTREE,
  KEY `indx_user_ds` (`DESIGNATION`) USING BTREE,
  KEY `indx_user_em` (`EMAIL`) USING BTREE,
  KEY `indx_user_dh` (`DEP_HEAD`) USING BTREE,
  KEY `indx_user_st` (`STATUS`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=826 DEFAULT CHARSET=latin1 COMMENT='Store user details including employee no, first and last names, location, department, designation etc.';

-- ----------------------------
-- Records of mas_user
-- ----------------------------
INSERT INTO `mas_user` VALUES ('823', '', 'loshaka', 'pathirana', 'Male', '1996-02-14', '960453301v', '0', '0', '0', '0', '0714190031', 'akramleathercentre69@gmail.com', 'profile_pix/823.jpg', '0', '0', '0', '0', '0', '1', '0000-00-00', '0000-00-00', '2018-10-01', '1');
