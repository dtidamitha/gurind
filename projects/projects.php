<?php
	session_start();
	
	if (empty($_SESSION['ACCESS_CODE']) || $_SESSION['ACCESS_CODE'] == NULL){
		header ('Location: login.php');
		exit;
	}
	
	$folder_depth = "";
	$prefix = "";
	
	$folder_depth = substr_count($_SERVER["PHP_SELF"] , "/");
	$folder_depth = ($folder_depth == false) ? 2 : (int)$folder_depth;
	
    $prefix = str_repeat("../", $folder_depth - 2);
    
    $title_suffix = " Department Master";
?>
<?php include $prefix.'config.php'; ?>
<?php include $prefix.'menu.php'; ?>
<?php include $prefix.'template_start.php'; ?>
<?php include $prefix.'page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-podium"></i>Records Master<br><small>Create, Update or Delete Providers</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="../home.php">Home</a></li>
        <li>Projects</li>
    </ul>
    <!-- END Blank Header -->
		
	
    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
					<h2>Projects</h2>
				</div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <form id="form-main" name="form-main" action="provider_crud.php" method="post"  class="form-horizontal form-bordered">  

                <div class="form-group">                      
                        <label class="col-md-2 control-label" for="sys">System</label>
                        <div class="col-md-4">
                           <input type="text" id="sys" name="sys" class="form-control" placeholder="Enter system">     
                        </div> 

                        <label class="col-md-2 control-label" for="mod">Module</label>
                        <div class="col-md-4">
                                <input type="text" id="mod" name="mod" class="form-control" placeholder="Enter module"> 
                        </div>             
                </div>
                       
                <div class="form-group">  
                           <label class="col-md-2 control-label" for="company">Company</label>        
                        <div class="col-md-4">                         
                            <input type="text" id="company" name="company" class="form-control" placeholder="Enter company name">
                        </div>

                         <label class="col-md-2 control-label" for="qamount">Quotation amount</label>
                        <div class="col-md-4">                           
                            <input type="text" id="qamount" name="qamount" class="form-control" placeholder="Enter quotation amount">
                        </div>
                </div>


                <div class="form-group">  
                           <label class="col-md-2 control-label" for="aamount">Agreed amount</label>        
                        <div class="col-md-4">                         
                            <input type="text" id="aamount" name="aamount" class="form-control" placeholder="Enter agreed amount name">
                        </div>

                         <label class="col-md-2 control-label" for="received">Received</label>
                        <div class="col-md-4">                           
                            <input type="text" id="received" name="received" class="form-control" placeholder="Enter quotation amount">
                        </div>
                </div>
                    

                    <div class="form-group">  
                        <label class="col-md-2 control-label" for="bal">Balance</label>
                        <div class="col-md-4">                           
                            <input type="text" id="bal" name="bal" class="form-control" placeholder="Enter quotation amount" readonly>
                        </div>
                           <label class="col-md-2 control-label" for="stage">Stage</label>        
                        <div class="col-md-4">                         
                            <select id="stage" name="stage" class="select-chosen" data-placeholder="Choose stage">
                                <option value="got the job">Got the job</option>
                                <option value="pending job">Pending job</option>
                                <option value="failed job">Failed job</option>
                                <option value="complete">Complete</option>
                                <option value="got the job-going">Got the job-going</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                      <label class="col-md-2 control-label" for="cstatus">Confirm status</label>
                         <div class="col-md-4">                           
                            <select id="cstatus" name="cstatus" class="select-chosen" data-placeholder="Choose status">
                                <option value="confirm">Confirm</option>
                                <option value="pending">Pending</option>
                            </select>
                        </div>
                        <label class="col-md-2 control-label" for="date">Date</label>
                        <div class="col-md-4">                           
                            <input type="text" id="date" name="date" class="form-control input-datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                        </div>
                    </div>


                    <div class="form-group">
                      <label class="col-md-2 control-label" for="propsent">Proposal sent</label>              
                         <div class="col-md-4">                           
                            <select id="propsent" name="propsent" class="select-chosen" data-placeholder="Choose proposal status">
                                <option value="yes">yes</option>
                                <option value="no">no</option>
                            </select>
                        </div>

                        <label class="col-md-2 control-label" for="propno">Proposal no</label>
                           <div class="col-md-4">
                              <input type="text" id="propno" name="propno" class="form-control" placeholder="Enter proposal no"> 
                           </div>
                    </div>

                
                    <div class="form-group form-actions">
                        <input type="hidden" name="id" id="id" value="0" />

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success primary-btn pull-right"><i class="fa fa-angle-right"></i> Submit</button>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>


                    </div>
                </form>
                <!-- END Basic Form Elements Block -->
		    </div>
            <!-- END Example Content -->
        </div>
        <!-- END Example Block -->
    </div>

    <!-- Table Block -->
    <div class="block full">
        <!-- Table Title -->
        <div class="block-title">
            <h2>Marketing</h2><small>Marketing currently exist in the system</small>
        </div>
        <!-- END Table Title -->

        <!-- Table Content -->
        <div class="table-responsive"><table id="table-data" class="table table-condensed table-striped table-hover"></table></div>
        <!-- END Table Content -->
    </div>
    <!-- END Table Block -->
<!-- END Page Content -->
</div>

<?php include $prefix.'page_footer.php'; ?>
<?php include $prefix.'template_scripts.php'; ?>
<?php include $prefix.'template_end.php'; ?>

<script type="text/javascript">

    // $( "#aamount" ).keyup(function() {
    //   var aamount = $('#aamount').val();
    //   var received = $('#received').val();

    //   alert (aamount - received);

    // });

    $('#received').on('change',function(){
        var aamount = $('#aamount').val();
        var received = $('#received').val();
        var balance = aamount - received;
        if(balance<0){
            alert("INVALID BALANCE AMOUNT...!");
        }else{

        $('#bal').val(balance);
        }
            
    });

	/*********** Data-table Initialize ***********/
    $('#main_category').on('change', function(){
        var cato = $('#main_category').val();

        $.ajax({
            url: 'data/data_provider.php',
            data: {
                main_category: cato
            },
            method: 'post',
            error: function(e){
                alert ('Error requesting provider data');
            },
            success: function(r){
                $('#main_provider').html('<option value=""></option>');

                if (r.result){
                    if (r.data.length > 0){
                        $.each(r.data, function (k, v){
                            let option_markup = "";

                            option_markup += "<option value='"+v.id+"'>";
                            option_markup += v.provider;
                            option_markup += "</option>";

                            $('#main_provider').append(option_markup)
                            
                        });
                    }
                }

                $('#main_provider').trigger("chosen:updated");
            }
        });
    });


    App.datatables();

    var dt = $('#table-data').DataTable({
        "processing": true,
        "serverSide": true,
        "select": true,
        "columns": [
			{ "data": "system", "name": "system", "title": "system" },
			{ "data": "module", "name": "module", "title": "module" },
            { "data": "company", "name": "company", "title": "company"}, 
            { "data": "qamount", "name": "qamount", "title": "Quot amount"},
            { "data": "agreedamt", "name": "agreedamt", "title": "Agreed amount"},
            { "data": "received", "name": "received", "title": "Received"},
            { "data": "balance", "name": "balance", "title": "Balance"},
            { "data": "stage", "name": "stage", "title": "stage"},
            { "data": "cstatus", "name": "cstatus", "title": "confirm status"},
            { "data": "date", "name": "date", "title": "Date"},
            { "data": "propsent", "name": "propsent", "title": "proposal sent"},
            { "data": "propno", "name": "propno", "title": "proposal no"},
            {"data": "actions", "name": "actions","title":"Actions", "searchable": false, "orderable": false, 
                mRender: function (data, type, row) {
                    return '<div class="btn-group btn-group-xs"><button id="btn-row-edit" class="btn btn-primary" title="Edit"><i class="fa fa-pencil"></i></button></div>'
                }
            }
        ],
        "columnDefs":[
            {"className": "dt-center", "targets": [1,2,3]}
        ],
        "language": {
            "emptyTable": "No providers to show..."
        },
        "ajax": "data/grid_data_projects.php"
    });
	
    $('.dataTables_filter input').attr('placeholder', 'Search');

    $("#table-data tbody").on('click', '#btn-row-edit', function() {
        var str_id = $(this).closest('tr').attr('id');
        var arr_id = str_id.split("_");

        var row_id = arr_id[1];
       // alert (row_id);

        $.ajax({
            url: 'data/data_projects.php',
            data: {
                id: row_id
            },
            method: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $('#table-data tbody #'+str_id+' #btn-row-edit').button('loading');
                NProgress.start();
            },
            error: function (e) {
                $.bootstrapGrowl('<h4>Error!</h4> <p>Error retrieving records data</p>', {
                    type: 'danger',
                    delay: 2500,
                    allow_dismiss: true
                });

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            },
            success: function (r) {
                if (!r.result) {
                    $.bootstrapGrowl('<h4>Error!</h4> <p>'+r.message+'</p>', {
                        type: 'danger',
                        delay: 2500,
                        allow_dismiss: true
                    });
                }else{
                    $('#id').val(r.data[0].id);
                    $('#sys').val(r.data[0].system);
                    $('#mod').val(r.data[0].module);
                    $('#company').val(r.data[0].company);
                    $('#qamount').val(r.data[0].quotamount);
                    $('#aamount').val(r.data[0].agreedamount);
                    $('#received').val(r.data[0].received);
                    $('#bal').val(r.data[0].balance);          
                    $('#date').val(r.data[0].date);
                    $('#propno').val(r.data[0].proposal_no);   

                    $('#stage').val(r.data[0].stage);  
                    $('#stage').trigger("chosen:updated");

                    $('#cstatus').val(r.data[0].confirmstatus);
                    $('#cstatus').trigger("chosen:updated");

                    $('#propsent').val(r.data[0].proposal_sent);
                    $('#propsent').trigger("chosen:updated");                             
                }

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            }
        });
    });
    /*********** Table Control End ***********/

    /*********** Form Validation and Submission ***********/
	$('#form-main').on('submit', function (e){
		e.preventDefault();
		
		var id = $('#id').val();
		var op = (id == 0) ? "insert" : "update";
		
		var formdata = $('#form-main').serializeArray();
		formdata.push({'name':'operation','value':op});
		
		$.ajax({
			url: 'projects_crud.php',
			data: formdata,
			success: function(r){
				var msg_typ = "info";
                var msg_txt = "";

                if (r.result){
                    msg_typ = 'success';
                    msg_txt = '<h4>Success!</h4> <p>Marketing saved</p>';

                    $('#form-main').trigger('reset');
                }else{
                    msg_typ = 'danger';
                    msg_txt = '<h4>Error!</h4> <p>'+r.message+'</p>';
                }

                $.bootstrapGrowl(msg_txt, {
                    type: msg_typ,
                    delay: 2500,
                    allow_dismiss: true
                });

                dt.ajax.reload();
                dt.draw();
			}
		});
	});

    $('#form-main').on('reset', function (e){
        $('#id').val("0");

        $('#main_category').val(0);
        $('#location').trigger("chosen:updated");

        $('#main_provider').val("");
        $('#status').trigger("chosen:updated");

        $('#location').val("");
        $('#username').val("");
        $('#password').val("");
        $('#expdate').val("");
    });
    /*********** Form Control End ***********/
		
	</script>
	
	<?php mysqli_close($con_main); ?>