<?php
require_once ('../../config.php');
?>
		
		<div class="form-group">
			<label class="col-md-2 control-label" for="cstatus">Confirm Status</label>
				<div class="col-md-4">
					<select id="cstatus" name="cstatus" class="form-control" size="1">
						<option value="0" disabled selected>Select Status</option>
						<option value="pending">Pending</option>
                        <option value="confirm">Confirm</option>
					</select>
				</div>

			<label class="col-md-2 control-label" for="cstage">Stage</label>
				<div class="col-md-4">
					<select id="cstage" name="cstage" class="form-control" size="1">
						<option value="0" disabled selected>Select stage</option>
                        <option value="got the job">Got the job</option>
                        <option value="pending job">Pending job</option>
                        <option value="failed job">Failed job</option>
                        <option value="complete">Complete</option>
                        <option value="got the job-going">Got the job-going</option>
					</select>
				</div>

				
		</div>

		<div class="form-group form-actions">
			<input type="hidden" name="report_url" id="report_url" value="reports/monthly_deduction.php" />

			<div class="col-md-12">
				<button type="submit" class="btn btn-success primary-btn pull-right"><i class="fa fa-angle-right"></i> Submit</button>
				<button type="reset" class="btn btn-warning"><i class="fa fa-repeat"></i> Reset</button>
			</div>
		</div>
	<?php
	mysqli_close($con_main);
	?>

	
	