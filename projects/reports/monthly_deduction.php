<?php
require_once ('../../config.php');

$cstage = $_REQUEST['cstage'];
$cstatus = $_REQUEST['cstatus'];

if($cstage == "" && $cstatus==""){
	echo "<script type='text/javascript'>alert('please select filters')</script>";
}
if($cstatus){

	$query = "SELECT
					projects.system,
					projects.module,
					projects.company,
					projects.confirmstatus,
					projects.proposal_no,
					projects.agreedamount,
					projects.balance,
					projects.date,
					projects.stage
				FROM
					`projects`
				WHERE
					projects.confirmstatus = '$cstatus'";

 } else if($cstage){

 	$query = "SELECT
					projects.system,
					projects.module,
					projects.company,
					projects.confirmstatus,
					projects.proposal_no,
					projects.agreedamount,
					projects.balance,
					projects.date,
					projects.stage
				FROM
					`projects`
				WHERE
					 projects.stage = '$cstage'";

 } else if($cstage && $cstatus){

 	$query = "SELECT
					projects.system,
					projects.module,
					projects.company,
					projects.confirmstatus,
					projects.proposal_no,
					projects.agreedamount,
					projects.balance,
					projects.date,
					projects.stage
				FROM
					`projects`
				WHERE
					projects.confirmstatus = '$cstatus'
				AND projects.stage = '$cstage'";

 }

?>

					<table style="border-collapse:collapse;" border="1" id="report" width="75%">
						<thead>
						<tr>
							<th>Proposal NO</th>
							<th>System</th>
							<th>Module</th>
							<th>Company</th>
							<th>Date</th>
							<th>Status</th>
							<th>Amount</th>
							<th>Balance</th>
						</tr>
						</thead>

						<tbody>
		<?php
			$sql = mysqli_query ($con_main, $query);
		
			while ($row = mysqli_fetch_array ($sql)){
		?>
							<tr>
								<td><?php echo ($row['proposal_no']); ?></td>
								<td><?php echo ($row['system']); ?></td>
								<td><?php echo ($row['module']); ?></td>
								<td><?php echo ($row['company']); ?></td>
								<td><?php echo ($row['date']); ?></td>
								<td><?php echo ($row['confirmstatus']); ?></td>
								<td><?php echo ($row['agreedamount']); ?></td>
								<td><?php echo ($row['balance']); ?></td>
							</tr>
	<?php
							}
	?>
					</tbody>	
		        </table>
