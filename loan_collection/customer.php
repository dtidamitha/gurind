<?php
	session_start();
	
	if (empty($_SESSION['ACCESS_CODE']) || $_SESSION['ACCESS_CODE'] == NULL){
		header ('Location: login.php');
		exit;
	}
	
	$folder_depth = "";
	$prefix = "";
	
	$folder_depth = substr_count($_SERVER["PHP_SELF"] , "/");
	$folder_depth = ($folder_depth == false) ? 2 : (int)$folder_depth;
	
    $prefix = str_repeat("../", $folder_depth - 2);
    
    $title_suffix = " User Profile";
?>
<?php include $prefix.'config.php'; ?>
<?php include $prefix.'menu.php'; ?>
<?php include $prefix.'template_start.php'; ?>
<?php include $prefix.'page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-user_add"></i>Customer Profile<br><small>Create, Update or Remove Customers</small>
            </h1>
        </div>
    </div>

    <ul class="breadcrumb breadcrumb-top">
        <li><a href="../home.php">Home</a></li>
        <li>Customer Profiles</li>
    </ul>
    <!-- END Header -->
	<div class="row">
        <div class="col-md-12">
             <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Search:</label>
                            <div class="col-md-9">
                                 <select name="search" id="search" style="width:100%;">
                                      <option value="" selected disabled>Select Employee</option>
                                </select>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <!-- Form Elements Block -->
            <div class="block">
                <!-- Form Elements Title -->
                <div class="block-title">
				    <h2>Customers</h2>
				</div>
                <!-- END Form Elements Title -->

                <!-- Form Elements Content -->
                <form id="form-main" name="form-main" action="user_profile_crud.php" method="post" class="form-horizontal form-bordered">
                    <fieldset>
					    <legend><i class="fa fa-angle-right"></i> Personal Details</legend>
                  <div class="form-group">
                           // <div class="row">                                                   
                            // <div class="col-md-9">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="calname">Calling Name</label>
                                        <div class="col-md-4">
                                            <input type="text" id="calname" name="calname" class="form-control"  placeholder="Enter Calling Name" size="1">
                                        </div>
                                    
                                        <label class="col-md-2 control-label" for="fullname">Full Name</label>
                                        <div class="col-md-4">
                                            <input type="text" id="fullname" name="fullname" class="form-control"  placeholder="Enter Full Name" size="1">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="initialname">Name With Initials</label>
                                        <div class="col-md-4">
                                            <input type="text" id="initialname" name="initialname" class="form-control"  placeholder="Name with initials" size="1">
                                        </div>
                                        
                                        <label class="col-md-2 control-label" for="dob">Date of Birth</label>
                                        <div class="col-md-4">
                                            <input type="text" id="dob" name="dob" class="form-control input-datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="nic">NIC</label>
                                        <div class="col-md-4">
                                            <input type="text" id="nic" name="nic" class="form-control"  placeholder="Enter NIC No" size="1">
                                        </div>

                                        <label class="col-md-2 control-label" for="income">Income</label>
                                        <div class="col-md-4">
                                            <input type="text" id="income" name="income" class="form-control"  placeholder="Enter Income" size="1">
                                        </div>
                                    </div>
                               // </div>
                          //  </div>
                        </div>
					</fieldset>
                   
					<fieldset>
					    <legend><i class="fa fa-angle-right"></i>Agent Details</legend>
					
                        <div class="form-group">
                           <label class="col-md-2 control-label" for="astatus">Agent Status</label>
                             <div class="col-md-4">
                                <select id="astatus" name="astatus" class="select-chosen" data-placeholder="Select Agent Status" size="1">
                                        <option value=""></option>
                                        <Option value="0">Agent</Option>
                                        <Option value="1">Leader</Option>
                                 </select>
                             </div>
						
						    <label class="col-md-2 control-label" for="emp_no">Agent Code</label>
                            <div class="col-md-4">
                                <input type="text" id="emp_no" name="emp_no" class="form-control"  placeholder="Enter Agent Code" size="1">
                            </div>
					    </div>
					
					    <div class="form-group">
                            <label class="col-md-2 control-label" for="bank">Bank</label>
                            <div class="col-md-4">
                              <input type="text" id="bank" name="bank" class="form-control"  placeholder="Enter Agent's Bank" size="1">  
                            </div>
						
						    <label class="col-md-2 control-label" for="accountno">Bank Account No</label>
                            <div class="col-md-4">
                                <input type="text" id="accountno" name="accountno" class="form-control"  placeholder="Enter Bank Account No" size="1">  
                            </div>
					    </div>
					
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="teamcode">Team Code</label>
                            <div class="col-md-4">
                               <select id="teamcode" name="teamcode" class="select-chosen" data-placeholder="Choose team code"> 
                                   <option value="" selected disabled>Select Team Code</option>
                                   <?php
                                    $query="SELECT
                                              collection_team.id,
                                              collection_team.team_code,
                                              collection_team.`status`
                                            FROM
                                             `collection_team`
                                            WHERE
                                              collection_team.`status` = '1'";

                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['id']."\">".$type['team_code']."</option>");
                                    }
                                ?>


                               </select>
                            </div>

                            <label class="col-md-2 control-label" for="commethod">Comission Method</label>
                                <div class="col-md-4">
                                    <select id="commethod" name="commethod" class="select-chosen" data-placeholder="Select Comission Method" size="1">
                                        <option value="" selected disabled>Select Comission Method</option>
                                   <?php
                                    $query="SELECT
                                                collection_team_comission.id,
                                                collection_team_comission.comid
                                            FROM
                                                `collection_team_comission`";

                                            
                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['id']."\">".$type['comid']."</option>");
                                    }
                                ?>
                                 </select>
                                </div>           
                        </div>

                        <div class="form-group">
                        	 <label class="col-md-2 control-label" for="agentcontact">Agent Contact No</label>
                            <div class="col-md-4">
                                <input type="text" id="agentcontact" name="agentcontact" class="form-control"  placeholder="Enter Agent Contact NO" size="1">
                            </div>
                              <label class="col-md-2 control-label" for="joined_date">Date joined</label>
                            <div class="col-md-4">
                                <input type="text" id="joined_date" name="joined_date" class="form-control input-datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                            </div>  
                        </div>

                        <div class="form-group">
                             <label class="col-md-2 control-label" for="previous">Previous Jobs</label>
                              <div class="col-md-4">
                                 <textarea name="previous" id="previous" rows="4" cols="39"></textarea> 
                              </div>                                    
                        </div>

					</fieldset>

					<div class="form-group form-actions">
                        <input type="hidden" name="id" id="id" value="0" />
					
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success primary-btn pull-right"><i class="fa fa-angle-right"></i> Submit</button>
                            <button type="button" id="btn-reset" class="btn btn-warning"><i class="fa fa-repeat"></i>New</button>
                        </div>
                    </div>
                </form>
                <!-- END Content -->  
		    </div>
            <!-- END Form Elements Block -->
        </div>
    </div>

    <!-- Table Block -->
    <div class="block full">
        <!-- Table Title -->
        <div class="block-title">
            <h2>Customers</h2><small>Customers currently exist in the system</small>
        </div>
        <!-- END Table Title -->

        <!-- Table Content -->
        <div class="table-responsive"><table id="table-data" class="table table-condensed table-striped table-hover"></table></div>
        <!-- END Table Content -->
    </div>
    <!-- END Table Block -->
</div>
<!-- END Page Content -->

<?php include $prefix.'page_footer.php'; ?>
<?php include $prefix.'template_scripts.php'; ?>
<?php include $prefix.'template_end.php'; ?>

<script src="<?php echo ($prefix); ?>js/lib/jquery.maskedinput.js"></script>
<script src="<?php echo ($prefix); ?>js/lib/jquery.validate.js"></script>
<script src="<?php echo ($prefix); ?>js/lib/jquery.form.js"></script>
<script src="<?php echo ($prefix); ?>js/lib/j-forms.js"></script>
	
<!--Bootbox-->
<script src="<?php echo ($prefix); ?>js/lib/bootbox.js"></script>


<script type="text/javascript">

$('#search').select2({
            minimumInputLength:2,
            ajax: {
                url: 'data/customer_select.php',
                dataType: 'json',
                delay: 100,
                data: function (term) {
                    return term;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.crn_id
                            }
                        })
                    };
                }
            }
        });

$('#search').on('change', function (e){
            var crn_id = $('#search').select2('val');

            $.ajax({
                url: 'data/customer_set.php',
                data: {
                    id: crn_id
                },
                method: 'POST',
                dataType: 'json',
                error: function (e){
                    alert ("Something went wrong when getting employee details.");
                },
                success: function (r){
                    var result = r.result;
                    var message = r.message;
                    var data = r.data;

                    if (result){
                        $('#user_id').val(data.ID);
                        $('#epf_no').val(data.EPF);
                        $('#card_no').val(data.EPF_2);
                        $('#n_initial').val(data.NAME_INITIAL);
                        $('#full_name').val(data.NAME_FULL);
                        $('#calling_name').val(data.NAME_CALLING);
                        $('#nic_name').val(data.NIC);
                        $('#dob').val(data.dob);
                        $('#Nationality').val(data.NATIONALITY);
                        $('#marital_status').val(data.MARITAL_STATUS);
                        $('#gender').val(data.GENDER);
                        $('#gender').val(data.GENDER);
                        $('#add_str_fist').val(data.ADDRESS_LsINE1);
                        $('#add_str_second').val(data.ADDRESS_LINE2);
                        $('#city').val(data.ADDRESS_CITY);
                        $('#electorate').val(data.ELECORATE);
                        $('#district').val(data.DISTRICT);
                        $('#province').val(data.PROVINCE);
                        $('#country').val(data.COUNTRY);
                        $('#land_number').val(data.LAND_PHONE);
                        $('#personal_number').val(data.PERSONAL_PHONE);
                        $('#office_number').val(data.OFFICE_PHONE);
                        $('#office_email').val(data.EMAIL_OFFICE);
                        $('#other_email').val(data.EMAIL_OTHER);
                        $('#location').val(data.LOCATION).trigger('change');
                        $('#job_title').val(data.JOB_TITLE).trigger('change');
                        $('#job_status').val(data.JOB_STATUS);
                        $('#job_level').val(data.JOB_LEVEL);
                        $('#job_location').val(data.JOB_LOCATION);
                        $('#join_date').val(data.JOIN_DATE);
                        $('#b_salary').val(data.BASIC_SALARY);
                        $('#allowence').val(data.ALLOWANCE);
                        $('#special_benifit').val(data.BENEFITS);

                        var bool_ot_pay = (data.OT_PAY == 1) ? true : false;

                        $('#ot_payerbility').prop('checked',bool_ot_pay);

                        $('#person_name').val(data.CONTACT_PERSON);
                        $('#relationship').val(data.CONTACT_RELATION);
                        $('#contact_number').val(data.CONTACT_NUMBER);
                        $('#s_name').val(data.CONTRACT_START);
                        $('#e_name').val(data.CONTRACT_END);
                        $('#contract').val(data.CONTRACT_AGREEMENT);

                        var bool_ot_pay = (data.ACTIVE == 1) ? true : false;
                        $('#active').prop('checked',bool_ot_pay);

                        var bool_ot_pay = (data.SUSPEND == 1) ? true : false;
                        $('#suspend').prop('checked',bool_ot_pay);

                        var bool_ot_pay = (data.SAL_PAY == 1) ? true : false;
                        $('#sal_pay').prop('checked',bool_ot_pay);

                        var bool_ot_pay = (data.RESIGNED == 1) ? true : false;
                        $('#resigned').prop('checked',bool_ot_pay);


                        setTimeout(function(){ 
                            $('#department').val(data.DEPARTMENT); 
                        }, 500);

                        setTimeout(function(){ 
                            $('#job_title_breack').val(data.JOB_TYPE);
                        }, 500);
                        

                        if (data.IMAGE != ""){
                            $('#emp_img').attr('src',data.IMAGE);
                        }else{
                            $('#emp_img').attr('src','assets/images/users/no-image.jpg');
                        }
                    }else{
                        alert (message);
                    }
                }
            });
        });














   $('#astatus').on('change',function(){

    var status = $('#astatus').val();
    $.ajax({
        url: 'data/agent_code_generator2.php',

        data: {
                astatus : status
            },

            method: 'post',
            error: function(e){
                alert ('Error requesting agent code data');
            },

            success: function(r){
                $('#emp_no').val(r.data);
            }
    });

   });

   $('#joined_date').on('mouseover',function(){

    var contact = $('#agentcontact').val();
    var patt = /\d+/gm;
   
    if(contact.length!=10 || isNaN(contact)){
            alert('Invalid Phone Number');
            $('#agentcontact').val('');
        }

   });


    $('#address').on('mouseover', function(){
        var national = $('#nic').val();

         var vali = /[0-9]{9}[x|X|v|V]$/gm;
        // var res = vali.exec(national);


        if(national.length>12){
            alert('wrong');
            validation = false;
        } else {
                var patt = /\d+/gm;
                var res = patt.exec(national);
               

                 if(res[0].length==12){
                //   alert('NEW ID FORMAT');
                       validation = true;

                } else if (res[0].length==9){
                
                 if(vali.exec(national)){
                    // alert('OLD ID FORMAT');
                        validation = true;
                    }else{
                        validation = false;
                     alert('INVALID NIC');
                     $('#nic').val('');
                    }
                } else if (res[0].length<12){
                    validation = false;
                    alert('INVALID NIC');
                    $('#nic').val('');
                }      
            }

            if(validation){

                $.ajax({
            url: 'data/nic_validation.php',
            data: {
                agentnid : national
            },
            method: 'post',
            error: function(e){
                alert ('Error requesting data');
            },
            success: function(r){
               if(r.result==false){
                    alert('NIC no already exist!!');
                    $('#nic').val('');
                }
            }
        });

            }
    });

    /*********** Data-table Initialize ***********/
    App.datatables();

    var dt = $('#table-data').DataTable({
        "processing": true,
        "serverSide": true,
        "select": true,
        "columns": [
            { "data": "astatus", "name": "astatus", "title": "Agent Status"},
            { "data": "agentcode", "name": "agentcode", "title": "Agent Code"},
            { "data": "commethod", "name": "commethod", "title": "Comission Method"},
            { "data": "fname", "name": "fname", "title": "First Name" },
            { "data": "lname", "name": "lname", "title": "Last Name" },
            { "data": "gender", "name": "gender", "title": "Gender"}, 
            { "data": "dob", "name": "dob", "title": "DOB"},
            { "data": "nic", "name": "nic", "title": "NIC"},
            { "data": "address", "name": "address", "title": "Address"},              
            { "data": "contactno", "name": "contactno", "title": "Contact No"},
            { "data": "bank", "name": "bank", "title": "Bank"},
            { "data": "accountno", "name": "accountno", "title": "Account No"},
            { "data": "teamcode", "name": "teamcode", "title": "Team Code"},
            { "data": "datejoined", "name": "datejoined", "title": "Date Joined"},
            { "data": "previous", "name": "previous", "title": "Previous Jobs"},
            
            {"data": "actions", "name": "actions","title":"Actions", "searchable": false, "orderable": false, 
                mRender: function (data, type, row) {
                    return '<div class="btn-group btn-group-xs"><button id="btn-row-edit" class="btn btn-primary" title="Edit"><i class="fa fa-pencil"></i></button></div>'
                }
            }
        ],
        "columnDefs":[
            {"className": "dt-center", "targets": [1,2,3]}
        ],
        "language": {
            "emptyTable": "No customers to show..."
        },
        "ajax": "data/grid_data_customer.php"
    });
    
    $('.dataTables_filter input').attr('placeholder', 'Search');

    $("#table-data tbody").on('click', '#btn-row-edit', function() {
        var str_id = $(this).closest('tr').attr('id');
        var arr_id = str_id.split("_");

        var row_id = arr_id[1];
       // alert (row_id);

        $.ajax({
            url: 'data/data_customer.php',
            data: {
                id: row_id
            },
            method: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $('#table-data tbody #'+str_id+' #btn-row-edit').button('loading');
                NProgress.start();
            },
            error: function (e) {
                $.bootstrapGrowl('<h4>Error!</h4> <p>Error retrieving customers data</p>', {
                    type: 'danger',
                    delay: 2500,
                    allow_dismiss: true
                });

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            },
            success: function (r) {
                if (!r.result) {
                    $.bootstrapGrowl('<h4>Error!</h4> <p>'+r.message+'</p>', {
                        type: 'danger',
                        delay: 2500,
                        allow_dismiss: true
                    });
                }else{
                    $('#id').val(r.data[0].id);
                    $('#first_name').val(r.data[0].fname);
                    $('#last_name').val(r.data[0].lname);

                    $('#gender').val(r.data[0].gender);
                    $('#gender').trigger("chosen:updated");

                  //  $('#photo').val(r.data[0].image);
                    $('#obj_id').val(r.data[0].id);

                    $('#dob').val(r.data[0].dob);
                    $('#nic').val(r.data[0].nic);
                    $('#address').val(r.data[0].address);
                    $('#emp_no').val(r.data[0].agent_code);
                    $('#agentcontact').val(r.data[0].agent_contactno);
                    $('#bank').val(r.data[0].bank);
                    $('#accountno').val(r.data[0].bank_accountno);
                    $('#joined_date').val(r.data[0].date_joined );
                    $('#previous').val(r.data[0].previous_jobs);

                    $('#astatus').val(r.data[0].agent_status); 
                     $('#astatus').trigger("chosen:updated");            

                    $('#teamcode').val(r.data[0].tid);                        
                    $('#teamcode').trigger("chosen:updated");
                    
                    $('#commethod').val(r.data[0].com_method);
                    $('#commethod').trigger("chosen:updated");
                }

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            }
        });
    });
    /*********** Table Control End ***********/

    /*********** Form Validation and Submission ***********/
  // $('#modal-upload').on('shown.bs.modal', function (e) {
  //       Dropzone.forElement("#uploader").removeAllFiles(true);
  //   });

  //  $('#modal-upload').on('hide.bs.modal', function (e) {
  //       Dropzone.forElement("#uploader").removeAllFiles(true);
  //   });

    $('#form-main').on('submit', function (e){
        e.preventDefault();
         var id = $('#id').val();
         var op = (id == 0) ? "insert" : "update";
         var validation = false;

         var formdata = $('#form-main').serializeArray();
         formdata.push({'name':'operation','value':op});
        

        var national = $('#nic').val();

         var vali = /[0-9]{9}[x|X|v|V]$/gm;
        // var res = vali.exec(national);


        if(national.length>12){
            alert('wrong');
            validation = false;
        } else {
                var patt = /\d+/gm;
                var res = patt.exec(national);
               

                 if(res[0].length==12){
                //   alert('NEW ID FORMAT');
                       validation = true;

                } else if (res[0].length==9){
                
                 if(vali.exec(national)){
                    // alert('OLD ID FORMAT');
                        validation = true;
                    }else{
                    	validation = false;
                     alert('INVALID NIC')
                    }
                } else if (res[0].length<12){
                	validation = false;
                    alert('INVALID NIC');
                }      
            }

            if(validation) {
                $.ajax({
            url: 'customer_crud.php',
            data: formdata,
            success: function(r){
                var msg_typ = "info";
                var msg_txt = "";

                $('#id').val(r.id);   // to get the id 
                $('#obj_id').val(r.id);  //to get the id

                var msg_title = "Error";     //new
                var msg_body = "Unknown result";  //new
                var msg_type = "danger";  //new 

                if (r.result){
                    msg_typ = 'success';
                    msg_txt = '<h4>Success!</h4> <p>Customer saved</p>';

                    $('#form-main').trigger('reset');
                }else{
                    msg_typ = 'danger';
                    msg_txt = '<h4>Error!</h4> <p>'+r.message+'</p>';
                }

                $.bootstrapGrowl(msg_txt, {
                    type: msg_typ,
                    delay: 2500,
                    allow_dismiss: true
                });

                dt.ajax.reload();
                dt.draw();
            }
        });
            }

        
    });

    $('#btn-reset').on('click', function (e){
        $('#id').val("0");
        $('#gender').val("");
        $('#gender').trigger("chosen:updated");

        $('#first_name').val("");
        $('#last_name').val("");
        $('#dob').val("");
        $('#nic').val("");
        $('#address').val("");
        $('#emp_no').val("");
        $('#agentcontact').val("");
        $('#bank').val("");
        $('#accountno').val("");

        $('#teamcode').val("");
        $('#teamcode').trigger("chosen:updated");
        
        $('#joined_date').val("");           
        $('#previous').val("");

        $('#astatus').val("");
        $('#astatus').trigger("chosen:updated");

        $('#commethod').val("");
        $('#commethod').trigger("chosen:updated");

        $('#photo').prop('src','profile_pix/avatar_guest.jpg');
    });
    /*********** Form Control End ***********/
        
    </script>
    
    <?php mysqli_close($con_main); ?>
