<?php
header('Content-Type: application/json');

include ('../../config.php');

$where = "";
$responce = array();
$data = "";
$result = true;
$message = "";

$id = (isset($_REQUEST['id']) && $_REQUEST['id'] != NULL && !empty($_REQUEST['id'])) ? $_REQUEST['id'] : 0;

$query = "SELECT
    collection_team.id AS tid,
    collection_agents.com_method,
    collection_agents.id,
    collection_agents.fname,
    collection_agents.lname,
    collection_agents.gender,
    collection_agents.dob,
    collection_agents.nic,
    collection_agents.image,
    collection_agents.address,
    collection_agents.agent_code,
    collection_agents.agent_contactno,
    collection_agents.bank,
    collection_agents.bank_accountno,
    collection_agents.date_joined,
    collection_agents.previous_jobs,
    collection_agents.agent_status
FROM
    collection_team
INNER JOIN collection_agents ON collection_agents.team_id = collection_team.id";

if ($id > 0){
    $where .= (empty($where)) ? " WHERE " : " AND ";
    $where .= "collection_agents.id = '$id'";
}

$query = $query.$where;

$sql = mysqli_query ($con_main, $query);

if (!$sql){
    $result = false;
    $message .= " Error Sql : (".mysqli_errno($con_main).") ".mysqli_error($con_main).". ";
}

$num_rows = mysqli_num_rows($sql);

if ($num_rows > 0){
    $i = 0;

    while ($rows = mysqli_fetch_assoc ($sql)){
        $data[$i] = $rows;

        $i++;
    }
}else{
    $result = false;
    $message .= " Empty results ";
}

$responce['data'] = $data;
$responce['result'] = $result;
$responce['message'] = $message;

echo (json_encode($responce));

mysqli_close($con_main);
?>