<?php
	header('Content-Type: application/json');
	session_start();
	require ('../config.php');
	
	$op = $_REQUEST['operation'];
	$id = $_REQUEST['id'];
	$first_name = $_REQUEST['first_name'];
	$last_name = $_REQUEST['last_name'];
	$gender = $_REQUEST['gender'];
	$dob = $_REQUEST['dob'];
	$nic = $_REQUEST['nic'];
	$address = $_REQUEST['address'];
	$agentcode = $_REQUEST['emp_no'];
	$contact = $_REQUEST['agentcontact'];
	$bank = $_REQUEST['bank'];
	$accountno = $_REQUEST['accountno'];
	$teamcode = $_REQUEST['teamcode'];
	$joindate = $_REQUEST['joined_date'];
	$previous = $_REQUEST['previous'];
	$astatus = $_REQUEST['astatus'];
	$commethod = $_REQUEST['commethod'];
	$today = date("Y-m-d");

	
	$query = "";
	$result = true;
	$message = "";
	$debug = "";
	$responce = array();
	
	if ($op == "insert"){
		$profile_query = "INSERT INTO `mobiman_main`.`collection_agents` (
	`fname`,
	`lname`,
	`gender`,
	`dob`,
	`nic`,
	`address`,
	`agent_code`,
	`agent_contactno`,
	`bank`,
	`bank_accountno`,
	`team_id`,
	`date_joined`,
	`previous_jobs`,
	`agent_status`,
	`enter_date`,
	`com_method`
)
VALUES
	(
		'$first_name',
		'$last_name',
		'$gender',
		'$dob',
		'$nic',
		'$address',
		'$agentcode',
		'$contact',
		'$bank',
		'$accountno',
		'$teamcode',
		'$joindate',
		'$previous',
		'$astatus',
		'$today',
		'$commethod'	
	);";

		
	}else if ($op == "update"){
		$profile_query = "UPDATE `mobiman_main`.`collection_agents`
SET

 `fname` = '$first_name',
 `lname` = '$last_name',
 `gender` = '$gender',
 `dob` = '$dob',
 `nic` = '$nic',
 `address` = '$address',
 `agent_code` = '$agentcode',
 `agent_contactno` = '$contact',
 `bank` = '$bank',
 `bank_accountno` = '$accountno',
 `team_id` = '$teamcode',
 `date_joined` = '$joindate',
 `previous_jobs` = '$previous',
 `agent_status` = '$astatus',
 `enter_date` = '$today',
 `com_method` = '$commethod'
WHERE
	(`id` = '$id');";
	}



	else if ($op == "upload-image"){
		$obj_id = $_REQUEST['obj_id'];
		$moved_url = "profile_pix/".$obj_id.".jpg";

		if (!empty($_FILES["file"])){
			if ($_FILES["file"]["error"] > 0){
				$result = false;
				$message .= "<br>Error uploading file.<br>".$_FILES["file"]["error"];
				$debug .= "\nError: " . $_FILES["file"]["error"];
			}else{
				$responce['file_name'] = $_FILES["file"]["name"];
				$responce['file_size_kb'] = $_FILES["file"]["size"]/1024;

				$file_moved = move_uploaded_file($_FILES["file"]["tmp_name"],$moved_url);

				if ($file_moved){
					$upload_query = "UPDATE `collection_agents` SET `image` = '$moved_url' WHERE (`id` = '$obj_id')";
					$sql = mysqli_query($con_main, $upload_query);

					if ($sql){
						$message .= "<br>Image uploaded successfully.";
					}else{
						$result = false;
						$message .= "<br>Image upload failed.";
						$debug .= "\nError SQL. (".mysqli_errno($con_main).") ".mysqli_error($con_main)." Query: ".$upload_query;
					}
				}else{
					$result = false;
					$message .= "<br>Error moving uploaded file.";
					$debug .= "\nError moving uploaded file";
				}
			}
		}else{
			$result = false;
			$message .= "<br>No file to upload.";
			$debug .= "\nError: No file to upload";
		}
	}

		$profile_sql = mysqli_query ($con_main, $profile_query);

		$id = ($op == "insert") ? mysqli_insert_id($con_main) : $id;
	
	if ($profile_sql){
		$success = true;
		$message = "Success";
	}else{
		$success = false;
		$message = "Error SQL: (".mysqli_errno($con_main).") ".mysqli_error($con_main);
	}
	
	$responce['operation'] = $op;
	$responce['result'] = $success;
	$responce['id'] = $id;
	$responce['message'] = $message;
	$responce['debug'] = $debug;
	
	echo (json_encode($responce));
	
	mysqli_close($con_main);
?>