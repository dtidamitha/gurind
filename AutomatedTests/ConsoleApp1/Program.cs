﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Microsoft.Office.Interop.Excel;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using System.Drawing.Imaging;

namespace ConsoleApp1
{
    class Program
    {
        public static IWebDriver MyWebDriver =null;
        public static string TestingDataFolder = @"E:\SlChina";
        public static string BrowserDriversFolder = @"E:\BrowserDrivers";//C:\Users\Damitha\Documents\DTI\BrowserDrivers//E:\BrowserDrivers

        static void Main(string[] args)
        {

            MyWebDriver = new OpenQA.Selenium.Firefox.FirefoxDriver(BrowserDriversFolder);


            MyWebDriver.Manage().Timeouts().PageLoad= TimeSpan.FromMinutes(5);

            //MyWebDriver.Url = "http://adminscjf.weddinplanner.com/admin_panel";//http://scjf.weddinplanner.com

            MyWebDriver.Navigate().GoToUrl("http://adminscjf.weddinplanner.com/admin_panel");

            IWait<IWebDriver> wait = new OpenQA.Selenium.Support.UI.WebDriverWait(MyWebDriver, TimeSpan.FromSeconds(300.00));
            wait.Until(MyWebDriver => ((IJavaScriptExecutor)MyWebDriver).ExecuteScript("return document.readyState").Equals("complete"));

            MyWebDriver.FindElement(By.Id("login-username")).SendKeys("tharanga");
            MyWebDriver.FindElement(By.Id("login-password")).SendKeys("Tha@#123");
            MyWebDriver.FindElement(By.Id("btn-login")).Click();

            AddAdvertisements();



            MyWebDriver.Close();
            MyWebDriver = null;
            wait = null;

            Console.WriteLine("\n\nTests Completed.");

        }

        public static void ScrollTo(int xPosition = 0, int yPosition = 0)
        {
            var js = String.Format("window.scrollTo({0}, {1})", xPosition, yPosition);
            IJavaScriptExecutor JavaScriptExec = (IJavaScriptExecutor)MyWebDriver;
            JavaScriptExec.ExecuteScript(js);
        }

        public static void AddAdvertisements()
        {

            //ExcelApp.Visible = true;

            while (MyWebDriver.Url != "http://adminscjf.weddinplanner.com/admin_panel/dashboard/dashboard.php")
            {
                System.Threading.Thread.Sleep(1000);
            }

            IWait<IWebDriver> wait = new OpenQA.Selenium.Support.UI.WebDriverWait(MyWebDriver, TimeSpan.FromSeconds(60.00));
            wait.Until(MyWebDriver => ((IJavaScriptExecutor)MyWebDriver).ExecuteScript("return document.readyState").Equals("complete"));

            IWebElement[] ListItems = (IWebElement[])(MyWebDriver.FindElements(By.CssSelector(".sidebar-nav-mini-hide")).ToArray());

            foreach (IWebElement ListItem in ListItems)
            {
                if (ListItem.Text == "Master Files")
                {
                    ListItem.Click();
                    break;
                }
            }

            IWebElement[] AnchorTags = (IWebElement[])(MyWebDriver.FindElements(By.TagName("a")).ToArray());

            foreach (IWebElement AnchorTag in AnchorTags)
            {
                if (AnchorTag.GetAttribute("href").IndexOf("/add_collection/mas_add_management.php") != -1)
                {
                    AnchorTag.Click();
                    break;
                }
            }

            while (MyWebDriver.Url != "http://adminscjf.weddinplanner.com/admin_panel/add_collection/mas_add_management.php")
            {
                System.Threading.Thread.Sleep(1000);
            }

            wait = new OpenQA.Selenium.Support.UI.WebDriverWait(MyWebDriver, TimeSpan.FromSeconds(300.00));
            wait.Until(MyWebDriver => ((IJavaScriptExecutor)MyWebDriver).ExecuteScript("return document.readyState").Equals("complete"));

            int RowNo = 2;

            //System.Speech.Synthesis.SpeechSynthesizer speech = new System.Speech.Synthesis.SpeechSynthesizer();

            Microsoft.Office.Interop.Excel.Application ExcelApp = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook ExcelWorkbook = ExcelApp.Workbooks.Open(TestingDataFolder + "\\AddAddvertisements.xlsx");
            Microsoft.Office.Interop.Excel.Worksheet ExcelWorksheet = ExcelWorkbook.Worksheets[1];


            while (ExcelWorksheet.Range["A" + RowNo.ToString()].Text!="")
            {
                System.Threading.Thread.Sleep(2000);

                string AddDisplayLevelToSelect = ExcelWorksheet.Range["A" + RowNo.ToString()].Text;

                IWebElement SpecialDropDown = (IWebElement)MyWebDriver.FindElement(By.Id("addposition_chosen"));

                SpecialDropDown.Click();

                while (!SpecialDropDown.FindElement(By.CssSelector("ul.chosen-results")).Displayed)
                {
                    System.Threading.Thread.Sleep(1000);
                }

                IWebElement[] AddDisplayLevels = (IWebElement[])SpecialDropDown.FindElement(By.CssSelector("ul.chosen-results")).FindElements(By.CssSelector("li.active-result")).ToArray();

                foreach (IWebElement AddDisplayLevel in AddDisplayLevels)
                {
                    if (AddDisplayLevel.Text == AddDisplayLevelToSelect)
                    {
                        AddDisplayLevel.Click();
                    }
                }

                IWebElement AddDescription = (IWebElement)MyWebDriver.FindElement(By.Id("description"));

                string AddDescriptionToPut = ExcelWorksheet.Range["B" + RowNo.ToString()].Text;

                AddDescription.SendKeys(AddDescriptionToPut);

                IWebElement DateFrom = (IWebElement)MyWebDriver.FindElement(By.Id("from"));

                string FromDate= ExcelWorksheet.Range["D" + RowNo.ToString()].Text;

                DateFrom.SendKeys(FromDate);

                IWebElement DateTo = (IWebElement)MyWebDriver.FindElement(By.Id("to"));

                string ToDate = ExcelWorksheet.Range["E" + RowNo.ToString()].Text;

                DateTo.SendKeys(ToDate);

                IWebElement AddRemarks = (IWebElement)MyWebDriver.FindElement(By.Id("remarks"));

                string Remarks = ExcelWorksheet.Range["F" + RowNo.ToString()].Text;

                AddRemarks.SendKeys(Remarks);

                IWebElement AddImage = (IWebElement)MyWebDriver.FindElement(By.Id("file"));

                string PhotoNameWithExtention = ExcelWorksheet.Range["C" + RowNo.ToString()].Text;
                AddImage.SendKeys(TestingDataFolder + "\\" + PhotoNameWithExtention);//E:\SlChina\photo1.jpeg//C:\Users\Damitha\Documents\DTI\SlChina\photo1.jpeg

                AddDescription.Click();
                System.Threading.Thread.Sleep(1000);

                IWebElement SubmitButton = (IWebElement)MyWebDriver.FindElement(By.CssSelector("button.btn.btn-success.primary-btn.pull-right"));

                //OpenQA.Selenium.Interactions.Actions actions = new OpenQA.Selenium.Interactions.Actions(MyWebDriver);
                //actions.MoveToElement(SubmitButton);

                //IWebElement H2Heading=(IWebElement)MyWebDriver.FindElement(By.TagName("h2"));

                bool Clicked = false;

                while (!Clicked)
                {
                    try
                    {
                        ScrollTo(SubmitButton.Location.X, DateTo.Location.Y);
                        SubmitButton.Click();
                        Clicked = true;
                    }
                    catch (Exception cexcError)
                    {
                        System.Threading.Thread.Sleep(1);
                    }

                }

                IWebElement Growl = null;
                IWebElement ErrorModalPopUp = null;

                DateTime dateTime = System.DateTime.Now;

                TimeSpan timeSpan = DateTime.Now - dateTime;
                bool PopUpErrorDisplayed = false;

                while (Growl == null && !PopUpErrorDisplayed && timeSpan.Minutes<2)
                {

                    try
                    {
                        Growl = MyWebDriver.FindElement(By.CssSelector("div.bootstrap-growl"));
                    }
                    catch
                    {

                    }

                    try
                    {
                        ErrorModalPopUp = MyWebDriver.FindElement(By.CssSelector("div.swal-modal"));
                        PopUpErrorDisplayed = ErrorModalPopUp.Displayed;
                    }
                    catch
                    {

                    }

                    timeSpan = DateTime.Now - dateTime;
                }

                IWebElement Footer = MyWebDriver.FindElement(By.TagName("footer"));

                if(Growl!=null)
                {
                    IWebElement h4Messege = Growl.FindElement(By.TagName("h4"));

                    ExcelWorksheet.Range["H" + RowNo.ToString()].Value = h4Messege.Text;

                    try
                    {
                        Growl = MyWebDriver.FindElement(By.CssSelector("div.bootstrap-growl"));
                        while (Growl.Displayed && Growl != null)
                        {
                            try
                            {
                                Growl = MyWebDriver.FindElement(By.CssSelector("div.bootstrap-growl"));
                            }
                            catch
                            {
                                System.Threading.Thread.Sleep(1);
                            }
                        }
                    }
                    catch
                    {
                        System.Threading.Thread.Sleep(1);
                    }

                    bool Clickable = false;

                    while (!Clickable)
                    {
                        try
                        {
                            ScrollTo(SubmitButton.Location.X, 0);
                            AddDescription.Click();
                            Clickable = true;
                        }
                        catch
                        {

                        }
                    }

                    Growl = null;
                }
                else if (PopUpErrorDisplayed)
                {
                    String popUpHandle = MyWebDriver.CurrentWindowHandle;
                    MyWebDriver.SwitchTo().Window(popUpHandle);

                    IWebElement ModalDialog = MyWebDriver.FindElement(By.CssSelector("div.swal-modal"));
                    IWebElement MessegeDiv = ModalDialog.FindElement(By.CssSelector("div.swal-text"));

                    ExcelWorksheet.Range["I" + RowNo.ToString()].Value = MessegeDiv.Text;

                    System.Threading.Thread.Sleep(2000);
                    IWebElement ModalCloseOkButton = ModalDialog.FindElement(By.CssSelector("div.swal-button-container"));

                    bool Clickable = false;

                    while (!Clickable)
                    {
                        try
                        {
                            ModalDialog = MyWebDriver.FindElement(By.CssSelector("div.swal-modal"));
                            ModalCloseOkButton = ModalDialog.FindElement(By.CssSelector("div.swal-button-container"));
                            ModalCloseOkButton.Click();
                            System.Threading.Thread.Sleep(3000);
                            ScrollTo(SubmitButton.Location.X, 0);
                            AddDescription.Click();
                            Clickable = true;
                        }
                        catch
                        {
                            System.Threading.Thread.Sleep(1);
                        }
                    }

                    System.Threading.Thread.Sleep(2000);
                    popUpHandle = MyWebDriver.CurrentWindowHandle;
                    MyWebDriver.SwitchTo().Window(popUpHandle);

                    ScrollTo(SubmitButton.Location.X, 0);

                    Screenshot screenShot = ((ITakesScreenshot)MyWebDriver).GetScreenshot();

                    string screenshot = screenShot.AsBase64EncodedString;
                    byte[] screenshotAsByteArray = screenShot.AsByteArray;
                    screenShot.SaveAsFile(TestingDataFolder + @"\ScreenShotTopArea" + (RowNo - 1).ToString() + ".png", OpenQA.Selenium.ScreenshotImageFormat.Png);
                    ExcelWorksheet.Range["J" + RowNo.ToString()].Value = @"ScreenShotTopArea" + (RowNo - 1).ToString() + ".png";

                    IWebElement imageAdd = MyWebDriver.FindElement(By.CssSelector("img#added_img_preview"));

                    ScrollTo(SubmitButton.Location.X, imageAdd.Location.Y);

                    screenShot = ((ITakesScreenshot)MyWebDriver).GetScreenshot();

                    screenshot = screenShot.AsBase64EncodedString;
                    screenshotAsByteArray = screenShot.AsByteArray;
                    screenShot.SaveAsFile(TestingDataFolder + @"\ScreenShotMiddleArea" + (RowNo - 1).ToString() + ".png", OpenQA.Selenium.ScreenshotImageFormat.Png);
                    ExcelWorksheet.Range["K" + RowNo.ToString()].Value = @"ScreenShotMiddleArea" + (RowNo - 1).ToString() + ".png";

                    ScrollTo(SubmitButton.Location.X, Footer.Location.Y+500);

                    screenShot = ((ITakesScreenshot)MyWebDriver).GetScreenshot();

                    screenshot = screenShot.AsBase64EncodedString;
                    screenshotAsByteArray = screenShot.AsByteArray;
                    screenShot.SaveAsFile(TestingDataFolder + @"\ScreenShotBottomArea" + (RowNo - 1).ToString() + ".png", OpenQA.Selenium.ScreenshotImageFormat.Png);
                    ExcelWorksheet.Range["L" + RowNo.ToString()].Value = @"ScreenShotBottomArea" + (RowNo - 1).ToString() + ".png";

                    

                    ErrorModalPopUp = null;
                }
                else
                {
                    ScrollTo(SubmitButton.Location.X, 0);

                    Screenshot screenShot = ((ITakesScreenshot)MyWebDriver).GetScreenshot();

                    string screenshot = screenShot.AsBase64EncodedString;
                    byte[] screenshotAsByteArray = screenShot.AsByteArray;
                    screenShot.SaveAsFile(TestingDataFolder + @"\ScreenShotTopArea" + (RowNo-1).ToString() + ".png", OpenQA.Selenium.ScreenshotImageFormat.Png);
                    ExcelWorksheet.Range["J" + RowNo.ToString()].Value = @"ScreenShotTopArea" + (RowNo - 1).ToString() + ".png";

                    IWebElement imageAdd = MyWebDriver.FindElement(By.CssSelector("img#added_img_preview"));

                    ScrollTo(SubmitButton.Location.X, imageAdd.Location.Y);

                    screenShot = ((ITakesScreenshot)MyWebDriver).GetScreenshot();

                    screenshot = screenShot.AsBase64EncodedString;
                    screenshotAsByteArray = screenShot.AsByteArray;
                    screenShot.SaveAsFile(TestingDataFolder + @"\ScreenShotMiddleArea" + (RowNo - 1).ToString() + ".png", OpenQA.Selenium.ScreenshotImageFormat.Png);
                    ExcelWorksheet.Range["K" + RowNo.ToString()].Value = @"ScreenShotMiddleArea" + (RowNo - 1).ToString() + ".png";

                    ScrollTo(SubmitButton.Location.X, Footer.Location.Y+500);

                    screenShot = ((ITakesScreenshot)MyWebDriver).GetScreenshot();

                    screenshot = screenShot.AsBase64EncodedString;
                    screenshotAsByteArray = screenShot.AsByteArray;
                    screenShot.SaveAsFile(TestingDataFolder + @"\ScreenShotBottomArea" + (RowNo - 1).ToString() + ".png", OpenQA.Selenium.ScreenshotImageFormat.Png);
                    ExcelWorksheet.Range["L" + RowNo.ToString()].Value = @"ScreenShotBottomArea" + (RowNo - 1).ToString() + ".png";
                }

               

                

               

               

                //speech.Speak("Completed Test Number " + RowNo.ToString());

                ExcelWorkbook.Save();
                ExcelWorkbook.Close();

                
                ExcelWorkbook = null;
                ExcelWorksheet = null;

                System.GC.Collect();

                ExcelWorkbook = ExcelApp.Workbooks.Open(TestingDataFolder + "\\AddAddvertisements.xlsx");
                ExcelWorksheet = ExcelWorkbook.Worksheets[1];

                RowNo++;
            }

            ExcelWorkbook.Save();
            ExcelWorkbook.Close();

            
            ExcelWorkbook = null;
            ExcelWorksheet = null;
            ExcelApp.Quit();
            ExcelApp = null;

            System.GC.Collect();

        }

        public static void SetHomeImages()
        {
            
        }

    }
}
