<?php
	session_start();
	
	if (empty($_SESSION['ACCESS_CODE']) || $_SESSION['ACCESS_CODE'] == NULL){
		header ('Location: login.php');
		exit;
	}
	
	$folder_depth = "";
	$prefix = "";
	
	$folder_depth = substr_count($_SERVER["PHP_SELF"] , "/");
	$folder_depth = ($folder_depth == false) ? 2 : (int)$folder_depth;
	
    $prefix = str_repeat("../", $folder_depth - 2);
    
    $title_suffix = " Bill Uploads";
?>
<?php include $prefix.'config.php'; ?>
<?php include $prefix.'menu.php'; ?>
<?php include $prefix.'template_start.php'; ?>
<?php include $prefix.'page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-file_import"></i>Upload<br><small>Upload/process bill pdf files</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="../home.php">Home</a></li>
        <li>Upload</li>
    </ul>
    <!-- END Blank Header -->

    <!-- Upload Block -->
    <div class="block full">
        <!-- Upload Title -->
        <div class="block-title">
            <h2>Upload File</h2><small>Drag &amp; Drop or click to browse files for upload</small>
        </div>
        <!-- END Upload Title -->

        <!-- Upload Content -->
        <form action="uploader_crud.php" class="dropzone" id="uploader">
            <input type="hidden" name="request" value="upload" />
            <div class="fallback">
                <input name="file" type="file" multiple />
            </div>
        </form>
        <!-- END Upload Content -->

        <h4 class="sub-header"></h4>

        <button type="button" class="btn btn-sm btn-warning" id="btn-uploader-reset"><i class="gi gi-refresh"></i> Clear files</button>
    </div>
    <!-- END Upload Block -->

    <!-- Process Block -->
    <div class="block full">
        <!-- Process Title -->
        <div class="block-title">
            <h2>Process File</h2><small>Uploaded and ready to process files</small>
        </div>
        <!-- END Process Title -->

        <!-- Process Content -->
        <div class="table-responsive"><table id="table-process" class="table table-condensed table-striped table-hover"></table></div>
        <!-- END Process Content -->
    </div>
    <!-- END Process Block -->
</div>
<!-- END Page Content -->

<?php include $prefix.'page_footer.php'; ?>
<?php include $prefix.'template_scripts.php'; ?>

<script type="text/javascript">
    var template_html = '<?php
        $query_templates = "SELECT
        MT.ID,
        MT.TEMPLATE_NAME AS `NAME`
        FROM
        mobi_template AS MT
        INNER JOIN mobi_service_provider AS MSP ON MT.SERVICE_PROVIDER = MSP.SP_ID
        WHERE
        MT.`STATUS` = 1 AND
        MSP.`STATUS` = 1
        ORDER BY
        `NAME` ASC";

        $sql_templates = mysqli_query ($con_main, $query_templates);

        echo ("<select name=\"process-template\" id=\"process-template\" class=\"select-select2\" style=\"width: 100%;\" data-placeholder=\"Choose template\">");
        echo ("<option value=\"\" selected>Choose template</option>");
        while ($templates = mysqli_fetch_assoc($sql_templates)){
            echo ("<option value=\"".$templates['ID']."\">".$templates['NAME']."</option>");
        }

        echo ("</select>");
    ?>';

    App.datatables();

    var dt = $('#table-process').DataTable({
        "processing": true,
        "serverSide": true,
        "select": true,
        "columns": [
            { "data": "file_name", "name": "file_name", "title": "File Name" },
            { "data": "uploaded_date", "name": "uploaded_date", "title": "Uploaded Date" },
            { "data": "uploaded_by", "name": "uploaded_by", "title": "Uploaded By" },
            { "data": "processed", "name": "processed", "title": "Processed" },
            /* TEMPLATE */
            { "data": "template", "name": "template", "title": "Template", "searchable": false, "orderable": false,
                mRender: function (data, type, row) {
                    return template_html
                }
            },
            /* ACTIONS */ 
            {	"data": "actions", "name": "actions","title":"Actions", "searchable": false, "orderable": false, 
                mRender: function (data, type, row) {
                    return '<div class="btn-group btn-group-xs"><button id="btn-row-process" class="btn btn-primary" title="Process"><i class="fa fa-cogs"></i></button><button class="btn btn-danger" id="btn-row-delete" title="Delete"><i class="fa fa-trash-o"></i></button></div>'
                }
            }
        ],
        "columnDefs": [
            {"className": "dt-center", "targets": [1,2,3,4,5]}
        ],
        "language": {
            "emptyTable": "No files to show..."
        },
        "ajax": "data/grid_data_upload.php"
    });

    $('.dataTables_filter input').attr('placeholder', 'Search');

    Dropzone.options.uploader = {
        init: function() {
            this.on("success", function(r) {
                dt.ajax.reload();
                dt.draw();
            });
        },
        acceptedFiles: 'application/pdf'
    };
    
    $("#table-process tbody").on('click', '#btn-row-process', function() {
        var str_id = $(this).closest('tr').attr('id');
        var arr_id = str_id.split("_");

        var row_id = arr_id[1];
        var template_id = $(this).closest('tr').find('#process-template').val();

        $.ajax({
            url: 'uploader_crud.php',
            data: {
                request: 'process',
                file_id: row_id,
                template_id: template_id
            },
            method: 'POST',
            beforeSend: function () {
                $('#table-process tbody #'+str_id+' #btn-row-process').button('loading');
                NProgress.start();
            },
            error: function (e) {
                $.bootstrapGrowl('<h4>Error Processing!</h4> <p>Error processing file</p>', {
                    type: 'danger',
                    delay: 2500,
                    allow_dismiss: true
                });

                $('#table-process tbody #'+str_id+' #btn-row-process').button('reset');
                NProgress.done();
            },
            success: function (r) {
                var message_type = 'warning';

                if (r.result) {
                    message_type = 'success';
                }else{
                    message_type = 'danger';
                }

                $.bootstrapGrowl('<h4>Results</h4> <p>'+r.message+'</p>', {
                    type: message_type,
                    delay: 2500,
                    allow_dismiss: true
                });

                $('#table-process tbody #'+str_id+' #btn-row-process').button('reset');
                NProgress.done();

                dt.ajax.reload();
                dt.draw();
            }
        });
    });

    $("#table-process tbody").on('click', '#btn-row-delete', function() {
        var str_id = $(this).closest('tr').attr('id');
        var arr_id = str_id.split("_");

        var row_id = arr_id[1];

        $.ajax({
            url: 'uploader_crud.php',
            data: {
                request: 'delete',
                file_id: row_id
            },
            method: 'POST',
            beforeSend: function () {
                $('#table-process tbody #'+str_id+' #btn-row-delete').button('loading');
                NProgress.start();
            },
            error: function (e) {
                $.bootstrapGrowl('<h4>Error Deleting!</h4> <p>Error deleting file</p>', {
                    type: 'danger',
                    delay: 2500,
                    allow_dismiss: true
                });

                $('#table-process tbody #'+str_id+' #btn-row-delete').button('reset');
                NProgress.done();
            },
            success: function (r) {
                var message_type = 'warning';

                if (r.result) {
                    message_type = 'success';
                }else{
                    message_type = 'danger';
                }

                $.bootstrapGrowl('<h4>Results</h4> <p>'+r.message+'</p>', {
                    type: message_type,
                    delay: 2500,
                    allow_dismiss: true
                });

                $('#table-process tbody #'+str_id+' #btn-row-delete').button('reset');
                NProgress.done();

                dt.ajax.reload();
                dt.draw();
            }
        });
    });

    $('#btn-uploader-reset').on('click', function () {
        Dropzone.forElement("#uploader").removeAllFiles(true);
    });
</script>

<?php include $prefix.'template_end.php'; ?>