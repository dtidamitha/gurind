<?php
	session_start();
	
	if (empty($_SESSION['ACCESS_CODE']) || $_SESSION['ACCESS_CODE'] == NULL){
		header ('Location: login.php');
		exit;
	}
	
	$folder_depth = "";
	$prefix = "";
	
	$folder_depth = substr_count($_SERVER["PHP_SELF"] , "/");
	$folder_depth = ($folder_depth == false) ? 2 : (int)$folder_depth;
	
    $prefix = str_repeat("../", $folder_depth - 2);
    
    $title_suffix = " Payment Log";
?>
<?php include $prefix.'config.php'; ?>
<?php include $prefix.'menu.php'; ?>
<?php include $prefix.'template_start.php'; ?>
<?php include $prefix.'page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-address_book"></i>Payment Log<br><small>Create, Update or Delete Manual Payments</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="../home.php">Home</a></li>
        <li>Payment Log</li>
    </ul>
    <!-- END Blank Header -->
	
    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
					<h2>Payment Log</h2>
				</div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <form id="form-main" name="form-main" action="payment_log_crud.php" method="post"  class="form-horizontal form-bordered">
					<div class="form-group">
                        <label class="col-md-2 control-label" for="employee">Employee</label>
                        <div class="col-md-4">
                            <select id="employee" name="employee" style="width: 100%;">
                                <option value="0" selected disabled>Select Employee</option>
                            </select>
                        </div>

                        <label class="col-md-2 control-label" for="mobile_account">Mobile No</label>
                        <div class="col-md-4">
                            <select class="select-chosen" id="mobile_account" name="mobile_account" data-placeholder="Select Mobile Account">
                                <option value="0"></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="effective_year">Effective Year</label>
                        <div class="col-md-4">
                            <select id="effective_year" name="effective_year" class="form-control" size="1">
                                <option value="0" disabled>Select Year</option>
                                <?php
                                
                                $currently_selected = date('Y'); 
                                $earliest_year = 2016; 
                                $latest_year = date('Y');
                                
                                foreach ( range( $latest_year, $earliest_year ) as $i ) {
                                    
                                    print '<option value="'.$i.'"'.($i === $currently_selected ? ' selected="selected"' : '').'>'.$i.'</option>';
                                }
                                
                                ?>
                            </select>
                        </div>

                        <label class="col-md-2 control-label" for="effective_month">Effective Month</label>
                        <div class="col-md-4">
                            <select id="effective_month" name="effective_month" class="form-control">
                                <option value="1">January</option>
                                <option value="2">February</option>
                                <option value="3">March</option>
                                <option value="4">April</option>
                                <option value="5">May</option>
                                <option value="6">June</option>
                                <option value="7">July</option>
                                <option value="8">August</option>
                                <option value="9">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="amount">Paid Amount</label>
                        <div class="col-md-4">
                            <input type="text" id="amount" name="amount" class="form-control" placeholder="Enter Amount">
                        </div>

                        <label class="col-md-2 control-label" for="paid_date">Paid Date</label>
                        <div class="col-md-4">
                            <input type="text" id="paid_date" name="paid_date" class="form-control input-datepicker" value = "<?php echo (date('Y-m-d')); ?>" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="remarks">Remarks</label>
                        <div class="col-md-4">
                            <input type="text" id="remarks" name="remarks" class="form-control" placeholder="Enter remarks">
                        </div>

                        <label class="col-md-2 control-label" for="status">Status</label>
                        <div class="col-md-4">
                            <select id="status" name="status" class="select-chosen" data-placeholder="Choose Status">
                                <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                <option value="1" selected>Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group form-actions">
                        <input type="hidden" name="id" id="id" value="0" />

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-sm btn-success primary-btn pull-right"><i class="fa fa-angle-right"></i> Submit</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-plus"></i> New</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Block -->
            </div>
		</div>
        <!-- END Example Content -->
    </div>
    <!-- END Example Block -->

    <!-- Table Block -->
    <div class="block full">
        <!-- Table Title -->
        <div class="block-title">
            <h2>Log Entries </h2><small>Log entries currently exist in the system</small>
        </div>
        <!-- END Table Title -->

        <!-- Table Content -->
        <div class="table-responsive"><table id="table-data" class="table table-condensed table-striped table-hover"></table></div>
        <!-- END Table Content -->
    </div>
    <!-- END Table Block -->
</div>
<!-- END Page Content -->

<?php include $prefix.'page_footer.php'; ?>
<?php include $prefix.'template_scripts.php'; ?>
<?php include $prefix.'template_end.php'; ?>

<script type="text/javascript">
    var today = "<?php echo (date(Y-m-d)); ?>";

    $('#employee').select2({
        minimumInputLength: 2,
        ajax: {
            url: 'data/employee_data.php',
            dataType: 'json',
            delay: 100,
            data: function (term) {
                return term;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: "[" + item.emp_no + "] " + item.full_name,
                            id: item.user_code
                        }
                    })
                };
            }
        }
    });

    $('#employee').on('change', function (e){
        var user_code = $('#employee').val();

        $.ajax({
            url: 'data/data_mobile_accounts_for_emp.php',
            data: {
                user_id: user_code
            },
            method: 'POST',
            dataType: 'json',
            error: function (e) {
                $.bootstrapGrowl('<h4>Error!</h4> <p>Error retrieving mobile account data</p>', {
                    type: 'danger',
                    delay: 2500,
                    allow_dismiss: true
                });
            },
            success: function (r) {
                $('#mobile_account').html('<option value="0"></option>').trigger('chosen:updated');

                if (r.result){
                    $.each(r.data, function (k,v){
                        $('#mobile_account').append('<option value="'+v.NUMBER_ID+'">'+v.NUMBER+' ['+v.SP_NAME+' - '+v.CON_TYPE+']</option>').trigger('chosen:updated');
                    });
                }else{
                    $.bootstrapGrowl('<h4>Error!</h4> <p>'+r.message+'</p>', {
                        type: 'danger',
                        delay: 2500,
                        allow_dismiss: true
                    });
                }
            }
        });
    });

	/*********** Data-table Initialize ***********/	 	
	App.datatables();

    var dt = $('#table-data').DataTable({
        "processing": true,
        "serverSide": true,
        "select": true,
        "columns": [
            { "data": "mobile_no", "name": "mobile_no", "title": "Mobile No" },
            { "data": "employee", "name": "employee", "title": "Employee" },
            { "data": "service_provider", "name": "service_provider", "title": "Service Provider" },
            { "data": "connection_type", "name": "connection_type", "title": "Connection" },
            { "data": "effective_year", "name": "effective_year", "title": "Year" },
            { "data": "effective_month", "name": "effective_month", "title": "Month" },
            { "data": "remarks", "name": "remarks", "title": "Remarks" },

            /* ACTIONS */ 
            {	"data": "actions", "name": "actions","title":"Actions", "searchable": false, "orderable": false, 
                mRender: function (data, type, row) {
                    return '<div class="btn-group btn-group-xs"><button id="btn-row-edit" class="btn btn-primary" title="Edit"><i class="fa fa-pencil"></i></button></div>'
                }
            }
        ],
        "columnDefs": [
            {"className": "dt-center", "targets": [0,1,2,3,4,5,7]},
            {"className": "dt-left", "targets": [6]}
        ],
        "language": {
            "emptyTable": "No logs to show..."
        },
        "ajax": "data/grid_data_payment_log.php"
    });

	$('.dataTables_filter input').attr('placeholder', 'Search');

    $("#table-data tbody").on('click', '#btn-row-edit', function() {
        var str_id = $(this).closest('tr').attr('id');
        var arr_id = str_id.split("_");

        var row_id = arr_id[1];

        $.ajax({
            url: 'data/data_mobile_accounts.php',
            data: {
                id: row_id
            },
            method: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $('#table-data tbody #'+str_id+' #btn-row-edit').button('loading');
                NProgress.start();
            },
            error: function (e) {
                $.bootstrapGrowl('<h4>Error!</h4> <p>Error retrieving mobile account data</p>', {
                    type: 'danger',
                    delay: 2500,
                    allow_dismiss: true
                });

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            },
            success: function (r) {
                if (!r.result) {
                    $.bootstrapGrowl('<h4>Error!</h4> <p>'+r.message+'</p>', {
                        type: 'danger',
                        delay: 2500,
                        allow_dismiss: true
                    });
                }else{
                    $('#id').val(r.data[0].NUMBER_ID);
                    $('#employee').empty().append('<option value=\"' + r.data[0].USER_CODE + '\" selected=\"selected\">[' + r.data[0].EMP_NO + '] ' + r.data[0].EMP_NAME + '</option>').val(r.data[0].USER_CODE).trigger('change');
                    $('#mobile_no').val(r.data[0].NUMBER);
                    $('#mobile_account_no').val(r.data[0].MOBILE_ACCOUNT_NO);
                    $('#con_date').val(r.data[0].CONNECTION_DATE);

                    $('#service_provider').val("").trigger("chosen:updated");
                    $('#con_type').val("").trigger("chosen:updated");

                    $('#service_provider').val(r.data[0].SP_ID).trigger("chosen:updated");
                    $('#service_provider').trigger('change');

                    $('#con_type').val(r.data[0].CON_TYPE_ID).trigger("chosen:updated");
                    $('#con_type').trigger('change');

                    $('#limit').val(r.data[0].LIMIT);

                    var is_limited = (r.data[0].UNLIMITED == 1) ? true : false;
                    
                    $('#is_unlimited').prop('checked',is_limited).trigger('change');

                    $('#remarks').val(r.data[0].REMARKS);

                    $('#status').val(r.data[0].STATUS).trigger("chosen:updated");

                    setTimeout(function () {
                        $('#company_pac_type').val(r.data[0].COM_PAC_ID).trigger("chosen:updated");
                        $('#mobile_pac_type').val(r.data[0].MOB_PAC_ID).trigger("chosen:updated");
                    }, 300);
                }

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            }
        });
    });
    /*********** Table Control End ***********/	

    $('#form-main').on('submit', function (e){
		e.preventDefault();
		
		var id = $('#id').val();
		var op = (id == 0) ? "insert" : "update";
		
		var formdata = $('#form-main').serializeArray();
		formdata.push({'name':'operation','value':op});
		
		$.ajax({
			url: 'payment_log_crud.php',
			data: formdata,
			success: function(r){
				var msg_typ = "info";
                var msg_txt = "";

                if (r.result){
                    msg_typ = 'success';
                    msg_txt = '<h4>Success!</h4> <p>Mobile account saved</p>';

                    $('#form-main').trigger('reset');
                }else{
                    msg_typ = 'danger';
                    msg_txt = '<h4>Error!</h4> <p>'+r.message+'</p>';
                }

                $.bootstrapGrowl(msg_txt, {
                    type: msg_typ,
                    delay: 2500,
                    allow_dismiss: true
                });

                dt.ajax.reload();
                dt.draw();
			}
		});
	});

    $('#form-main').on('reset', function (e){
        $('#id').val("0");

        $('#employee').empty().append('<option value=\"0\" selected disabled>Select Employee</option>').val(0).trigger('change');
        $('#mobile_no').val("");
        $('#mobile_account_no').val("");
        $('#con_date').val(today);

        $('#service_provider').val("").trigger("chosen:updated");
        $('#con_type').val("").trigger("chosen:updated");

        $('#limit').val("");

        $('#is_unlimited').prop('checked',false).trigger('change');

        $('#remarks').val("");

        $('#status').val(1).trigger("chosen:updated");

        $('#company_pac_type').html("<option></option>").trigger("chosen:updated");
        $('#mobile_pac_type').html("<option></option>").trigger("chosen:updated");
    });
</script>
	
<?php mysqli_close($con_main); ?>