<?php
	header('Content-Type: application/json');
	
	include ('../../config.php');
	
	$term = $_REQUEST['term'];
	$status = $_REQUEST['status'];
	$user_code = $_REQUEST['id'];
	$emp_no = $_REQUEST['emp_no'];
	$first_name = $_REQUEST['first_name'];
	$last_name = $_REQUEST['last_name'];
	$loc = $_REQUEST['loc_code'];
	$dep = $_REQUEST['dep_code'];
	
	$result_array = array();
	$rule = "";
	$where = "";
	
	if (count($_REQUEST) > 0){ $where = " WHERE "; }else{ $where = ""; }
	
	if ($term != NULL && !empty($term)){
		if (!empty($rule)){ $rule .= " AND "; }else{ $rule = ""; }
		$rule .= "(MUSR.FIRST_NAME LIKE '%".$term."%' OR 
				   MUSR.LAST_NAME LIKE '%".$term."%' OR 
				   MUSR.EMP_NO LIKE '%".$term."%')";
	}
	
	if ($status != NULL && !empty($status)){
		if (!empty($rule)){ $rule .= " AND "; }else{ $rule = ""; }
		$rule .= "MUSR.`STATUS` = '$status'";
	}else{
		if (!empty($rule)){ $rule .= " AND "; }else{ $rule = ""; }
		$rule .= "MUSR.`STATUS` = 1";
	}
	
	if ($user_code != NULL && !empty($user_code) && $user_code != 0){
		if (!empty($rule)){ $rule .= " AND "; }else{ $rule = ""; }
		$rule .= "MUSR.USER_CODE = '$user_code'";
	}
	
	if ($emp_no != NULL && !empty($emp_no)){
		if (!empty($rule)){ $rule .= " AND "; }else{ $rule = ""; }
		$rule .= "MUSR.EMP_NO = '$emp_no'";
	}
	
	if ($first_name != NULL && !empty($first_name)){
		if (!empty($rule)){ $rule .= " AND "; }else{ $rule = ""; }
		$rule .= "MUSR.FIRST_NAME = '$first_name'";
	}
	
	if ($last_name != NULL && !empty($last_name)){
		if (!empty($rule)){ $rule .= " AND "; }else{ $rule = ""; }
		$rule .= "MUSR.LAST_NAME = '$last_name'";
	}
	
	if ($loc != NULL && !empty($loc) && $loc != 0){
		if (!empty($rule)){ $rule .= " AND "; }else{ $rule = ""; }
		$rule .= "MUSR.LOCATION = '$loc'";
	}
	
	if ($dep != NULL && !empty($dep) && $dep != 0){
		if (!empty($rule)){ $rule .= " AND "; }else{ $rule = ""; }
		$rule .= "MUSR.DEPARTMENT = '$dep'";
	}
	
	if (!empty($rule)){ $rule .= " AND "; }else{ $rule = " WHERE "; }
	$rule .= "MUSR.USER_CODE <> '0'";
	
	$user_query =   " SELECT
	MUSR.USER_CODE,
	MUSR.EMP_NO,
	MUSR.FIRST_NAME,
	MUSR.LAST_NAME,
	CONCAT_WS(' ',MUSR.FIRST_NAME,MUSR.LAST_NAME) AS `NAME`,
	MUSR.GENDER,
	MLOC.LOC_CODE,
	MLOC.LOCATION,
	MDEP.DEP_CODE,
	MDEP.DEPARTMENT,
	MDES.DES_CODE,
	MDES.DESIGNATION,
	MUSR.EMAIL,
	MUSR.MOBILE_NO,
	MUSR.PHOTO,
	MUSR.DEP_HEAD,
	MUSR.`STATUS`,
	MUSR.DOB,
	MUSR.NIC,
	MCOS.LOC_CODE AS COST_CENTR_CODE,
	MCOS.LOCATION AS COST_CENTR_NAME
	FROM
	mas_user AS MUSR
	LEFT JOIN mas_location AS MLOC ON MUSR.LOCATION = MLOC.LOC_CODE
	LEFT JOIN mas_department AS MDEP ON MUSR.DEPARTMENT = MDEP.DEP_CODE
	LEFT JOIN mas_designation AS MDES ON MUSR.DESIGNATION = MDES.DES_CODE
	LEFT JOIN mas_location AS MCOS ON MUSR.COST_CENTER = MCOS.LOC_CODE ";
	
	$user_query .= $where.$rule;
	
	$user_query .= " ORDER BY
					NAME ASC";
	
	$user_sql = mysqli_query($con_main, $user_query) or die (mysqli_error($con_main));
	
	$count = 0;
	
	while ($user = mysqli_fetch_array($user_sql)){
		$result_array[$count] = array(
										'user_code' => $user['USER_CODE'],
										'emp_no' => $user['EMP_NO'],
										'first_name' => $user['FIRST_NAME'],
										'last_name' => $user['LAST_NAME'],
										'full_name' => $user['NAME'],
										'gender' => $user['GENDER'],
										'loc_code' => $user['LOC_CODE'],
										'location' => $user['LOCATION'],
										'dep_code' => $user['DEP_CODE'],
										'department' => $user['DEPARTMENT'],
										'des_code' => $user['DES_CODE'],
										'designation' => $user['DESIGNATION'],
										'email' => $user['EMAIL'],
										'phone' => $user['MOBILE_NO'],
										'photo' => $user['PHOTO'],
										'dep_head' => $user['DEP_HEAD'],
										'status' => $user['STATUS'],
										'dob' => $user['DOB'],
										'nic' => $user['NIC'],
										'cost_center_code' => $user['COST_CENTR_CODE'],
										'cost_center_name' => $user['COST_CENTR_NAME']
								);
		
		$count++;
	}
	
	mysqli_close($con_main);
	
	echo (json_encode($result_array));
?>