<?php
	header('Content-Type: application/json');
    
    include ('../../config.php');
	
	$sp = (isset($_REQUEST['sp'])) ? $_REQUEST['sp'] : "";
	$con = (isset($_REQUEST['con'])) ? $_REQUEST['con'] : "";
    $type = (isset($_REQUEST['type'])) ? $_REQUEST['type'] : "";
    $status = (isset($_REQUEST['status'])) ? $_REQUEST['status'] : "";
	
    $result_array = array();
    $data = "";
    $message = "";
    $result = true;

    $where = "";

    if (!empty($sp)){
        $where .= (empty($where)) ? " WHERE " : " AND ";
        $where .= " MP.SERVICE_PROVIDER = '$sp' ";
    }

    if (!empty($con)){
        $where .= (empty($where)) ? " WHERE " : " AND ";
        $where .= " MP.CON_TYPE = '$con' ";
    }

    if (!empty($type)){
        $where .= (empty($where)) ? " WHERE " : " AND ";

        switch($type){
            case 'COM_PAC':
                $where .= " MP.COM_PAC = 1 ";
            break;

            case 'MOBI_PAC':
                $where .= " MP.MOBI_PAC = 1 ";
            break;
        }
    }

    if (!empty($status)){
        $where .= (empty($where)) ? " WHERE " : " AND ";
        $where .= " MP.`STATUS` = '$status' ";
    }else{
        $where .= (empty($where)) ? " WHERE " : " AND ";
        $where .= " MP.`STATUS` = 1 ";
    }
    
	
	$query = "SELECT
    MP.PAC_ID AS ID,
    MP.PAC_NAME AS `NAME`,
    MP.SERVICE_PROVIDER,
    MP.CON_TYPE,
    MP.COM_PAC AS COMPANY_PACKAGE,
    MP.MOBI_PAC AS MOBILE_PACKAGE,
    MP.`STATUS`
    FROM
    mobi_package AS MP ".$where." ORDER BY `NAME` ASC";
	
    $sql = mysqli_query($con_main, $query) or die (mysqli_error($con_main));
    
    if ($sql){
        $count = 0;
        
        while ($row = mysqli_fetch_assoc($sql)){
            $data[$count] = $row;
            
            $count++;
        }
    }else{
        $result = false;
        $message = 'Error SQL: ['.mysqli_errno($con_main).'] '.mysqli_error($con_main);
        $result_array['debug'] = $query;
    }
    
    $result_array['data'] = $data;
    $result_array['result'] = $result;
    $result_array['message'] = $message;
	
	mysqli_close($con_main);
	
	echo (json_encode($result_array));
?>