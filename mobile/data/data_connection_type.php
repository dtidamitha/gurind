<?php
	header('Content-Type: application/json');
	
	include ('../../config.php');
	
	$id = (isset($_REQUEST['id']) && !empty($_REQUEST['id'])) ? $_REQUEST['id'] : 0;

	$result = true;
	$where = "";
	$message = "";
	$debug = "";
	
	$data = "";
	$responce = array();

	if ($id > 0){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " T0.CON_TYPE_ID = $id ";
	}
	
	$query = "SELECT
	T0.CON_TYPE_ID,
	T0.CON_TYPE,
	T0.`STATUS`
	FROM
	mobi_con_type AS T0".$where;
	
	$sql = mysqli_query($con_main, $query);
	
	if ($sql){
		$count = 0;

		while ($res = mysqli_fetch_assoc($sql)){
			$data[$count] = $res;
			
			$count++;
		}
	}else{
		$result = false;
		$message .= "<br>Error retrieving connection type data";
		$debug .= "\nError SQL: (".mysqli_errno($con_main).") ".mysqli_error($con_main);
	}
	
	mysqli_close($con_main);

	$responce['result'] = $result;
	$responce['message'] = $message;
	$responce['debug'] = $debug;
	$responce['data'] = $data;
	
	echo (json_encode($responce));
?>