<?php
	header('Content-Type: application/json');
	
	include ('../../config.php');
	
	$result_array = array();
	
	$draw = $_REQUEST['draw'];
	$length = $_REQUEST['length'];
	$start = $_REQUEST['start'];
	$columns = $_REQUEST['columns'];
	$order_cols = $_REQUEST['order'];
	$search = $_REQUEST['search'];
	$data = array();
	
	// Column arrangement in grid. index to table column mapping array
	$col_set = array('MU.FILE_NAME','MU.UPLOADED_DATE','MU.UPLOADED_BY','MU.PROCESSED_DATE');
	
	// Where and Order By Clause string
	$order_by = "";
	$where = "";
	
	// Generate order by string
	// Loop over all ordered fields (inside array) received from datatable client
	foreach ($order_cols as $order_col){
		// if order by string is empty start order by clause with ORDER BY else append order by with comma
		$order_by .= (empty($order_by)) ? " ORDER BY " : ", ";
		
		$order_col_index = $order_col['column']; // Order column index no
		$order_col_dir = $order_col['dir']; // Order direction (asc/desc)
		
		// Generate order by string (convert index to table column + direction)
		$order_by .= $col_set[$order_col_index]." ".$order_col_dir." ";
	}
	
	// Generate where clause if search term is not empty (received with client's request)
	if (!empty($search['value'])){
		// get search term
		$term = $search['value'];
		
		$i = 0;
		
		// Loop over available columns
		foreach ($columns as $column){
			// Get current column name and searchable (true/false)
			$col_name = $column['name'];
			$col_searchable = $column['searchable'];
			
			// If current column is searchable
			if ($col_searchable == "true"){
				// if where string is empty start where clause with WHERE else append where string with OR
				$where .= (empty($where)) ? " WHERE " : " OR ";
			
				if ($i == 2){
					// If column is FULL_NAME then seperately search like search term for first name and last name
					$where .= " (UB.FIRST_NAME LIKE '%$term%' OR UB.LAST_NAME LIKE '%$term%') ";
				}else{
					// Convert column index to table column + LIKE + 'search term'
					$where .= $col_set[$i]." LIKE '%$term%' ";
				}
			}
			
			$i = $i + 1;
		}
	}

	$where .= (empty($where)) ? " WHERE " : " AND ";
	$where .= " MU.PROCESSED_FLAG = 0 ";
	
	$query = "SELECT
			MU.ID,
			MU.FILE_NAME,
			MU.FILE_DIR,
			DATE(MU.UPLOADED_DATE) AS UPLOADED_DATE,
			MU.UPLOADED_BY,
			MU.PROCESSED_FLAG,
			DATE(MU.PROCESSED_DATE) AS PROCESSED_DATE,
			UB.EMP_NO AS UB_EMP_NO,
			CONCAT_WS(' ', UB.FIRST_NAME, UB.LAST_NAME) AS UB_NAME
			FROM
			mobi_upload AS MU
			INNER JOIN mas_user AS UB ON MU.UPLOADED_BY = UB.USER_CODE".$where." ".$order_by."
			LIMIT ".$start.", ".$length;
	
	$sql = mysqli_query($con_main, $query);
	
	$i = 0;

	$result_array['data'] = "";
	
	while ($row = mysqli_fetch_assoc($sql)){
		$processed = ($row['PROCESSED_FLAG'] == 1) ? $row['PROCESSED_DATE'] : 'No';

		$data['DT_RowId'] = "row_".$row['ID'];
		$data['file_name'] = $row['FILE_NAME'];
		$data['uploaded_date'] = $row['UPLOADED_DATE'];
		$data['uploaded_by'] = $row['UB_NAME'];
		$data['processed'] = $processed;
		
		$result_array['data'][$i] = $data;
		
		$i = $i + 1;
	}
	
	$filtered_row_count_query = "SELECT 
	IFNULL(COUNT(MU.ID), 0) AS ROW_COUNT
	FROM
	mobi_upload AS MU
	INNER JOIN mas_user AS UB ON MU.UPLOADED_BY = UB.USER_CODE".$where;
	
	$filtered_row_count_sql = mysqli_query($con_main, $filtered_row_count_query);
	$filtered_row_count = mysqli_fetch_assoc($filtered_row_count_sql);
	$filtered_records = $filtered_row_count['ROW_COUNT'];
	$filtered_records = 0;
	
	$full_row_count_sql = mysqli_query($con_main, "SELECT COUNT(MU.ID) AS C FROM mobi_upload AS MU WHERE MU.PROCESSED_FLAG = 0");
	$full_row_count = mysqli_fetch_assoc($full_row_count_sql);
	
	$total_records = $full_row_count['C'];
	
	$result_array['draw'] = $draw; // Return same draw id received from datatable client request
	$result_array['recordsTotal'] = $total_records; // Return total record count in table
	$result_array['recordsFiltered'] = (!empty($search['value'])) ? $filtered_records : $total_records; // If search term is available return filtered records count or else total record count
	//$result_array['query'] = $query; //For debugging
	
	mysqli_close($con_main);
	
	echo (json_encode($result_array));
?>