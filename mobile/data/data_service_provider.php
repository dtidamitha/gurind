<?php
	header('Content-Type: application/json');
	
	include ('../../config.php');
	
	$id = (isset($_REQUEST['id']) && !empty($_REQUEST['id'])) ? $_REQUEST['id'] : 0;

	$result = true;
	$where = "";
	$message = "";
	$debug = "";
	
	$data = "";
	$responce = array();

	if ($id > 0){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " T0.SP_ID = $id ";
	}
	
	$query = "SELECT
    T0.SP_ID,
    T0.CORPORATE_CODE,
    T0.FULL_NAME,
    T0.SHORT_NAME,
    T0.NAME_IN_BILL,
    T0.BILLING_ADDRESS,
    T0.VAT_REG,
    T0.SVAT_REG,
    T0.HOTMAIL,
    T0.LOGO,
    T0.`STATUS`,
    T0.ADDED_DATE,
    T0.ADDED_BY
    FROM
    mobi_service_provider AS T0".$where;
	
	$sql = mysqli_query($con_main, $query);
	
	if ($sql){
		$count = 0;

		while ($res = mysqli_fetch_assoc($sql)){
			$data[$count] = $res;
			
			$count++;
		}
	}else{
		$result = false;
		$message .= "<br>Error retrieving service provider data";
		$debug .= "\nError SQL: (".mysqli_errno($con_main).") ".mysqli_error($con_main);
	}
	
	mysqli_close($con_main);

	$responce['result'] = $result;
	$responce['message'] = $message;
	$responce['debug'] = $debug;
	$responce['data'] = $data;
	
	echo (json_encode($responce));
?>