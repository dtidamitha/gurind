<?php
	header('Content-Type: application/json');
	
	include ('../../config.php');
	
	$result_array = array();
	
	$draw = $_REQUEST['draw'];
	$length = $_REQUEST['length'];
	$start = $_REQUEST['start'];
	$columns = $_REQUEST['columns'];
	$order_cols = $_REQUEST['order'];
	$search = $_REQUEST['search'];
	$data = array();
	
	// Column arrangement in grid. index to table column mapping array
	$col_set = array('MBN.NUMBER','EMP_NAME','MSP.SHORT_NAME','MCT.CON_TYPE','CPT.PAC_NAME','MPT.PAC_NAME','MBN.LIMIT');
	
	// Where and Order By Clause string
	$order_by = "";
	$where = "";
	
	// Generate order by string
	// Loop over all ordered fields (inside array) received from datatable client
	foreach ($order_cols as $order_col){
		// if order by string is empty start order by clause with ORDER BY else append order by with comma
		$order_by .= (empty($order_by)) ? " ORDER BY " : ", ";
		
		$order_col_index = $order_col['column']; // Order column index no
		$order_col_dir = $order_col['dir']; // Order direction (asc/desc)
		
		// Generate order by string (convert index to table column + direction)
		$order_by .= $col_set[$order_col_index]." ".$order_col_dir." ";
	}
	
	// Generate where clause if search term is not empty (received with client's request)
	if (!empty($search['value'])){
		// get search term
		$term = $search['value'];
		
		$i = 0;
		
		// Loop over available columns
		foreach ($columns as $column){
			// Get current column name and searchable (true/false)
			$col_name = $column['name'];
			$col_searchable = $column['searchable'];
			
			// If current column is searchable
			if ($col_searchable == "true"){
				// if where string is empty start where clause with WHERE else append where string with OR
				$where .= (empty($where)) ? " WHERE " : " OR ";

                if ($col_set[$i] != 'EMP_NAME'){
                    // Convert column index to table column + LIKE + 'search term'
                    $where .= $col_set[$i]." LIKE '%$term%' ";
                }else{
                    $where .= " (MUS.FIRST_NAME LIKE '%$term%' OR MUS.LAST_NAME LIKE '%$term%') ";
                }
			}
			
			$i = $i + 1;
		}
	}
	
	$query = "SELECT
    MBN.NUMBER_ID,
    MBN.NUMBER,
    MBN.MOBILE_ACCOUNT_NO,
    MSP.SP_ID AS SP_ID,
    MSP.SHORT_NAME AS SP_NAME,
    MCT.CON_TYPE_ID,
    MCT.CON_TYPE,
    CPT.PAC_ID AS COM_PAC_ID,
    CPT.PAC_NAME AS COM_PAC_NAME,
    MPT.PAC_ID AS MOB_PAC_ID,
    MPT.PAC_NAME AS MOB_PAC_NAME,
    MBN.`LIMIT`,
    MBN.UNLIMITED,
    MUS.USER_CODE,
    MUS.EMP_NO,
    CONCAT_WS(' ',MUS.FIRST_NAME,MUS.LAST_NAME) AS EMP_NAME,
    MBN.CONNECTION_DATE,
    MBN.REMARKS,
    MBN.`STATUS`
    FROM
    mobi_number AS MBN
    LEFT JOIN mobi_service_provider AS MSP ON MBN.SERVICE_PROVIDER = MSP.SP_ID
    LEFT JOIN mobi_con_type AS MCT ON MBN.CON_TYPE = MCT.CON_TYPE_ID
    LEFT JOIN mas_user AS MUS ON MBN.USER_ID = MUS.USER_CODE
    LEFT JOIN mobi_package AS CPT ON MBN.COM_PAC_TYPE = CPT.PAC_ID
    LEFT JOIN mobi_package AS MPT ON MBN.MOB_PAC_TYPE = MPT.PAC_ID".$where." ".$order_by."
	LIMIT ".$start.", ".$length;
	
	$sql = mysqli_query($con_main, $query);
	
	$i = 0;

	$result_array['data'] = "";
	
	while ($row = mysqli_fetch_assoc($sql)){
        $status_text = ($row['STATUS'] == 1) ? "Active" : "Inactive";
        $limit_text = ($row['UNLIMITED'] == 1) ? "UNLIMITED" : $row['LIMIT'];

		$data['DT_RowId'] = "row_".$row['NUMBER_ID'];
		$data['mobile_no'] = $row['NUMBER'];
		$data['employee'] = $row['EMP_NAME'];
		$data['service_provider'] = $row['SP_NAME'];
		$data['connection_type'] = $row['CON_TYPE'];
		$data['company_package'] = $row['COM_PAC_NAME'];
		$data['mobile_package'] = $row['MOB_PAC_NAME'];
		$data['limit'] = $limit_text;
		
		$result_array['data'][$i] = $data;
		
		$i = $i + 1;
	}

	$filtered_row_count_query = "SELECT
    COUNT(MBN.NUMBER_ID) AS ROW_COUNT
    FROM
    mobi_number AS MBN
    LEFT JOIN mobi_service_provider AS MSP ON MBN.SERVICE_PROVIDER = MSP.SP_ID
    LEFT JOIN mobi_con_type AS MCT ON MBN.CON_TYPE = MCT.CON_TYPE_ID
    LEFT JOIN mas_user AS MUS ON MBN.USER_ID = MUS.USER_CODE
    LEFT JOIN mobi_package AS CPT ON MBN.COM_PAC_TYPE = CPT.PAC_ID
    LEFT JOIN mobi_package AS MPT ON MBN.MOB_PAC_TYPE = MPT.PAC_ID".$where;
	
	$filtered_row_count_sql = mysqli_query($con_main, $filtered_row_count_query);
	$filtered_row_count = mysqli_fetch_assoc($filtered_row_count_sql);
	$filtered_records = $filtered_row_count['ROW_COUNT'];
	
	$full_row_count_sql = mysqli_query($con_main, "SELECT COUNT(MBN.NUMBER_ID) AS C FROM mobi_number AS MBN");
	$full_row_count = mysqli_fetch_array($full_row_count_sql);
	
	$total_records = $full_row_count['C'];
	
	$result_array['draw'] = $draw; // Return same draw id received from datatable client request
	$result_array['recordsTotal'] = $total_records; // Return total record count in table
	$result_array['recordsFiltered'] = (!empty($search['value'])) ? $filtered_records : $total_records; // If search term is available return filtered records count or else total record count
	//$result_array['query'] = $query; //For debugging
	
	mysqli_close($con_main);
	
	echo (json_encode($result_array));
?>