<?php
header('Content-Type: application/json');

include ('../../config.php');

$where = "";
$responce = array();
$data = "";
$result = true;
$message = "";
$debug = "";

$user_id = (isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : 0;

if ($user_id > 0){
    $where .= (empty($where)) ? " WHERE " : " AND ";
    $where .= " MBN.USER_ID = '$user_id' ";
}

$query = "SELECT
MBN.NUMBER_ID,
MBN.NUMBER,
MBN.MOBILE_ACCOUNT_NO,
MSP.SP_ID AS SP_ID,
MSP.SHORT_NAME AS SP_NAME,
MCT.CON_TYPE_ID,
MCT.CON_TYPE,
CPT.PAC_ID AS COM_PAC_ID,
CPT.PAC_NAME AS COM_PAC_NAME,
MPT.PAC_ID AS MOB_PAC_ID,
MPT.PAC_NAME AS MOB_PAC_NAME,
MBN.`LIMIT`,
MBN.UNLIMITED,
MUS.USER_CODE,
MUS.EMP_NO,
CONCAT_WS(' ',MUS.FIRST_NAME,MUS.LAST_NAME) AS EMP_NAME,
MBN.CONNECTION_DATE,
MBN.REMARKS,
MBN.`STATUS`
FROM
mobi_number AS MBN
LEFT JOIN mobi_service_provider AS MSP ON MBN.SERVICE_PROVIDER = MSP.SP_ID
LEFT JOIN mobi_con_type AS MCT ON MBN.CON_TYPE = MCT.CON_TYPE_ID
LEFT JOIN mas_user AS MUS ON MBN.USER_ID = MUS.USER_CODE
LEFT JOIN mobi_package AS CPT ON MBN.COM_PAC_TYPE = CPT.PAC_ID
LEFT JOIN mobi_package AS MPT ON MBN.MOB_PAC_TYPE = MPT.PAC_ID".$where;

$sql = mysqli_query ($con_main, $query);

if (!$sql){
    $result = false;
    $message .= "<br>Failed fetching mobile account data";
    $debug .= "\nError Sql : (".mysqli_errno($con_main).") ".mysqli_error($con_main).". ";
}

$num_rows = mysqli_num_rows($sql);

if ($num_rows > 0){
    $i = 0;

    while ($rows = mysqli_fetch_assoc ($sql)){
        $data[$i] = $rows;

        $i++;
    }
}else{
    $result = ($result) ? true : false;
    $message .= "<br>Empty results";
}

$responce['data'] = $data;
$responce['result'] = $result;
$responce['message'] = $message;
$responce['debug'] = $debug;

echo (json_encode($responce));

mysqli_close($con_main);
?>