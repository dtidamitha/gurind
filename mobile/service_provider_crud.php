<?php
	header('Content-Type: application/json');
	
	session_start();
	
	require_once ('../config.php');
	
	$billing_address = $_REQUEST['billing_address'];
	$coporate_code = $_REQUEST['corporate_code'];
	$full_name = $_REQUEST['full_name'];
	$hotmail = $_REQUEST['hotmail'];
	$id = $_REQUEST['id'];
	$name_in_bill = $_REQUEST['name_in_bill'];
	$op = $_REQUEST['operation'];
	$short_name = $_REQUEST['short_name'];
	$status = $_REQUEST['status'];
	$svat_reg = $_REQUEST['svat_reg'];
	$vat_reg = $_REQUEST['vat_reg'];

	$user = $_SESSION['USER_CODE'];
	
	$query = "";
	$result = true;
	$message = "";
	$debug = "";
	$responce = array();
	
	if ($op == "insert"){
		$query = "INSERT INTO `mobi_service_provider` (
			`CORPORATE_CODE`,
			`FULL_NAME`,
			`SHORT_NAME`,
			`NAME_IN_BILL`,
			`BILLING_ADDRESS`,
			`VAT_REG`,
			`SVAT_REG`,
			`HOTMAIL`,
			`STATUS`,
			`ADDED_DATE`,
			`ADDED_BY`
		)
		VALUES
		(
			'$coporate_code',
			'$full_name',
			'$short_name',
			'$name_in_bill',
			'$billing_address',
			'$vat_reg',
			'$svat_reg',
			'$hotmail',
			'$status',
			NOW(),
			'$user'
		)";

		$sql = mysqli_query ($con_main, $query);

		if (!$sql){
			$result = false;
			$id = 0;
			$message .= "<br>Service provider insert failed.";
			$debug .= "\nError SQL: (".mysqli_errno($con_main).") ".mysqli_error($con_main);
		}else{
			$id = mysqli_insert_id($con_main);
			$message .= "<br>Service provider insert succeed.";
		}
	}else if ($op == "update"){
		$query = "UPDATE `mobi_service_provider`
		SET `CORPORATE_CODE` = '$coporate_code',
		`FULL_NAME` = '$full_name',
		`SHORT_NAME` = '$short_name',
		`NAME_IN_BILL` = '$name_in_bill',
		`BILLING_ADDRESS` = '$billing_address',
		`VAT_REG` = '$vat_reg',
		`SVAT_REG` = '$svat_reg',
		`HOTMAIL` = '$hotmail',
		`STATUS` = '$status'
		WHERE
		(`SP_ID` = '$id')";

		$sql = mysqli_query ($con_main, $query);

		if (!$sql){
			$result = false;
			$message .= "<br>Service provider update failed.";
			$debug .= "\nError SQL: (".mysqli_errno($con_main).") ".mysqli_error($con_main);
		}else{
			$message .= "<br>Service provider update succeed.";
		}
	}else if ($op == "upload-image"){
		$obj_id = $_REQUEST['obj_id'];
		$moved_url = "sp_pix/".$obj_id.".jpg";

		if (!empty($_FILES["file"])){
			if ($_FILES["file"]["error"] > 0){
				$result = false;
				$message .= "<br>Error uploading file.<br>".$_FILES["file"]["error"];
				$debug .= "\nError: " . $_FILES["file"]["error"];
			}else{
				$responce['file_name'] = $_FILES["file"]["name"];
				$responce['file_size_kb'] = $_FILES["file"]["size"]/1024;

				$file_moved = move_uploaded_file($_FILES["file"]["tmp_name"],$moved_url);

				if ($file_moved){
					$upload_query = "UPDATE `mobi_service_provider` SET `LOGO` = '$moved_url' WHERE (`SP_ID` = '$obj_id')";
					$sql = mysqli_query($con_main, $upload_query);

					if ($sql){
						$message .= "<br>Logo uploaded successfully.";
					}else{
						$result = false;
						$message .= "<br>Logo upload failed.";
						$debug .= "\nError SQL. (".mysqli_errno($con_main).") ".mysqli_error($con_main)." Query: ".$upload_query;
					}
				}else{
					$result = false;
					$message .= "<br>Error moving uploaded file.";
					$debug .= "\nError moving uploaded file";
				}
			}
		}else{
			$result = false;
			$message .= "<br>No file to upload.";
			$debug .= "\nError: No file to upload";
		}
	}else{
		$result = false;
		$message .= "<br>Failed! Undefined operation.";
		$debug .= "\nUndefined operation requested. Op: ".$op;
	}

	mysqli_close($con_main);
	
	$responce['result'] = $result;
	$responce['message'] = $message;
	$responce['debug'] = $debug;
	$responce['id'] = $id;
	
	echo (json_encode($responce));
?>