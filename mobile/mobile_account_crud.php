<?php
	header('Content-Type: application/json');
	
	session_start();
	
	require_once ('../config.php');

	$user = $_SESSION['USER_CODE'];
	
    $op = $_REQUEST['operation'];
    $id = $_REQUEST['id'];
    $employee = $_REQUEST['employee'];
    $mobile_no = $_REQUEST['mobile_no'];
    $mobile_account_no = $_REQUEST['mobile_account_no'];
    $con_date = $_REQUEST['con_date'];
    $service_provider = $_REQUEST['service_provider'];
    $con_type = $_REQUEST['con_type'];
    $company_pac_type = $_REQUEST['company_pac_type'];
    $mobile_pac_type = $_REQUEST['mobile_pac_type'];
    $is_unlimited = $_REQUEST['is_unlimited'];
    $limit = $_REQUEST['limit'];
    $remarks = $_REQUEST['remarks'];
	$status = $_REQUEST['status'];
	
	$query = "";
	$success = true;
	$message = "";
	$responce = array();
	
	if ($op == "insert"){
		$query = "INSERT INTO `mobi_number` (
            `NUMBER`,
            `MOBILE_ACCOUNT_NO`,
            `SERVICE_PROVIDER`,
            `CON_TYPE`,
            `COM_PAC_TYPE`,
            `MOB_PAC_TYPE`,
            `LIMIT`,
            `UNLIMITED`,
            `USER_ID`,
            `CONNECTION_DATE`,
            `REMARKS`,
            `STATUS`,
            `ADDED_DATE`,
            `ADDED_BY`
            )VALUES(
            '$mobile_no',
            '$mobile_account_no',
            '$service_provider',
            '$con_type',
            '$company_pac_type',
            '$mobile_pac_type',
            '$limit',
            '$is_unlimited',
            '$employee',
            '$con_date',
            '$remarks',
            '$status',
            NOW(),
            '$user'
            )";
	}else if ($op = "update"){
		$query = "UPDATE `mobi_number`
        SET `NUMBER` = '$mobile_no',
        `MOBILE_ACCOUNT_NO` = '$mobile_account_no',
        `SERVICE_PROVIDER` = '$service_provider',
        `CON_TYPE` = '$con_type',
        `COM_PAC_TYPE` = '$company_pac_type',
        `MOB_PAC_TYPE` = '$mobile_pac_type',
        `LIMIT` = '$limit',
        `UNLIMITED` = '$is_unlimited',
        `USER_ID` = '$employee',
        `CONNECTION_DATE` = '$con_date',
        `REMARKS` = '$remarks',
        `STATUS` = '$status'
        WHERE
        (`NUMBER_ID` = '$id')";
	}
	
	$sql = mysqli_query ($con_main, $query);
	
	$id = ($op == 'insert') ? mysqli_insert_id($con_main) : $id;
	
	if ($sql){
		$success = true;
		$message = "Success";
	}else{
		$success = false;
		$message = "Error SQL. (".mysqli_errno($con_main).") ".mysqli_error($con_main);
	}
	
	$responce['operation'] = $op;
	$responce['result'] = $success;
	$responce['id'] = $id;
	$responce['message'] = $message;
	
	echo (json_encode($responce));
	
	mysqli_close($con_main);
?>