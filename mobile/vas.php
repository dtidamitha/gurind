<?php
	session_start();
	
	if (empty($_SESSION['ACCESS_CODE']) || $_SESSION['ACCESS_CODE'] == NULL){
		header ('Location: login.php');
		exit;
	}
	
	$folder_depth = "";
	$prefix = "";
	
	$folder_depth = substr_count($_SERVER["PHP_SELF"] , "/");
	$folder_depth = ($folder_depth == false) ? 2 : (int)$folder_depth;
	
	$prefix = str_repeat("../", $folder_depth - 2);
?>
<?php include $prefix.'config.php'; ?>
<?php include $prefix.'menu.php'; ?>
<?php include $prefix.'template_start.php'; ?>
<?php include $prefix.'page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-brush"></i>Value Added Services<br><small>Add Value Added Services</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="../home.php">Home</a></li>
        <li>Value Added Services</li>
    </ul>
    <!-- END Blank Header -->
		
	
<div class="row">
    <div class="col-md-12">
            <!-- Basic Form Elements Block -->
        <div class="block">
                <!-- Basic Form Elements Title -->
                 <div class="block-title">
					<h2>Value Added Services</h2>
				</div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <form id="form-srevice-provider" name="form-connection-type" action="vas_crud.php" method="post"  class="form-horizontal form-bordered" >
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="service_provider">Select Service Provider</label>
                        <div class="col-md-8">
                            <select id="service_provider" name="service_provider" class="form-control" size="1">
                                <option value="" selected disabled>Please select Service Provider</option>
								<?php
								$query="SELECT
										mobi_service_provider.SP_ID,
										mobi_service_provider.SHORT_NAME
										FROM
										mobi_service_provider
										WHERE
										mobi_service_provider.`STATUS` = '1'";
								$sql = mysqli_query($con_main, $query);
																		
								while ($type = mysqli_fetch_array($sql)){
									echo ("<option value=\"".$type['SP_ID']."\">".$type['SHORT_NAME']."</option>");
								}
								?>
                            </select>
                        </div>
					</div>
						
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="vas">Value Added Services</label>
                        <div class="col-md-8">
                            <input id="vas" name="vas" class="form-control" placeholder="vas" size="1">
                            
                        </div>
                    </div>
					
					<div class="form-group">
                        <label class="col-md-2 control-label" for="remark">Remark</label>
                        <div class="col-md-8">
                            <input id="remark" name="remark" class="form-control" placeholder="Remark" size="1">
                            
                        </div>
                    </div>
					
					
					
					<div class="form-group">
                        <input type="hidden" name="sp_id" id="sp_id" value="0" />
					</div>
                    
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-success primary-btn multi-submit-btn"><i class="fa fa-angle-right"></i> Submit</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                </form>
            <!-- END Basic Form Elements Block -->
			
			
		</div>

    <!-- END Example Content -->

		    
    </div>
    <!-- END Example Block -->
</div>
<!-- END Page Content -->

<?php include $prefix.'page_footer.php'; ?>
<?php include $prefix.'template_scripts.php'; ?>
<?php include $prefix.'template_end.php'; ?>
<script src="<?php echo ($prefix); ?>js/lib/jquery.maskedinput.js"></script>
	<script src="<?php echo ($prefix); ?>js/lib/jquery.validate.js"></script>
	<script src="<?php echo ($prefix); ?>js/lib/jquery.form.js"></script>
	<script src="<?php echo ($prefix); ?>js/lib/j-forms.js"></script>
	
	<!--Bootbox-->
	<script src="<?php echo ($prefix); ?>js/lib/bootbox.js"></script>
	
	<script type="text/javascript">
	
	$('#form-srevice-provider').on('submit', function (e){
		e.preventDefault();
		
		var sp_id = $('#sp_id').val();
		var op = "insert";

		if (sp_id == 0){
			op = "insert";
		}else{
			op = "update";
		}
		
		var formdata = $('#form-srevice-provider').serializeArray();
		formdata.push({'name':'operation','value':op});
		
		$.ajax({
			url: 'vas_crud.php',
			data: formdata,
			success: function(r){
				alert (r.message+" "+r.id);
			}
		});
	});
	 	
	App.datatables();

    var dt = $('#table-process').DataTable({
        "processing": true,
        "serverSide": true,
        "select": true,
        "columns": [
			{ "data": "id", "visible": false, "sortable": false, "filterable": false },
            { "data": "co_code", "name": "co_code", "title": "Co-operative Code" },
            { "data": "f_name", "name": "f_name", "title": "Full Name" }
            { "data": "f_name", "name": "f_name", "title": "Full Name" }
            
            /* ACTIONS */ 
            {	"data": "actions", "name": "actions","title":"Actions", "searchable": false, "orderable": false, 
                mRender: function (data, type, row) {
                    return '<div class="btn-group btn-group-xs"><button id="btn-row-process" class="btn btn-primary" title="Process"><i class="fa fa-cogs"></i></button><button class="btn btn-danger" id="btn-row-delete" title="Delete"><i class="fa fa-trash-o"></i></button></div>'
                }
            }
        ],
      /*  "columnDefs": [
            {"className": "dt-center", "targets": [1,2,3,4,5,6,7,8,9]}
        ],
        "language": {
            "emptyTable": "No files to show..."
        },
        "ajax": "service_provider_grid_data.php"
    });	 */
		
		
	</script>
	
	<?php mysqli_close($con_main); ?>