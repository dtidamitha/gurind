<?php
header('Content-Type: application/json');

session_start();

include ('../config.php');
// Include Composer autoloader if not already done.
require_once ('vendor/autoload.php');

$request = $_REQUEST['request'];
$responce = array();
$result = true;
$message = "";
$user = $_SESSION['USER_CODE'];

if ($request == "upload"){
	if (!empty($_FILES["file"]))
	{
		if ($_FILES["file"]["error"] > 0){
			$result = false;
			$message .= "Error: " . $_FILES["file"]["error"] . "<br>";
		}else{
			$responce['file_name'] = $_FILES["file"]["name"];
			$responce['file_size_kb'] = $_FILES["file"]["size"]/1024;

			$file_moved = move_uploaded_file($_FILES["file"]["tmp_name"],"files/".$_FILES["file"]["name"]);

			if ($file_moved){
				$query_insert = "INSERT INTO `mobi_upload` (
					`FILE_NAME`,
					`FILE_DIR`,
					`UPLOADED_DATE`,
					`UPLOADED_BY`,
					`PROCESSED_FLAG`
				)
				VALUES
				(
					'".$responce['file_name']."',
					'files/".$responce['file_name']."',
					CURDATE(),
					'$user',
					0
				)";

				$sql_insert = mysqli_query ($con_main, $query_insert);
				
				if ($sql_insert){
					$message .= "Uploaded successfully";
				}else{
					$result = false;
					$message .= "Error recording file upload. Sql Error: (".mysqli_errno($con_main).") ".mysqli_error($con_main);
				}
			}else{
				$result = false;
				$message .= "Error moving uploaded file";
			}
		}
	}else{
		$result = false;
		$message .= "No file to upload";
	}
}else if($request == "process"){
	$file_id = (isset($_REQUEST['file_id']) && !empty($_REQUEST['file_id'])) ? $_REQUEST['file_id'] : 0;
	$template_id = (isset($_REQUEST['template_id']) && !empty($_REQUEST['template_id'])) ? $_REQUEST['template_id'] : 0;

	if ($file_id == 0){
		$result = false;
		$message .= "Invalid file";
	}

	if ($template_id == 0){
		$result = false;
		$message .= "Invalid template";
	}

	if ($result){
		$sql_file_dir = mysqli_query ($con_main, "SELECT MU.FILE_DIR FROM mobi_upload AS MU WHERE MU.ID = $file_id");
		$res_file_dir = mysqli_fetch_assoc ($sql_file_dir);
		
		$file_dir = $res_file_dir['FILE_DIR'];

		$query_template_dir = "SELECT
		MT.ID AS TEMPLATE_ID,
		MT.TEMPLATE_FILE_DIR,
		MSP.SP_ID,
		MSP.SHORT_NAME,
		MSP.FULL_NAME,
		MSP.CORPORATE_CODE,
		MSP.NAME_IN_BILL,
		MSP.VAT_REG
		FROM
		mobi_template AS MT
		INNER JOIN mobi_service_provider AS MSP ON MT.SERVICE_PROVIDER = MSP.SP_ID
		WHERE
		MT.ID = '$template_id'
		ORDER BY
		MSP.SHORT_NAME ASC";

		$sql_template_dir = mysqli_query ($con_main, $query_template_dir);
		$res_template_dir = mysqli_fetch_assoc ($sql_template_dir);
		
		$template_dir = $res_template_dir['TEMPLATE_FILE_DIR'];
		$template_name_in_bill = $res_template_dir['NAME_IN_BILL'];
		$template_vat_reg_no = $res_template_dir['VAT_REG'];

		if (!file_exists($file_dir)){
			$result = false;
			$message .= "Uploaded file not found";
		}

		if (!file_exists($template_dir)){
			$result = false;
			$message .= "Template file not found";
		}

		if ($result){
			include ($template_dir);

			$update_query = mysqli_query($con_main, "UPDATE `mobi_upload` SET `PROCESSED_FLAG`='1', `PROCESSED_TEMPLATE`='$template_id', `PROCESSED_DATE`=NOW(), `PROCESSED_BY`='$user' WHERE (`ID`='$file_id')");
		}
	}
}else if($request == "delete"){
	$file_id = (isset($_REQUEST['file_id']) && !empty($_REQUEST['file_id'])) ? $_REQUEST['file_id'] : 0;

	if ($file_id == 0){
		$result = false;
		$message .= "Invalid file";
	}

	if ($result){
		$sql_file_dir = mysqli_query ($con_main, "SELECT MU.FILE_DIR FROM mobi_upload AS MU WHERE MU.ID = $file_id");
		$res_file_dir = mysqli_fetch_assoc ($sql_file_dir);
		
		$file_dir = $res_file_dir['FILE_DIR'];

		if (!file_exists($file_dir)){
			$message .= "File already deleted";
		}else{
			unlink($file_dir);
		}

		$sql_file_delete = mysqli_query ($con_main, "DELETE FROM `mobi_upload` WHERE (`ID`='$file_id')");

		if (!$sql_file_delete){
			$result = false;
			$message .= "File record delete failed";
		}
	}
}

$responce['result'] = $result;
$responce['message'] = $message;

$json = json_encode($responce);

echo ($json);

mysqli_close($con_main);

exit;
?>