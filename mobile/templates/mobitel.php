<?php
// Parse pdf file and build necessary objects.
$parser = new \Smalot\PdfParser\Parser();
$pdf    = $parser->parseFile($file_dir);

// Retrieve all pages from the pdf file.
$pages  = $pdf->getPages();

$i = 1;
$c = 0;
$x = 0;
$record_id = 0;

$last_dtl_mob_no = "";
$last_dtl_heading = "";
$last_dtl_date = "";

// Loop over each page to extract text.
foreach ($pages as $page) {
    $page_text = "";

    $page_text = $page->getText();
    $page_proc_array = array_filter(preg_split ('/$\R?^/m', $page_text));

    $acc_summ_needle = "ACCOUNT SUMMARY";
    $acc_summ_found = strpos($page_text,$acc_summ_needle);

    if ($acc_summ_found === false){
        // Not the account summary page - Most likely the internal page of the bill. May be a detail page
        // Then keyword search for summary of specific mobile no

        $regex_pattern_summary_mobile_no = "/(?=.*summary).*(for|of).*mobile.*(no|number)/ix";
        
        if (preg_match($regex_pattern_summary_mobile_no, $page_text)) {
            $mobile_no = preg_replace("/[^0-9]/", '', trim($page_proc_array[2]));
            $mobile_acc_no = preg_replace("/[^0-9]/", '', trim($page_proc_array[3]));
            $total_charge = 0;
            $total_due = 0;
            $total_tax = 0;
            $rental = 0;

            $mob_summ_lines_array = "";
            $mob_summ_lines_index = 0;
            $mob_summ_lines_start_index = -1;
            $mob_summ_lines_start_value = "";
            $head_line_counter = 0;
            
            foreach ($page_proc_array as $k=>$v) {
                if (preg_match("/_{20,}/x", $v)) {
                    $mob_summ_lines_start_index = $k;
                    $mob_summ_lines_start_value = trim($v);
                    continue;
                }

                if (preg_match("/_{5,}/x", $v)) continue;

                if (empty(trim($v))) continue;

                if ($mob_summ_lines_start_index >= 0 && !empty($mob_summ_lines_start_value)){
                    $head_line_counter++;

                    if ($head_line_counter <= 3) continue;

                    $mob_summ_line = preg_replace("/[.]{5,}/x", '', trim($v));

                    $mob_summ_portion_array = preg_split("/\s{3,}/", $mob_summ_line);

                    if (count($mob_summ_portion_array) < 2) continue;

                    if (preg_match('/total.*(charges|amount).*(for|of).*mobile.*\d/ix', $mob_summ_portion_array[0])) {
                        $total_charge = (double)filter_var(trim($mob_summ_portion_array[1]), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    }

                    if (preg_match('/(total|total.*due)$/ix', $mob_summ_portion_array[0])) {
                        $total_due = (double)filter_var(trim($mob_summ_portion_array[1]), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    }

                    if (preg_match('/(vpn)/ix', $mob_summ_portion_array[0])) {
                        $rental = (double)filter_var(trim($mob_summ_portion_array[1]), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    }

                    $mob_summ_sub_array = $mob_summ_portion_array;
                }else{
                    continue;
                }

                $mob_summ_lines_array[$mob_summ_lines_index] = $mob_summ_sub_array;
                
                $mob_summ_lines_index++;
            }

            $total_tax = $total_due - $total_charge;

            $detail_summ_query = "INSERT INTO `mobitel_detail_summary` (
                `RECORD_ID`,
                `MOBILE_NO`,
                `MONTHLY_RENTAL`,
                `TOTAL_USAGE_CHARGES`,
                `TOTAL_TAX`,
                `TOTAL_DUE`
            )
            VALUES
            (
                '$record_id',
                '$mobile_no',
                '$rental',
                '$total_charge',
                '$total_tax',
                '$total_due'
            )";

            $detail_summ_sql = mysqli_query ($con_main, $detail_summ_query);
        }else if (preg_match('/(?=.*detail).*(for|of).*mobile.*\d{9,15}/ix',$page_text)){

            // Mobile detail page processing
            $detail_mobile_no = "";
            $detail_mobile_acc_no = "";

            preg_match('/\d.*71.*\d{7}/ix', trim($page_proc_array[3]), $detail_mobile_no);
            preg_match_all('/\d+/ix', trim($page_proc_array[3]), $detail_mobile_acc_no);

            $detail_mobile_acc_no = $detail_mobile_acc_no[0][1];

            $last_dtl_mob_no = $detail_mobile_no[0];

            $mob_dtl_lines_array = "";
            $mob_dtl_lines_index = 0;
            $mob_dtl_lines_start_index = -1;
            $mob_dtl_lines_start_value = "";
            $head_line_counter = 0;
            
            foreach ($page_proc_array as $k=>$v) {
                if (preg_match("/_{20,}/x", $v)) {
                    $mob_dtl_lines_start_index = $k;
                    $mob_dtl_lines_start_value = trim($v);
                    continue;
                }

                if (preg_match("/_{5,}/x", $v)) continue;

                if (empty(trim($v))) continue;

                if ($mob_dtl_lines_start_index >= 0 && !empty($mob_dtl_lines_start_value)){
                    $head_line_counter++;

                    if ($head_line_counter <= 2) continue;

                    if (preg_match('/(?=.*charge).*(for|of)/ix',trim($v))) continue;

                    if (preg_match('/total/ix', trim($v))) continue;

                    $mob_dtl_portion_array = preg_split("/\s{3,}/", $v);

                    $mob_dtl_sub_array['mobile_no'] = $last_dtl_mob_no;
                    $mob_dtl_sub_array['category'] = $last_dtl_heading;

                    if (!empty(trim($mob_dtl_portion_array[0]))){
                        $new_dtl_date_arr = explode(' ',trim($mob_dtl_portion_array[0]));
                        $last_dtl_date = date('Y-m-d',strtotime($new_dtl_date_arr[1]."-".$new_dtl_date_arr[0]."-".$new_dtl_date_arr[2]));
                    }

                    $new_dtl_time = (!empty($mob_dtl_portion_array[1])) ? $mob_dtl_portion_array[1] : '00:00:00';

                    if (preg_match('/\d+[:]\d+[:]\d+/',trim($new_dtl_time)) === false) continue;

                    $mob_dtl_sub_array['count'] = count($mob_dtl_portion_array);
                    //$mob_dtl_sub_array['data'] = $mob_dtl_portion_array;

                    if (count($mob_dtl_portion_array) == 1) {
                        $last_dtl_heading = trim($v);
                        continue;
                    }else if (count($mob_dtl_portion_array) == 6) {
                        $mob_dtl_sub_array['date_time'] = $last_dtl_date." ".$new_dtl_time;
                        $mob_dtl_sub_array['phone_no'] = $mob_dtl_portion_array[2];
                        $mob_dtl_sub_array['peak_offpeak'] = $mob_dtl_portion_array[3];
                        $mob_dtl_sub_array['units'] = (double)filter_var(trim($mob_dtl_portion_array[4]), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                        $mob_dtl_sub_array['charge'] = (double)filter_var(trim($mob_dtl_portion_array[5]), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    }else if (count($mob_dtl_portion_array) == 5) {
                        $mob_dtl_sub_array['date_time'] = $last_dtl_date." ".$new_dtl_time;
                        $mob_dtl_sub_array['phone_no'] = $mob_dtl_portion_array[2];
                        $mob_dtl_sub_array['peak_offpeak'] = "";
                        $mob_dtl_sub_array['units'] = (double)filter_var(trim($mob_dtl_portion_array[3]), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                        $mob_dtl_sub_array['charge'] = (double)filter_var(trim($mob_dtl_portion_array[4]), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    }else if (count($mob_dtl_portion_array) == 4){
                        $mob_dtl_sub_array['date_time'] = $last_dtl_date." ".$new_dtl_time;
                        $mob_dtl_sub_array['phone_no'] = "";
                        $mob_dtl_sub_array['peak_offpeak'] = "";
                        $mob_dtl_sub_array['units'] = (double)filter_var(trim($mob_dtl_portion_array[2]), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                        $mob_dtl_sub_array['charge'] = (double)filter_var(trim($mob_dtl_portion_array[3]), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    }else if (count($mob_dtl_portion_array) == 3){
                        $mob_dtl_sub_array['date_time'] = $last_dtl_date." ".$new_dtl_time;
                        $mob_dtl_sub_array['phone_no'] = "";
                        $mob_dtl_sub_array['peak_offpeak'] = "";
                        $mob_dtl_sub_array['units'] = (preg_match('/\d+/',$mob_dtl_portion_array[2])) ? (double)filter_var(trim($mob_dtl_portion_array[2]), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION) : 0;
                        $mob_dtl_sub_array['charge'] = 0;
                    }else{
                        continue;
                    }
                }else{
                    continue;
                }

                $mob_dtl_lines_array[$mob_dtl_lines_index] = $mob_dtl_sub_array;
                
                $mob_dtl_lines_index++;

                $detail_query = "INSERT INTO `mobitel_detail` (
                    `RECORD_ID`,
                    `FILE_ID`,
                    `TEMPLATE_ID`,
                    `BILL_NO`,
                    `MOBILE_NO`,
                    `ACCOUNT_NO`,
                    `CATEGORY`,
                    `DATE_TIME`,
                    `PHONE_NO`,
                    `PEAK_OFFPEAK`,
                    `UNITS`,
                    `CHARGE`
                )
                VALUES
                (
                    '$record_id',
                    '$file_id',
                    '$template_id',
                    '$bill_no',
                    '$last_dtl_mob_no',
                    '$detail_mobile_acc_no',
                    '$last_dtl_heading',
                    '".$mob_dtl_sub_array['date_time']."',
                    '".$mob_dtl_sub_array['phone_no']."',
                    '".$mob_dtl_sub_array['peak_offpeak']."',
                    '".$mob_dtl_sub_array['units']."',
                    '".$mob_dtl_sub_array['charge']."'
                )";

                $detail_sql = mysqli_query($con_main, $detail_query);
            }
        }

    }else{
        // Account summary page - Most likely the first page of the bill.

        $pay_summ_title_array_bill = array_filter(preg_split("/[\t]/", $page_proc_array[1]));
        $pay_summ_title_array_fixed = array ('Total Due','Total This Bill','Total Balance','Total Received','Total Last Bill');
        $pay_summ_value_array = array_filter(preg_split("/[\t]/", $page_proc_array[3]));
        $bill_no = trim($page_proc_array[11]);
        $bill_date = preg_split("/\s+/", trim($page_proc_array[8]));
        $bill_date = $bill_date[2];
        $bill_date_split_array = explode('/',$bill_date);
        $bill_date = $bill_date_split_array[2]."-".$bill_date_split_array[1]."-".$bill_date_split_array[0];
        $client_address_line_1 = trim($page_proc_array[12]);
        $client_address_line_2 = trim($page_proc_array[13]);
        $client_address_line_3 = trim($page_proc_array[14]);
        $client_address_line_4 = trim($page_proc_array[16]);
        $account_no = trim($page_proc_array[17]);
        $total_payable = (double)filter_var(trim($page_proc_array[18]), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $due_date = trim($page_proc_array[19]);
        $due_date_split_array = explode('/',$due_date);
        $due_date = $due_date_split_array[2]."-".$due_date_split_array[1]."-".$due_date_split_array[0];
        $sp_vat_reg_no = preg_replace("/[^0-9]/", '', trim($page_proc_array[2]));
        $client_vat_reg_no = preg_replace("/[^0-9]/", '', trim($page_proc_array[22]));

        $bill_summ_lines_array = "";
        $bill_summ_lines_index = 0;
        $bill_summ_lines_start_index = -1;
        $bill_summ_lines_start_value = "";
        
        foreach ($page_proc_array as $k=>$v) {
            if (preg_match("/\bMonthly Bill Summary\b/i", $v)) {
                $bill_summ_lines_start_index = $k;
                $bill_summ_lines_start_value = trim($v);
                continue;
            }

            if (preg_match("/[=]{2,}/", $v)) {
                break;
            }

            if (empty(trim($v))) continue;

            if ($bill_summ_lines_start_index >= 0 && !empty($bill_summ_lines_start_value)){
                $bill_summ_portion_array = preg_split("/[.]{2,}/", trim($v));
                $bill_summ_sub_array = "";

                if (count($bill_summ_portion_array) < 2) continue;

                foreach ($bill_summ_portion_array as $i_portion=>$v_portion){
                    if ($i_portion == 1){
                        $v_portion_val = (double)filter_var(trim($v_portion), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                        
                        if (stripos($v_portion,'cr') === false){
                            $bill_summ_sub_array[$i_portion] = round($v_portion_val,2);
                        }else{
                            $bill_summ_sub_array[$i_portion] = round(-1*$v_portion_val,2);
                        }
                    }else{
                        $bill_summ_sub_array[$i_portion] = trim($v_portion);
                    }
                }

                $bill_summ_lines_array[$bill_summ_lines_index] = $bill_summ_sub_array;

                $bill_summ_lines_index++;
            }
        }

        $bill_total = (double)filter_var(trim($pay_summ_value_array[1]), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $balance_total = (double)filter_var(trim($pay_summ_value_array[2]), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $received_total = (double)filter_var(trim($pay_summ_value_array[3]), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $last_bill_total = (double)filter_var(trim($pay_summ_value_array[4]), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

        $sql_header_insert = mysqli_query ($con_main, "INSERT INTO `mobitel_header` (`FILE_ID`, `TEMPLATE_ID`, `BILL_NO`, `BILL_TOTAL`, `TOTAL_BALANCE`, `TOTAL_RECEIVED`, `LAST_BILL_TOTAL`, `BILL_DATE`, `DUE_DATE`) VALUES ('$file_id', '$template_id', '$bill_no', '$bill_total', '$balance_total', '$received_total', '$last_bill_total', '$bill_date', '$due_date')");
        $record_id = mysqli_insert_id ($con_main);
    }
}

$responce['pages_parsed'] = $i;
$responce['summary_pages'] = $c;
$responce['summary_pages_written'] = $x;
?>