<?php
// Parse pdf file and build necessary objects.
$parser = new \Smalot\PdfParser\Parser();
$pdf    = $parser->parseFile($file_dir);

// Retrieve all pages from the pdf file.
$pages  = $pdf->getPages();

$i = 1;
$c = 0;
$x = 0;
$record_id = 0;

$last_dtl_mob_no = "";
$last_dtl_heading = "";
$last_dtl_date = "";

// Loop over each page to extract text.
foreach ($pages as $page) {
    $page_array = $page->getTextArray();
    
    if ($i == 1){
        //First page of the bill
        $invoice_no_key = array_search('Invoice No:  ',$page_array);
        $invoice_no_key = $invoice_no_key+1;
        $invoice_no = $page_array[$invoice_no_key];

        $previous_month = trim($page_array[13]);
        $previous_month_explode = explode('/', $previous_month);
        $previous_month = $previous_month_explode[2]."-".$previous_month_explode[1]."-".$previous_month_explode[0];

        $invoice_date = trim($page_array[15]);
        $invoice_date_explode = explode('/', $invoice_date);
        $invoice_date = $invoice_date_explode[2]."-".$invoice_date_explode[1]."-".$invoice_date_explode[0];
                
        $corporate_code = $page_array[31];

        if (strlen($corporate_code) < 10){
            $corporate_code = str_pad ($corporate_code, 10, "0", STR_PAD_LEFT);
        }

        $com_vat_reg_no = trim($page_array[478]);

        $sp_vat_reg_no_array = explode(':', trim($page_array[10]));
        $sp_vat_reg_no = trim($sp_vat_reg_no_array[1]);

        $sp_svat_reg_no_array = explode(':', trim($page_array[11]));
        $sp_svat_reg_no = trim($sp_svat_reg_no_array[1]);

        $total_services = (double)trim($page_array[122]);
        $total_roaming = (double)trim($page_array[123]);
        $total_usage = (double)trim($page_array[124]);
        $total_misc = (double)trim($page_array[125]);
        $total_contract = (double)trim($page_array[126]);
        $total_surcharges = (double)trim($page_array[127]);
        $total_taxes = (double)trim($page_array[128]);
        $total_vat = (double)trim($page_array[476]);
        $total_brought_forward = (double)trim($page_array[130]);
        $total_payments = (double)trim($page_array[131]);
        $total_other = (double)trim($page_array[129]);
        $total_arrears = (double)trim($page_array[26]);
        $total_amount = (double)trim($page_array[29]);
        $total_due = (double)trim($page_array[30]);

        $sql_header_insert = mysqli_query ($con_main, "INSERT INTO `dialog_header` (`FILE_ID`, `TEMPLATE_ID`, `INVOICE_NO`, `BILL_DATE_FROM`, `BILL_DATE_TO`, `CORPORATE_CODE`) VALUES ('$file_id', '$template_id', '$invoice_no', '$previous_month', '$invoice_date', '$corporate_code')");
        $record_id = mysqli_insert_id ($con_main);
    }else{

        if (trim($page_array[0]) == "Mobile No"){
            $c++;

            $spliced_arr = array_splice ($page_array, 25);
            $chunked_arr = array_chunk($spliced_arr, 14);

            foreach ($chunked_arr as $value) {
                $x++;
                
                if (count($value) >= 14){
                    $mobile_no = trim($value[0]);

                    if(strlen($mobile_no) < 10){
                        $mobile_no = str_pad ($mobile_no, 10, "0", STR_PAD_LEFT);
                    }

                    $total_tax = $value[6]+$value[13];

                    $query_detail_summary = "INSERT INTO `dialog_detail_summary` (
                        `RECORD_ID`,
                        `MOBILE_NO`,
                        `BROUGHT_FORWARD`,
                        `PAYMENTS`,
                        `MONTHLY_SERVICES`,
                        `INTERNATIONAL_ROAMING`,
                        `TOTAL_USAGE_CHARGES`,
                        `MISC_CHARGES`,
                        `CONTRACT_CHARGES`,
                        `SURCHARGES`,
                        `TAXES_LEVIES`,
                        `TOTAL_VAT`,
                        `TOTAL_TAX`,
                        `OTHER_SERVICES`,
                        `TOTAL_BILL_AMOUNT`,
                        `TOTAL_DUE`
                    )
                    VALUES
                    (
                        '$record_id',
                        '".$mobile_no."',
                        '".$value[8]."',
                        '".$value[9]."',
                        '".$value[1]."',
                        '".$value[2]."',
                        '".$value[3]."',
                        '".$value[4]."',
                        '".$value[5]."',
                        '".$value[12]."',
                        '".$value[6]."',
                        '".$value[13]."',
                        '".$total_tax."',
                        '".$value[7]."',
                        '".$value[10]."',
                        '".$value[11]."'
                    )";

                    $sql_detail_summary = mysqli_query ($con_main, $query_detail_summary);
                }
            }
        }else if(preg_match('/\b(.*monthly.*charges)|\b(.*charges.*monthly)/ix',trim($page_array[4])) && preg_match('/0?\d\d\d{7}/ix',trim($page_array[3]))){
            $found_rental = array_search('Monthly Rental',$page_array);
            $found_data_rental = array_search('GPRS Service Rental',$page_array);
            $phone_no = trim($page_array[3]);

            if(strlen($phone_no) < 10){
                $phone_no = str_pad ($phone_no, 10, "0", STR_PAD_LEFT);
            }

            $monthly_rental = 0;

            if ($found_rental !== false){
                for ($inc_till_found_rental = $found_rental;$inc_till_found_rental < count($page_array); $inc_till_found_rental++){
                    if (preg_match('/[-+]?\d+\.\d+/ix',$page_array[$inc_till_found_rental])){
                        $monthly_rental = $page_array[$inc_till_found_rental];
                        break;
                    }
                }
            }else if ($found_data_rental !== false){
                for ($inc_till_found_rental = $found_data_rental;$inc_till_found_rental < count($page_array); $inc_till_found_rental++){
                    if (preg_match('/[-+]?\d+\.\d+/ix',$page_array[$inc_till_found_rental])){
                        $monthly_rental = $page_array[$inc_till_found_rental];
                        break;
                    }
                }
            }

            $query_rental_update = "UPDATE `dialog_detail_summary`
            SET `MONTHLY_RENTAL` = '$monthly_rental'
            WHERE
            (`RECORD_ID` = '$record_id') AND 
            (`MOBILE_NO` = '$phone_no')";

            $sql_rental_update = mysqli_query ($con_main, $query_rental_update);
        }
    }

    $i++;
}

$responce['pages_parsed'] = $i;
$responce['summary_pages'] = $c;
$responce['summary_pages_written'] = $x;
?>