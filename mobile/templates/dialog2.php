<?php
// Parse pdf file and build necessary objects.
$parser = new \Smalot\PdfParser\Parser();
$pdf    = $parser->parseFile($file_dir);

// Retrieve all pages from the pdf file.
$pages  = $pdf->getPages();

$i = 1;
$c = 0;
$x = 0;
$record_id = 0;

$last_dtl_mob_no = "";
$last_dtl_heading = "";
$last_dtl_date = "";

// Loop over each page to extract text.
foreach ($pages as $page) {
    $page_array = $page->getTextArray();
    
    if ($i == 1){
        //First page of the bill
        $invoice_no_str = preg_grep('/.*(invoice.number).{0,}.\d*/mi',$page_array);
        $invoice_no_key = key($invoice_no_str);
        $invoice_no_exp = explode(':', trim($invoice_no_str[$invoice_no_key]));
        $invoice_no = trim($invoice_no_exp[1]);

        $invoice_dt_str = preg_grep('/.*(invoice.date).{0,}.\d*/mi',$page_array);
        $invoice_dt_key = key($invoice_dt_str);
        $invoice_dt_exp = explode(':', trim($invoice_dt_str[$invoice_dt_key]));
        $invoice_dt = trim($invoice_dt_exp[1]);
        $invoice_dt_format_exp = explode('/', $invoice_dt);
        $invoice_dt = $invoice_dt_format_exp[2]."-".$invoice_dt_format_exp[1]."-".$invoice_dt_format_exp[0];

        $corporate_code = "";
        $title_corp_code = preg_grep('/.*(corporate.code).*/mi',$page_array);
        $key_of_corp_code = key($title_corp_code);

        if (!empty($key_of_corp_code) && $key_of_corp_code != NULL){
            $key_of_corp_code = (int)$key_of_corp_code + 1;
            $corporate_code = $page_array[$key_of_corp_code];
        }

        if (strlen($corporate_code) < 10){
            $corporate_code = str_pad ($corporate_code, 10, "0", STR_PAD_LEFT);
        }

        $from_dt = "";
        $to_dt = "";
        $str_bill_period = "";
        $title_bill_period = preg_grep('/.*(bill.period).*/mi',$page_array);
        $key_of_bill_period = key($title_bill_period);

        if (!empty($key_of_bill_period) && $key_of_bill_period != NULL){
            $key_of_bill_period = (int)$key_of_bill_period + 1;
            $str_bill_period = $page_array[$key_of_bill_period];
        }

        $bill_period_str_exp = explode('-', trim($str_bill_period));
        $from_dt = trim($bill_period_str_exp[0]);
        $to_dt = trim($bill_period_str_exp[1]);

        $from_dt_format_exp = explode('/', $from_dt);
        $from_dt = $from_dt_format_exp[2]."-".$from_dt_format_exp[1]."-".$from_dt_format_exp[0];

        $to_dt_format_exp = explode('/', $to_dt);
        $to_dt = $to_dt_format_exp[2]."-".$to_dt_format_exp[1]."-".$to_dt_format_exp[0];

        $sql_header_insert = mysqli_query ($con_main, "INSERT INTO `dialog_header` (`FILE_ID`, `TEMPLATE_ID`, `INVOICE_NO`, `BILL_DATE_FROM`, `BILL_DATE_TO`, `CORPORATE_CODE`) VALUES ('$file_id', '$template_id', '$invoice_no', '$from_dt', '$to_dt', '$corporate_code')");
        $record_id = mysqli_insert_id ($con_main);
    }else{

        if (preg_match('/.*(summary).*/mi',trim($page_array[0]))){
            $c++;

            $spliced_arr = array_splice ($page_array, 29);
			$chunked_arr = array_chunk ($spliced_arr, 16);

            foreach ($chunked_arr as $value) {
                $x++;
                
                if (count($value) >= 16){
                    $mobile_no = trim($value[0]);

                    if(strlen($mobile_no) < 10){
                        $mobile_no = str_pad ($mobile_no, 10, "0", STR_PAD_LEFT);
                    }

                    $previous_due_amount = filter_var($value[1], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    $payments = filter_var($value[2], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    $total_usage_charges = filter_var($value[3], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    $idd = filter_var($value[4], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    $roaming = filter_var($value[5], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    $vas = filter_var($value[6], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    $bill_adj = filter_var($value[7], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    $balance_adj = filter_var($value[8], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    $commitment_charges = filter_var($value[9], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    $late_payment_charges = filter_var($value[10], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    $taxes_levies = filter_var($value[11], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    $vat = filter_var($value[12], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    $add_to_bill = filter_var($value[13], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    $total_bill_amount = filter_var($value[14], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                    $total_due = filter_var($value[15], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

                    $total_tax = (float)$taxes_levies + (float)$vat;

                    $query_detail_summary = "INSERT INTO `dialog_detail_summary` (
                        `RECORD_ID`,
                        `MOBILE_NO`,
                        `BROUGHT_FORWARD`,
                        `PAYMENTS`,
                        `MONTHLY_SERVICES`,
                        `INTERNATIONAL_ROAMING`,
                        `TOTAL_USAGE_CHARGES`,
                        `MISC_CHARGES`,
                        `CONTRACT_CHARGES`,
                        `SURCHARGES`,
                        `TAXES_LEVIES`,
                        `TOTAL_VAT`,
                        `TOTAL_TAX`,
                        `OTHER_SERVICES`,
                        `TOTAL_BILL_AMOUNT`,
                        `TOTAL_DUE`
                    )
                    VALUES
                    (
                        '$record_id',
                        '".$mobile_no."',
                        '".$previous_due_amount."',
                        '".$payments."',
                        '',
                        '".$roaming."',
                        '".$total_usage_charges."',
                        '',
                        '".$commitment_charges."',
                        '".$add_to_bill."',
                        '".$taxes_levies."',
                        '".$vat."',
                        '".$total_tax."',
                        '".$vas."',
                        '".$total_bill_amount."',
                        '".$total_due."'
                    )";

                    $sql_detail_summary = mysqli_query ($con_main, $query_detail_summary);
                }
            }
        }else if(array_search('Charges for Bill Period',$page_array) !== false){
            $prev_to_mobile_no = preg_grep('/.*(mobile.number).*/mi',$page_array);
            $key_of_prev_elem = key($prev_to_mobile_no);

            if (!empty($key_of_prev_elem) && $key_of_prev_elem != NULL){
                $key_of_mob_num = (int)$key_of_prev_elem + 1;
                $last_dtl_mob_no = trim($page_array[$key_of_mob_num]);

                if(strlen($last_dtl_mob_no) < 10){
                    $last_dtl_mob_no = str_pad ($last_dtl_mob_no, 10, "0", STR_PAD_LEFT);
                }
            }
        }else if(array_search('Your Mobile Bill Breakdown',$page_array) !== false){
            $found_rental = array_search('Monthly Rental',$page_array);
            $found_data_rental = array_search('GPRS Service Rental',$page_array);
            $phone_no = $last_dtl_mob_no;
            $monthly_rental = 0;

            if ($found_rental !== false){
                for ($inc_till_found_rental = $found_rental;$inc_till_found_rental < count($page_array); $inc_till_found_rental++){
                    if (preg_match('/[-+]?\d+\.\d+/ix',$page_array[$inc_till_found_rental])){
                        $monthly_rental = $page_array[$inc_till_found_rental];
                        break;
                    }
                }
            }else if ($found_data_rental !== false){
                for ($inc_till_found_rental = $found_data_rental;$inc_till_found_rental < count($page_array); $inc_till_found_rental++){
                    if (preg_match('/[-+]?\d+\.\d+/ix',$page_array[$inc_till_found_rental])){
                        $monthly_rental = $page_array[$inc_till_found_rental];
                        break;
                    }
                }
            }

            $query_rental_update = "UPDATE `dialog_detail_summary`
            SET `MONTHLY_RENTAL` = '$monthly_rental'
            WHERE
            (`RECORD_ID` = '$record_id') AND 
            (`MOBILE_NO` = '$phone_no')";

            $sql_rental_update = mysqli_query ($con_main, $query_rental_update);
        }

    }

    $i++;
}

$responce['pages_parsed'] = $i;
$responce['summary_pages'] = $c;
$responce['summary_pages_written'] = $x;
?>