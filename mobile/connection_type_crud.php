<?php
	header('Content-Type: application/json');
	
	session_start();
	
	require_once ('../config.php');
	
	$op = $_REQUEST['operation'];
	$id = $_REQUEST['id'];
	$con_type = $_REQUEST['con_type'];
	$status = $_REQUEST['status'];

	$user = $_SESSION['USER_CODE'];
	
	$query = "";
	$result = true;
	$message = "";
	$debug = "";
	$responce = array();
	
	if ($op == "insert"){
		$query = "INSERT INTO `mobi_con_type` (`CON_TYPE`, `STATUS`, `ADDED_DATE`, `ADDED_BY`) VALUES ('$con_type', '$status', NOW(), '$user')";
	}else if ($op == "update"){
		$query = "UPDATE `mobi_con_type`
		SET `CON_TYPE` = '$con_type',
		`STATUS` = '$status'
		WHERE
		(`CON_TYPE_ID` = '$id')";
	}
	
	if (!empty($query)){
		$sql = mysqli_query ($con_main, $query);

		if (!$sql){
			$result = false;
			$message .= "<br>Operation failed.";
			$debug .= "\nError SQL: (".mysqli_errno($con_main).") ".mysqli_error($con_main);
		}else{
			$id = ($op == "insert") ? mysqli_insert_id($con_main) : $id;
		}
	}else{
		$result = false;
		$message .= "<br>Failed! Undefined operation.";
		$debug .= "\nUndefined operation requested. Op: ".$op;
	}

	mysqli_close($con_main);
	
	$responce['result'] = $result;
	$responce['message'] = $message;
	$responce['debug'] = $debug;
	$responce['id'] = $id;
	
	echo (json_encode($responce));
?>