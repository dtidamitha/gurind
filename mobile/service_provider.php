<?php
	session_start();
	
	if (empty($_SESSION['ACCESS_CODE']) || $_SESSION['ACCESS_CODE'] == NULL){
		header ('Location: login.php');
		exit;
	}
	
	$folder_depth = "";
	$prefix = "";
	
	$folder_depth = substr_count($_SERVER["PHP_SELF"] , "/");
	$folder_depth = ($folder_depth == false) ? 2 : (int)$folder_depth;
	
    $prefix = str_repeat("../", $folder_depth - 2);
    
    $title_suffix = " Service Provider Master";
?>
<?php include $prefix.'config.php'; ?>
<?php include $prefix.'menu.php'; ?>
<?php include $prefix.'template_start.php'; ?>
<?php include $prefix.'page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-wifi_alt"></i>Service Provider Master<br><small>Create, Update or Remove Service Providers</small>
            </h1>
        </div>
    </div>

    <ul class="breadcrumb breadcrumb-top">
        <li><a href="../home.php">Home</a></li>
        <li>Service Providers</li>
    </ul>
    <!-- END Header -->
	
    <div class="row">
        <div class="col-md-12">
            <!-- Form Elements Block -->
            <div class="block">
                <!-- Form Elements Title -->
                <div class="block-title">
				    <h2>Service Provider</h2>
				</div>
                <!-- END Form Elements Title -->

                <!-- Form Elements Content -->
                <form id="form-main" name="form-main" action="service_provider_crud.php" method="post" class="form-horizontal form-bordered">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2 col-md-offset-1" align="center">
                                <div class="row">
                                    <div class="col-md-12">
                                        <img id="photo" width="150" height="150" src="sp_pix/logo_default.jpg" style="background:#EAEAEA;border:#C4C4C4 1px dashed;">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" style="padding-top:5px;">
                                        <a id="toggle-uploader" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="bottom" title="Click to upload service provider logo" style="min-width:150px;">
                                            <i class="fa fa-upload"></i> Upload Logo
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-9">
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="full_name">Full Name</label>
                                    <div class="col-md-4">
                                        <input type="text" id="full_name" name="full_name" class="form-control"  placeholder="Enter Full Name" size="1">
                                    </div>
                                
                                    <label class="col-md-2 control-label" for="short_name">Short Name</label>
                                    <div class="col-md-4">
                                        <input type="text" id="short_name" name="short_name" class="form-control"  placeholder="Enter Short Name" size="1">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="name_in_bill">Name in Bill</label>
                                    <div class="col-md-4">
                                        <input type="text" id="name_in_bill" name="name_in_bill" class="form-control"  placeholder="Enter name as appear in bill" size="1">
                                    </div>
                                
                                    <label class="col-md-2 control-label" for="coporate_code">Corporate Code</label>
                                    <div class="col-md-4">
                                        <input type="text" id="coporate_code" name="coporate_code" class="form-control"  placeholder="Enter Corporate Code" size="1">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="vat_reg">VAT Reg No</label>
                                    <div class="col-md-4">
                                        <input type="text" id="vat_reg" name="vat_reg" class="form-control"  placeholder="Enter VAT Registration No" size="1">
                                    </div>
                                
                                    <label class="col-md-2 control-label" for="svat_reg">SVAT Reg No</label>
                                    <div class="col-md-4">
                                        <input type="text" id="svat_reg" name="svat_reg" class="form-control"  placeholder="Enter SVAT Registration No" size="1">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
					<div class="form-group">
                        <label class="col-md-1 control-label" for="billing_address">Billing Address</label>
                        <div class="col-md-3">
                            <textarea id="billing_address" name="billing_address" rows="3" class="form-control" placeholder="Address"></textarea>
                        </div>

                        <label class="col-md-1 control-label" for="hotmail">Hotmail</label>
                        <div class="col-md-3">
                            <input type="text" id="hotmail" name="hotmail" class="form-control"  placeholder="Enter Hotmail Number" size="1">
                        </div>
                    
                        <label class="col-md-1 control-label" for="status">Status</label>
                        <div class="col-md-3">
                            <select id="status" name="status" class="select-chosen" data-placeholder="Choose Status">
                                <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                <option value="1" selected>Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>
					
					<div class="form-group form-actions">
                        <input type="hidden" name="id" id="id" value="0" />
					
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success primary-btn pull-right"><i class="fa fa-angle-right"></i> Submit</button>
                            <button type="button" id="btn-reset" class="btn btn-warning"><i class="fa fa-repeat"></i> New</button>
                        </div>
                    </div>
                </form>
                <!-- END Content -->  
		    </div>
            <!-- END Form Elements Block -->

            <div id="modal-upload" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title">Upload Logo</h3>
                        </div>
                        <div class="modal-body">
                            <!-- Upload Content -->
                            <form action="service_provider_crud.php" class="dropzone" id="uploader">
                                <input type="hidden" name="operation" id="operation" value="upload-image" />
                                <input type="hidden" name="obj_id" id="obj_id" value="0" />
                                <div class="fallback">
                                    <input name="file" type="file" multiple />
                                </div>
                            </form>
                            <!-- END Upload Content -->
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btn-upload-done" class="btn btn-sm btn-primary" data-dismiss="modal">Done</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Table Block -->
    <div class="block full">
        <!-- Table Title -->
        <div class="block-title">
            <h2>Services Providers </h2><small>Service providers currently exist in the system</small>
        </div>
        <!-- END Table Title -->

        <!-- Table Content -->
        <div class="table-responsive"><table id="table-data" class="table table-condensed table-striped table-hover"></table></div>
        <!-- END Table Content -->
    </div>
    <!-- END Table Block -->
</div>
<!-- END Page Content -->

<?php include $prefix.'page_footer.php'; ?>
<?php include $prefix.'template_scripts.php'; ?>
<?php include $prefix.'template_end.php'; ?>

<script src="<?php echo ($prefix); ?>js/lib/jquery.maskedinput.js"></script>
<script src="<?php echo ($prefix); ?>js/lib/jquery.validate.js"></script>
<script src="<?php echo ($prefix); ?>js/lib/jquery.form.js"></script>
<script src="<?php echo ($prefix); ?>js/lib/j-forms.js"></script>
	
<!--Bootbox-->
<script src="<?php echo ($prefix); ?>js/lib/bootbox.js"></script>
	
<script type="text/javascript">
	/*********** Data-table Initialize ***********/	 	
	App.datatables();

    var dt = $('#table-data').DataTable({
        "processing": true,
        "serverSide": true,
        "select": true,
        "columns": [
			{ "data": "short_name", "name": "short_name", "title": "Short Name" },
            { "data": "corporate_code", "name": "corporate_code", "title": "Corporate Code" },
			{ "data": "hotmail", "name": "hotmail", "title": "Hotmail" },
			{ "data": "status", "name": "status", "title": "Status", "searchable": false, "orderable": false, },
			
            /* ACTIONS */ 
            { "data": "actions", "name": "actions","title":"Actions", "searchable": false, "orderable": false, 
                mRender: function (data, type, row) {
                    return '<div class="btn-group btn-group-xs"><button id="btn-row-edit" class="btn btn-primary" title="Edit"><i class="fa fa-pencil"></i></button></div>'
                }
            }
        ],
       "columnDefs": [
            {"className": "dt-center", "targets": [1,2,3,4]}
        ],
        "language": {
            "emptyTable": "No service providers to show..."
        },
        "ajax": "data/grid_data_service_provider.php"
    });

	$('.dataTables_filter input').attr('placeholder', 'Search');

    $("#table-data tbody").on('click', '#btn-row-edit', function() {
        var str_id = $(this).closest('tr').attr('id');
        var arr_id = str_id.split("_");

        var row_id = arr_id[1];

        $.ajax({
            url: 'data/data_service_provider.php',
            data: {
                id: row_id
            },
            method: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $('#table-data tbody #'+str_id+' #btn-row-edit').button('loading');
                NProgress.start();
            },
            error: function (e) {
                $.bootstrapGrowl('<h4>Error!</h4> <p>Error retrieving service provider data</p>', {
                    type: 'danger',
                    delay: 2500,
                    allow_dismiss: true
                });

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            },
            success: function (r) {
                if (!r.result) {
                    $.bootstrapGrowl('<h4>Error!</h4> <p>'+r.message+'</p>', {
                        type: 'danger',
                        delay: 2500,
                        allow_dismiss: true
                    });
                }else{
                    $('#id').val(r.data[0].SP_ID);
                    $('#obj_id').val(r.data[0].SP_ID);
                    $('#full_name').val(r.data[0].FULL_NAME);
                    $('#short_name').val(r.data[0].SHORT_NAME);
                    $('#name_in_bill').val(r.data[0].NAME_IN_BILL);
                    $('#corporate_code').val(r.data[0].CORPORATE_CODE);
                    $('#vat_reg').val(r.data[0].VAT_REG);
                    $('#svat_reg').val(r.data[0].SVAT_REG);
                    $('#billing_address').val(r.data[0].BILLING_ADDRESS);
                    $('#hotmail').val(r.data[0].HOTMAIL);
                    $('#status').val(r.data[0].STATUS).trigger("chosen:updated");

                    if (r.data[0].LOGO != null){ 
                        $('#photo').prop('src',r.data[0].LOGO + '?' + new Date().getTime());
                    }else{
                        $('#photo').prop('src','sp_pix/logo_default.jpg?' + new Date().getTime());
                    }
                }

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            }
        });
    });

    $('#form-main').on('submit', function (e){
		e.preventDefault();
		
		var id = $('#id').val();
		var op = "insert";

        op = (id == 0) ? "insert" : "update";
		
		var formdata = $('#form-main').serializeArray();
		formdata.push({'name':'operation','value':op});
		
		$.ajax({
			url: 'service_provider_crud.php',
			data: formdata,
			success: function(r){
                $('#id').val(r.id);
                $('#obj_id').val(r.id);
				
                var msg_title = "Error";
                var msg_body = "Unknown result";
                var msg_type = "danger";

                if (r.result){
                    msg_title = "Success!";
                    msg_body = r.message;
                    msg_type = 'success';

                    //msg_body = (r.operation == "insert") ? "User create succeed." : "User update succeed.";
                }else{
                    msg_title = "Failed!";
                    msg_body = r.message;
                    msg_type = 'danger';

                    //msg_body = (r.operation == "insert") ? "User create failed." : "User update failed.";
                }

                $.bootstrapGrowl('<h4>' + msg_title + '</h4> <p>' + msg_body + '</p>', {
                    type: msg_type,
                    delay: 2500,
                    allow_dismiss: true
                });

                dt.ajax.reload();
                dt.draw();
			}
		});
	});

    $('#btn-reset').on('click', function (e){
        $('#form-main').trigger('reset');

        $("#status").val('1').trigger("chosen:updated");

        $('#id').val('0');
        $('#obj_id').val('0');

        $('#photo').prop('src','sp_pix/logo_default.jpg');
    });

    $('#toggle-uploader').on('click', function (){
        if ($('#obj_id').val() > 0){
            $('#modal-upload').modal('show');
        }else{
            $.bootstrapGrowl('<h4>Warning!</h4> <p>Please save or select a service provider before try to upload the logo</p>', {
                type: 'warning',
                delay: 2500,
                allow_dismiss: true
            });
        }
    });

    $('#modal-upload').on('shown.bs.modal', function (e) {
        Dropzone.forElement("#uploader").removeAllFiles(true);
    });

    $('#modal-upload').on('hide.bs.modal', function (e) {
        Dropzone.forElement("#uploader").removeAllFiles(true);
    });

    Dropzone.options.uploader = {
        method: 'post',
        uploadMultiple: false,
        resizeWidth: 200,
        resizeMimeType: 'image/jpeg',
        resizeMethod: 'resizeWidth',
        maxFiles: 1,
        acceptedFiles: 'image/*',
        dictDefaultMessage: 'Drop image here',
        success: function (r){
            $.bootstrapGrowl('<h4>Success!</h4> <p>Image uploaded successfully.</p>', {
                type: 'success',
                delay: 2500,
                allow_dismiss: true
            });

            var sp_id = $('#obj_id').val();

            $.ajax({
                url: 'data/data_service_provider.php',
                data: {
                    id: sp_id
                },
                method: 'POST',
                dataType: 'json',
                beforeSend: function () {
                    $('#btn-upload-done').button('loading');
                    NProgress.start();
                },
                error: function (e) {
                    $.bootstrapGrowl('<h4>Error!</h4> <p>Error retrieving uploaded logo</p>', {
                        type: 'danger',
                        delay: 2500,
                        allow_dismiss: true
                    });

                    $('#btn-upload-done').button('reset');
                    NProgress.done();
                },
                success: function (r) {
                    if (!r.result) {
                        $.bootstrapGrowl('<h4>Error!</h4> <p>'+r.message+'</p>', {
                            type: 'danger',
                            delay: 2500,
                            allow_dismiss: true
                        });
                    }else{
                        if (r.data[0].LOGO != null){ 
                            $('#photo').prop('src',r.data[0].LOGO + '?' + new Date().getTime());
                        }else{
                            $('#photo').prop('src','sp_pix/logo_default.jpg?' + new Date().getTime());
                        }
                    }

                    $('#btn-upload-done').button('reset');
                    NProgress.done();
                }
            });
        }
    };
</script>
	
<?php mysqli_close($con_main); ?>