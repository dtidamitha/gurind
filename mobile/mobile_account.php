<?php
	session_start();
	
	if (empty($_SESSION['ACCESS_CODE']) || $_SESSION['ACCESS_CODE'] == NULL){
		header ('Location: login.php');
		exit;
	}
	
	$folder_depth = "";
	$prefix = "";
	
	$folder_depth = substr_count($_SERVER["PHP_SELF"] , "/");
	$folder_depth = ($folder_depth == false) ? 2 : (int)$folder_depth;
	
    $prefix = str_repeat("../", $folder_depth - 2);
    
    $title_suffix = " Mobile Account Master";
?>
<?php include $prefix.'config.php'; ?>
<?php include $prefix.'menu.php'; ?>
<?php include $prefix.'template_start.php'; ?>
<?php include $prefix.'page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-address_book"></i>Mobile Account Master<br><small>Create, Update or Delete Mobile Accounts</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="../home.php">Home</a></li>
        <li>Mobile Accounts</li>
    </ul>
    <!-- END Blank Header -->
	
    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
					<h2>Mobile Account</h2>
				</div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <form id="form-main" name="form-main" action="mobile_account_crud.php" method="post"  class="form-horizontal form-bordered">
					<div class="form-group">
                        <label class="col-md-2 control-label" for="employee">Employee</label>
                        <div class="col-md-4">
                            <select id="employee" name="employee" style="width: 100%;">
                                <option value="0" selected disabled>Select Employee</option>
                            </select>
                        </div>

                        <label class="col-md-2 control-label" for="mobile_no">Mobile No</label>
                        <div class="col-md-4">
                            <input type="text" id="mobile_no" name="mobile_no" class="form-control" placeholder="0999999999">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="mobile_account_no">Mobile Account No</label>
                        <div class="col-md-4">
                            <input type="text" id="mobile_account_no" name="mobile_account_no" class="form-control" placeholder="Enter Mobile Account No">
                        </div>

                        <label class="col-md-2 control-label" for="con_date">Connection Date</label>
                        <div class="col-md-4">
                            <input type="text" id="con_date" name="con_date" class="form-control input-datepicker" value = "<?php echo (date('Y-m-d')); ?>" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="service_provider">Service Provider</label>
                        <div class="col-md-4">
                            <select id="service_provider" name="service_provider" class="select-chosen load-packages" data-placeholder="Choose Service Provider">
                                <option></option>
                                <?php
                                $query="SELECT
                                        MSP.SP_ID,
                                        MSP.SHORT_NAME
                                        FROM
                                        mobi_service_provider AS MSP
                                        WHERE
                                        MSP.`STATUS` = 1";
                                $sql = mysqli_query($con_main, $query);
                                                                        
                                while ($type = mysqli_fetch_array($sql)){
                                    echo ("<option value=\"".$type['SP_ID']."\">".$type['SHORT_NAME']."</option>");
                                }
                                ?>
                            </select>
                        </div>

                        <label class="col-md-2 control-label" for="con_type">Connection Type</label>
                        <div class="col-md-4">
                            <select id="con_type" name="con_type" class="select-chosen load-packages" data-placeholder="Choose Connection Type">
                                <option></option>
								<?php
								$query="SELECT
										MCT.CON_TYPE,
										MCT.CON_TYPE_ID
										FROM
										mobi_con_type AS MCT
										WHERE
										MCT.`STATUS` = 1";
								$sql = mysqli_query($con_main, $query);
																		
								while ($type = mysqli_fetch_array($sql)){
									echo ("<option value=\"".$type['CON_TYPE_ID']."\">".$type['CON_TYPE']."</option>");
								}
								?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="company_pac_type">Company Package</label>
                        <div class="col-md-4">
                            <Select id="company_pac_type" name="company_pac_type" class="select-chosen" data-placeholder="Select Company Package" size="1">
                                <Option value=""></Option>
                            </Select>
                        </div>

                        <label class="col-md-2 control-label" for="mobile_pac_type">Mobile Package</label>
                        <div class="col-md-4">
                            <Select id="mobile_pac_type" name="mobile_pac_type" class="select-chosen" data-placeholder="Select Mobile Package" size="1">
                                <Option value=""></Option>
                            </Select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-3 col-md-offset-3">
                            <label class="switch switch-primary"><input type="checkbox" name="is_unlimited" id="is_unlimited" value="1"><span></span></label>
                            <span id="limit-unlimit-text" class="help-block">Limited - Define limit</span>
                        </div>

                        <label class="col-md-2 control-label" for="limit">Limit</label>
                        <div class="col-md-4">
                            <input type="text" id="limit" name="limit" class="form-control" placeholder="Enter Limit">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="remarks">Remarks</label>
                        <div class="col-md-4">
                            <input type="text" id="remarks" name="remarks" class="form-control" placeholder="Enter remarks">
                        </div>

                        <label class="col-md-2 control-label" for="status">Status</label>
                        <div class="col-md-4">
                            <select id="status" name="status" class="select-chosen" data-placeholder="Choose Status">
                                <option></option><!-- Required for data-placeholder attribute to work with Chosen plugin -->
                                <option value="1" selected>Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group form-actions">
                        <input type="hidden" name="id" id="id" value="0" />

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-sm btn-success primary-btn pull-right"><i class="fa fa-angle-right"></i> Submit</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-plus"></i> New</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Block -->
            </div>
		</div>
        <!-- END Example Content -->
    </div>
    <!-- END Example Block -->

    <!-- Table Block -->
    <div class="block full">
        <!-- Table Title -->
        <div class="block-title">
            <h2>Mobile Accounts </h2><small>Mobile accounts currently exist in the system</small>
        </div>
        <!-- END Table Title -->

        <!-- Table Content -->
        <div class="table-responsive"><table id="table-data" class="table table-condensed table-striped table-hover"></table></div>
        <!-- END Table Content -->
    </div>
    <!-- END Table Block -->
</div>
<!-- END Page Content -->

<?php include $prefix.'page_footer.php'; ?>
<?php include $prefix.'template_scripts.php'; ?>
<?php include $prefix.'template_end.php'; ?>

<script type="text/javascript">
    var today = "<?php echo (date(Y-m-d)); ?>";

    $('#mobile_no').mask('0999999999');

    $('#employee').select2({
        minimumInputLength: 2,
        ajax: {
            url: 'data/employee_data.php',
            dataType: 'json',
            delay: 100,
            data: function (term) {
                return term;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: "[" + item.emp_no + "] " + item.full_name,
                            id: item.user_code
                        }
                    })
                };
            }
        }
    });

    $('#is_unlimited').on('change', function (){
        var checked = $('#is_unlimited').prop('checked');
        var help_block_text = (checked) ? "Unlimited - No limit" : "Limited - Define limit";

        $('#limit').prop('disabled',checked);
        $('#limit-unlimit-text').html(help_block_text);
    });

    $(document).on('change', '.load-packages', function(e, p) {
        var selected_serv_pro = $('#service_provider').val();
        var selected_con_type = $('#con_type').val();

        if (selected_serv_pro == ""){
            return;
        }

        if (selected_con_type == ""){
            return;
        }

        load_packages(selected_serv_pro,selected_con_type,'COM_PAC','company_pac_type');
        load_packages(selected_serv_pro,selected_con_type,'MOBI_PAC','mobile_pac_type');
    });

	/*********** Data-table Initialize ***********/	 	
	App.datatables();

    var dt = $('#table-data').DataTable({
        "processing": true,
        "serverSide": true,
        "select": true,
        "columns": [
            { "data": "mobile_no", "name": "mobile_no", "title": "Mobile No" },
            { "data": "employee", "name": "employee", "title": "Employee" },
            { "data": "service_provider", "name": "service_provider", "title": "Service Provider" },
            { "data": "connection_type", "name": "connection_type", "title": "Connection" },
            { "data": "company_package", "name": "company_package", "title": "Company Package" },
            { "data": "mobile_package", "name": "mobile_package", "title": "Mobile Package" },
            { "data": "limit", "name": "limit", "title": "Limit" },

            /* ACTIONS */ 
            {	"data": "actions", "name": "actions","title":"Actions", "searchable": false, "orderable": false, 
                mRender: function (data, type, row) {
                    return '<div class="btn-group btn-group-xs"><button id="btn-row-edit" class="btn btn-primary" title="Edit"><i class="fa fa-pencil"></i></button></div>'
                }
            }
        ],
        "columnDefs": [
            {"className": "dt-center", "targets": [0,1,2,3,6,7]},
            {"className": "dt-left", "targets": [4,5]}
        ],
        "language": {
            "emptyTable": "No mobile accounts to show..."
        },
        "ajax": "data/grid_data_mobile_accounts.php"
    });

	$('.dataTables_filter input').attr('placeholder', 'Search');

    $("#table-data tbody").on('click', '#btn-row-edit', function() {
        var str_id = $(this).closest('tr').attr('id');
        var arr_id = str_id.split("_");

        var row_id = arr_id[1];

        $.ajax({
            url: 'data/data_mobile_accounts.php',
            data: {
                id: row_id
            },
            method: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $('#table-data tbody #'+str_id+' #btn-row-edit').button('loading');
                NProgress.start();
            },
            error: function (e) {
                $.bootstrapGrowl('<h4>Error!</h4> <p>Error retrieving mobile account data</p>', {
                    type: 'danger',
                    delay: 2500,
                    allow_dismiss: true
                });

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            },
            success: function (r) {
                if (!r.result) {
                    $.bootstrapGrowl('<h4>Error!</h4> <p>'+r.message+'</p>', {
                        type: 'danger',
                        delay: 2500,
                        allow_dismiss: true
                    });
                }else{
                    $('#id').val(r.data[0].NUMBER_ID);
                    $('#employee').empty().append('<option value=\"' + r.data[0].USER_CODE + '\" selected=\"selected\">[' + r.data[0].EMP_NO + '] ' + r.data[0].EMP_NAME + '</option>').val(r.data[0].USER_CODE).trigger('change');
                    $('#mobile_no').val(r.data[0].NUMBER);
                    $('#mobile_account_no').val(r.data[0].MOBILE_ACCOUNT_NO);
                    $('#con_date').val(r.data[0].CONNECTION_DATE);

                    $('#service_provider').val("").trigger("chosen:updated");
                    $('#con_type').val("").trigger("chosen:updated");

                    $('#service_provider').val(r.data[0].SP_ID).trigger("chosen:updated");
                    $('#service_provider').trigger('change');

                    $('#con_type').val(r.data[0].CON_TYPE_ID).trigger("chosen:updated");
                    $('#con_type').trigger('change');

                    $('#limit').val(r.data[0].LIMIT);

                    var is_limited = (r.data[0].UNLIMITED == 1) ? true : false;
                    
                    $('#is_unlimited').prop('checked',is_limited).trigger('change');

                    $('#remarks').val(r.data[0].REMARKS);

                    $('#status').val(r.data[0].STATUS).trigger("chosen:updated");

                    setTimeout(function () {
                        $('#company_pac_type').val(r.data[0].COM_PAC_ID).trigger("chosen:updated");
                        $('#mobile_pac_type').val(r.data[0].MOB_PAC_ID).trigger("chosen:updated");
                    }, 300);
                }

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            }
        });
    });
    /*********** Table Control End ***********/	

    $('#form-main').on('submit', function (e){
		e.preventDefault();
		
		var id = $('#id').val();
		var op = (id == 0) ? "insert" : "update";
		
		var formdata = $('#form-main').serializeArray();
		formdata.push({'name':'operation','value':op});
		
		$.ajax({
			url: 'mobile_account_crud.php',
			data: formdata,
			success: function(r){
				var msg_typ = "info";
                var msg_txt = "";

                if (r.result){
                    msg_typ = 'success';
                    msg_txt = '<h4>Success!</h4> <p>Mobile account saved</p>';

                    $('#form-main').trigger('reset');
                }else{
                    msg_typ = 'danger';
                    msg_txt = '<h4>Error!</h4> <p>'+r.message+'</p>';
                }

                $.bootstrapGrowl(msg_txt, {
                    type: msg_typ,
                    delay: 2500,
                    allow_dismiss: true
                });

                dt.ajax.reload();
                dt.draw();
			}
		});
	});

    $('#form-main').on('reset', function (e){
        $('#id').val("0");

        $('#employee').empty().append('<option value=\"0\" selected disabled>Select Employee</option>').val(0).trigger('change');
        $('#mobile_no').val("");
        $('#mobile_account_no').val("");
        $('#con_date').val(today);

        $('#service_provider').val("").trigger("chosen:updated");
        $('#con_type').val("").trigger("chosen:updated");

        $('#limit').val("");

        $('#is_unlimited').prop('checked',false).trigger('change');

        $('#remarks').val("");

        $('#status').val(1).trigger("chosen:updated");

        $('#company_pac_type').html("<option></option>").trigger("chosen:updated");
        $('#mobile_pac_type').html("<option></option>").trigger("chosen:updated");
    });

    function load_packages(sp,ct,typ,target){
        $.ajax({
            url: 'data/packages_data.php',
            data: {
                sp: sp,
                con: ct,
                type: typ
            },
            method: 'POST',
            error: function (re) {
                $.bootstrapGrowl('<h4>Error!</h4> <p>Error retrieving packages</p>', {
                    type: 'danger',
                    delay: 2500,
                    allow_dismiss: true
                });
            },
            success: function (re) {
                $('#'+target).html('<option value=""></option>');

                $.each(re.data, function (k,v){
                    $('#'+target).append('<option value="' + v.ID + '">' + v.NAME + '</option>');
                });

                $('#'+target).trigger("chosen:updated");
            }
        });
    }
</script>
	
<?php mysqli_close($con_main); ?>