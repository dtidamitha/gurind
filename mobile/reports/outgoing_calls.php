<!DOCTYPE html>
<html>
<head>
<title></title>
<style>

h1 {
    text-align: center;
}
a{
    text-decoration: none;
    display: inline-block;
    padding: 11px 16px;
    width: 100px;
}

a:hover {
    background-color: #ddd;
    color: black;
}

.previous {
    background-color: #f1f1f1;
    color: black;
}

.next {
    background-color: #4CAF50;
    color: white;
}

#report{
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#report td, #report th {
    border: 1px solid #ddd;
    padding: 8px;
}

#report tr:nth-child(even){background-color: #f2f2f2;}
#report tr:hover {background-color: #ddd;}

#report th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
}

</style>
</head>
<body>


		<?php
			require_once ('../../config.php');
			$mobile = $_REQUEST['mobile'];
			$year = $_REQUEST['year'];
			$month = $_REQUEST['month'];

        $category_select_query = "SELECT
									mobitel_detail.CATEGORY
								FROM
									`mobitel_detail`
								WHERE
                                     mobitel_detail.MOBILE_NO = '$mobile'
								GROUP BY
									mobitel_detail.CATEGORY";
		$category_select_exec = mysqli_query($con_main,$category_select_query);
		while($category_select_res = mysqli_fetch_array($category_select_exec)){

			 $category = $category_select_res['CATEGORY'];
        ?>
        <h1><?php echo($category);?></h1>
        <table style="border-collapse:collapse; margin: 0 auto;" border="1" id="report" width="98%">
	<thead>
			<tr>
				<th>Date</th>
				<th>Time</th>
				<th>Phone Number</th>
				<th>Peak/off-peak</th>
				<th>Units charged</th>
				<th>Charge in RS</th>							
			</tr>
	</thead>

    <tbody>

       <?php    
        			
		$call_summary_query = "SELECT
									mobitel_header.DUE_DATE,
									mobitel_detail.DATE_TIME,
									mobitel_detail.PHONE_NO,
									mobitel_detail.PEAK_OFFPEAK,
									mobitel_detail.UNITS,
									mobitel_detail.CHARGE,
									mobitel_detail.MOBILE_NO
								FROM
									mobitel_detail
								INNER JOIN mobitel_header ON mobitel_detail.RECORD_ID = mobitel_header.RECORD_ID
								WHERE
									mobitel_detail.CATEGORY = '$category'
								AND mobitel_detail.MOBILE_NO = '$mobile'
								AND YEAR (mobitel_header.DUE_DATE) = '$year'
								AND MONTH (mobitel_header.DUE_DATE) = '$month'";

			$call_summary_exec = mysqli_query($con_main,$call_summary_query);

            $total_outgoing_charge = 0;
			while ($call_summary_res = mysqli_fetch_assoc($call_summary_exec)){

			$date_time = $call_summary_res['DATE_TIME'];
			$time = new DateTime($date_time);
			$date = $time->format('F j, Y');
			$time = $time->format('H:i:s');
            $total_outgoing_charge = $total_outgoing_charge + $call_summary_res['CHARGE'];
		?>
							<tr>
								<td><?php echo ($date); ?></td>
								<td><?php echo ($time); ?></td>
								<td><?php echo ($call_summary_res['PHONE_NO']); ?></td>
								<td><?php echo ($call_summary_res['PEAK_OFFPEAK']); ?></td>
								<td><?php echo ($call_summary_res['UNITS']); ?></td>
								<td><?php echo (number_format($call_summary_res['CHARGE'],2)); ?></td>
							</tr>
	<?php
		}
	?>
	<tr>
		<td colspan="5"><strong><center>TOTAL <?php echo($category);?> CHARGE</center></strong></td>			
			<td><strong><?php echo (number_format($total_outgoing_charge,2)); ?></strong></td>		
		</td>
	</tr>
     </tbody>	
</table>
<?php
}
?>
<br>
<!-- <center><a href="usage_user_report.php" class="previous" id="prev_btn">&laquo; Previous</a>
<a href="sms_outgoing.php" class="next" id= "next_btn">Next &raquo;</a></center> -->
</body>
<!-- <script type="text/javascript" src="../../js/vendor/jquery.min.js"></script>
<script type="text/javascript">
	
	$('#next_btn').on('click', function(e){
		e.preventDefault();
        var mobile = '<?php echo $mobile ?>';
        var year = '<?php echo $year ?>';
        var month = '<?php echo $month ?>';

        window.open('sms_outgoing.php'+"?mobile="+mobile+"&year="+year+"&month="+month);
	});
</script> -->
</html>
