<!DOCTYPE html>
<html>
<head>
<title></title>
<style>
a{
    text-decoration: none;
    display: inline-block;
    padding: 11px 16px;
    width: 100px;
}

a:hover {
    background-color: #ddd;
    color: black;
}

.previous {
    background-color: #4CAF50;
    color: white;
}
h1 {
    text-align: center;
}

#report{
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#report td, #report th {
    border: 1px solid #ddd;
    padding: 8px;
}

#report tr:nth-child(even){background-color: #f2f2f2;}
#report tr:hover {background-color: #ddd;}

#report th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
}
</style>
</head>
<body>
<h1>SMS outgoing</h1>
<table style="border-collapse:collapse; margin: 0 auto;" border="1" id="report" width="98%">
	<thead>
			<tr>
				<th>Date</th>
				<th>Time</th>
				<th>Phone Number</th>
				<th>Units charged</th>
				<th>Charge in RS</th>							
			</tr>
	</thead>

    <tbody>
		<?php
			require_once ('../../config.php');

		$data_group = "";
		$mobile = $_REQUEST['mobile'];
		$year = $_REQUEST['year'];
		$month = $_REQUEST['month'];

		  $call_summary_query = "SELECT
									mobitel_detail.DATE_TIME,
									mobitel_detail.PHONE_NO,
									mobitel_detail.UNITS,
									mobitel_detail.CHARGE
								FROM
									`mobitel_detail`
								INNER JOIN mobitel_header ON mobitel_detail.RECORD_ID = mobitel_header.RECORD_ID
                                WHERE
									mobitel_detail.CATEGORY = 'SMS Outgoing'
					            AND mobitel_detail.MOBILE_NO = '$mobile'
					            AND YEAR (mobitel_header.DUE_DATE) = '$year'
					            AND MONTH (mobitel_header.DUE_DATE) = '$month'";

			$call_summary_exec = mysqli_query($con_main,$call_summary_query);

             $total_sms_charge = 0;
			while ($call_summary_res = mysqli_fetch_assoc($call_summary_exec)){

			$date_time = $call_summary_res['DATE_TIME'];
			$time = new DateTime($date_time);
			$date = $time->format('F j, Y');
			$time = $time->format('H:i:s');

			$total_sms_charge = $total_sms_charge + $call_summary_res['CHARGE'];
		?>
							<tr>
								<td><?php echo ($date); ?></td>
								<td><?php echo ($time); ?></td>
								<td><?php echo ($call_summary_res['PHONE_NO']); ?></td>
								<td><?php echo ($call_summary_res['UNITS']); ?></td>
								<td><?php echo (number_format($call_summary_res['CHARGE'],2)); ?></td>
							</tr>
	<?php
		}
	?>
	<tr>
		<td colspan="4"><strong><center>TOTAL SMS CHARGE</center></strong></td>			
			<td><strong><?php echo (number_format($total_sms_charge,2)); ?></strong></td>		
		</td>
	</tr>
     </tbody>	
</table>
<br>
<center><a href="outgoing_calls.php" class="previous" id="prev_btn">&laquo; Previous</a>
</body>
</html>
