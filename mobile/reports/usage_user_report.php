<?php
require_once ('../../config.php');
$month = $_REQUEST['month'];
$year = $_REQUEST['year'];
$sp = $_REQUEST['sp'];
$contype = $_REQUEST['conType'];
$dep = $_REQUEST['dep'];
$mobile = $_REQUEST['mobileNo'];

if($sp==2){

$total_rental = "SELECT
						mobitel_detail_summary.MONTHLY_RENTAL,
						mobitel_detail_summary.MOBILE_NO
				FROM
					mobitel_detail
				INNER JOIN mobitel_detail_summary ON mobitel_detail.RECORD_ID = mobitel_detail_summary.RECORD_ID
				AND mobitel_detail.MOBILE_NO = mobitel_detail_summary.MOBILE_NO
				WHERE
                    mobitel_detail_summary.MOBILE_NO = '$mobile'
                LIMIT 1";

$total_rental_exec = mysqli_query($con_main,$total_rental);
$total_rental_res = mysqli_fetch_assoc($total_rental_exec);

$total_subscription = $total_rental_res['MONTHLY_RENTAL'];
$total_outgoing_charges = "SELECT
                                mobitel_detail.UNITS,
                                mobitel_detail.CHARGE,
                                mobitel_detail.CATEGORY,
                                mobitel_header.DUE_DATE
                            FROM
                                mobitel_detail
                            INNER JOIN mobitel_header ON mobitel_detail.RECORD_ID = mobitel_header.RECORD_ID
                            WHERE
                                mobitel_detail.CATEGORY = 'Outgoing Call'
                            AND mobitel_detail.MOBILE_NO = '$mobile'
                            AND YEAR (mobitel_header.DUE_DATE) = '$year'
                            AND MONTH (mobitel_header.DUE_DATE) = '$month'";

$total_outhoing_charges_exec = mysqli_query($con_main,$total_outgoing_charges);

$total_charge = 0;
while($row = mysqli_fetch_assoc($total_outhoing_charges_exec)){

        $charge = (double)$row['CHARGE'];
        $total_charge = $total_charge + $charge;
}

$total_sms = "SELECT
                mobitel_detail.UNITS,
                mobitel_detail.CHARGE,
                mobitel_detail.CATEGORY,
                mobitel_header.DUE_DATE
            FROM
                mobitel_detail
            INNER JOIN mobitel_header ON mobitel_detail.RECORD_ID = mobitel_header.RECORD_ID
            WHERE
                mobitel_detail.CATEGORY = 'SMS Outgoing'
            AND mobitel_detail.MOBILE_NO = '$mobile'
            AND YEAR (mobitel_header.DUE_DATE) = '$year'
            AND MONTH (mobitel_header.DUE_DATE) = '$month'";

$total_sms_exec = mysqli_query($con_main,$total_sms);

$total_sms_charge = 0;

while($total_sms_res = mysqli_fetch_assoc($total_sms_exec)){

	$sms_charge = $total_sms_res['CHARGE'];
	$total_sms_charge = $total_sms_charge + $sms_charge;
}
}
?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Mobiman User Wise Detail Report</title>
    
    <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }

    a {
    text-decoration: none;
    display: inline-block;
    padding: 11px 16px;
    width: 100px;
}

a:hover {
    background-color: #ddd;
    color: black;
}

.previous {
    background-color: #f1f1f1;
    color: black;
}

.next {
    background-color: #4CAF50;
    color: white;
}
 </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <!-- <img src="https://www.sparksuite.com/images/logo.png" style="width:100%; max-width:300px;">-->  
                                 <h3>Mobiman</h3>
                            </td>
                            
                            <td>
                                Invoice #: 123<br>
                                Created: January 1, 2015<br>
                                Due: February 1, 2015
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                <strong>Charges summary for the Mobile service no <?php echo($mobile);?></strong>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
             
            <tr class="heading">
                <td>
                    Item
                </td>
                
                <td>
                    Price
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    Total of subscriptions
                </td>
                
                <td>
                    <?php echo(number_format($total_subscription,2));?>
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    Total outgoing call charges
                </td>
                
                <td>
                   <?php echo(number_format($total_charge,2));?>
                </td>
            </tr>
            
            <tr class="item last">
                <td>
                   Total SMS Charges
                </td>
                
                <td>
                   <?php echo(number_format($total_sms_charge,2))?>
                </td>
            </tr>
            
            <tr class="total">
                <td>
                	<strong>Total Charges for the Mobile No <?php echo($mobile)?></strong> 
                </td>
                
                <td>
                   <?php echo(number_format(($total_sms_charge+$total_charge+$total_subscription),2)) ?>
                </td>
            </tr>
        </table>
    </div>
<br>
<center>
<a href="outgoing_calls.php" class="next" id= "next_btn">Next &raquo;</a>
</center>
</body>
<script type="text/javascript" src="../../js/vendor/jquery.min.js"></script>
<script type="text/javascript">
	// $('#prev_btn').on('click', function(e){
	// 	e.preventDefault();
	// 	alert ('PREVIOUS BUTTON CLICK');
	// });
	$('#next_btn').on('click', function(e){
		e.preventDefault();

        var mobile = '<?php echo $mobile ?>';
        var year = '<?php echo $year ?>';
        var month = '<?php echo $month ?>';

        window.open('outgoing_calls.php'+"?mobile="+mobile+"&year="+year+"&month="+month);
	});
</script>
</html>