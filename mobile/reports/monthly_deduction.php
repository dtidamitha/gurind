<?php
require_once ('../../config.php');

$year = $_REQUEST['year'];
$month = $_REQUEST['month'];
$epfNo = $_REQUEST['epfNo'];
$sp = $_REQUEST['sp'];
$contype = $_REQUEST['conType'];
$mobileNo = $_REQUEST['mobileNo'];

$query = "";
$where = "";

if ($sp == 1){
	if (!empty($sp)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " MN.SERVICE_PROVIDER = '$sp' ";
	}

	if (!empty($year)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " YEAR(DH.BILL_DATE_TO) = '$year' ";
	}

	if (!empty($month)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " MONTH(DH.BILL_DATE_TO) = '$month' ";
	}

	if (!empty($epfNo)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " MU.EMP_NO = '$epfNo' ";
	}

	if (!empty($mobileNo)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " DDS.MOBILE_NO = '$mobileNo' ";
	}

	if (!empty($contype)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " MN.CON_TYPE = '$contype' ";
	}

   	$query = "SELECT
	DH.INVOICE_NO,
	DH.BILL_DATE_FROM,
	DH.BILL_DATE_TO,
	MU.EMP_NO,
	CONCAT_WS(' ',MU.FIRST_NAME,MU.LAST_NAME) AS EMP_NAME,
	MD.DEPARTMENT,
	DDS.MOBILE_NO,
	MSP.SHORT_NAME AS SERVICE_PROVIDER,
	MCT.CON_TYPE AS CONNECTION_TYPE,
	MN.`LIMIT`,
	IF(MN.UNLIMITED=1,'YES','NO') AS UNLIMITED,
	DDS.MONTHLY_RENTAL,
	DDS.TOTAL_USAGE_CHARGES,
	DDS.TOTAL_TAX,
	DDS.TOTAL_BILL_AMOUNT,
	DDS.TOTAL_DUE
	FROM
	dialog_detail_summary AS DDS
	LEFT JOIN mobi_number AS MN ON DDS.MOBILE_NO = MN.NUMBER
	LEFT JOIN mas_user AS MU ON MN.USER_ID = MU.USER_CODE
	LEFT JOIN mobi_con_type AS MCT ON MN.CON_TYPE = MCT.CON_TYPE_ID
	LEFT JOIN mobi_service_provider AS MSP ON MN.SERVICE_PROVIDER = MSP.SP_ID
	LEFT JOIN mas_department AS MD ON MU.DEPARTMENT = MD.DEP_CODE
	INNER JOIN dialog_header AS DH ON DDS.RECORD_ID = DH.RECORD_ID";

	$query .= $where;

	$query .= " GROUP BY
	DH.INVOICE_NO,
	DDS.MOBILE_NO ";
}else if($sp == 2){
	if (!empty($sp)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " MN.SERVICE_PROVIDER = '$sp' ";
	}

	if (!empty($year)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " YEAR(MH.BILL_DATE) = '$year' ";
	}

	if (!empty($month)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " MONTH(MH.BILL_DATE) = '$month' ";
	}

	if (!empty($epfNo)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " MU.EMP_NO = '$epfNo' ";
	}

	if (!empty($mobileNo)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " MDS.MOBILE_NO = '$mobileNo' ";
	}

	if (!empty($contype)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " MN.CON_TYPE = '$contype' ";
	}

	$query = "SELECT
	MU.EMP_NO,
	CONCAT_WS(' ',MU.FIRST_NAME,MU.LAST_NAME) AS EMP_NAME,
	MH.BILL_NO,
	MH.BILL_DATE,
	MH.DUE_DATE,
	MD.DEPARTMENT,
	MDS.MOBILE_NO,
	MS.SHORT_NAME AS SERVICE_PROVIDER,
	MC.CON_TYPE AS CONNECTION_TYPE,
	MN.`LIMIT`,
	IF(MN.UNLIMITED=1,'YES','NO') AS UNLIMITED,
	MDS.MONTHLY_RENTAL,
	MDS.TOTAL_USAGE_CHARGES,
	MDS.TOTAL_TAX,
	MDS.TOTAL_DUE AS TOTAL_BILL_AMOUNT,
	MDS.TOTAL_DUE
	FROM
	mobitel_detail_summary AS MDS
	LEFT JOIN mobi_number AS MN ON MDS.MOBILE_NO = MN.NUMBER
	LEFT JOIN mas_user AS MU ON MN.USER_ID = MU.USER_CODE
	LEFT JOIN mas_department AS MD ON MU.DEPARTMENT = MD.DEP_CODE
	LEFT JOIN mobi_service_provider AS MS ON MN.SERVICE_PROVIDER = MS.SP_ID
	LEFT JOIN mobi_con_type AS MC ON MN.CON_TYPE = MC.CON_TYPE_ID
	INNER JOIN mobitel_header AS MH ON MDS.RECORD_ID = MH.RECORD_ID";

	$query .= $where;

	$query .= " GROUP BY
	MH.BILL_NO,
	MDS.MOBILE_NO ";
}
?>

					<table style="border-collapse:collapse;" border="1" id="report" width="75%">
						<thead>
						<tr>
							<th>Employee No</th>
							<th>Employee Name</th>
							<th>Department</th>
							<th>Mobile No</th>
							<th>Service Provider</th>
							<th>Connection Type</th>
							<th>Limit</th>
							<th>Unlimited</th>
							<th>Monthly Rental</th>
							<th>Usage Charge</th>
							<th>Total Tax</th>
							<th>Bill Amount</th>
							<th>Total Due</th>
							<th>Company Payable</th>
							<th>Employee Payable</th>
						</tr>
						</thead>
						<tbody>
		<?php
			$sql = mysqli_query ($con_main, $query);
		
			while ($row = mysqli_fetch_array ($sql)){
				$company_payable = 0;
				$company_payable_balance = 0;
				$employee_payable = 0;
				$total_due = (double)$row['TOTAL_DUE'];
				$total_tax = (double)$row['TOTAL_TAX'];
				$limit = (double)$row['LIMIT'];
				$monthly_rental = (double)$row['MONTHLY_RENTAL'];
		?>
							<tr>
								<td><?php echo ($row['EMP_NO']); ?></td>
								<td><?php echo ($row['EMP_NAME']); ?></td>
								<td><?php echo ($row['DEPARTMENT']); ?></td>
								<td><?php echo ($row['MOBILE_NO']); ?></td>
								<td><?php echo ($row['SERVICE_PROVIDER']); ?></td>
								<td><?php echo ($row['CONNECTION_TYPE']); ?></td>
								<td><?php echo ($limit); ?></td>
								<td><?php echo ($row['UNLIMITED']); ?></td>
								<td><?php echo ($monthly_rental); ?></td>
								<td><?php echo ($row['TOTAL_USAGE_CHARGES']); ?></td>
								<td><?php echo ($total_tax); ?></td>
								<td><?php echo ($row['TOTAL_BILL_AMOUNT']); ?></td>
								<td><?php echo ($total_due); ?></td>

								<?php

								if ($row['UNLIMITED'] == 0){
									$company_payable = $monthly_rental + $total_tax;
									$company_payable_balance = $total_due - $company_payable;
									$employee_payable = $company_payable_balance - $limit;
									$employee_payable = ($employee_payable < 0) ? 0 : $employee_payable;
								}else{
									$company_payable = $total_due;
									$employee_payable = 0;
								}
								
								?>

								<td><?php echo (number_format($company_payable,2)); ?></td>
								<td><?php echo (number_format($employee_payable,2)); ?></td>

							</tr>
	<?php
							}
	?>
						</tbody>	
		</table>