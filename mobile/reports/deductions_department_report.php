<?php
require_once ('../../config.php');


$year = $_REQUEST['year'];
$month = $_REQUEST['month'];
$sp = $_REQUEST['sp'];
$contype = $_REQUEST['conType'];
$dep = $_REQUEST['dep'];

$query = "";
$where = "";

if ($sp == 1){
	if (!empty($sp)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " MN.SERVICE_PROVIDER = '$sp' ";
	}

	if (!empty($year)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " YEAR(DH.BILL_DATE_TO) = '$year' ";
	}

	if (!empty($month)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " MONTH(DH.BILL_DATE_TO) = '$month' ";
	}
	if (!empty($contype)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " MN.CON_TYPE = '$contype' ";
	}

	if (!empty($dep)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " MD.DEP_CODE = '$dep' ";
	}

   	$query = "SELECT
	DH.INVOICE_NO,
	DH.BILL_DATE_FROM,
	DH.BILL_DATE_TO,
	MU.EMP_NO,
	CONCAT_WS(' ',MU.FIRST_NAME,MU.LAST_NAME) AS EMP_NAME,
	MD.DEPARTMENT,
	DDS.MOBILE_NO,
	MSP.SHORT_NAME AS SERVICE_PROVIDER,
	MCT.CON_TYPE AS CONNECTION_TYPE,
	MN.`LIMIT`,
	IF(MN.UNLIMITED=1,'YES','NO') AS UNLIMITED,
	DDS.MONTHLY_RENTAL,
	DDS.TOTAL_USAGE_CHARGES,
	DDS.TOTAL_TAX,
	DDS.TOTAL_BILL_AMOUNT,
	DDS.TOTAL_DUE
	FROM
	dialog_detail_summary AS DDS
	LEFT JOIN mobi_number AS MN ON DDS.MOBILE_NO = MN.NUMBER
	LEFT JOIN mas_user AS MU ON MN.USER_ID = MU.USER_CODE
	LEFT JOIN mobi_con_type AS MCT ON MN.CON_TYPE = MCT.CON_TYPE_ID
	LEFT JOIN mobi_service_provider AS MSP ON MN.SERVICE_PROVIDER = MSP.SP_ID
	LEFT JOIN mas_department AS MD ON MU.DEPARTMENT = MD.DEP_CODE
	INNER JOIN dialog_header AS DH ON DDS.RECORD_ID = DH.RECORD_ID";

	$query .= $where;

	$query .= " GROUP BY
	DH.INVOICE_NO,
	DDS.MOBILE_NO ";

$query_exec = mysqli_query($con_main,$query);
$query_res = mysqli_fetch_assoc($query_exec);

$department = $query_res['DEPARTMENT']; 
$service_provider = $query_res['SERVICE_PROVIDER']; 
$con_type = ($contype==0)? "Any" : $query_res['CONNECTION_TYPE'];


require_once ('../../reporting_part/report_header_oa1.php');

}else if($sp == 2){
	if (!empty($sp)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " MN.SERVICE_PROVIDER = '$sp' ";
	}

	if (!empty($year)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " YEAR(MH.BILL_DATE) = '$year' ";
	}

	if (!empty($month)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " MONTH(MH.BILL_DATE) = '$month' ";
	}

	if (!empty($contype)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " MN.CON_TYPE = '$contype' ";
	}

	if (!empty($dep)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " MD.DEP_CODE = '$dep' ";
	}

	$query = "SELECT
	MU.EMP_NO,
	CONCAT_WS(' ',MU.FIRST_NAME,MU.LAST_NAME) AS EMP_NAME,
	MH.BILL_NO,
	MH.BILL_DATE,
	MH.DUE_DATE,
	MD.DEPARTMENT,
	MDS.MOBILE_NO,
	MS.SHORT_NAME AS SERVICE_PROVIDER,
	MC.CON_TYPE AS CONNECTION_TYPE,
	MN.`LIMIT`,
	IF(MN.UNLIMITED=1,'YES','NO') AS UNLIMITED,
	MDS.MONTHLY_RENTAL,
	MDS.TOTAL_USAGE_CHARGES,
	MDS.TOTAL_TAX,
	MDS.TOTAL_DUE AS TOTAL_BILL_AMOUNT,
	MDS.TOTAL_DUE
	FROM
	mobitel_detail_summary AS MDS
	LEFT JOIN mobi_number AS MN ON MDS.MOBILE_NO = MN.NUMBER
	LEFT JOIN mas_user AS MU ON MN.USER_ID = MU.USER_CODE
	LEFT JOIN mas_department AS MD ON MU.DEPARTMENT = MD.DEP_CODE
	LEFT JOIN mobi_service_provider AS MS ON MN.SERVICE_PROVIDER = MS.SP_ID
	LEFT JOIN mobi_con_type AS MC ON MN.CON_TYPE = MC.CON_TYPE_ID
	INNER JOIN mobitel_header AS MH ON MDS.RECORD_ID = MH.RECORD_ID";

	$query .= $where;

	$query .= " GROUP BY
	MH.BILL_NO,
	MDS.MOBILE_NO ";

$query_exec = mysqli_query($con_main,$query);
$query_res = mysqli_fetch_assoc($query_exec);

$department = $query_res['DEPARTMENT']; 
$service_provider = $query_res['SERVICE_PROVIDER']; 
$con_type = ($contype==0)? "Any" : $query_res['CONNECTION_TYPE'];


require_once ('../../reporting_part/report_header_oa1.php');

}
?>

					<table style="border-collapse:collapse; margin: 0 auto;" border="1" id="report" width="98%">
						<thead>
						<tr>
							<!-- <th>Department</th>
							<th>Service Provider</th> -->
							<th>Connection Type</th>
							<th>Usage Charge</th>
							<th>Bill Amount</th>
							<th>Total Tax</th>
							<th>Total Due</th>
							<th>Company Payable</th>
							<th>Employee Payable</th>
						</tr>
						</thead>
						<tbody>
		<?php
			$sql = mysqli_query ($con_main, $query);

			$total_usage = 0;
			$total_bill = 0;
            $tot_tax = 0;
            $tot_due = 0;
		
			while ($row = mysqli_fetch_array ($sql)){
				$company_payable = 0;
				$company_payable_balance = 0;
				$employee_payable = 0;
                $total_company_payable = 0;
                $total_employee_payable = 0;

				$total_due = (double)$row['TOTAL_DUE'];
				$total_tax = (double)$row['TOTAL_TAX'];
				$limit = (double)$row['LIMIT'];
				$monthly_rental = (double)$row['MONTHLY_RENTAL'];

				$total_usage = $total_usage + $row['TOTAL_USAGE_CHARGES'];
				$total_bill = $total_bill + $row['TOTAL_BILL_AMOUNT'];
				$tot_tax = $tot_tax + $total_tax;
                $tot_due = $tot_due + $total_due;
		?>
							<tr>
                                <td><?php echo ($row['CONNECTION_TYPE']); ?></td>
                                <td><?php echo (number_format($row['TOTAL_USAGE_CHARGES'],2)); ?></td>
                                <td><?php echo (number_format($row['TOTAL_BILL_AMOUNT'],2)); ?></td>
                                <td><?php echo (number_format($total_tax,2)); ?></td>
                                <td><?php echo (number_format($total_due,2)); ?></td>

								<?php

								if ($row['UNLIMITED'] == 0){
									$company_payable = $monthly_rental + $total_tax;
									$company_payable_balance = $total_due - $company_payable;
									$employee_payable = $company_payable_balance - $limit;
									$employee_payable = ($employee_payable < 0) ? 0 : $employee_payable;
								}else{
									$company_payable = $total_due;
									$employee_payable = 0;
								}
								$total_company_payable = $total_company_payable + $company_payable;
								$total_employee_payable = $total_employee_payable + $employee_payable;
								?>

								<td><?php echo (number_format($company_payable,2)); ?></td>
								<td><?php echo (number_format($employee_payable,2)); ?></td>

							</tr>
	<?php
							}
	?>
	<tr>
		<td colspan="1"><strong><center>TOTAL</center></strong></td>			
			<td><strong><?php echo (number_format($total_usage,2)); ?></strong></td>	
			<td><strong><?php echo (number_format($total_bill,2)); ?></strong></td>
			<td><strong><?php echo (number_format($tot_tax,2)); ?></strong></td>
			<td><strong><?php echo (number_format($tot_due,2)); ?></strong></td>
			<td><strong><?php echo (number_format($total_company_payable,2)); ?></strong></td>
			<td><strong><?php echo (number_format($total_employee_payable,2)); ?></strong></td>	
		</td>
	</tr>
</tbody>	
		</table>