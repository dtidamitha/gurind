<?php
require_once ('../../config.php');

$year = $_REQUEST['year'];
$month = $_REQUEST['month'];
$sp = $_REQUEST['sp'];
$mobileNo = $_REQUEST['mobileNo'];

$query = "";
$where = "";

if ($sp == 1){
	$sp_text = "Dialog";

	$where = " WHERE (MSP.SHORT_NAME IS NULL AND
	MCT.CON_TYPE IS NULL AND
	MN.`LIMIT` IS NULL) ";

	if (!empty($year)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " YEAR(DH.BILL_DATE_TO) = '$year' ";
	}

	if (!empty($month)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " MONTH(DH.BILL_DATE_TO) = '$month' ";
	}

	if (!empty($mobileNo)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " DDS.MOBILE_NO LIKE '%$mobileNo%' ";
	}

   	$query = "SELECT
	DH.INVOICE_NO,
	DH.BILL_DATE_FROM,
	DH.BILL_DATE_TO AS DUE_DATE,
	MU.EMP_NO,
	CONCAT_WS(' ',MU.FIRST_NAME,MU.LAST_NAME) AS EMP_NAME,
	MD.DEPARTMENT,
	DDS.MOBILE_NO,
	MSP.SHORT_NAME AS SERVICE_PROVIDER,
	MCT.CON_TYPE AS CONNECTION_TYPE,
	MN.`LIMIT`,
	IF(MN.UNLIMITED=1,'YES','NO') AS UNLIMITED,
	DDS.MONTHLY_RENTAL,
	DDS.TOTAL_USAGE_CHARGES,
	DDS.TOTAL_TAX,
	DDS.TOTAL_BILL_AMOUNT,
	DDS.TOTAL_DUE
	FROM
	dialog_detail_summary AS DDS
	LEFT JOIN mobi_number AS MN ON DDS.MOBILE_NO = MN.NUMBER
	LEFT JOIN mas_user AS MU ON MN.USER_ID = MU.USER_CODE
	LEFT JOIN mobi_con_type AS MCT ON MN.CON_TYPE = MCT.CON_TYPE_ID
	LEFT JOIN mobi_service_provider AS MSP ON MN.SERVICE_PROVIDER = MSP.SP_ID
	LEFT JOIN mas_department AS MD ON MU.DEPARTMENT = MD.DEP_CODE
	INNER JOIN dialog_header AS DH ON DDS.RECORD_ID = DH.RECORD_ID";

	$query .= $where;
}else if($sp == 2){
	$sp_text = "Mobitel";

	$where = " WHERE
	(MS.SHORT_NAME IS NULL AND
	MC.CON_TYPE IS NULL AND
	MN.`LIMIT` IS NULL) ";

	if (!empty($year)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " YEAR(MH.DUE_DATE) = '$year' ";
	}

	if (!empty($month)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " MONTH(MH.DUE_DATE) = '$month' ";
	}

	if (!empty($mobileNo)){
		$where .= (empty($where)) ? " WHERE " : " AND ";
		$where .= " MDS.MOBILE_NO LIKE '%$mobileNo%' ";
	}

	$query = "SELECT
	MU.EMP_NO,
	CONCAT_WS(' ',MU.FIRST_NAME,MU.LAST_NAME) AS EMP_NAME,
	MH.BILL_NO,
	MH.BILL_DATE,
	MH.DUE_DATE,
	MD.DEPARTMENT,
	MDS.MOBILE_NO,
	MS.SHORT_NAME AS SERVICE_PROVIDER,
	MC.CON_TYPE AS CONNECTION_TYPE,
	MN.`LIMIT`,
	IF(MN.UNLIMITED=1,'YES','NO') AS UNLIMITED,
	MDS.MONTHLY_RENTAL,
	MDS.TOTAL_USAGE_CHARGES,
	MDS.TOTAL_TAX,
	MDS.TOTAL_DUE AS TOTAL_BILL_AMOUNT,
	MDS.TOTAL_DUE
	FROM
	mobitel_detail_summary AS MDS
	LEFT JOIN mobi_number AS MN ON MDS.MOBILE_NO = MN.NUMBER
	LEFT JOIN mas_user AS MU ON MN.USER_ID = MU.USER_CODE
	LEFT JOIN mas_department AS MD ON MU.DEPARTMENT = MD.DEP_CODE
	LEFT JOIN mobi_service_provider AS MS ON MN.SERVICE_PROVIDER = MS.SP_ID
	LEFT JOIN mobi_con_type AS MC ON MN.CON_TYPE = MC.CON_TYPE_ID
	INNER JOIN mobitel_header AS MH ON MDS.RECORD_ID = MH.RECORD_ID";

	$query .= $where;
}
?>

					<table style="border-collapse:collapse;" border="1" id="report" width="75%">
						<thead>
						<tr>
							<th>Mobile No</th>
							<th>Service Provider</th>
							<th>Monthly Rental</th>
							<th>Usage Charge</th>
							<th>Total Tax</th>
							<th>Bill Amount</th>
							<th>Total Due</th>
							<th>Due Date</th>
						</tr>
						</thead>
						<tbody>
		<?php
			$sql = mysqli_query ($con_main, $query);
		
			while ($row = mysqli_fetch_array ($sql)){
				$total_due = (double)$row['TOTAL_DUE'];
				$total_tax = (double)$row['TOTAL_TAX'];
				$limit = (double)$row['LIMIT'];
				$monthly_rental = (double)$row['MONTHLY_RENTAL'];
		?>
							<tr>
								<td><?php echo ($row['MOBILE_NO']); ?></td>
								<td><?php echo ($sp_text); ?></td>
								<td><?php echo ($monthly_rental); ?></td>
								<td><?php echo ($row['TOTAL_USAGE_CHARGES']); ?></td>
								<td><?php echo ($total_tax); ?></td>
								<td><?php echo ($row['TOTAL_BILL_AMOUNT']); ?></td>
								<td><?php echo ($total_due); ?></td>
								<td><?php echo ($row['DUE_DATE']); ?></td>

							</tr>
	<?php
							}
	?>
						</tbody>	
		</table>