<?php
	session_start();
	
	if (empty($_SESSION['ACCESS_CODE']) || $_SESSION['ACCESS_CODE'] == NULL){
		header ('Location: login.php');
		exit;
	}
	
	$folder_depth = "";
	$prefix = "";
	
	$folder_depth = substr_count($_SERVER["PHP_SELF"] , "/");
	$folder_depth = ($folder_depth == false) ? 2 : (int)$folder_depth;
	
    $prefix = str_repeat("../", $folder_depth - 2);
    
    $title_suffix = " Department Master";
?>
<?php include $prefix.'config.php'; ?>
<?php include $prefix.'menu.php'; ?>
<?php include $prefix.'template_start.php'; ?>
<?php include $prefix.'page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-podium"></i>Marketing Master<br><small>Create, Update or Delete Providers</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="../home.php">Home</a></li>
        <li>Marketing</li>
    </ul>
    <!-- END Blank Header -->
		
	
    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
					<h2>Marketing</h2>
				</div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <form id="form-main" name="form-main" action="provider_crud.php" method="post"  class="form-horizontal form-bordered">                  
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="no">No</label>
                        <div class="col-md-4">
                            <input id="no" name="no" class="form-control" placeholder="Enter the no">        
                        </div>
                       
            
                        <label class="col-md-2 control-label" for="date">Date</label>
                        <div class="col-md-4">
                            <input type="text" id="date" name="date" class="form-control" placeholder="Enter visit">
                        </div>
                    </div>

                    <div class="form-group">    
                         <label class="col-md-2 control-label" for="company">Company</label>
                        <div class="col-md-4">
                            <input type="text" id="company" name="company" class="form-control" placeholder="Enter Company Name">
                        </div>
                
            
                       <label class="col-md-2 control-label" for="dc">DC</label>
                        <div class="col-md-4">
                            <input type="text" id="dc" name="dc" class="form-control" placeholder="Enter DC">                        
                        </div>
                    </div>

                    <div class="form-group"> 
                     <label class="col-md-2 control-label" for="contactno">Contact No</label>
                        <div class="col-md-4">
                            <input type="text" id="contactno" name="contactno" class="form-control" placeholder="Enter Company Name">
                        </div>
                
            
                       <label class="col-md-2 control-label" for="comweb">Company Website</label>
                        <div class="col-md-4">
                            <input type="text" id="comweb" name="comweb" class="form-control" placeholder="Enter the Website name">                        
                        </div>

                    </div>


                    <div class="form-group"> 
                     <label class="col-md-2 control-label" for="email">Email</label>
                        <div class="col-md-4">
                            <input type="text" id="email" name="email" class="form-control" placeholder="Enter email">
                        </div>
                
            
                       <label class="col-md-2 control-label" for="remark">Remark</label>
                        <div class="col-md-4">
                            <input type="text" id="remark" name="remark" class="form-control" placeholder="Enter the remark">                        
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="status">Status</label>
                        <div class="col-md-14">
                        <div class="col-md-4">
                                <select id="status" name="status" class="select-chosen" data-placeholder="Choose status"> 
                                   <option></option>
                                   <?php
                                    $query="SELECT
                                                   marketing_status.id,
                                                   marketing_status.`marketing status`,
                                                   marketing_status.`status`
                                            FROM
                                                  `marketing_status`
                                            WHERE
                                                   marketing_status.`status` = '1'";

                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['id']."\">".$type['marketing status']."</option>");
                                    }
                                ?>
                               </select>
                        </div>  
                     </div> 

                     <label class="col-md-2 control-label" for="tcomment">Tharanga comment</label>
                     <div class="col-md-4">
                        <input type="text" id="tcomment" name="tcomment" class="form-control" placeholder="Enter the comment"> 
                        </div>                    
                 </div>

                 <div class="form-group">
                    <label class="col-md-2 control-label" for="gavecall">Gave a call</label>
                    <div class="col-md-4">
                        <input type="text" id="gcall" name="gcall" class="form-control input-datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                    </div>

                    <label class="col-md-2 control-label" for="kremark">Remark by Kawshalya</label>
                    <div class="col-md-4">
                        <input type="text" id="kremark" name="kremark" class="form-control" placeholder="Enter the comment">
                    </div>
                     
                 </div>

                    <div class="form-group form-actions">
                        <input type="hidden" name="id" id="id" value="0" />

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success primary-btn pull-right"><i class="fa fa-angle-right"></i> Submit</button>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Block -->
		    </div>
            <!-- END Example Content -->
        </div>
        <!-- END Example Block -->
    </div>

    <!-- Table Block -->
    <div class="block full">
        <!-- Table Title -->
        <div class="block-title">
            <h2>Marketing</h2><small>Marketing currently exist in the system</small>
        </div>
        <!-- END Table Title -->

        <!-- Table Content -->
        <div class="table-responsive"><table id="table-data" class="table table-condensed table-striped table-hover"></table></div>
        <!-- END Table Content -->
    </div>
    <!-- END Table Block -->
<!-- END Page Content -->
</div>

<?php include $prefix.'page_footer.php'; ?>
<?php include $prefix.'template_scripts.php'; ?>
<?php include $prefix.'template_end.php'; ?>

<script type="text/javascript">

	/*********** Data-table Initialize ***********/
  /*  $('#main_category').on('change', function(){
        var cato = $('#main_category').val();

        $.ajax({
            url: 'data/data_provider.php',
            data: {
                main_category: cato
            },
            method: 'post',
            error: function(e){
                alert ('Error requesting provider data');
            },
            success: function(r){
                $('#main_provider').html('<option value=""></option>');

                if (r.result){
                    if (r.data.length > 0){
                        $.each(r.data, function (k, v){
                            let option_markup = "";

                            option_markup += "<option value='"+v.id+"'>";
                            option_markup += v.provider;
                            option_markup += "</option>";

                            $('#main_provider').append(option_markup)
                            
                        });
                    }
                }

                $('#main_provider').trigger("chosen:updated");
            }
        });
    });*/

    App.datatables();

    var dt = $('#table-data').DataTable({
        "processing": true,
        "serverSide": true,
        "select": true,
        "columns": [
			{ "data": "no", "name": "no", "title": "no" },
			{ "data": "date", "name": "date", "title": "date" },
            { "data": "company name", "name": "company name", "title": "company name"}, 
            { "data": "contact no", "name": "contact no", "title": "contact no"},
            { "data": "company web", "name": "company web", "title": "company web"},
            { "data": "email", "name": "email", "title": "email"},
            { "data": "remark", "name": "remark", "title": "remark"},
            { "data": "status", "name": "status", "title": "status"},
            { "data": "tcomment", "name": "tcomment", "title": "Tharanga "}, 
            { "data": "gcall", "name": "gcall", "title": "Gave call"},
            { "data": "kcomment", "name": "kcomment", "title": "Kawshalya comment"}, 

            {"data": "actions", "name": "actions","title":"Actions", "searchable": false, "orderable": false, 
                mRender: function (data, type, row) {
                    return '<div class="btn-group btn-group-xs"><button id="btn-row-edit" class="btn btn-primary" title="Edit"><i class="fa fa-pencil"></i></button></div>'
                }
            }
        ],
        "columnDefs":[
            {"className": "dt-center", "targets": [1,2,3]}
        ],
        "language": {
            "emptyTable": "No providers to show..."
        },
        "ajax": "data/grid_data_marketing.php"
    });
	
    $('.dataTables_filter input').attr('placeholder', 'Search');

    $("#table-data tbody").on('click', '#btn-row-edit', function() {
        var str_id = $(this).closest('tr').attr('id');
        var arr_id = str_id.split("_");

        var row_id = arr_id[1];
       // alert (row_id);

        $.ajax({
            url: 'data/data_marketing.php',
            data: {
                id: row_id
            },
            method: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $('#table-data tbody #'+str_id+' #btn-row-edit').button('loading');
                NProgress.start();
            },
            error: function (e) {
                $.bootstrapGrowl('<h4>Error!</h4> <p>Error retrieving records data</p>', {
                    type: 'danger',
                    delay: 2500,
                    allow_dismiss: true
                });

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            },
            success: function (r) {
                if (!r.result) {
                    $.bootstrapGrowl('<h4>Error!</h4> <p>'+r.message+'</p>', {
                        type: 'danger',
                        delay: 2500,
                        allow_dismiss: true
                    });
                }else{
                    $('#id').val(r.data[0].id);
                    $('#no').val(r.data[0].no);
                    
                    $('#status').val(r.data[0].status);                        
                    $('#status').trigger("chosen:updated");

                    $('#date').val(r.data[0].date);
                    $('#company').val(r.data[0].cmpname);
                    $('#dc').val(r.data[0].dc);
                    $('#contactno').val(r.data[0].contactno);
                    $('#comweb').val(r.data[0].cmpweb);
                    $('#email').val(r.data[0].email);
                    $('#remark').val(r.data[0].remark);
                    $('#tcomment').val(r.data[0].Tharanga_comment);
                    $('#gcall').val(r.data[0].Gave_call);
                    $('#kremark').val(r.data[0].Remark_kawshalya);
                }

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            }
        });
    });
    /*********** Table Control End ***********/

    /*********** Form Validation and Submission ***********/
	$('#form-main').on('submit', function (e){
		e.preventDefault();
		
		var id = $('#id').val();
		var op = (id == 0) ? "insert" : "update";
		
		var formdata = $('#form-main').serializeArray();
		formdata.push({'name':'operation','value':op});
		
		$.ajax({
			url: 'marketing_crud.php',
			data: formdata,
			success: function(r){
				var msg_typ = "info";
                var msg_txt = "";

                if (r.result){
                    msg_typ = 'success';
                    msg_txt = '<h4>Success!</h4> <p>Marketing saved</p>';

                    $('#form-main').trigger('reset');
                }else{
                    msg_typ = 'danger';
                    msg_txt = '<h4>Error!</h4> <p>'+r.message+'</p>';
                }

                $.bootstrapGrowl(msg_txt, {
                    type: msg_typ,
                    delay: 2500,
                    allow_dismiss: true
                });

                dt.ajax.reload();
                dt.draw();
			}
		});
	});

    $('#form-main').on('reset', function (e){
        $('#id').val("0");

        $('#main_category').val(0);
        $('#location').trigger("chosen:updated");

        $('#main_provider').val("");
        $('#status').trigger("chosen:updated");

        $('#location').val("");
        $('#username').val("");
        $('#password').val("");
        $('#expdate').val("");
    });
    /*********** Form Control End ***********/
		
	</script>
	
	<?php mysqli_close($con_main); ?>