<?php
header('Content-Type: application/json');

include ('../../config.php');

$where = "";
$responce = array();
$data = "";
$result = true;
$message = "";

$id = (isset($_REQUEST['id']) && $_REQUEST['id'] != NULL && !empty($_REQUEST['id'])) ? $_REQUEST['id'] : 0;
$query = "SELECT
    marketing.id,
    marketing.`no`,
    marketing.date,
    marketing.cmpname,
    marketing.dc,
    marketing.contactno,
    marketing.cmpweb,
    marketing.email,
    marketing.remark,
    marketing.`status`,
    marketing.`Tharanga_comment`,
    marketing.`Gave_call`,
    marketing.`Remark_kawshalya`
FROM
    `marketing`";

if ($id > 0){
    $where .= (empty($where)) ? " WHERE " : " AND ";
    $where .= " marketing.id = '$id' ";
}

$query = $query.$where;

$sql = mysqli_query ($con_main, $query);

if (!$sql){
    $result = false;
    $message .= " Error Sql : (".mysqli_errno($con_main).") ".mysqli_error($con_main).". ";
}

$num_rows = mysqli_num_rows($sql);

if ($num_rows > 0){
    $i = 0;

    while ($rows = mysqli_fetch_assoc ($sql)){
        $data[$i] = $rows;
        $i++;
    }
}else{
    $result = false;
    $message .= " Empty results ";
}

$responce['data'] = $data;
$responce['result'] = $result;
$responce['message'] = $message;

echo (json_encode($responce));

mysqli_close($con_main);
?>