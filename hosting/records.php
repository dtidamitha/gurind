<?php
	session_start();
	
	if (empty($_SESSION['ACCESS_CODE']) || $_SESSION['ACCESS_CODE'] == NULL){
		header ('Location: login.php');
		exit;
	}
	
	$folder_depth = "";
	$prefix = "";
	
	$folder_depth = substr_count($_SERVER["PHP_SELF"] , "/");
	$folder_depth = ($folder_depth == false) ? 2 : (int)$folder_depth;
	
    $prefix = str_repeat("../", $folder_depth - 2);
    
    $title_suffix = " Department Master";
?>
<?php include $prefix.'config.php'; ?>
<?php include $prefix.'menu.php'; ?>
<?php include $prefix.'template_start.php'; ?>
<?php include $prefix.'page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-podium"></i>Records Master<br><small>Create, Update or Delete Providers</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="../home.php">Home</a></li>
        <li>Records</li>
    </ul>
    <!-- END Blank Header -->
		
	
    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
					<h2>Records</h2>
				</div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <form id="form-main" name="form-main" action="provider_crud.php" method="post"  class="form-horizontal form-bordered">  

                <div class="form-group">                      
                        <label class="col-md-2 control-label" for="main_category">Category</label>
                        <div class="col-md-4">
                                <select id="main_category" name="main_category" class="select-chosen" data-placeholder="Choose Category"> 
                                   <option></option>
                                   <?php
                                    $query="SELECT
                                              host_category.id,
                                              host_category.category_name,
                                              host_category.`status`
                                            FROM
                                             `host_category`
                                            WHERE
                                              host_category.`status` = '1'";

                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['id']."\">".$type['category_name']."</option>");
                                    }
                                ?>


                               </select>
                        </div> 

                        <label class="col-md-2 control-label" for="main_provider">Provider</label>
                        <div class="col-md-4">
                                <select id="main_provider" name="main_provider" class="select-chosen" data-placeholder="Choose Category"> 
                                   <option value=""></option>                                                      
                               </select>
                        </div>
                        
                </div>
                       
                <div class="form-group">  
                           <label class="col-md-2 control-label" for="location">Location</label>        
                        <div class="col-md-4">                         
                            <input type="text" id="location" name="location" class="form-control" placeholder="Enter location">
                        </div>

                        <label class="col-md-2 control-label" for="expdate">Expire date</label>
                        <div class="col-md-4">                           
                            <input type="text" id="expdate" name="expdate" class="form-control input-datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                        </div>
                </div>
                    

                    <div class="form-group">  
                           <label class="col-md-2 control-label" for="dbname">DB Name</label>        
                        <div class="col-md-4">                         
                            <input type="text" id="dbname" name="dbname" class="form-control" placeholder="Enter database name">
                        </div>

                         <label class="col-md-2 control-label" for="dbusername">DB Username</label>
                        <div class="col-md-4">                           
                            <input type="text" id="dbusername" name="dbusername" class="form-control" placeholder="Enter databse username">
                        </div>
                    </div>


                    <div class="form-group">
                         <label class="col-md-2 control-label" for="dbpassword">DB Password</label>
                        <div class="col-md-4">                           
                            <input type="text" id="dbpassword" name="dbpassword" class="form-control" placeholder="Enter database password">
                        </div>

                        <label class="col-md-2 control-label" for="wpurl">WP URL</label>
                        <div class="col-md-4">                           
                            <input type="text" id="wpurl" name="wpurl" class="form-control" placeholder="Enter wordpress URL">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="wpusername">WP Username</label>
                        <div class="col-md-4">                           
                            <input type="text" id="wpusername" name="wpusername" class="form-control" placeholder="Enter wordpress username">
                        </div>

                        <label class="col-md-2 control-label" for="wppassword">WP Password</label>
                        <div class="col-md-4">                           
                            <input type="text" id="wppassword" name="wppassword" class="form-control" placeholder="Enter wordpress password">
                        </div>
                    </div>

                     <div class="form-group">
                        <label class="col-md-2 control-label" for="ftpusername">FTP Username</label>
                        <div class="col-md-4">                           
                            <input type="text" id="ftpusername" name="ftpusername" class="form-control" placeholder="Enter wordpress username">
                        </div>

                        <label class="col-md-2 control-label" for="ftppassword">FTP Password</label>
                        <div class="col-md-4">                           
                            <input type="text" id="ftppassword" name="ftppassword" class="form-control" placeholder="Enter wordpress password">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="cpusername">CP Username</label>
                        <div class="col-md-4">                           
                            <input type="text" id="cpusername" name="cpusername" class="form-control" placeholder="Enter wordpress username">
                        </div>

                        <label class="col-md-2 control-label" for="cppassword">CP Password</label>
                        <div class="col-md-4">                           
                            <input type="text" id="cppassword" name="cppassword" class="form-control" placeholder="Enter wordpress password">
                        </div>
                    </div>
                
                    <div class="form-group form-actions">
                        <input type="hidden" name="id" id="id" value="0" />

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success primary-btn pull-right"><i class="fa fa-angle-right"></i> Submit</button>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Block -->
		    </div>
            <!-- END Example Content -->
        </div>
        <!-- END Example Block -->
    </div>

    <!-- Table Block -->
    <div class="block full">
        <!-- Table Title -->
        <div class="block-title">
            <h2>Marketing</h2><small>Marketing currently exist in the system</small>
        </div>
        <!-- END Table Title -->

        <!-- Table Content -->
        <div class="table-responsive"><table id="table-data" class="table table-condensed table-striped table-hover"></table></div>
        <!-- END Table Content -->
    </div>
    <!-- END Table Block -->
<!-- END Page Content -->
</div>

<?php include $prefix.'page_footer.php'; ?>
<?php include $prefix.'template_scripts.php'; ?>
<?php include $prefix.'template_end.php'; ?>

<script type="text/javascript">

	/*********** Data-table Initialize ***********/
    $('#main_category').on('change', function(){
        var cato = $('#main_category').val();

        $.ajax({
            url: 'data/data_provider.php',
            data: {
                main_category: cato
            },
            method: 'post',
            error: function(e){
                alert ('Error requesting provider data');
            },
            success: function(r){
                $('#main_provider').html('<option value=""></option>');

                if (r.result){
                    if (r.data.length > 0){
                        $.each(r.data, function (k, v){
                            let option_markup = "";

                            option_markup += "<option value='"+v.id+"'>";
                            option_markup += v.provider;
                            option_markup += "</option>";

                            $('#main_provider').append(option_markup)
                            
                        });
                    }
                }

                $('#main_provider').trigger("chosen:updated");
            }
        });
    });

    App.datatables();

    var dt = $('#table-data').DataTable({
        "processing": true,
        "serverSide": true,
        "select": true,
        "columns": [
			{ "data": "category", "name": "category", "title": "Category" },
			{ "data": "provider", "name": "provider", "title": "Provider" },
            { "data": "location", "name": "location", "title": "Location"}, 
            { "data": "expdate", "name": "expdate", "title": "expdate"},
            { "data": "dbname", "name": "dbname", "title": "DB Name"},
            { "data": "dbusername", "name": "dbusername", "title": "DB Username"},
            { "data": "dbpassword", "name": "dbpassword", "title": "DB Password"},
            { "data": "wpurl", "name": "wpurl", "title": "WP URL"},
            { "data": "wpusername", "name": "wpusername", "title": "WP Username"},
            { "data": "wppassword", "name": "wppassword", "title": "WP Password"},
            { "data": "cpusername", "name": "cpusername", "title": "CP Username"},
            { "data": "cppassword", "name": "cppassword", "title": "CP Password"},
            { "data": "ftpusername", "name": "ftpusername", "title": "FTP Username"},
            { "data": "ftppassword", "name": "ftppassword", "title": "FTP Password"},
            {"data": "actions", "name": "actions","title":"Actions", "searchable": false, "orderable": false, 
                mRender: function (data, type, row) {
                    return '<div class="btn-group btn-group-xs"><button id="btn-row-edit" class="btn btn-primary" title="Edit"><i class="fa fa-pencil"></i></button></div>'
                }
            }
        ],
        "columnDefs":[
            {"className": "dt-center", "targets": [1,2,3]}
        ],
        "language": {
            "emptyTable": "No providers to show..."
        },
        "ajax": "data/grid_data_records.php"
    });
	
    $('.dataTables_filter input').attr('placeholder', 'Search');

    $("#table-data tbody").on('click', '#btn-row-edit', function() {
        var str_id = $(this).closest('tr').attr('id');
        var arr_id = str_id.split("_");

        var row_id = arr_id[1];
       // alert (row_id);

        $.ajax({
            url: 'data/data_records.php',
            data: {
                id: row_id
            },
            method: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $('#table-data tbody #'+str_id+' #btn-row-edit').button('loading');
                NProgress.start();
            },
            error: function (e) {
                $.bootstrapGrowl('<h4>Error!</h4> <p>Error retrieving records data</p>', {
                    type: 'danger',
                    delay: 2500,
                    allow_dismiss: true
                });

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            },
            success: function (r) {
                if (!r.result) {
                    $.bootstrapGrowl('<h4>Error!</h4> <p>'+r.message+'</p>', {
                        type: 'danger',
                        delay: 2500,
                        allow_dismiss: true
                    });
                }else{
                    $('#id').val(r.data[0].recordid);
                    $('#location').val(r.data[0].location);
                    $('#main_category').val(r.data[0].categoryid);                        
                    $('#main_category').trigger("chosen:updated");
                     $('#expdate').val(r.data[0].expdate);

                    $('#main_provider').val(r.data[0].provider);                        
                     $('#main_provider').trigger("chosen:updated");

                    $('#dbname').val(r.data[0].dbname);
                    $('#dbusername').val(r.data[0].dbusername);
                    $('#dbpassword').val(r.data[0].dbpassword);
                    $('#wpurl').val(r.data[0].wpurl);
                    $('#wpusername').val(r.data[0].wpusername);
                    $('#wppassword').val(r.data[0].wppassword);

                    $('#cpusername').val(r.data[0].cpusername);
                    $('#cppassword').val(r.data[0].cppassword);
                    $('#ftpusername').val(r.data[0].ftpusername);
                    $('#ftppassword').val(r.data[0].ftppassword);
                }

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            }
        });
    });
    /*********** Table Control End ***********/

    /*********** Form Validation and Submission ***********/
	$('#form-main').on('submit', function (e){
		e.preventDefault();
		
		var id = $('#id').val();
		var op = (id == 0) ? "insert" : "update";
		
		var formdata = $('#form-main').serializeArray();
		formdata.push({'name':'operation','value':op});
		
		$.ajax({
			url: 'records_crud.php',
			data: formdata,
			success: function(r){
				var msg_typ = "info";
                var msg_txt = "";

                if (r.result){
                    msg_typ = 'success';
                    msg_txt = '<h4>Success!</h4> <p>Marketing saved</p>';

                    $('#form-main').trigger('reset');
                }else{
                    msg_typ = 'danger';
                    msg_txt = '<h4>Error!</h4> <p>'+r.message+'</p>';
                }

                $.bootstrapGrowl(msg_txt, {
                    type: msg_typ,
                    delay: 2500,
                    allow_dismiss: true
                });

                dt.ajax.reload();
                dt.draw();
			}
		});
	});

    $('#form-main').on('reset', function (e){
        $('#id').val("0");

        $('#main_category').val(0);
        $('#location').trigger("chosen:updated");

        $('#main_provider').val("");
        $('#status').trigger("chosen:updated");

        $('#location').val("");
        $('#username').val("");
        $('#password').val("");
        $('#expdate').val("");
    });
    /*********** Form Control End ***********/
		
	</script>
	
	<?php mysqli_close($con_main); ?>