<?php
header('Content-Type: application/json');

include ('../../config.php');

$where = "";
$responce = array();
$data = "";
$result = true;
$message = "";

$id = (isset($_REQUEST['id']) && $_REQUEST['id'] != NULL && !empty($_REQUEST['id'])) ? $_REQUEST['id'] : 0;
$cat = (isset($_REQUEST['main_category']) && $_REQUEST['main_category'] != NULL && !empty($_REQUEST['main_category'])) ? $_REQUEST['main_category'] : 0;
$pro = (isset($_REQUEST['main_provider']) && $_REQUEST['main_provider'] != NULL && !empty($_REQUEST['main_provider'])) ? $_REQUEST['main_provider'] : 0;
// $user =(isset($_REQUEST['username']) && $_REQUEST['username'] != NULL && !empty($_REQUEST['username'])) ? $_REQUEST['username'] : 0;
// $pass =(isset($_REQUEST['password']) && $_REQUEST['password'] != NULL && !empty($_REQUEST['password'])) ? $_REQUEST['password'] : 0;


$query = "SELECT
host_records.recordid,
host_records.categoryid,
host_records.providerid,
host_records.location,
host_records.expdate,
host_records.dbname,
host_records.dbusername,
host_records.`dbpassword`,
host_records.wpurl,
host_records.wpusername,
host_records.wppassword,
host_records.ftpusername,
host_records.ftppassword,
host_records.cpusername,
host_records.cppassword,
host_provider.provider
FROM
host_records
INNER JOIN host_provider ON host_records.providerid = host_provider.id";

if ($id > 0){
    $where .= (empty($where)) ? " WHERE " : " AND ";
    $where .= " host_records.recordid = '$id' ";
}


if ($cat > 0){
    $where .= (empty($where)) ? " WHERE " : " AND ";
    $where .= " host_records.categoryid = '$cat' ";
}

 if($pro > 0){
    $where .= (empty($where)) ? " WHERE " : " AND ";
    $where .= " host_records.providerid = '$pro' ";
}

// if($user > 0){
//     $where .= (empty($where)) ? " WHERE " : " AND ";
//     $where .= " host_records.username = '$user' ";
// }

// if($pass > 0){
//     $where .= (empty($where)) ? " WHERE " : " AND ";
//     $where .= " host_records.password = '$pass' ";
// }


$query = $query.$where;

$sql = mysqli_query ($con_main, $query);

if (!$sql){
    $result = false;
    $message .= " Error Sql : (".mysqli_errno($con_main).") ".mysqli_error($con_main).". ";
}

$num_rows = mysqli_num_rows($sql);

if ($num_rows > 0){
    $i = 0;

    while ($rows = mysqli_fetch_assoc ($sql)){
        $data[$i] = $rows;
        $i++;
    }
}else{
    $result = false;
    $message .= " Empty results ";
}

$responce['data'] = $data;
$responce['result'] = $result;
$responce['message'] = $message;

echo (json_encode($responce));

mysqli_close($con_main);
?>