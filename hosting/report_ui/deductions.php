<?php
require_once ('../../config.php');
?>
		
		<div class="form-group">
			<label class="col-md-2 control-label" for="cat">Category</label>
				<div class="col-md-4">
					<select id="cat" name="cat" class="form-control" size="1">
						<option value="0" disabled selected>Select Category</option>
						<?php
						$query="SELECT
                                 host_category.id,
                                 host_category.category_name,
                                 host_category.`status`
                                FROM
                                `host_category`
                                WHERE
                                host_category.`status` = '1'";
                               $sql = mysqli_query($con_main, $query);                                                                            
                               while ($type = mysqli_fetch_array($sql)){
                                   echo ("<option value=\"".$type['id']."\">".$type['category_name']."</option>");
                               }
						?>
					</select>
				</div>

			<label class="col-md-2 control-label" for="pro">Provider</label>
				<div class="col-md-4">
					<select id="pro" name="pro" class="form-control" size="1">
						<option value="0" disabled selected>Select provider</option>
					</select>
				</div>

				
		</div>

		<div class="form-group">
			<label class="col-md-2 control-label" for="expdate">Expire date</label>
                        <div class="col-md-4">                           
                            <input type="text" id="expdate" name="expdate" class="form-control input-datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                        </div>
		</div>
		
		<div class="form-group form-actions">
			<input type="hidden" name="report_url" id="report_url" value="reports/monthly_deduction.php" />

			<div class="col-md-12">
				<button type="submit" class="btn btn-success primary-btn pull-right"><i class="fa fa-angle-right"></i> Submit</button>
				<button type="reset" class="btn btn-warning"><i class="fa fa-repeat"></i> Reset</button>
			</div>
		</div>
	<?php
	mysqli_close($con_main);
	?>

	<script type="text/javascript">
		$('#cat').on('change', function(){
        var cato = $('#cat').val();

        $.ajax({
            url: 'reports/data_provider.php',
            data: {
                main_category: cato
            },
            method: 'post',
            error: function(e){
                alert ('Error requesting provider data');
            },
            success: function(r){
                $('#pro').html('<option value=""></option>');

                if (r.result){
                    if (r.data.length > 0){
                        $.each(r.data, function (k, v){
                            let option_markup = "";

                            option_markup += "<option value='"+v.id+"'>";
                            option_markup += v.provider;
                            option_markup += "</option>";

                            $('#pro').append(option_markup)
                            
                        });
                    }
                }

                $('#pro').trigger("chosen:updated");
            }
        });
    });
	</script>