<?php
require_once ('../../config.php');

$cat = $_REQUEST['cat'];
$pro = $_REQUEST['pro'];
$expdate = $_REQUEST['expdate'];

$query = "SELECT
	host_records.location,
	host_records.username,
	host_records.`password`,
	host_records.expdate,
	host_provider.provider,
	host_category.category_name
FROM
	host_records
INNER JOIN host_category ON host_records.categoryid = host_category.id
INNER JOIN host_provider ON host_records.providerid = host_provider.id
WHERE
	host_records.categoryid = $cat
AND host_records.providerid = $pro";

?>

					<table style="border-collapse:collapse;" border="1" id="report" width="75%">
						<thead>
						<tr>
							<th>Category</th>
							<th>Provider</th>
							<th>Location</th>
							<th>User Name</th>
							<th>Password</th>
							<th>Expire Date</th>
						</tr>
						</thead>

						<tbody>
		<?php
			$sql = mysqli_query ($con_main, $query);
		
			while ($row = mysqli_fetch_array ($sql)){
		?>
							<tr>
								<td><?php echo ($row['category_name']); ?></td>
								<td><?php echo ($row['provider']); ?></td>
								<td><?php echo ($row['location']); ?></td>
								<td><?php echo ($row['username']); ?></td>
								<td><?php echo ($row['password']); ?></td>
								<td><?php echo ($row['expdate']); ?></td>
							</tr>
	<?php
							}
	?>
					</tbody>	
		        </table>