<?php
require_once ('../../config.php');

$epfNo = $_REQUEST['epfNo'];
$sp = $_REQUEST['sp'];
$contype = $_REQUEST['conType'];
$mobileNo = $_REQUEST['mobileNo'];

$query = "";
$where = "";

if (!empty($sp)){
	$where .= (empty($where)) ? " WHERE " : " AND ";
	$where .= " MN.SERVICE_PROVIDER = '$sp' ";
}

if (!empty($epfNo)){
	$where .= (empty($where)) ? " WHERE " : " AND ";
	$where .= " MU.EMP_NO = '$epfNo' ";
}

if (!empty($mobileNo)){
	$where .= (empty($where)) ? " WHERE " : " AND ";
	$where .= " MN.NUMBER LIKE '%$mobileNo%' ";
}

if (!empty($contype)){
	$where .= (empty($where)) ? " WHERE " : " AND ";
	$where .= " MN.CON_TYPE = '$contype' ";
}

$query = "SELECT
MN.NUMBER,
MSP.SHORT_NAME,
MCT.CON_TYPE,
MN.`LIMIT`,
IF(MN.UNLIMITED=1,'Yes','No') AS UNLIMITED_STATUS,
MU.EMP_NO,
CONCAT_WS(' ',MU.FIRST_NAME,MU.LAST_NAME) AS EMP_NAME,
MN.CONNECTION_DATE,
MN.REMARKS,
IF(MN.`STATUS`=1,'Active','Inactive') AS STATUS_TEXT,
MD.DEPARTMENT
FROM
mobi_number AS MN
INNER JOIN mobi_service_provider AS MSP ON MN.SERVICE_PROVIDER = MSP.SP_ID
INNER JOIN mobi_con_type AS MCT ON MN.CON_TYPE = MCT.CON_TYPE_ID
INNER JOIN mas_user AS MU ON MN.USER_ID = MU.USER_CODE
INNER JOIN mas_department AS MD ON MU.DEPARTMENT = MD.DEP_CODE";

$query .= $where;

?>

					<table style="border-collapse:collapse;" border="1" id="report" width="75%">
						<thead>
						<tr>
							<th>Employee No</th>
							<th>Employee Name</th>
							<th>Department</th>
							<th>Mobile No</th>
							<th>Service Provider</th>
							<th>Connection Type</th>
							<th>Limit</th>
							<th>Unlimited</th>
							<th>Connection Date</th>
							<th>Remarks</th>
							<th>Status</th>
						</tr>
						</thead>
						<tbody>
		<?php
			$sql = mysqli_query ($con_main, $query);
		
			while ($row = mysqli_fetch_array ($sql)){
		?>
							<tr>
								<td><?php echo ($row['EMP_NO']); ?></td>
								<td><?php echo ($row['EMP_NAME']); ?></td>
								<td><?php echo ($row['DEPARTMENT']); ?></td>
								<td><?php echo ($row['NUMBER']); ?></td>
								<td><?php echo ($row['SHORT_NAME']); ?></td>
								<td><?php echo ($row['CON_TYPE']); ?></td>
								<td><?php echo (number_format($row['LIMIT'],2)); ?></td>
								<td><?php echo ($row['UNLIMITED_STATUS']); ?></td>
								<td><?php echo ($row['CONNECTION_DATE']); ?></td>
								<td><?php echo ($row['REMARKS']); ?></td>
								<td><?php echo ($row['STATUS_TEXT']); ?></td>

							</tr>
	<?php
							}
	?>
						</tbody>	
		</table>