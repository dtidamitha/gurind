<?php
	session_start();
	
	if (empty($_SESSION['ACCESS_CODE']) || $_SESSION['ACCESS_CODE'] == NULL){
		header ('Location: login.php');
		exit;
	}
	
	$folder_depth = "";
	$prefix = "";
	
	$folder_depth = substr_count($_SERVER["PHP_SELF"] , "/");
	$folder_depth = ($folder_depth == false) ? 2 : (int)$folder_depth;
	
    $prefix = str_repeat("../", $folder_depth - 2);
    
    $title_suffix = "GRN";
?>
<?php include $prefix.'config.php'; ?>
<?php include $prefix.'menu.php'; ?>
<?php include $prefix.'template_start.php'; ?>
<?php include $prefix.'page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-podium"></i>Supplier/Product<br>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="../home.php">Home</a></li>
        <li>Master Files</li>
        <li>Supplier Product Master</li>
    </ul>
    <!-- END Blank Header -->
		
	
    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
					<h2>Supplier/Product</h2>
				</div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <form id="form-main" name="form-main" action="provider_crud.php" method="post"  class="form-horizontal form-bordered">  

                <div class="form-group">                      
                        <label class="col-md-2 control-label" for="supcode">Supplier Code</label>
                         <div class="col-md-4">
                            <select id="supcode" name="supcode" class="select-chosen" data-placeholder="Select Supplier"> 
                                   <option></option>
                                   <?php
                                    $query="SELECT
                                                mas_supplier.ID,
                                                mas_supplier.SupplierCode,
                                                mas_supplier.ShortName
                                            FROM
                                                `mas_supplier`";
                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['ID']."\">".$type['SupplierCode']."-".$type['ShortName']."</option>");
                                    }
                                   ?>
                               </select>
                        </div>  

                        <label class="col-md-2 control-label" for="colorcode">Color Code</label>
                        <div class="col-md-4">
                            <select id="colorcode" name="colorcode" class="select-chosen" data-placeholder="Select Color"> 
                                <option></option>
                                   <?php
                                    $query="SELECT
                                                mas_color.ID,
                                                mas_color.ColorCode,
                                                mas_color.Color
                                            FROM
                                                `mas_color`";
                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['ID']."\">".$type['ColorCode']."-".$type['Color']."</option>");
                                    }
                                   ?>
                            </select>
                        </div>             
                </div>
                       
                <div class="form-group">  
                           <label class="col-md-2 control-label" for="solartrans">Solar Transmission</label>        
                        <div class="col-md-4">                         
                            <input type="text" id="solartrans" name="solartrans" class="form-control" placeholder="Enter Solar Transmission">
                        </div>

                         <label class="col-md-2 control-label" for="uvalue">UValue</label>
                        <div class="col-md-4">
                            <input type="text" id="uvalue" name="uvalue" class="form-control" placeholder="Enter Uvalue">
                        </div>  
                </div>


                <div class="form-group">  
                           <label class="col-md-2 control-label" for="externalref">External Reflection</label>        
                        <div class="col-md-4">
                            <input type="text" id="externalref" name="externalref" class="form-control" placeholder="Enter external reflection">
                        </div>  

                         <label class="col-md-2 control-label" for="internalref">Internal Reflection</label>
                        <div class="col-md-4">                           
                            <input type="text" id="internalref" name="internalref" class="form-control" placeholder="Enter internal reflection">
                        </div>
                </div>
                    
                    <div class="form-group">
                      <label class="col-md-2 control-label" for="file_main">Attached Tec.Card</label>              
                       <div class="col-md-4">
                            <input type="file" name="file_main" id="file_main">
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <input type="hidden" name="id" id="id" value="0" />

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success primary-btn pull-right"><i class="fa fa-angle-right"></i> Submit</button>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                </form>     
		    </div>      
        </div>
    </div>

    <div class="block full">
        <div class="block-title">
            <h2>Supplier/Product</h2><small>Supplier/Products currently exist in the system</small>
        </div>
        <div class="table-responsive"><table id="table-data" class="table table-condensed table-striped table-hover"></table></div>    
    </div>
</div>

<?php include $prefix.'page_footer.php'; ?>
<?php include $prefix.'template_scripts.php'; ?>
<?php include $prefix.'template_end.php'; ?>

<script type="text/javascript">

    App.datatables();

    var dt = $('#table-data').DataTable({
        "processing": true,
        "serverSide": true,
        "select": true,
        "columns": [
			{ "data": "supcode", "name": "supcode", "title": "Supplier Code" },
			{ "data": "colorcode", "name": "colorcode", "title": "Color Code" },
            { "data": "solartrans", "name": "solartrans", "title": "Solar Transmission"}, 
            { "data": "uvalue", "name": "uvalue", "title": "UValue"},
            { "data": "externalref", "name": "externalref", "title":"External Reflection"},
            { "data": "internalref", "name": "internalref", "title": "Internal Reflection"},
            {"data": "actions", "name": "actions","title":"Actions", "searchable": false, "orderable": false, 
                mRender: function (data, type, row) {
                    return '<div class="btn-group btn-group-xs"><button id="btn-row-edit" class="btn btn-primary" title="Edit"><i class="fa fa-pencil"></i></button></div>'
                }
            }
        ],
        "columnDefs":[
            {"className": "dt-center", "targets": [1,2,3]}
        ],
        "language": {
            "emptyTable": "No data to show..."
        },
        "ajax": "data/grid_supplier_product.php"
    });
	
    $('.dataTables_filter input').attr('placeholder', 'Search');

    $("#table-data tbody").on('click', '#btn-row-edit', function() {
        var str_id = $(this).closest('tr').attr('id');
        var arr_id = str_id.split("_");

        var row_id = arr_id[1];
       // alert (row_id);

        $.ajax({
            url: 'data/data_supplier_product.php',
            data: {
                id: row_id
            },
            method: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $('#table-data tbody #'+str_id+' #btn-row-edit').button('loading');
                NProgress.start();
            },
            error: function (e) {
                $.bootstrapGrowl('<h4>Error!</h4> <p>Error retrieving data</p>', {
                    type: 'danger',
                    delay: 2500,
                    allow_dismiss: true
                });

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            },
            success: function (r) {
                if (!r.result) {
                    $.bootstrapGrowl('<h4>Error!</h4> <p>'+r.message+'</p>', {
                        type: 'danger',
                        delay: 2500,
                        allow_dismiss: true
                    });
                }else{
                    $('#id').val(r.data[0].ID);
                    $('#supcode').val(r.data[0].SupplierID);
                    $('#supcode').trigger("chosen:updated");

                    $('#colorcode').val(r.data[0].ColorID);
                    $('#colorcode').trigger("chosen:updated");

                    $('#solartrans').val(r.data[0].SolarTrans);
                    $('#uvalue').val(r.data[0].Uvalue);
                    $('#externalref').val(r.data[0].ExternalRef);
                    $('#internalref').val(r.data[0].InternalRef);                        
                }

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            }
        });
    });
    /*********** Table Control End ***********/

    /*********** Form Validation and Submission ***********/
	$('#form-main').on('submit', function (e){
		e.preventDefault();
		
		var id = $('#id').val();
		var op = (id == 0) ? "insert" : "update";
		
		var formdata = $('#form-main').serializeArray();
		formdata.push({'name':'operation','value':op});
		
		$.ajax({
			url: 'supplier_product_crud.php',
			data: formdata,
			success: function(r){
				var msg_typ = "info";
                var msg_txt = "";

                if (r.result){
                    msg_typ = 'success';
                    msg_txt = '<h4>Success!</h4> <p>Data saved</p>';

                    $('#form-main').trigger('reset');
                }else{
                    msg_typ = 'danger';
                    msg_txt = '<h4>Error!</h4> <p>'+r.message+'</p>';
                }

                $.bootstrapGrowl(msg_txt, {
                    type: msg_typ,
                    delay: 2500,
                    allow_dismiss: true
                });

                dt.ajax.reload();
                dt.draw();
			}
		});
	});

    $('#form-main').on('reset', function (e){
        $('#id').val("0");
        
        $('#supcode').val("");
        $('#supcode').trigger("chosen:updated");

        $('#colorcode').val("");
        $('#colorcode').trigger("chosen:updated");
        $('#solartrans').val("");
        $('#uvalue').val("");
        $('#externalref').val("");
        $('#internalref').val("");
        $('#file_main').val("");
    });
	</script>
	<?php mysqli_close($con_main); ?>