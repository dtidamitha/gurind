<?php
	header('Content-Type: application/json');
	
	session_start();
	
	require_once ('../config.php');
	
	$op = $_REQUEST['operation']; 
	$id = $_REQUEST['id'];  
	$supcode = $_REQUEST['supcode'];
	$supname = $_REQUEST['supname'];
	$billname = $_REQUEST['billname'];
	$shortname = $_REQUEST['shortname']; 
	$sourcecountry = $_REQUEST['sourcecountry']; 
	$country = $_REQUEST['country']; 
	$address1 = $_REQUEST['address1']; 
	$address2 = $_REQUEST['address2'];
    $city = $_REQUEST['city'];
    $province = $_REQUEST['province'];
    $postcode = $_REQUEST['postcode'];
    $supnote = $_REQUEST['supnote'];
    $contact1_name = $_REQUEST['contact1_name'];
    $contact1_jobrole = $_REQUEST['contact1_jobrole'];
    $contact1_tele = $_REQUEST['contact1_tele'];
    $contact1_fax = $_REQUEST['contact1_fax'];
    $contact1_email = $_REQUEST['contact1_email'];
    $contact2_name = $_REQUEST['contact2_name'];
    $contact2_jobrole = $_REQUEST['contact2_jobrole'];
    $contact2_tele = $_REQUEST['contact2_tele'];
    $contact2_fax = $_REQUEST['contact2_fax'];
    $contact2_email = $_REQUEST['contact2_email'];
    $contact3_name = $_REQUEST['contact3_name'];
    $contact3_jobrole = $_REQUEST['contact3_jobrole'];
    $contact3_tele = $_REQUEST['contact3_tele'];
    $contact3_fax = $_REQUEST['contact3_fax'];
    $contact3_email = $_REQUEST['contact3_email'];
    $resigned = $_REQUEST['resigned'];
    $status = $_REQUEST['status'];
    $user = $_SESSION['USER_CODE'];
	
	$query = ""; 
	$success = true;
	$message = "";
	$responce = array();
	
	if ($op == "insert"){

$query = "INSERT INTO `mas_supplier` (
	`SupplierCode`,
	`SupplierName`,
	`BillingName`,
	`ShortName`,
	`SourcingCountry`,
	`Country`,
	`Address1`,
	`Address2`,
	`City`,
	`Province`,
	`PostCode`,
	`SupplierNotes`,
	`CP1_Name`,
	`CP1_JobRole`,
	`CP1_Phone`,
	`CP1_Fax`,
	`CP1_Email`,
	`CP2_Name`,
	`CP2_JobRole`,
	`CP2_Phone`,
	`CP2_Fax`,
	`CP2_Email`,
	`CP3_Name`,
	`CP3_JobRole`,
	`CP3_Phone`,
	`CP3_Fax`,
	`CP3_Email`,
	`ResignedDate`,
	`Status`,
	`EnteredBy`,
	`EnteredDate`
)
VALUES
	(
		'$supcode',
		'$supname',
		'$billname',
		'$shortname',
		'$sourcecountry',
		'$country',
		'$address1',
		'$address2',
		'$city',
		'$province',
		'$postcode',
		'$supnote',
		'$contact1_name',
		'$contact1_jobrole',
		'$contact1_tele',
		'$contact1_fax',
		'$contact1_email',
		'$contact2_name',
		'$contact2_jobrole',
		'$contact2_tele',
		'$contact2_fax',
		'$contact2_email',
		'$contact3_name',
		'$contact3_jobrole',
		'$contact3_tele',
		'$contact3_fax',
		'$contact3_email',
		'$resigned',
		'$status',
		'$user',
		NOW()
	);";
	}

	else if ($op == "update"){
		$query = "UPDATE `mas_supplier`
SET `SupplierCode` = '$supcode',
	 `SupplierName` = '$supname',
	 `BillingName` = '$billname',
	 `ShortName` = '$shortname',
	 `SourcingCountry` = '$sourcecountry',
	 `Country` = '$country',
	 `Address1` = '$address1',
	 `Address2` = '$address2',
	 `City` = '$city',
	 `Province` = '$province',
	 `PostCode` = '$postcode',
	 `SupplierNotes` = '$supnote',
	 `CP1_Name` = '$contact1_name',
	 `CP1_JobRole` = '$contact1_jobrole',
	 `CP1_Phone` = '$contact1_tele',
	 `CP1_Fax` = '$contact1_fax',
	 `CP1_Email` = '$contact1_email',
	 `CP2_Name` = '$contact2_name',
	 `CP2_JobRole` = '$contact2_jobrole',
	 `CP2_Phone` = '$contact2_tele',
	 `CP2_Fax` = '$contact2_fax',
	 `CP2_Email` = '$contact2_email',
	 `CP3_Name` = '$contact3_name',
	 `CP3_JobRole` = '$contact3_jobrole',
	 `CP3_Phone` = '$contact3_tele',
	 `CP3_Fax` = '$contact3_fax',
	 `CP3_Email` = '$contact3_email',
	 `ResignedDate` = '$resigned',
	 `Status` = '$status',
	 `EnteredBy` = '$user',
	 `EnteredDate` = NOW()
WHERE
	(`ID` = '$id');";
	}
	
	$sql = mysqli_query ($con_main, $query);
	
	$id = ($op == "insert") ? mysqli_insert_id($con_main) : $id;
	
	if ($sql){
		$success = true;
		$message = "Success";
	}else{
		$success = false;
		$message = "Error SQL: (".mysqli_errno($con_main).") ".mysqli_error($con_main);
	}
	
	$responce['operation'] = $op;
	$responce['result'] = $success;
	$responce['id'] = $id;
	$responce['message'] = $message;
	$responce['debug'] = $query;

	
	echo (json_encode($responce));

	
	mysqli_close($con_main);
?>