<?php
	session_start();
	
	if (empty($_SESSION['ACCESS_CODE']) || $_SESSION['ACCESS_CODE'] == NULL){
		header ('Location: login.php');
		exit;
	}
	
	$folder_depth = "";
	$prefix = "";
	
	$folder_depth = substr_count($_SERVER["PHP_SELF"] , "/");
	$folder_depth = ($folder_depth == false) ? 2 : (int)$folder_depth;
	
    $prefix = str_repeat("../", $folder_depth - 2);
    
    $title_suffix = "Finish Good Master";
?>
<?php include $prefix.'config.php'; ?>
<?php include $prefix.'menu.php'; ?>
<?php include $prefix.'template_start.php'; ?>
<?php include $prefix.'page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-podium"></i>Finish Good Master<br><small>Create, Update or Delete Finish Goods</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="../home.php">Home</a></li>
        <li>Master Files</li>
        <li>Finish Good Master</li>
    </ul>
    <!-- END Blank Header -->
		
	
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <div class="block-title">
					<h2>Finish Good</h2>
				</div>
                <form id="form-main" name="form-main" action="provider_crud.php" method="post"  class="form-horizontal form-bordered">  
                <div class="form-group">                      
                        <label class="col-md-2 control-label" for="production_type">Production Type</label>
                        <div class="col-md-4">
                            <select id="production_type" name="production_type" class="select-chosen" data-placeholder="Select production type"> 
                              <option selected disabled>Select production type</option>
                               <option value="1">SGU - Single Glaze Unit</option>
                               <option value="2">DGU - Double Glaze Unit</option>
                               <option value="3">TGU - Trible Glaze Unit</option>
                            </select>
                        </div>

                  <label class="col-md-2 control-label" for="item_code">Enter Item Code</label>
                     <div class="col-md-4">
                       <input type="text" id="item_code" name="item_code" class="form-control" placeholder="Enter item code">
                     </div>    
                </div>

                <div class="form-group">

                     <label class="col-md-2 control-label" for="item_desc">Enter Item Description</label>
                     <div class="col-md-4">
                       <textarea name="item_desc" id="item_desc" rows="4" cols="39" class="form-control" placeholder="Enter item description"></textarea> 
                     </div>

                    <div id="thickness1">
                     <label class="col-md-2 control-label" for="uom">Glass 1 UOM & Thickness</label>
                        <div class="col-md-4">
                            <select id="uom" name="uom" class="select-chosen" data-placeholder="Select glass 1 UOM and Thickness"> 
                                <option></option>
                                <?php
                                    $query=  "SELECT
                                                mas_thickness.ID,
                                                mas_thickness.UOM,
                                                mas_thickness.thickness
                                              FROM
                                                `mas_thickness`
                                              WHERE
                                                mas_thickness.`status` = '1'";
                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['ID']."\">".$type['UOM']." - ".$type['thickness']."</option>");
                                    }
                                   ?>
                            </select>   
                        </div>
                    </div>  
                </div>
                       
                <div class="form-group">  
                  <div id="color_id">
                           <label class="col-md-2 control-label" for="color">Select Glass 1 Color</label>        
                        <div class="col-md-4">                         
                            <select id="color" name="color" class="select-chosen" data-placeholder="Select glass 1 color"> 
                                <option></option>
                                <?php
                                    $query=  "SELECT
                                                mas_color.ID,
                                                mas_color.ColorCode,
                                                mas_color.Color
                                              FROM
                                                `mas_color`
                                              WHERE
                                                mas_color.`Status` = '1'";
                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['ID']."\">".$type['ColorCode']." - ".$type['Color']."</option>");
                                    }
                                   ?>
                            </select>   
                        </div>
                  </div>
 
                     <div id="air_id">
                      <label class="col-md-2 control-label" for="airgap1">Air Gap 1</label>
                            <div class="col-md-4">
                               <select id="airgap1" name="airgap1" class="select-chosen" data-placeholder="Select air gap 1"> 
                                  <option></option>
                                  <option value="6">6 mm</option>
                                  <option value="10">10 mm</option>
                                  <option value="12">12 mm</option>
                                  <option value="14">14 mm</option>
                              </select>  
                            </div> 
                        </div>  
                </div>

                 <div class="form-group">  
                  <div id="glass2_thickness">
                           <label class="col-md-2 control-label" for="thickness2">Glass 2 UOM & Thickness</label>        
                        <div class="col-md-4">                         
                            <select id="thickness2" name="thickness2" class="select-chosen" data-placeholder="Select glass 2 UOM and Thickness"> 
                                <option></option>
                                <?php
                                    $query=  "SELECT
                                                mas_thickness.ID,
                                                mas_thickness.UOM,
                                                mas_thickness.thickness
                                              FROM
                                                `mas_thickness`
                                              WHERE
                                                mas_thickness.`status` = '1'";
                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['ID']."\">".$type['UOM']." - ".$type['thickness']."</option>");
                                    }
                                   ?>
                            </select>   
                        </div>
                  </div>
 
                     <div id="color_id2">
                      <label class="col-md-2 control-label" for="color2">Select Glass 2 Color</label>
                            <div class="col-md-4">
                               <select id="color2" name="color2" class="select-chosen" data-placeholder="Select glass 2 color"> 
                                  <option></option>
                                   <?php
                                      $query=  "SELECT
                                                  mas_color.ID,
                                                  mas_color.ColorCode,
                                                  mas_color.Color
                                                FROM
                                                  `mas_color`
                                                WHERE
                                                  mas_color.`Status` = '1'";
                                      $sql = mysqli_query($con_main, $query);
                                                                              
                                      while ($type = mysqli_fetch_array($sql)){
                                          echo ("<option value=\"".$type['ID']."\">".$type['ColorCode']." - ".$type['Color']."</option>");
                                      }
                                     ?>
                              </select>  
                            </div> 
                        </div>  
                </div>

                 <div class="form-group">  
                  <div id="air_id2">
                      <label class="col-md-2 control-label" for="airgap2">Air Gap 2</label>
                            <div class="col-md-4">
                               <select id="airgap2" name="airgap2" class="select-chosen" data-placeholder="Select air gap 2"> 
                                   <option></option>
                                  <option value="6">6 mm</option>
                                  <option value="10">10 mm</option>
                                  <option value="12">12 mm</option>
                                  <option value="14">14 mm</option>
                              </select>  
                            </div> 
                        </div>  
 
                        <div id="glass3_thickness">
                           <label class="col-md-2 control-label" for="thickness3">Glass 3 UOM & Thickness</label>        
                        <div class="col-md-4">                         
                            <select id="thickness3" name="thickness3" class="select-chosen" data-placeholder="Select glass 3 UOM and Thickness"> 
                                <option></option>
                                <?php
                                    $query=  "SELECT
                                                mas_thickness.ID,
                                                mas_thickness.UOM,
                                                mas_thickness.thickness
                                              FROM
                                                `mas_thickness`
                                              WHERE
                                                mas_thickness.`status` = '1'";
                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['ID']."\">".$type['UOM']." - ".$type['thickness']."</option>");
                                    }
                                   ?>
                            </select>   
                        </div>
                  </div> 
                </div>

                <div class="form-group">
                    <div id="color_id3">
                      <label class="col-md-2 control-label" for="color3">Select Glass 3 Color</label>
                            <div class="col-md-4">
                               <select id="color3" name="color3" class="select-chosen" data-placeholder="Select glass 3 color"> 
                                  <option></option>
                                   <?php
                                      $query=  "SELECT
                                                  mas_color.ID,
                                                  mas_color.ColorCode,
                                                  mas_color.Color
                                                FROM
                                                  `mas_color`
                                                WHERE
                                                  mas_color.`Status` = '1'";
                                      $sql = mysqli_query($con_main, $query);
                                                                              
                                      while ($type = mysqli_fetch_array($sql)){
                                          echo ("<option value=\"".$type['ID']."\">".$type['ColorCode']." - ".$type['Color']."</option>");
                                      }
                                     ?>
                              </select>  
                            </div> 
                        </div> 
                </div>

                    <div class="form-group form-actions">
                        <input type="hidden" name="id" id="id" value="0" />

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success primary-btn pull-right"><i class="fa fa-angle-right"></i> Submit</button>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Block -->
		    </div>
            <!-- END Example Content -->
        </div>
        <!-- END Example Block -->
    </div>

    <!-- Table Block -->
    <div class="block full">
        <!-- Table Title -->
        <div class="block-title">
            <h2>Finish Goods</h2><small>Finish Goods currently exist in the system</small>
        </div>
        <!-- END Table Title -->

        <!-- Table Content -->
        <div class="table-responsive"><table id="table-data" class="table table-condensed table-striped table-hover"></table></div>
        <!-- END Table Content -->
    </div>
    <!-- END Table Block -->
<!-- END Page Content -->
</div>

<?php include $prefix.'page_footer.php'; ?>
<?php include $prefix.'template_scripts.php'; ?>
<?php include $prefix.'template_end.php'; ?>

<script type="text/javascript">

	/*********** Data-table Initialize ***********/

  $('#color_id').hide();
  $('#air_id').hide();
  $('#glass2_thickness').hide();
  $('#color_id2').hide();
  $('#glass3_thickness').hide();
  $('#color_id3').hide();
  $('#air_id2').hide();
  $('#thickness1').hide();

    $('#production_type').on('change', function(){

        var protype = $('#production_type').val();
        // alert(protype);

        if(protype == 1){
            $('#color_id').show();
            $('#thickness1').show();
            $('#air_id').hide();
            $('#glass2_thickness').hide();
            $('#color_id2').hide();
            $('#glass3_thickness').hide();
            $('#color_id3').hide();
            $('#air_id2').hide();
        }
        else if(protype == 2){

           $('#color_id').show();
           $('#air_id').show();
           $('#glass2_thickness').show();
           $('#color_id2').show();
           $('#glass3_thickness').hide();
           $('#color_id3').hide();
           $('#air_id2').hide();
        }

         else if(protype == 3){

           $('#color_id').show();
           $('#air_id').show();
           $('#glass2_thickness').show();
           $('#color_id2').show();
           $('#glass3_thickness').show();
           $('#color_id3').show();
           $('#air_id2').show();
        }

    });


    App.datatables();

   var dt = $('#table-data').DataTable({
        "processing": true,
        "serverSide": true,
        "select": true,
        "columns": [
            { "data": "category", "name": "category", "title": "Category"},
            { "data": "itemcode", "name": "itemcode", "title": "Item Code" },
            { "data": "itemname", "name": "itemname", "title": "Item Name"}, 
            { "data": "country", "name": "country", "title": "Country"},
            { "data": "brand", "name": "brand", "title":"Brand"},
            { "data": "materialtype", "name": "materialtype", "title": "Material Type"},
            {"data": "actions", "name": "actions","title":"Actions", "searchable": false, "orderable": false, 
                mRender: function (data, type, row) {
                    return '<div class="btn-group btn-group-xs"><button id="btn-row-edit" class="btn btn-primary" title="Edit"><i class="fa fa-pencil"></i></button></div>'
                }
            }
        ],
        "columnDefs":[
            {"className": "dt-center", "targets": [1,2,3]}
        ],
        "language": {
            "emptyTable": "No suppliers to show..."
        },
        "ajax": "data/grid_data_item.php"
    });
	
    $('.dataTables_filter input').attr('placeholder', 'Search');

    $("#table-data tbody").on('click', '#btn-row-edit', function() {
        var str_id = $(this).closest('tr').attr('id');
        var arr_id = str_id.split("_");

        var row_id = arr_id[1];
       // alert (row_id);

        $.ajax({
            url: 'data/data_item.php',
            data: {
                id: row_id
            },
            method: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $('#table-data tbody #'+str_id+' #btn-row-edit').button('loading');
                NProgress.start();
            },
            error: function (e) {
                $.bootstrapGrowl('<h4>Error!</h4> <p>Error retrieving records data</p>', {
                    type: 'danger',
                    delay: 2500,
                    allow_dismiss: true
                });

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            },
            success: function (r) {
                if (!r.result) {
                    $.bootstrapGrowl('<h4>Error!</h4> <p>'+r.message+'</p>', {
                        type: 'danger',
                        delay: 2500,
                        allow_dismiss: true
                    });
                }else{
                    $('#id').val(r.data[0].id);
                    $('#brand').val(r.data[0].proposal_no); 
                    $('#itemname').val(r.data[0].proposal_no); 
                    $('#itemcode').val(r.data[0].proposal_no); 

                    $('#category').val(r.data[0].stage);  
                    $('#category').trigger("chosen:updated"); 

                    $('#country').val(r.data[0].stage);  
                    $('#country').trigger("chosen:updated"); 

                    $('#materialtype').val(r.data[0].stage);  
                    $('#materialtype').trigger("chosen:updated");                          
                }

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            }
        });
    });
    /*********** Table Control End ***********/

    /*********** Form Validation and Submission ***********/
	$('#form-main').on('submit', function (e){
		e.preventDefault();
		
		var id = $('#id').val();
		var op = (id == 0) ? "insert" : "update";
		
		var formdata = $('#form-main').serializeArray();
		formdata.push({'name':'operation','value':op});
		
		$.ajax({
			url: 'item_crud.php',
			data: formdata,
			success: function(r){
				var msg_typ = "info";
                var msg_txt = "";

                if (r.result){
                    msg_typ = 'success';
                    msg_txt = '<h4>Success!</h4> <p>Item saved</p>';

                    $('#form-main').trigger('reset');
                }else{
                    msg_typ = 'danger';
                    msg_txt = '<h4>Error!</h4> <p>'+r.message+'</p>';
                }

                $.bootstrapGrowl(msg_txt, {
                    type: msg_typ,
                    delay: 2500,
                    allow_dismiss: true
                });

                dt.ajax.reload();
                dt.draw();
			}
		});
	});

    $('#form-main').on('reset', function (e){

        $('#id').val("0");
        $('#brand').val(""); 
        $('#itemname').val("");  
        $('#itemcode').val(""); 

        $('#category').val("");  
        $('#category').trigger("chosen:updated"); 

        $('#country').val("");  
        $('#country').trigger("chosen:updated"); 

        $('#materialtype').val("");  
        $('#materialtype').trigger("chosen:updated"); 
    });
    /*********** Form Control End ***********/
		
	</script>
	
	<?php mysqli_close($con_main); ?>