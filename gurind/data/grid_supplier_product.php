<?php
	header('Content-Type: application/json');
	
	include ('../../config.php');
	
	$result_array = array();
	
	$draw = $_REQUEST['draw'];
	$length = $_REQUEST['length'];
	$start = $_REQUEST['start'];
	$columns = $_REQUEST['columns'];
	$order_cols = $_REQUEST['order'];
	$search = $_REQUEST['search'];
	$data = array();
	
	// Column arrangement in grid. index to table column mapping array
	$col_set = array('T0.SolarTrans','T0.Uvalue','T0.ExternalRef','T0.InternalRef','T1.SupplierCode','T1.ShortName','T2.ColorCode');
	
	// Where and Order By Clause string
	$order_by = "";
	$where = "";
	
	// Generate order by string
	// Loop over all ordered fields (inside array) received from datatable client
	foreach ($order_cols as $order_col){
		// if order by string is empty start order by clause with ORDER BY else append order by with comma
		$order_by .= (empty($order_by)) ? " ORDER BY " : ", ";
		
		$order_col_index = $order_col['column']; // Order column index no
		$order_col_dir = $order_col['dir']; // Order direction (asc/desc)
		
		// Generate order by string (convert index to table column + direction)
		$order_by .= $col_set[$order_col_index]." ".$order_col_dir." ";
	}
	
	// Generate where clause if search term is not empty (received with client's request)
	// if (!empty($search['value'])){
	// 	// get search term
	// 	$term = $search['value'];
		
	// 	$i = 0;
		
	// 	// Loop over available columns
	// 	foreach ($columns as $column){
	// 		// Get current column name and searchable (true/false)
	// 		$col_name = $column['name'];
	// 		$col_searchable = $column['searchable'];
			
	// 		// If current column is searchable
	// 		if ($col_searchable == "true"){
	// 			// if where string is empty start where clause with WHERE else append where string with OR
	// 			$where .= (empty($where)) ? " WHERE " : " OR ";

	// 			// Convert column index to table column + LIKE + 'search term'
	// 			$where .= $col_set[$i]." LIKE '%".$term."%' ";
	// 			$where .= $col_set[$i]." LIKE '%$term%' ";
	// 		}
			
	// 		$i = $i + 1;
	// 	}
	// }

if (empty($order_by)){
	    $order_by = " ORDER BY T0.ID DESC ";
	}

if (!empty($search['value'])){
		// get search term
		$term = $search['value'];
		
		$i = 0;
		
		// Loop over available columns
		foreach ($columns as $column){
			// Get current column name and searchable (true/false)
			$col_name = $column['name'];
			$col_searchable = $column['searchable'];
			
			// If current column is searchable
			if ($col_searchable == "true"){
			    // if where string is empty start where clause with WHERE else append where string with OR
				$where .= (empty($where)) ? " WHERE " : " OR ";
				
				if ($i == 5){
					// Convert column index to table column + LIKE + 'search term'
					$where .= " T0.SolarTrans LIKE '%$term%' ";
				}else if($i == 6){
				    $where .= " T0.Uvalue LIKE '%$term%' ";
				}else if($i == 7){
				    $where .= " T0.ExternalRef LIKE '%$term%' ";
				}
				else if($i == 8){
				    $where .= " T0.InternalRef LIKE '%$term%' ";
				}
				else if($i == 9){
				    $where .= " T1.SupplierCode LIKE '%$term%' ";
				}
				else if($i == 10){
				    $where .= " T1.ShortName LIKE '%$term%' ";
				}
				else if($i == 10){
				    $where .= " T2.ColorCode LIKE '%$term%' ";
				}else{
					$where .= $col_set[$i]." LIKE '%$term%' ";
				}
			}
			
			$i = $i + 1;
		}
	}

$query =   "SELECT
				T1.ShortName,
				T2.ColorCode,
				T0.ID,
				T0.SolarTrans,
				T0.Uvalue,
				T0.ExternalRef,
				T0.InternalRef,
				T0.File,
				T1.SupplierCode
			FROM
				mas_supplier_product AS T0
			INNER JOIN mas_supplier AS T1 ON T0.SupplierID = T1.ID
			INNER JOIN mas_color AS T2 ON T0.ColorID = T2.ID";

	$sql = mysqli_query($con_main, $query);
	
	$i = 0;

	$result_array['data'] = "";
	
	while ($row = mysqli_fetch_assoc($sql)){
		

		$data['DT_RowId'] = "row_".$row['ID'];
		$data['supcode'] = $row['SupplierCode'];
		$data['colorcode'] = $row['ColorCode'];
		$data['solartrans'] = $row['SolarTrans'];
		$data['uvalue'] = $row['Uvalue'];
		$data['externalref'] = $row['ExternalRef'];
		$data['internalref'] = $row['InternalRef'];
		$result_array['data'][$i] = $data;
		$i = $i + 1;
	}

    $filtered_row_count_query = "SELECT
									Count(mas_supplier_product.ID) AS ROW_COUNT,
									mas_supplier_product.SupplierID,
									mas_supplier_product.ColorID
								FROM
									`mas_supplier_product` ".$where;

$filtered_row_count_sql = mysqli_query($con_main, $filtered_row_count_query);
$filtered_row_count = mysqli_fetch_assoc($filtered_row_count_sql);
$filtered_records = $filtered_row_count['ROW_COUNT'];

$full_row_count_sql = mysqli_query($con_main, "SELECT COUNT(T0.ID) AS C FROM mas_supplier_product AS T0");
$full_row_count = mysqli_fetch_array($full_row_count_sql);

$total_records = $full_row_count['C'];


	$result_array['draw'] = $draw; // Return same draw id received from datatable client request
	$result_array['recordsTotal'] = $total_records; // Return total record count in table
	$result_array['recordsFiltered'] = (!empty($search['value'])) ? $filtered_records : $total_records; //
	mysqli_close($con_main);
	
	echo (json_encode($result_array));
?>