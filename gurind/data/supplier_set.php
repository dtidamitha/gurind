<?php
	require ('../../config.php');
	
	$crn_id = $_REQUEST['id'];
	
	$result_array = array();
	$result = false;
	$message = "";
	$debug = "";
	
	$emp_query =  "SELECT
						mas_supplier.ID,
						mas_supplier.SupplierCode,
						mas_supplier.SupplierName,
						mas_supplier.BillingName,
						mas_supplier.ShortName,
						mas_supplier.SourcingCountry,
						mas_supplier.Country,
						mas_supplier.Address1,
						mas_supplier.Address2,
						mas_supplier.City,
						mas_supplier.Province,
						mas_supplier.PostCode,
						mas_supplier.SupplierNotes,
						mas_supplier.CP1_Name,
						mas_supplier.CP1_JobRole,
						mas_supplier.CP1_Phone,
						mas_supplier.CP1_Fax,
						mas_supplier.CP1_Email,
						mas_supplier.CP2_Name,
						mas_supplier.CP2_JobRole,
						mas_supplier.CP2_Phone,
						mas_supplier.CP2_Fax,
						mas_supplier.CP2_Email,
						mas_supplier.CP3_Name,
						mas_supplier.CP3_JobRole,
						mas_supplier.CP3_Phone,
						mas_supplier.CP3_Fax,
						mas_supplier.CP3_Email,
						mas_supplier.ResignedDate,
						mas_supplier.`Status`
					FROM
						`mas_supplier`
					WHERE
						mas_supplier.ID = '$crn_id'";
	
	$emp_sql = mysqli_query($con_main, $emp_query);
	
	$emp = mysqli_fetch_assoc($emp_sql);
	$debug = $emp_query;

	if ($emp_sql){
		$result = true;
		$message = "Success";

		$result_array['data'] = $emp;
	}else{
		$result = false;
		$message = "Error results";
	}

	$result_array['result'] = $result;
	$result_array['message'] = $message;
	$result_array['debug'] = $debug;
	
	mysqli_close($conn);
	
	echo (json_encode($result_array));
?>