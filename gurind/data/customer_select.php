<?php
	require ('../../config.php');
	
	$term = $_GET['term'];
	
	$result_array = array();
	$debug = "";
	
	$emp_query =  "SELECT
					ID,
					CustomerCode,
					CustomerName,
					BillingName
				FROM
					`mas_customer`
				WHERE
					mas_customer.CustomerCode LIKE '%$term%'
				OR mas_customer.CustomerName LIKE '%$term%'
				";
	
	$emp_sql = mysqli_query($con_main, $emp_query);
	
	$count = 0;
	
	while ($emp = mysqli_fetch_array($emp_sql)){
		$name = $emp['CustomerCode']." - ".$emp['CustomerName'];
		
		$result_array[$count] = array('id' => $emp['ID'], 'BillingName' => $emp['BillingName'], 'name' => $name);
		
		$count++;
	}
	
	mysqli_close($con_main);
	$result_array['debug'] = $emp_query;
	
	echo (json_encode($result_array));
?>