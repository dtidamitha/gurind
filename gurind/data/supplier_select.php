<?php
	require ('../../config.php');
	
	$term = $_GET['term'];
	
	$result_array = array();
	$debug = "";
	
	$emp_query =  "SELECT
					mas_supplier.ID,
					mas_supplier.SupplierCode,
					mas_supplier.SupplierName,
					mas_supplier.BillingName
				FROM
					`mas_supplier`
				WHERE
					mas_supplier.SupplierCode LIKE '%$term%'
				OR mas_supplier.SupplierName LIKE '%$term%'
				ORDER BY
					mas_supplier.ID ASC";
	
	$emp_sql = mysqli_query($con_main, $emp_query);
	
	$count = 0;
	
	while ($emp = mysqli_fetch_array($emp_sql)){
		$name = $emp['SupplierCode']." - ".$emp['SupplierName'];
		
		$result_array[$count] = array('id' => $emp['ID'], 'BillingName' => $emp['BillingName'], 'name' => $name);
		
		$count++;
	}
	
	mysqli_close($con_main);
	$result_array['debug'] = $emp_query;
	
	echo (json_encode($result_array));
?>