<?php
	header('Content-Type: application/json');
	
	include ('../../config.php');
	
	$result_array = array();
	
	$draw = $_REQUEST['draw'];
	$length = $_REQUEST['length'];
	$start = $_REQUEST['start'];
	$columns = $_REQUEST['columns'];
	$order_cols = $_REQUEST['order'];
	$search = $_REQUEST['search'];

	$data = array();
	$col_set = array('T0.ColorCode','T0.Color','T0.Remarks','STATUS');

	$order_by = "";
	$where = "";

	foreach ($order_cols as $order_col){
		$order_by .= (empty($order_by)) ? " ORDER BY " : ", ";
		
		$order_col_index = $order_col['column']; 
		$order_col_dir = $order_col['dir']; 


		$order_by .= $col_set[$order_col_index]." ".$order_col_dir." ";
	}
	// if (!empty($search['value'])){
	// 	$term = $search['value'];
		
	// 	$i = 0;
	// 	foreach ($columns as $column){
	// 		$col_name = $column['name'];
	// 		$col_searchable = $column['searchable'];
	// 		if ($col_searchable == "true"){
	// 			$where .= (empty($where)) ? " WHERE " : " OR ";
	// 			$where .= $col_set[$i]." LIKE '%".$term."%' ";
	// 			$where .= $col_set[$i]." LIKE '%$term%' ";
	// 		}
			
	// 		$i = $i + 1;
	// 	}
	// }

	if (empty($order_by)){
	    $order_by = " ORDER BY T0.id DESC ";
	}

    if (!empty($search['value'])){
		// get search term
		$term = $search['value'];
		
		$i = 0;
		
		// Loop over available columns
		foreach ($columns as $column){
			// Get current column name and searchable (true/false)
			$col_name = $column['name'];
			$col_searchable = $column['searchable'];
			
			// If current column is searchable
			if ($col_searchable == "true"){
			    // if where string is empty start where clause with WHERE else append where string with OR
				$where .= (empty($where)) ? " WHERE " : " OR ";
				
				if ($i == 5){
					// Convert column index to table column + LIKE + 'search term'
					$where .= " T0.ColorCode LIKE '%$term%' ";
				}else if($i == 6){
				    $where .= " T0.Color LIKE '%$term%' ";
				}else if($i == 7){
				    $where .= " T1.Remarks LIKE '%$term%' ";
				}else{
					$where .= $col_set[$i]." LIKE '%$term%' ";
				}
			}
			
			$i = $i + 1;
		}
	}



 $query = "SELECT
            T0.ID,
            T0.ColorCode,
            T0.Color,
            T0.Remarks,
            IF(T0. Status = 1,'Active','Inactive') AS STATUS,
            T0.EnteredBy,
            T0.EnteredDate
        FROM
            `mas_color` AS T0";
	
	$sql = mysqli_query($con_main, $query);
	
	$i = 0;

	$result_array['data'] = "";
	
	while ($row = mysqli_fetch_assoc($sql)){
		
		$data['DT_RowId'] = "row_".$row['ID'];
		$data['colorcode'] = $row['ColorCode'];
		$data['colorname'] = $row['Color'];
		$data['remarks'] = $row['Remarks'];
		$data['status'] =$row['STATUS'];

		$result_array['data'][$i] = $data;

		$i = $i + 1;
	}

 $filtered_row_count_query = "SELECT
									mas_color.Color,
									mas_color.Remarks,
									Count(mas_color.ID) AS ROW_COUNT
								FROM
									`mas_color` ".$where;

$filtered_row_count_sql = mysqli_query($con_main, $filtered_row_count_query);
$filtered_row_count = mysqli_fetch_assoc($filtered_row_count_sql);
$filtered_records = $filtered_row_count['ROW_COUNT'];

$full_row_count_sql = mysqli_query($con_main, "SELECT COUNT(T0.ID) AS C FROM mas_color AS T0");
$full_row_count = mysqli_fetch_array($full_row_count_sql);

$total_records = $full_row_count['C'];


	$result_array['draw'] = $draw; 
	$result_array['recordsTotal'] = $total_records; 
	$result_array['recordsFiltered'] = (!empty($search['value'])) ? $filtered_records : $total_records;

	mysqli_close($con_main);
	echo (json_encode($result_array));
?>