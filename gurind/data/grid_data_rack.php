<?php
	header('Content-Type: application/json');
	
	include ('../../config.php');
	
	$result_array = array();
	
	$draw = $_REQUEST['draw'];
	$length = $_REQUEST['length'];
	$start = $_REQUEST['start'];
	$columns = $_REQUEST['columns'];
	$order_cols = $_REQUEST['order'];
	$search = $_REQUEST['search'];
	$data = array();
	
	
	$col_set = array('T1.loc_name','T0.rack_code','T0.rack_name','STATUS');
	
	
	$order_by = "";
	$where = "";
	
	
	foreach ($order_cols as $order_col){
		
		$order_by .= (empty($order_by)) ? " ORDER BY " : ", ";
		
		$order_col_index = $order_col['column']; 
		$order_col_dir = $order_col['dir']; 
		
		
		$order_by .= $col_set[$order_col_index]." ".$order_col_dir." ";
	}
	
	
	// if (!empty($search['value'])){
		
	// 	$term = $search['value'];
	// 	$i = 0;
	// 	foreach ($columns as $column){
	// 		$col_name = $column['name'];
	// 		$col_searchable = $column['searchable'];
	// 		if ($col_searchable == "true"){	
	// 			$where .= (empty($where)) ? " WHERE " : " OR ";
	// 			$where .= $col_set[$i]." LIKE '%".$term."%' ";
	// 			$where .= $col_set[$i]." LIKE '%$term%' ";
	// 		}
	// 		$i = $i + 1;
	// 	}
	// }

	if (empty($order_by)){
	    $order_by = " ORDER BY T0.id DESC ";
	}

	if (!empty($search['value'])){
		// get search term
		$term = $search['value'];
		
		$i = 0;
		
		// Loop over available columns
		foreach ($columns as $column){
			// Get current column name and searchable (true/false)
			$col_name = $column['name'];
			$col_searchable = $column['searchable'];
			
			// If current column is searchable
			if ($col_searchable == "true"){
			    // if where string is empty start where clause with WHERE else append where string with OR
				$where .= (empty($where)) ? " WHERE " : " OR ";
				
				if ($i == 5){
					// Convert column index to table column + LIKE + 'search term'
					$where .= " T0.rack_code LIKE '%$term%' ";
				}else if($i == 6){
				    $where .= " T0.rack_name LIKE '%$term%' ";
				}else if($i == 7){
				    $where .= " T1.loc_name LIKE '%$term%' ";
				}else{
					$where .= $col_set[$i]." LIKE '%$term%' ";
				}
			}
			
			$i = $i + 1;
		}
	}

$query = "	SELECT
				T0.rack_code,
				T0.rack_name,
				IF(T0.status = 1, 'Active','Inactive') AS STATUS,
				T0.id,
				T1.loc_name
			FROM
				mas_rack AS T0
			INNER JOIN mas_location AS T1 ON T0.loc_id = T1.id";
	
	$sql = mysqli_query($con_main, $query);
	
	$i = 0;

	$result_array['data'] = "";
	
	while ($row = mysqli_fetch_assoc($sql)){
		

		$data['DT_RowId'] = "row_".$row['id'];
		$data['rackcode'] = $row['rack_code'];
		$data['rackname'] = $row['rack_name'];
		$data['location'] = $row['loc_name'];
		$data['status'] = $row['STATUS'];
		
		$result_array['data'][$i] = $data;
		
		$i = $i + 1;
	}

$filtered_row_count_query  =  "SELECT
								Count(T0.id) AS ROW_COUNT,
								T0.loc_id
							   FROM
								mas_rack AS T0 ".$where;

$filtered_row_count_sql = mysqli_query($con_main, $filtered_row_count_query);
$filtered_row_count = mysqli_fetch_assoc($filtered_row_count_sql);
$filtered_records = $filtered_row_count['ROW_COUNT'];

$full_row_count_sql = mysqli_query($con_main, "SELECT COUNT(T0.id) AS C FROM mas_rack AS T0");
$full_row_count = mysqli_fetch_array($full_row_count_sql);

$total_records = $full_row_count['C'];

$result_array['draw'] = $draw; // Return same draw id received from datatable client request
$result_array['recordsTotal'] = $total_records; // Return total record count in table
$result_array['recordsFiltered'] = (!empty($search['value'])) ? $filtered_records : $total_records; //
mysqli_close($con_main);
	
echo (json_encode($result_array));
?>