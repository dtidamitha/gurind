<?php
	session_start();
	
	if (empty($_SESSION['ACCESS_CODE']) || $_SESSION['ACCESS_CODE'] == NULL){
		header ('Location: login.php');
		exit;
	}
	
	$folder_depth = "";
	$prefix = "";
	
	$folder_depth = substr_count($_SERVER["PHP_SELF"] , "/");
	$folder_depth = ($folder_depth == false) ? 2 : (int)$folder_depth;
	
    $prefix = str_repeat("../", $folder_depth - 2);
    
    $title_suffix = "Return To Supplier";
?>
<?php include $prefix.'config.php'; ?>
<?php include $prefix.'menu.php'; ?>
<?php include $prefix.'template_start.php'; ?>
<?php include $prefix.'page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-podium"></i>Return To Supplier<br><small>Create, Update or Delete Return To Supplier</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="../home.php">Home</a></li>
        <li>Returns</li>
        <li>Return To Supplier</li>
    </ul>
    <!-- END Blank Header -->
		
	
    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
					<h2>Return To Supplier</h2>
				</div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <form id="form-main" name="form-main" action="provider_crud.php" method="post"  class="form-horizontal form-bordered">  

                <div class="form-group">                      
                        <label class="col-md-2 control-label" for="returnno">Return No</label>
                        <div class="col-md-4">
                           <input type="text" id="returnno" name="returnno" class="form-control" placeholder="Enter return number">     
                        </div> 

                        <label class="col-md-2 control-label" for="date">Date</label>
                        <div class="col-md-4">
                                <input type="text" id="date" name="date" class="form-control input-datepicker" data-data-format="yyyy-mm-dd" placeholder="yyyy-mm-dd"> 
                        </div>             
                </div>
   
                <div class="form-group">
                	<label class="col-md-2 control-label" for="item">Item Name</label>
                     <div class="col-md-4">
                        <select id="item" name="item" class="select-chosen">
                            <option></option>
                        </select>
                    </div>

                    <label class="col-md-2 control-label" for="supplier">Supplier</label>
                     <div class="col-md-4">
                       <select id="supplier" name="supplier" class="select-chosen">
                        <option></option>
                       </select>
                    </div>
                </div>

                <div class="form-group">
                	<label class="col-md-2 control-label" for="invoice">Invoice No</label>
                     <div class="col-md-4">
                        <input type="text" id="invoice" name="invoice" class="form-control" placeholder="Enter invoice no">
                     </div>

                     <label class="col-md-2 control-label" for="order">Order No</label>
                     <div class="col-md-4">
                        <input type="text" id="order" name="order" class="form-control" placeholder="Enter invoice no">
                     </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label" for="returnby">Return By</label>
                	 <div class="col-md-4">
                        <input type="text" id="returnby" name="returnby" class="form-control" placeholder="Enter return by">
                     </div>

                      <label class="col-md-2 control-label" for="remark">Remark</label>
                     <div class="col-md-4">
                        <input type="text" id="remark" name="remark" class="form-control" placeholder="Enter remark">
                     </div>
                </div>

                
                    <div class="form-group form-actions">
                        <input type="hidden" name="id" id="id" value="0" />

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success primary-btn pull-right"><i class="fa fa-angle-right"></i> Submit</button>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Block -->
		    </div>
            <!-- END Example Content -->
        </div>
        <!-- END Example Block -->
    </div>

    <!-- Table Block -->
    <div class="block full">
        <!-- Table Title -->
        <div class="block-title">
            <h2>Return To Supplier</h2><small>Return to suppliers currently exist in the system</small>
        </div>
        <!-- END Table Title -->

        <!-- Table Content -->
        <div class="table-responsive"><table id="table-data" class="table table-condensed table-striped table-hover"></table></div>
        <!-- END Table Content -->
    </div>
    <!-- END Table Block -->
<!-- END Page Content -->
</div>

<?php include $prefix.'page_footer.php'; ?>
<?php include $prefix.'template_scripts.php'; ?>
<?php include $prefix.'template_end.php'; ?>

<script type="text/javascript">

    // $( "#aamount" ).keyup(function() {
    //   var aamount = $('#aamount').val();
    //   var received = $('#received').val();

    //   alert (aamount - received);

    // });

    $('#received').on('change',function(){
        var aamount = $('#aamount').val();
        var received = $('#received').val();
        var balance = aamount - received;
        if(balance<0){
            alert("INVALID BALANCE AMOUNT...!");
        }else{

        $('#bal').val(balance);
        }
            
    });

	/*********** Data-table Initialize ***********/
    $('#main_category').on('change', function(){
        var cato = $('#main_category').val();

        $.ajax({
            url: 'data/data_provider.php',
            data: {
                main_category: cato
            },
            method: 'post',
            error: function(e){
                alert ('Error requesting provider data');
            },
            success: function(r){
                $('#main_provider').html('<option value=""></option>');

                if (r.result){
                    if (r.data.length > 0){
                        $.each(r.data, function (k, v){
                            let option_markup = "";

                            option_markup += "<option value='"+v.id+"'>";
                            option_markup += v.provider;
                            option_markup += "</option>";

                            $('#main_provider').append(option_markup)
                            
                        });
                    }
                }

                $('#main_provider').trigger("chosen:updated");
            }
        });
    });


    App.datatables();

    var dt = $('#table-data').DataTable({
        "processing": true,
        "serverSide": true,
        "select": true,
        "columns": [
			{ "data": "system", "name": "system", "title": "Return No" },
			{ "data": "module", "name": "module", "title": "Date" },
            { "data": "company", "name": "company", "title": "Item Name"},
             { "data": "company", "name": "company", "title": "Supplier"},
            { "data": "company", "name": "company", "title": "Invoice No"}, 
            { "data": "company", "name": "company", "title": "Order No"},
            { "data": "company", "name": "company", "title": "Return By"},    
            { "data": "company", "name": "company", "title": "Remarks"},
            {"data": "actions", "name": "actions","title":"Actions", "searchable": false, "orderable": false, 
                mRender: function (data, type, row) {
                    return '<div class="btn-group btn-group-xs"><button id="btn-row-edit" class="btn btn-primary" title="Edit"><i class="fa fa-pencil"></i></button></div>'
                }
            }
        ],
        "columnDefs":[
            {"className": "dt-center", "targets": [1,2,3]}
        ],
        "language": {
            "emptyTable": "No return items to show..."
        },
        "ajax": "data/grid_data_return_supplier.php"
    });
	
    $('.dataTables_filter input').attr('placeholder', 'Search');

    $("#table-data tbody").on('click', '#btn-row-edit', function() {
        var str_id = $(this).closest('tr').attr('id');
        var arr_id = str_id.split("_");

        var row_id = arr_id[1];
       // alert (row_id);

        $.ajax({
            url: 'data/data_return_supplier.php',
            data: {
                id: row_id
            },
            method: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $('#table-data tbody #'+str_id+' #btn-row-edit').button('loading');
                NProgress.start();
            },
            error: function (e) {
                $.bootstrapGrowl('<h4>Error!</h4> <p>Error retrieving records data</p>', {
                    type: 'danger',
                    delay: 2500,
                    allow_dismiss: true
                });

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            },
            success: function (r) {
                if (!r.result) {
                    $.bootstrapGrowl('<h4>Error!</h4> <p>'+r.message+'</p>', {
                        type: 'danger',
                        delay: 2500,
                        allow_dismiss: true
                    });
                }else{
                    $('#id').val(r.data[0].id);
                    $('#returnno').val(r.data[0].system);
                    $('#date').val(r.data[0].date);
                    $('#remark').val(r.data[0].module);
                    $('#invoice').val(r.data[0].module);
                    $('#order').val(r.data[0].module);
                    $('#returnby').val(r.data[0].module);
                    
                    $('#item').val(r.data[0].stage);  
                    $('#item').trigger("chosen:updated");
                   
                    $('#supplier').val(r.data[0].stage);  
                    $('#supplier').trigger("chosen:updated");
                }

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            }
        });
    });
    /*********** Table Control End ***********/

    /*********** Form Validation and Submission ***********/
	$('#form-main').on('submit', function (e){
		e.preventDefault();
		
		var id = $('#id').val();
		var op = (id == 0) ? "insert" : "update";
		
		var formdata = $('#form-main').serializeArray();
		formdata.push({'name':'operation','value':op});
		
		$.ajax({
			url: 'bin_allocation_crud.php',
			data: formdata,
			success: function(r){
				var msg_typ = "info";
                var msg_txt = "";

                if (r.result){
                    msg_typ = 'success';
                    msg_txt = '<h4>Success!</h4> <p>Return to supplier saved</p>';

                    $('#form-main').trigger('reset');
                }else{
                    msg_typ = 'danger';
                    msg_txt = '<h4>Error!</h4> <p>'+r.message+'</p>';
                }

                $.bootstrapGrowl(msg_txt, {
                    type: msg_typ,
                    delay: 2500,
                    allow_dismiss: true
                });

                dt.ajax.reload();
                dt.draw();
			}
		});
	});

    $('#form-main').on('reset', function (e){
        $('#id').val("0");
        $('#returnno').val("");
        $('#date').val("");
        $('#remark').val("");
        $('#invoice').val("");
        $('#order').val("");
        $('#returnby').val("");
        
        $('#item').val("");  
        $('#item').trigger("chosen:updated");
       
        $('#supplier').val("");  
        $('#supplier').trigger("chosen:updated");
    });
    /*********** Form Control End ***********/
		
	</script>
	
	<?php mysqli_close($con_main); ?>