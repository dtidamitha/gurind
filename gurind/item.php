<?php
	session_start();
	
	if (empty($_SESSION['ACCESS_CODE']) || $_SESSION['ACCESS_CODE'] == NULL){
		header ('Location: login.php');
		exit;
	}
	
	$folder_depth = "";
	$prefix = "";
	
	$folder_depth = substr_count($_SERVER["PHP_SELF"] , "/");
	$folder_depth = ($folder_depth == false) ? 2 : (int)$folder_depth;
	
    $prefix = str_repeat("../", $folder_depth - 2);
    
    $title_suffix = "Item Master";
?>
<?php include $prefix.'config.php'; ?>
<?php include $prefix.'menu.php'; ?>
<?php include $prefix.'template_start.php'; ?>
<?php include $prefix.'page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-podium"></i>Item Master<br><small>Create, Update or Delete Items</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="../home.php">Home</a></li>
        <li>Master Files</li>
        <li>Item Master</li>
    </ul>
    <!-- END Blank Header -->
		
	
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <div class="block-title">
					<h2>Items</h2>
				</div>
             
                <form id="form-main" name="form-main" action="provider_crud.php" method="post"  class="form-horizontal form-bordered">  

                <div class="form-group">                      
                        <label class="col-md-2 control-label" for="category">Category</label>
                        <div class="col-md-4">
                            <select id="category" name="category" class="select-chosen" data-placeholder="Select category"> 
                               <option></option>
                                  <?php
                                    $query="SELECT
												main_category.ID,
												main_category.CategoryCode,
												main_category.CategoryName
											FROM
												`main_category`";
                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['ID']."\">".$type['CategoryCode']." - ".$type['CategoryName']."</option>");
                                    }
                                   ?>
                               </select>
                        </div>

                         <label class="col-md-2 control-label" for="type">Type</label>
                        <div class="col-md-4">
                            <select id="type" name="type" class="select-chosen" data-placeholder="Enter item type"> 
                                   <!-- <option value="1" selected>Active</option>
                                   <option value="0">Inactive</option> -->
                               </select>
                        </div>        
                </div>
                       
                <div class="form-group">  
                           <label class="col-md-2 control-label" for="itemname">Item Name</label>        
                        <div class="col-md-4">                         
                            <input type="text" id="itemname" name="itemname" class="form-control" placeholder="Enter item name">
                        </div>
 
                          <label class="col-md-2 control-label" for="itemcode">Item Code</label>
                        <div class="col-md-4">
                             <input type="text" id="itemcode" name="itemcode" class="form-control" placeholder="Enter item code"> 
                        </div>   
                </div>


                <div class="form-group">  

                	  <label class="col-md-2 control-label" for="country">Select Country</label>
                        <div class="col-md-4">                           
                            <select id="country" name="country" class="select-chosen" data-placeholder="Select country"> 
                                   <option></option>
                                   <?php
                                    $query="SELECT
												mas_countries.ID,
												mas_countries.CountryCode,
												mas_countries.CountryName
											FROM
												`mas_countries`
											WHERE
												mas_countries.`Status` = '1'";
                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['ID']."\">".$type['CountryCode']." - ".$type['CountryName']."</option>");
                                    }
                                   ?>
                                   
                            </select>
                        </div>
                           <label class="col-md-2 control-label" for="brand">Brand</label>        
                        <div class="col-md-4">                           
                             <select id="brand" name="brand" class="select-chosen" data-placeholder="Select item brand"> 
                                   <option></option>
                                   <?php
                                    $query="SELECT
                                                mas_brand.id,
                                                mas_brand.brand_code,
                                                mas_brand.brand_name
                                            FROM
                                                mas_brand
                                            WHERE
                                                mas_brand.`status` = '1'";
                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['id']."\">".$type['brand_code']." - ".$type['brand_name']."</option>");
                                    }
                                   ?>
                                   
                            </select>
                        </div>
                </div>
                    

                    <div class="form-group"> 
                         <label class="col-md-2 control-label" for="materialtype">Select Material Type</label>
                        <div class="col-md-4">                           
                            <select id="materialtype" name="materialtype" class="select-chosen" data-placeholder="Select material type"> 
                                   <option></option>
                                   <?php
                                    $query="SELECT
												materialtype.ID,
												materialtype.MaterialTypeCode,
												materialtype.MaterialType
											FROM
												`materialtype`";
                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['ID']."\">".$type['MaterialTypeCode']." - ".$type['MaterialType']."</option>");
                                    }
                                   ?>
                            </select>
                        </div> 

                        <label class="col-md-2 control-label" for="color">Color</label>        
                        <div class="col-md-4">                           
                            <select id="color" name="color" class="select-chosen" data-placeholder="Select color"> 
                                   <option></option>
                                   <?php
                                    $query="SELECT
												mas_color.ID,
												mas_color.ColorCode,
												mas_color.Color
											FROM
												`mas_color`
											WHERE
												mas_color.`Status` = '1'";
                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['ID']."\">".$type['ColorCode']." - ".$type['Color']."</option>");
                                    }
                                   ?>
                                   
                            </select>
                        </div>
                    </div>


                    <div class="form-group">

                    	   <label class="col-md-2 control-label" for="supplier">Supplier</label>
                        <div class="col-md-4">                           
                            <select id="supplier" name="supplier" class="select-chosen" data-placeholder="Select supplier"> 
                               <option></option>
                                   <?php
                                    $query="SELECT
												mas_supplier.ID,
												mas_supplier.SupplierCode,
												mas_supplier.SupplierName
											FROM
												`mas_supplier`
											WHERE
												mas_supplier.`Status` = '1'";
                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['ID']."\">".$type['SupplierCode']." - ".$type['SupplierName']."</option>");
                                    }
                                   ?>
                            </select>
                        </div>

                      <label class="col-md-2 control-label" for="shcode">HS Code</label>
                         <div class="col-md-4">                           
                           <input type="text" id="shcode" name="shcode" class="form-control" placeholder="Enter HS Code">
                        </div>
                    </div>


                    <div class="form-group">
                      <label class="col-md-2 control-label" for="thickness">Thickness</label>              
                        <div class="col-md-4">                           
                            <select id="thickness" name="thickness" class="select-chosen" data-placeholder="Select thickness"> 
                               <option></option>
                                   <?php
                                    $query="SELECT
                                                mas_thickness.ID,
                                                mas_thickness.UOM,
                                                mas_thickness.thickness
                                            FROM
                                                `mas_thickness`
                                            WHERE
                                                mas_thickness.`status` = '1'";
                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['ID']."\">".$type['UOM']." - ".$type['thickness']."</option>");
                                    }
                                   ?>
                            </select>
                        </div>

                        <label class="col-md-2 control-label" for="customuom">Custom UOM</label>
                           <div class="col-md-4">
                              <select id="customuom" name="customuom" class="select-chosen" data-placeholder="Select custom uom"> 
                                <option></option>
                              </select>
                           </div>
                    </div>

                    <div class="form-group form-actions">
                        <input type="hidden" name="id" id="id" value="0" />

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success primary-btn pull-right"><i class="fa fa-angle-right"></i> Submit</button>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Block -->
		    </div>
            <!-- END Example Content -->
        </div>
        <!-- END Example Block -->
    </div>

    <!-- Table Block -->
    <div class="block full">
        <!-- Table Title -->
        <div class="block-title">
            <h2>Items</h2><small>Items currently exist in the system</small>
        </div>
        <!-- END Table Title -->

        <!-- Table Content -->
        <div class="table-responsive"><table id="table-data" class="table table-condensed table-striped table-hover"></table></div>
        <!-- END Table Content -->
    </div>
    <!-- END Table Block -->
<!-- END Page Content -->
</div>

<?php include $prefix.'page_footer.php'; ?>
<?php include $prefix.'template_scripts.php'; ?>
<?php include $prefix.'template_end.php'; ?>

<script type="text/javascript">

	/*********** Data-table Initialize ***********/
    $('#category').on('change', function(){
        var cato = $('#category').val();
        alert(cato);

        // $.ajax({
        //     url: 'data/data_item.php',
        //     data: {
        //         main_category: cato
        //     },
        //     method: 'post',
        //     error: function(e){
        //         alert ('Error requesting provider data');
        //     },
        //     success: function(r){
        //         $('#main_provider').html('<option value=""></option>');

        //         if (r.result){
        //             if (r.data.length > 0){
        //                 $.each(r.data, function (k, v){
        //                     let option_markup = "";

        //                     option_markup += "<option value='"+v.id+"'>";
        //                     option_markup += v.provider;
        //                     option_markup += "</option>";

        //                     $('#main_provider').append(option_markup)
                            
        //                 });
        //             }
        //         }

        //         $('#main_provider').trigger("chosen:updated");
        //     }
        // });
    });


    App.datatables();

   var dt = $('#table-data').DataTable({
        "processing": true,
        "serverSide": true,
        "select": true,
        "columns": [
            { "data": "category", "name": "category", "title": "Category"},
            { "data": "itemcode", "name": "itemcode", "title": "Item Code" },
            { "data": "itemname", "name": "itemname", "title": "Item Name"}, 
            { "data": "country", "name": "country", "title": "Country"},
            { "data": "brand", "name": "brand", "title":"Brand"},
            { "data": "materialtype", "name": "materialtype", "title": "Material Type"},
            {"data": "actions", "name": "actions","title":"Actions", "searchable": false, "orderable": false, 
                mRender: function (data, type, row) {
                    return '<div class="btn-group btn-group-xs"><button id="btn-row-edit" class="btn btn-primary" title="Edit"><i class="fa fa-pencil"></i></button></div>'
                }
            }
        ],
        "columnDefs":[
            {"className": "dt-center", "targets": [1,2,3]}
        ],
        "language": {
            "emptyTable": "No suppliers to show..."
        },
        "ajax": "data/grid_data_item.php"
    });
	
    $('.dataTables_filter input').attr('placeholder', 'Search');

    $("#table-data tbody").on('click', '#btn-row-edit', function() {
        var str_id = $(this).closest('tr').attr('id');
        var arr_id = str_id.split("_");

        var row_id = arr_id[1];
       // alert (row_id);

        $.ajax({
            url: 'data/data_item.php',
            data: {
                id: row_id
            },
            method: 'POST',
            dataType: 'json',
            beforeSend: function () {
                $('#table-data tbody #'+str_id+' #btn-row-edit').button('loading');
                NProgress.start();
            },
            error: function (e) {
                $.bootstrapGrowl('<h4>Error!</h4> <p>Error retrieving records data</p>', {
                    type: 'danger',
                    delay: 2500,
                    allow_dismiss: true
                });

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            },
            success: function (r) {
                if (!r.result) {
                    $.bootstrapGrowl('<h4>Error!</h4> <p>'+r.message+'</p>', {
                        type: 'danger',
                        delay: 2500,
                        allow_dismiss: true
                    });
                }else{
                    $('#id').val(r.data[0].id);
                    $('#brand').val(r.data[0].proposal_no); 
                    $('#itemname').val(r.data[0].proposal_no); 
                    $('#itemcode').val(r.data[0].proposal_no); 

                    $('#category').val(r.data[0].stage);  
                    $('#category').trigger("chosen:updated"); 

                    $('#country').val(r.data[0].stage);  
                    $('#country').trigger("chosen:updated"); 

                    $('#materialtype').val(r.data[0].stage);  
                    $('#materialtype').trigger("chosen:updated");                          
                }

                $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
                NProgress.done();
            }
        });
    });
    /*********** Table Control End ***********/

    /*********** Form Validation and Submission ***********/
	$('#form-main').on('submit', function (e){
		e.preventDefault();
		
		var id = $('#id').val();
		var op = (id == 0) ? "insert" : "update";
		
		var formdata = $('#form-main').serializeArray();
		formdata.push({'name':'operation','value':op});
		
		$.ajax({
			url: 'item_crud.php',
			data: formdata,
			success: function(r){
				var msg_typ = "info";
                var msg_txt = "";

                if (r.result){
                    msg_typ = 'success';
                    msg_txt = '<h4>Success!</h4> <p>Item saved</p>';

                    $('#form-main').trigger('reset');
                }else{
                    msg_typ = 'danger';
                    msg_txt = '<h4>Error!</h4> <p>'+r.message+'</p>';
                }

                $.bootstrapGrowl(msg_txt, {
                    type: msg_typ,
                    delay: 2500,
                    allow_dismiss: true
                });

                dt.ajax.reload();
                dt.draw();
			}
		});
	});

    $('#form-main').on('reset', function (e){

        $('#id').val("0");
        $('#brand').val(""); 
        $('#itemname').val("");  
        $('#itemcode').val(""); 

        $('#category').val("");  
        $('#category').trigger("chosen:updated"); 

        $('#country').val("");  
        $('#country').trigger("chosen:updated"); 

        $('#materialtype').val("");  
        $('#materialtype').trigger("chosen:updated"); 
    });
    /*********** Form Control End ***********/
		
	</script>
	
	<?php mysqli_close($con_main); ?>