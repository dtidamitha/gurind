<?php
	
	session_start();
	
	require_once ('../config.php');
	$id = $_REQUEST['id'];
	$op = ($id==0||$id=="0"||$id==null)?"insert":"update"; 
	  
	$code = $_REQUEST['code']; 
	$name = $_REQUEST['name']; 
	$billingName=$_REQUEST['billingName'];
        $address=mysqli_real_escape_string($con_main,$_REQUEST['address']);
        $city=$_REQUEST['city'];
	$country = $_REQUEST['country'];
        $genLineNos=$_REQUEST['genLineNos'];
        
        $businessRegistrationNo=$_REQUEST['businessRegistrationNo'];
        $businessRegistrationCertificate=$_FILES['businessRegistrationCertificate'];
        $deleteBusinessRegistrationCertificate=$_REQUEST['deleteBusinessRegistrationCertificate']=='on'?"1":"0";
        $Extention=substr($businessRegistrationCertificate["name"],strpos($businessRegistrationCertificate["name"],".")+1);
        $businessRegistrationCertificateFileName=$Extention;
        
        $VATRegistrationNo=$_REQUEST['VATRegistrationNo'];
        $VATRegistrationCertificate=$_FILES['VATRegistrationCertificate'];
        $deleteVATRegistrationCertificate=$_REQUEST['deleteVATRegistrationCertificate']=='on'?"1":"0";
        $Extention=substr($VATRegistrationCertificate["name"],strpos($VATRegistrationCertificate["name"],".")+1);
        $VATRegistrationCertificateFileName=$Extention;
                
        $SVATRegistrationNo=$_REQUEST['SVATRegistrationNo'];
        $SVATRegistrationCertificate=$_FILES['SVATRegistrationCertificate'];
        $deleteSVATRegistrationCertificate=$_REQUEST['deleteSVATRegistrationCertificate']=='on'?"1":"0";
        $Extention=substr($SVATRegistrationCertificate["name"],strpos($SVATRegistrationCertificate["name"],".")+1);
        $SVATRegistrationCertificateFileName=$Extention;
        
        
        $BOIRegistrationNo=$_REQUEST['BOIRegistrationNo'];
        $BOIRegistrationCertificate=$_FILES['BOIRegistrationCertificate'];
        $deleteBOIRegistrationCertificate=$_REQUEST['deleteBOIRegistrationCertificate']=='on'?"1":"0";
        $Extention=substr($BOIRegistrationCertificate["name"],strpos($BOIRegistrationCertificate["name"],".")+1);
        $BOIRegistrationCertificateFileName=$Extention;
                
        $cordinationContact1Name=$_REQUEST['cordinationContact1Name'];
        $cordinationContact1Designation=$_REQUEST['cordinationContact1Designation'];
        $cordinationContact1Mobile=$_REQUEST['cordinationContact1Mobile'];
        $cordinationContact1Email=$_REQUEST['cordinationContact1Email'];
        
        $cordinationContact2Name=$_REQUEST['cordinationContact2Name'];
        $cordinationContact2Designation=$_REQUEST['cordinationContact2Designation'];
        $cordinationContact2Mobile=$_REQUEST['cordinationContact2Mobile'];
        $cordinationContact2Email=$_REQUEST['cordinationContact2Email'];
        
        $paymentsContact1Name=$_REQUEST['paymentsContact1Name'];
        $paymentsContact1Designation=$_REQUEST['paymentsContact1Designation'];
        $paymentsContact1Mobile=$_REQUEST['paymentsContact1Mobile'];
        $paymentsContact1Email=$_REQUEST['paymentsContact1Email'];
        
        $paymentsContact2Name=$_REQUEST['paymentsContact2Name'];
        $paymentsContact2Designation=$_REQUEST['paymentsContact2Designation'];
        $paymentsContact2Mobile=$_REQUEST['paymentsContact2Mobile'];
        $paymentsContact2Email=$_REQUEST['paymentsContact2Email'];
        
        $customsContact1Name=$_REQUEST['logisticsBoiCusDecContact1Name'];
        $customsContact1Designation=$_REQUEST['logisticsBoiCusDecContact1Designation'];
        $customsContact1Mobile=$_REQUEST['logisticsBoiCusDecContact1Mobile'];
        $customsContact1Email=$_REQUEST['logisticsBoiCusDecContact1Email'];
        
        $proprietorName=$_REQUEST['proprietorName'];
        $proprietorDesignation=$_REQUEST['proprietorDesignation'];
        $proprietorMobile=$_REQUEST['proprietorMobile'];
        $proprietorEmail=$_REQUEST['proprietorEmail'];
        
        $Bank1Name=$_REQUEST['Bank1Name'];
        $Bank1Branch=$_REQUEST['Bank1Branch'];
        $Bank1LkrFcbuBoth=$_REQUEST['Bank1LkrFcbuBoth'];
        
        $Bank2Name=$_REQUEST['Bank2Name'];
        $Bank2Branch=$_REQUEST['Bank2Branch'];
        $Bank2LkrFcbuBoth=$_REQUEST['Bank2LkrFcbuBoth'];
        
        $locality=$_REQUEST['locality'];
        $registered=$_REQUEST['registered'];
        
        $customerType=$_REQUEST['customerType'];
        
        $settlementTermsAttachment=$_FILES['settlementTerms_Attachment'];
        $deleteSettlementTermsAttachment=$_REQUEST['deleteSettlementTerms_Attachment']=='on'?"1":"0";
        $Extention=substr($settlementTermsAttachment["name"],strpos($settlementTermsAttachment["name"],".")+1);
        $settlementTermsAttachmentFileName=$Extention;
                
        $settlementTermsBankGuarantor=$_REQUEST['settlementTermsBankGuarantor']=='on'?"1":"0";
        
        $settlementTermsBankGuaranteeCurrency=$_REQUEST['settlementTermsBankGuaranteeCurrency'];
        $settlementTerms_BankGuarantee_Amount=$_REQUEST['settlementTerms_BankGuarantee_Amount']==""?0:$_REQUEST['settlementTerms_BankGuarantee_Amount'];
        $settlementTerms_BankGuarantee_Expiry=$_REQUEST['settlementTerms_BankGuarantee_Expiry'];
        $settlementTerms_BankGuarantee_Expiry=$settlementTerms_BankGuarantee_Expiry==""?"NULL":"'".$settlementTerms_BankGuarantee_Expiry."'";
        $settlementTerms_Bank=$_REQUEST['settlementTerms_Bank'];
        $settlementTerms_BankBranch=$_REQUEST['settlementTerms_BankBranch'];
        $settlementTerms_GuarantorNo=$_REQUEST['settlementTerms_GuarantorNo'];
        $settlementTerms_Advance=$_REQUEST['settlementTerms_Advance']=='on'?"1":"0";
        $settlementTerms_AdvancePercentage=$_REQUEST['settlementTerms_AdvancePercentage']==""?0:$_REQUEST['settlementTerms_AdvancePercentage'];
        $settlementTerms_AdvancePaymentType=$_REQUEST['settlementTerms_AdvancePaymentType'];
        
        $settlementTerms_Credit=$_REQUEST['settlementTerms_Credit']=='on'?"1":"0";
        $settlementTerms_CreditPdcRequired=$_REQUEST['settlementTerms_Credit_PdcRequired']=='on'?"1":"0";
        $settlementTerms_CreditPeriod=$_REQUEST['settlementTerms_CreditPeriod']==""?0:$_REQUEST['settlementTerms_CreditPeriod'];
        $settlementTerms_CreditPeriodBasis=$_REQUEST['settlementTerms_CreditPeriodFrom'];
        $settlementTerms_CreditPeriodBasis=$settlementTerms_CreditPeriodBasis==NULL?"NULL":$settlementTerms_CreditPeriodBasis;
        $settlementTerms_CreditLimit=$_REQUEST['settlementTerms_CreditLimit']==""?0:$_REQUEST['settlementTerms_CreditLimit'];
        $shippingTerms=$_REQUEST['shippingTerms'];
        $pricingPolicy=$_REQUEST['pricingPolicy'];
        $status=$_REQUEST['status'];
        
	$user = $_SESSION['USER_CODE']; 
	
	$query = "";
        try 
        {
            mysqli_autocommit ($con_main, FALSE);
            mysqli_query($con_main, "START TRANSACTION;");
        } 
        catch (Exception $cexcError) 
        {
            //echo var_dump($cexcError);
        }
        
        try 
        {
            $success = true;
            $message = "";
            $responce = array();

            if ($id == "0" || $id == null || $id == 0){

                $query = "INSERT INTO `mas_customer` (`CustomerCode`,"
                        . "`CustomerName`, "
                        . "`BillingName`, "
                        . "`CustomerAddress`, "
                        . "`City`, "
                        . "`Country`, "
                        . "`GeneralLineNo`, "
                        . "`BusinessRegistrationNo`, "
                        . "`VATRegNo`, "
                        . "`SVATRegNo`, "
                        . "`BOIRegNo`, "
                        . "`CordinationContact1Name`, "
                        . "`CordinationContact1Designation`, "
                        . "`CordinationContact1Mobile`, "
                        . "`CordinationContact1Email`, "
                        . "`CordinationContact2Name`, "
                        . "`CordinationContact2Designation`, "
                        . "`CordinationContact2Mobile`, "
                        . "`CordinationContact2Email`, "
                        . "`PaymentsContact1Name`, "
                        . "`PaymentsContact1Designation`, "
                        . "`PaymentsContact1Mobile`, "
                        . "`PaymentsContact1Email`, "
                        . "`PaymentsManagerName`, "
                        . "`PaymentsManagerDesignation`, "
                        . "`PaymentsManagerMobile`, "
                        . "`PaymentsManagerEmail`, "
                        . "`LogisticsBoiCusDecContactName`, "
                        . "`LogisticsBoiCusDecContactDesignation`, "
                        . "`LogisticsBoiCusDecContactMobile`, "
                        . "`LogisticsBoiCusDecContactEmail`, "
                        . "`ProprietorName`, "
                        . "`ProprietorDesignation`, "
                        . "`ProprietorMobile`, "
                        . "`ProprietorEmail`, "
                        . "`Bank1Name`, "
                        . "`Bank1Branch`, "
                        . "`Bank1LkrFcbuBoth`, "
                        . "`Bank2Name`, "
                        . "`Bank2Branch`, "
                        . "`Bank2LkrFcbuBoth`, "
                        . "`SettlementTerms_BankGuarantee_Flag`, "
                        . "`SettlementTerms_BankGuaranteeCurrencyID`, "
                        . "`SettlementTerms_BankGuarantee_Amount`, "
                        . "`SettlementTerms_BankGuarantee_Expiry`, "
                        . "`SettlementTerms_Bank`, "
                        . "`SettlementTerms_BankBranch`, "
                        . "`SettlementTerms_GuarantorNo`, "
                        . "`SettlementTerms_AdvanceFlag`, "
                        . "`SettlementTerms_AdvancePercentage`, "
                        . "`SettlementTerms_AdvancePaymentType`, "
                        . "`SettlementTerms_Credit`, "
                        . "`SettlementTerms_CreditPdcRequired`, "
                        . "`SettlementTerms_CreditPeriod`, "
                        . "`SettlementTerms_CreditPeriodBasis`, "
                        . "`SettlementTerms_CreditLimit`, "
                        . "`ShippingTermsID`, "
                        . "`PricingPolicyID`, "
                        . "`Status`, "
                        . "`IsWalkInCustomer`, "
                        . "`CustomerLocality`, "
                        . "`EnteredBy`, "
                        . "`EnteredDate`)"
                        . "VALUES ('$code', '$name', '$billingName', '$address', '$city', $country, '$genLineNos', '$businessRegistrationNo',"
                        . "'$VATRegistrationNo', '$SVATRegistrationNo', '$BOIRegistrationNo', "
                        . "'$cordinationContact1Name', '$cordinationContact1Designation', '$cordinationContact1Mobile', '$cordinationContact1Email', "
                        . "'$cordinationContact2Name', '$cordinationContact2Designation', '$cordinationContact2Mobile', '$cordinationContact2Email', "
                        . "'$paymentsContact1Name', '$paymentsContact1Designation', '$paymentsContact1Mobile', '$paymentsContact1Email', "
                        . "'$paymentsContact2Name', '$paymentsContact2Designation', '$paymentsContact2Mobile', '$paymentsContact2Email', "
                        . "'$customsContact1Name', '$customsContact1Designation', '$customsContact1Mobile', '$customsContact1Email', "
                        . "'$proprietorName', '$proprietorDesignation', '$proprietorMobile', '$proprietorEmail', "
                        . "'$Bank1Name', '$Bank1Branch', $Bank1LkrFcbuBoth,"
                        . "'$Bank2Name', '$Bank2Branch', $Bank2LkrFcbuBoth,"
                        . " $settlementTermsBankGuarantor, $settlementTermsBankGuaranteeCurrency, $settlementTerms_BankGuarantee_Amount, $settlementTerms_BankGuarantee_Expiry, '$settlementTerms_Bank', '$settlementTerms_BankBranch', '$settlementTerms_GuarantorNo',"
                        . " $settlementTerms_Advance, $settlementTerms_AdvancePercentage, $settlementTerms_AdvancePaymentType, "
                        . " $settlementTerms_Credit, $settlementTerms_CreditPdcRequired, $settlementTerms_CreditPeriod, $settlementTerms_CreditPeriodBasis , $settlementTerms_CreditLimit, "
                        . " $shippingTerms, $pricingPolicy, $status, $registered, $locality, $user, NOW());";
            }
            else {
                    $query = "UPDATE `mas_customer`
                                    SET `CustomerCode` = '$code',
                                     `CustomerName`='$name',
                                     `BillingName`='$billingName',
                                     `CustomerAddress`='$address',
                                      `City`='$city',
                                      `Country`=$country,
                                      `GeneralLineNo`='$genLineNos',
                                      `BusinessRegistrationNo`='$businessRegistrationNo',
                                      `VATRegNo`='$VATRegistrationNo',
                                      `SVATRegNo`='$SVATRegistrationNo',
                                      `BOIRegNo`='$BOIRegistrationNo',
                                      `CordinationContact1Name`='$cordinationContact1Name',
                                      `CordinationContact1Designation`='$cordinationContact1Designation', 
                                      `CordinationContact1Mobile`='$cordinationContact1Mobile',
                                      `CordinationContact1Email`='$cordinationContact1Email',
                                      `CordinationContact2Name`='$cordinationContact2Name',
                                      `CordinationContact2Designation`='$cordinationContact2Designation', 
                                      `CordinationContact2Mobile`='$cordinationContact2Mobile',
                                      `CordinationContact2Email`='$cordinationContact2Email', 
                                      `PaymentsContact1Name`='$paymentsContact1Name',
                                      `PaymentsContact1Designation`='$paymentsContact1Designation', 
                                      `PaymentsContact1Mobile`='$paymentsContact1Mobile',
                                      `PaymentsContact1Email`='$paymentsContact1Email',
                                      `PaymentsManagerName`='$paymentsContact2Name',
                                      `PaymentsManagerDesignation`='$paymentsContact2Designation', 
                                      `PaymentsManagerMobile`='$paymentsContact2Mobile',
                                      `PaymentsManagerEmail`='$paymentsContact2Email',
                                      `LogisticsBoiCusDecContactName`='$customsContact1Name',
                                      `LogisticsBoiCusDecContactDesignation`='$customsContact1Designation',
                                      `LogisticsBoiCusDecContactMobile`='$customsContact1Mobile',
                                      `LogisticsBoiCusDecContactEmail`='$customsContact1Email',
                                      `ProprietorName`='$proprietorName',  
                                      `ProprietorDesignation`='$proprietorDesignation', 
                                      `ProprietorMobile`='$proprietorMobile', 
                                      `ProprietorEmail`='$proprietorEmail', 
                                      `Bank1Name`='$Bank1Name',
                                      `Bank1Branch`='$Bank1Branch',
                                      `Bank1LkrFcbuBoth`=$Bank1LkrFcbuBoth,
                                      `Bank2Name`='$Bank2Name',
                                      `Bank2Branch`='$Bank2Branch',
                                      `Bank2LkrFcbuBoth`=$Bank2LkrFcbuBoth, 
                                      `SettlementTerms_BankGuarantee_Flag`=$settlementTermsBankGuarantor,
                                      `SettlementTerms_BankGuaranteeCurrencyID`=$settlementTermsBankGuaranteeCurrency,
                                      `SettlementTerms_BankGuarantee_Amount`=$settlementTerms_BankGuarantee_Amount,
                                      `SettlementTerms_BankGuarantee_Expiry`= $settlementTerms_BankGuarantee_Expiry,
                                      `SettlementTerms_Bank`='$settlementTerms_Bank',
                                      `SettlementTerms_BankBranch`='$settlementTerms_BankBranch',    
                                      `SettlementTerms_GuarantorNo`='$settlementTerms_GuarantorNo',
                                      `SettlementTerms_AdvanceFlag`= $settlementTerms_Advance,
                                      `SettlementTerms_AdvancePercentage`=$settlementTerms_AdvancePercentage,
                                      `SettlementTerms_AdvancePaymentType`=$settlementTerms_AdvancePaymentType,
                                      `SettlementTerms_Credit`=$settlementTerms_Credit,
                                      `SettlementTerms_CreditPdcRequired`=$settlementTerms_CreditPdcRequired,
                                      `SettlementTerms_CreditPeriodBasis`=$settlementTerms_CreditPeriodBasis,
                                      `SettlementTerms_CreditPeriod`=$settlementTerms_CreditPeriod,
                                      `SettlementTerms_CreditLimit`=$settlementTerms_CreditLimit,
                                      `ShippingTermsID`=$shippingTerms,
                                      `PricingPolicyID`=$pricingPolicy,
                                      `Status`=$status,
                                      `IsWalkInCustomer`= $registered,
                                      `CustomerLocality`= $locality,   
                                      `EnteredBy` = $user,
                                      `EnteredDate` = NOW()
                                    WHERE
                                            (`ID` = $id);";
            }



            try 
            {
                $sql = mysqli_query ($con_main, $query);
            } 
            catch (Exception $cexcError) 
            {
                //echo var_dump($cexcError);
            }

//            echo $query;
//            exit();

            $id = ($op == "insert") ? mysqli_insert_id($con_main) : $id;

            if ($sql&&$id>0){
                    $success = true;
                    $message = "Success";
                    if($deleteBusinessRegistrationCertificate||$businessRegistrationCertificate["error"]==0)
                    {
                        $query="UPDATE `mas_customer` SET BusinessRegistrationCertificateFileName='' WHERE ID=" . $id . ";";
                        try 
                        {
                            $sql = mysqli_query ($con_main, $query);
                        } 
                        catch (Exception $cexcError) 
                        {
                            //echo var_dump($cexcError);
                        }
                        $folder=$_SERVER["DOCUMENT_ROOT"] . "/Gurind/CustomerBusinessRegistrationAttachments";
                        $files=scandir($folder);
                        foreach($files as $file)
                        {
                            $fileNameWithoutExtention=substr($file,0,strpos($file, "."));
                            if($fileNameWithoutExtention==$id)
                            {
                                unlink($folder . "/" . $file);
                            }
                        }
                    }
                    if($businessRegistrationCertificate["error"]==0)
                    {
                        $fileExtention=(substr($businessRegistrationCertificate["name"],strpos($businessRegistrationCertificate["name"],".")));
                        $query="UPDATE `mas_customer` SET BusinessRegistrationCertificateFileName='" . $fileExtention . "' WHERE ID=" . $id . ";";
                        try 
                        {
                            $sql = mysqli_query ($con_main, $query);
                        } 
                        catch (Exception $cexcError) 
                        {
                            //echo var_dump($cexcError);
                        }
                        $sourceFilePath=$businessRegistrationCertificate["tmp_name"];
                        $destinationFilePath=$_SERVER["DOCUMENT_ROOT"] . "/Gurind/CustomerBusinessRegistrationAttachments/" . $id . $fileExtention;
                        move_uploaded_file($sourceFilePath, $destinationFilePath);
                    }

                    if($deleteVATRegistrationCertificate||$VATRegistrationCertificate["error"]==0)
                    {
                        $query="UPDATE `mas_customer` SET VATRegCertificateFileName='' WHERE ID=" . $id . ";";
                        try 
                        {
                            $sql = mysqli_query ($con_main, $query);
                        } 
                        catch (Exception $cexcError) 
                        {
                            //echo var_dump($cexcError);
                        }
                        $folder=$_SERVER["DOCUMENT_ROOT"] . "/Gurind/CustomerVatRegistrationAttachments";
                        $files=scandir($folder);
                        foreach($files as $file)
                        {
                            $fileNameWithoutExtention=substr($file,0,strpos($file, "."));
                            if($fileNameWithoutExtention==$id)
                            {
                                unlink($folder . "/" . $file);
                            }
                        }
                    }
                    if($VATRegistrationCertificate["error"]==0)
                    {
                        $fileExtention=(substr($VATRegistrationCertificate["name"],strpos($VATRegistrationCertificate["name"],".")));
                        $query="UPDATE `mas_customer` SET VATRegCertificateFileName='" . $fileExtention . "' WHERE ID=" . $id . ";";
                        try 
                        {
                            $sql = mysqli_query ($con_main, $query);
                        } 
                        catch (Exception $cexcError) 
                        {
                            //echo var_dump($cexcError);
                        }
                        $sourceFilePath=$VATRegistrationCertificate["tmp_name"];
                        $destinationFilePath=$_SERVER["DOCUMENT_ROOT"] . "/Gurind/CustomerVatRegistrationAttachments/" . $id . $fileExtention;
                        move_uploaded_file($sourceFilePath, $destinationFilePath);
                    }

                    if($deleteSVATRegistrationCertificate||$SVATRegistrationCertificate["error"]==0)
                    {
                        $query="UPDATE `mas_customer` SET SVATRegCertificateFileName='' WHERE ID=" . $id . ";";
                        try 
                        {
                            $sql = mysqli_query ($con_main, $query);
                        } 
                        catch (Exception $cexcError) 
                        {
                            //echo var_dump($cexcError);
                        }
                        $folder=$_SERVER["DOCUMENT_ROOT"] . "/Gurind/CustomerSvatRegistrationAttachments";
                        $files=scandir($folder);
                        foreach($files as $file)
                        {
                            $fileNameWithoutExtention=substr($file,0,strpos($file, "."));
                            if($fileNameWithoutExtention==$id)
                            {
                                unlink($folder . "/" . $file);
                            }
                        }
                    }
                    if($SVATRegistrationCertificate["error"]==0)
                    {
                        $fileExtention=(substr($SVATRegistrationCertificate["name"],strpos($SVATRegistrationCertificate["name"],".")));
                        $query="UPDATE `mas_customer` SET SVATRegCertificateFileName='" . $fileExtention . "' WHERE ID=" . $id . ";";
                        try 
                        {
                            $sql = mysqli_query ($con_main, $query);
                        } 
                        catch (Exception $cexcError) 
                        {
                            //echo var_dump($cexcError);
                        }
                        $sourceFilePath=$SVATRegistrationCertificate["tmp_name"];
                        $destinationFilePath=$_SERVER["DOCUMENT_ROOT"] . "/Gurind/CustomerSvatRegistrationAttachments/" . $id . (substr($SVATRegistrationCertificate["name"],strpos($SVATRegistrationCertificate["name"],".")));
                        move_uploaded_file($sourceFilePath, $destinationFilePath);
                    }

                    if($deleteBOIRegistrationCertificate||$BOIRegistrationCertificate["error"]==0)
                    {
                        $query="UPDATE `mas_customer` SET BOIRegCertificateFileName='' WHERE ID=" . $id . ";";
                        try 
                        {
                            $sql = mysqli_query ($con_main, $query);
                        } 
                        catch (Exception $cexcError) 
                        {
                            //echo var_dump($cexcError);
                        }
                        $folder=$_SERVER["DOCUMENT_ROOT"] . "/Gurind/CustomerBoiRegistrationAttachments";
                        $files=scandir($folder);
                        foreach($files as $file)
                        {
                            $fileNameWithoutExtention=substr($file,0,strpos($file, "."));
                            if($fileNameWithoutExtention==$id)
                            {
                                unlink($folder . "/" . $file);
                            }
                        }
                    }
                    if($BOIRegistrationCertificate["error"]==0)
                    {
                        $fileExtention=(substr($BOIRegistrationCertificate["name"],strpos($BOIRegistrationCertificate["name"],".")));
                        $query="UPDATE `mas_customer` SET BOIRegCertificateFileName='" . $fileExtention . "' WHERE ID=" . $id . ";";
                        try 
                        {
                            $sql = mysqli_query ($con_main, $query);
                        } 
                        catch (Exception $cexcError) 
                        {
                            //echo var_dump($cexcError);
                        }
                        $sourceFilePath=$BOIRegistrationCertificate["tmp_name"];
                        $destinationFilePath=$_SERVER["DOCUMENT_ROOT"] . "/Gurind/CustomerBoiRegistrationAttachments/" . $id . (substr($BOIRegistrationCertificate["name"],strpos($BOIRegistrationCertificate["name"],".")));
                        move_uploaded_file($sourceFilePath, $destinationFilePath);
                    }

                    if($deleteSettlementTermsAttachment||$settlementTermsAttachment["error"]==0)
                    {
                        $query="UPDATE `mas_customer` SET SettlementTerms_Attachment='' WHERE ID=" . $id . ";";
                        try 
                        {
                            $sql = mysqli_query ($con_main, $query);
                        } 
                        catch (Exception $cexcError) 
                        {
                            //echo var_dump($cexcError);
                        }
                        $folder=$_SERVER["DOCUMENT_ROOT"] . "/Gurind/CustomerSettlementAttachments";
                        $files=scandir($folder);
                        foreach($files as $file)
                        {
                            $fileNameWithoutExtention=substr($file,0,strpos($file, "."));
                            if($fileNameWithoutExtention==$id)
                            {
                                unlink($folder . "/" . $file);
                            }
                        }
                    }
                    if($settlementTermsAttachment["error"]==0)
                    {
                        $fileExtention=(substr($settlementTermsAttachment["name"],strpos($settlementTermsAttachment["name"],".")));
                        $query="UPDATE `mas_customer` SET SettlementTerms_Attachment='" . $fileExtention . "' WHERE ID=" . $id . ";";
                        try 
                        {
                            $sql = mysqli_query ($con_main, $query);
                        } 
                        catch (Exception $cexcError) 
                        {
                            //echo var_dump($cexcError);
                        }
                        $sourceFilePath=$settlementTermsAttachment["tmp_name"];
                        $destinationFilePath=$_SERVER["DOCUMENT_ROOT"] . "/Gurind/CustomerSettlementAttachments/" . $id . (substr($settlementTermsAttachment["name"],strpos($settlementTermsAttachment["name"],".")));
                        move_uploaded_file($sourceFilePath, $destinationFilePath);
                    }
            }
            else
            {
                //echo $query . "<br/><br/><br/>";
                $success = false;
                $message = "Database SQL Error. ";
                //echo $query;
                echo "<script>window.parent.window.document.body.innerHTML=\"".$query."\";</script>";
                exit();
            }
            
            //throw new Exception("");


            try 
            {
                mysqli_query($con_main, "COMMIT;");
                mysqli_autocommit ($con_main);
            } 
            catch (Exception $cexcError) 
            {
                //echo var_dump($cexcError);
            }

            //$JsonStringToReturn="Damitha";

            echo ("<script>window.parent.window.ServerResponseReceived(\"".$op."\","."true".",".$id.",\"".$message."\");</script>");

        } catch (Exception $cexcError) {
            mysqli_query($con_main, "ROLLBACK;");
            mysqli_autocommit ($con_main);
            echo ("<script>window.parent.window.ServerResponseReceived(\"".$op."\","."false".",".$id.",\""."Error Occured At Server."."\");</script>");
        }        

	mysqli_close($con_main);
?>