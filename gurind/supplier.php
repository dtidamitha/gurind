<?php
	session_start();
	
	if (empty($_SESSION['ACCESS_CODE']) || $_SESSION['ACCESS_CODE'] == NULL){
		header ('Location: login.php');
		exit;
	}
	
	$folder_depth = "";
	$prefix = "";
	
	$folder_depth = substr_count($_SERVER["PHP_SELF"] , "/");
	$folder_depth = ($folder_depth == false) ? 2 : (int)$folder_depth;
	
    $prefix = str_repeat("../", $folder_depth - 2);
    
    $title_suffix = " Supplier Master";
?>
<?php include $prefix.'config.php'; ?>
<?php include $prefix.'menu.php'; ?>
<?php include $prefix.'template_start.php'; ?>
<?php include $prefix.'page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-podium"></i>Suppliers Master<br><small>Create, Update or Delete Suppliers</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="../home.php">Home</a></li>
        <li>Master Files</li>
        <li>Supplier Master</li>
    </ul>
    <!-- END Blank Header -->
    <div class="row">
        <div class="col-md-12">
             <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                       <label class="col-md-2 control-label" for="search">Search</label>
                            <div class="col-md-9">
                                 <select name="search" id="search" style="width:100%;">
                                     <option value="" selected disabled>Enter Supplier Name or Code</option>
                                </select>
                            </div>   
                    </div>
                </div>
            </div>
        </div>
    </div>
		
	
    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
					<h2>Suppliers</h2>
				</div>
                <!-- END Form Elements Title -->

                <!-- Basic Form Elements Content -->
                <form id="form-main" name="form-main" action="provider_crud.php" method="post"  class="form-horizontal form-bordered">  

                <div class="form-group">    
                     <label class="col-md-2 control-label" for="supcode">Supplier Code</label>
                        <div class="col-md-4">
                            <input type="text" id="supcode" name="supcode" class="form-control" placeholder="Enter supplier code"> 
                        </div>  

                    <label class="col-md-2 control-label" for="supname">Supplier Name</label>
                        <div class="col-md-4">
                           <input type="text" id="supname" name="supname" class="form-control" placeholder="Enter supplier">     
                        </div>             
                </div>
                       
                <div class="form-group">  
                           <label class="col-md-2 control-label" for="billname">Billing Name</label>        
                        <div class="col-md-4">                         
                            <input type="text" id="billname" name="billname" class="form-control" placeholder="Enter billing name">
                        </div>

                         <label class="col-md-2 control-label" for="shortname">Short Name</label>
                        <div class="col-md-4">                           
                            <input type="text" id="shortname" name="shortname" class="form-control" placeholder="Enter short name">
                        </div>
                </div>


                <div class="form-group"> 
                    <label class="col-md-2 control-label" for="country">Country</label>
                        <div class="col-md-4">                           
                            <select id="country" name="country" class="select-chosen" data-placeholder="Select Supplier Country"> 
                                <option></option>
                                   <?php
                                    $query="SELECT
                                                mas_countries.ID,
                                                mas_countries.CountryCode,
                                                mas_countries.CountryName
                                            FROM
                                               `mas_countries`";
                                    $sql = mysqli_query($con_main, $query);
                                                                            
                                    while ($type = mysqli_fetch_array($sql)){
                                        echo ("<option value=\"".$type['ID']."\">".$type['CountryCode']."-".$type['CountryName']."</option>");
                                    }
                                   ?>
                            </select>
                        </div> 
                           <label class="col-md-2 control-label" for="sourcecountry">Sourcing Country</label>        
                        <div class="col-md-4">                         
                            <input type="text" id="sourcecountry" name="sourcecountry" class="form-control" placeholder="Enter sourcing country">
                        </div>
                </div>
                    

                    <div class="form-group">  
                        <label class="col-md-2 control-label" for="address1">Address 1</label>
                        <div class="col-md-4">                           
                           <input type="text" id="address1" name="address1" class="form-control" placeholder="Enter address line 1">
                        </div>

                        <label class="col-md-2 control-label" for="address2">Address 2</label>        
                        <div class="col-md-4">                           
                           <input type="text" id="address2" name="address2" class="form-control" placeholder="Enter address line 2">
                        </div>
                    </div>


                    <div class="form-group">
                      <label class="col-md-2 control-label" for="city">City</label>
                         <div class="col-md-4">                           
                           <input type="text" id="city" name="city" class="form-control" placeholder="Enter city">
                        </div>

                        <label class="col-md-2 control-label" for="province">Province</label>
                        <div class="col-md-4">                           
                            <input type="text" id="province" name="province" class="form-control" placeholder="Enter province">
                        </div>
                    </div>


                    <div class="form-group">
                      <label class="col-md-2 control-label" for="postcode">Post Code</label>              
                        <div class="col-md-4">                           
                            <input type="text" id="postcode" name="postcode" class="form-control" placeholder="Enter post code">
                        </div>
                     <label class="col-md-2 control-label" for="supnote">Supplier Notes</label>  
                      <div class="col-md-4">
                            <textarea name="supnote" id="supnote" rows="4" cols="39" placeholder="Supplier notes" class="form-control"></textarea> 
                      </div>

                        
                    </div>

                    <fieldset>
                        <legend><i class="fa fa-angle-right"></i> Contact Person 1 Details</legend>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="contact1_name">Name</label>
                           <div class="col-md-4">
                              <input type="text" id="contact1_name" name="contact1_name" class="form-control" placeholder="Enter contact1 name"> 
                           </div>
                    	<label class="col-md-2 control-label" for="contact1_jobrole">Job Role</label>
                    	 <div class="col-md-4">
                            <input type="text" id="contact1_jobrole" name="contact1_jobrole" class="form-control" placeholder="Enter contact1 job role">
                        </div>    
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="contact1_tele">Phone No</label>
                         <div class="col-md-4">
                            <input type="text" id="contact1_tele" name="contact1_tele" class="form-control" placeholder="Enter contact1 phone no">
                        </div>
                        <label class="col-md-2 control-label" for="contact1_fax">fax</label>
                         <div class="col-md-4">
                            <input type="text" id="contact1_fax" name="contact1_fax" class="form-control" placeholder="Enter contact1 fax">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="contact1_email">Email</label>
                         <div class="col-md-4">
                            <input type="text" id="contact1_email" name="contact1_email" class="form-control" placeholder="Enter contact1 email">
                        </div>
                    </div>
                </fieldset>


                  <fieldset>
                        <legend><i class="fa fa-angle-right"></i> Contact Person 2 Details</legend>
                    <div class="form-group">

                        <label class="col-md-2 control-label" for="contact2_name">Name</label>
                           <div class="col-md-4">
                              <input type="text" id="contact2_name" name="contact2_name" class="form-control" placeholder="Enter contact2 name"> 
                           </div>

                           <label class="col-md-2 control-label" for="contact2_jobrole">Job Role</label>
                         <div class="col-md-4">
                            <input type="text" id="contact2_jobrole" name="contact2_jobrole" class="form-control" placeholder="Enter contact2 job role">
                        </div>

                    </div>

                    <div class="form-group">
                        
                        <label class="col-md-2 control-label" for="contact2_tele">Phone No</label>
                         <div class="col-md-4">
                            <input type="text" id="contact2_tele" name="contact2_tele" class="form-control" placeholder="Enter contact2 phone no">
                        </div>

                        <label class="col-md-2 control-label" for="contact2_fax">fax</label>
                         <div class="col-md-4">
                            <input type="text" id="contact2_fax" name="contact2_fax" class="form-control" placeholder="Enter contact2 fax">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="contact2_email">Email</label>
                         <div class="col-md-4">
                            <input type="text" id="contact2_email" name="contact2_email" class="form-control" placeholder="Enter contact2 email">
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                        <legend><i class="fa fa-angle-right"></i> Contact Person 3 Details</legend>
                    <div class="form-group">

                        <label class="col-md-2 control-label" for="contact3_name">Name</label>
                           <div class="col-md-4">
                              <input type="text" id="contact3_name" name="contact3_name" class="form-control" placeholder="Enter contact3 name"> 
                           </div>

                           <label class="col-md-2 control-label" for="contact3_jobrole">Job Role</label>
                         <div class="col-md-4">
                            <input type="text" id="contact3_jobrole" name="contact3_jobrole" class="form-control" placeholder="Enter contact2 job role">
                        </div>

                    </div>

                    <div class="form-group">
                        
                        <label class="col-md-2 control-label" for="contact3_tele">Phone No</label>
                         <div class="col-md-4">
                            <input type="text" id="contact3_tele" name="contact3_tele" class="form-control" placeholder="Enter contact2 phone no">
                        </div>

                        <label class="col-md-2 control-label" for="contact3_fax">fax</label>
                         <div class="col-md-4">
                            <input type="text" id="contact3_fax" name="contact3_fax" class="form-control" placeholder="Enter contact2 fax">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="contact3_email">Email</label>
                         <div class="col-md-4">
                            <input type="text" id="contact3_email" name="contact3_email" class="form-control" placeholder="Enter contact2 email">
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <legend><i class="fa fa-angle-right"></i> Supplier Availability Details</legend>
                    <div class="form-group">      
                        <label class="col-md-2 control-label" for="resigned">Select Resigned Date</label>
                        <div class="col-md-4">
                        <input type="text" id="resigned" name="resigned" class="form-control input-datepicker" data-date format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                        </div>
          
                       <label class="col-md-2 control-label" for="status">Supplier Status</label>
                        <div class="col-md-4">
                            <select id="status" name="status" class="select-chosen"> 
                               <option value="1" selected>Active</option>
                               <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>
                </fieldset>

                
                    <div class="form-group form-actions">
                        <input type="hidden" name="id" id="id" value="0" />

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success primary-btn pull-right"><i class="fa fa-angle-right"></i> Submit</button>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Block -->
		    </div>
            <!-- END Example Content -->
        </div>
        <!-- END Example Block -->
    </div>
</div>

<?php include $prefix.'page_footer.php'; ?>
<?php include $prefix.'template_scripts.php'; ?>
<?php include $prefix.'template_end.php'; ?>

<script type="text/javascript">

	/*********** Data-table Initialize ***********/
    $('#search').select2({
            minimumInputLength:2,
            ajax: {
                url: 'data/supplier_select.php',
                dataType: 'json',
                delay: 100,
                data: function (term) {
                    return term;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });

    $('#search').on('change', function (e){
            var crn_id = $('#search').select2('val');

            $.ajax({
                url: 'data/supplier_set.php',
                data: {
                    id: crn_id
                },
                method: 'POST',
                dataType: 'json',
                error: function (e){
                    alert ("Something went wrong when getting supplier details.");
                },
                success: function (r){
                    var result = r.result;
                    var message = r.message;
                    var data = r.data;

                    if (result){
                     $('#id').val(data.ID);
                     $('#supcode').val(data.SupplierCode);
                     $('#supname').val(data.SupplierName);
                     $('#billname').val(data.BillingName);
                     $('#shortname').val(data.ShortName);
                     $('#sourcecountry').val(data.SourcingCountry);
                     $('#country').val(data.Country);
                     $('#country').trigger("chosen:updated");
                     $('#address1').val(data.Address1);
                     $('#address2').val(data.Address2);
                     $('#city').val(data.City);
                     $('#province').val(data.Province);  
                     $('#postcode').val(data.PostCode);
                     $('#supnote').val(data.SupplierNotes);
                     $('#contact1_name').val(data.CP1_Name);
                     $('#contact1_jobrole').val(data.CP1_JobRole);
                     $('#contact1_tele').val(data.CP1_Phone);
                     $('#contact1_fax').val(data.CP1_Fax);
                     $('#contact1_email').val(data.CP1_Email);
                     $('#contact2_name').val(data.CP2_Name);
                     $('#contact2_jobrole').val(data.CP2_JobRole);
                     $('#contact2_tele').val(data.CP2_Phone);
                     $('#contact2_fax').val(data.CP2_Fax);
                     $('#contact2_email').val(data.CP2_Email);
                     $('#contact3_name').val(data.CP3_Name);
                     $('#contact3_jobrole').val(data.CP3_JobRole);
                     $('#contact3_tele').val(data.CP3_Phone);
                     $('#contact3_fax').val(data.CP3_Fax);
                     $('#contact3_email').val(data.CP3_Email);
                     $('#resigned').val(data.ResignedDate);
                     $('#status').val(data.status);
                     $('#status').trigger("chosen:updated");

                        }else{
                          alert (message);
                    }
                }
            });
        });
   // App.datatables();

   //  var dt = $('#table-data').DataTable({
   //      "processing": true,
   //      "serverSide": true,
   //      "select": true,
   //      "columns": [
			// { "data": "system", "name": "system", "title": "Supplier Name" },
			// { "data": "module", "name": "module", "title": "Full Name" },
   //          { "data": "company", "name": "company", "title": "Short Name"}, 
   //          { "data": "qamount", "name": "qamount", "title": "Address1"},
   //          { "data": "agreedamt", "name": "agreedamt", "title":"Address2"},
   //          { "data": "received", "name": "received", "title": "Country"},
   //          { "data": "balance", "name": "balance", "title": "Contact Person"},
   //          { "data": "stage", "name": "stage", "title": "Contact"},
   //          { "data": "cstatus", "name": "cstatus", "title": "Fax"},
   //          { "data": "date", "name": "date", "title": "Email"},
   //          { "data": "propsent", "name": "propsent", "title": "Shipment Mode"},
   //          { "data": "propno", "name": "propno", "title": "Payment Terms"},
   //          { "data": "status", "name": "status", "title": "Status"},
   //          {"data": "actions", "name": "actions","title":"Actions", "searchable": false, "orderable": false, 
   //              mRender: function (data, type, row) {
   //                  return '<div class="btn-group btn-group-xs"><button id="btn-row-edit" class="btn btn-primary" title="Edit"><i class="fa fa-pencil"></i></button></div>'
   //              }
   //          }
   //      ],
   //      "columnDefs":[
   //          {"className": "dt-center", "targets": [1,2,3]}
   //      ],
   //      "language": {
   //          "emptyTable": "No suppliers to show..."
   //      },
   //      "ajax": "data/grid_data_supplier.php"
   //  });
	
    // $('.dataTables_filter input').attr('placeholder', 'Search');

    // $("#table-data tbody").on('click', '#btn-row-edit', function() {
    //     var str_id = $(this).closest('tr').attr('id');
    //     var arr_id = str_id.split("_");

    //     var row_id = arr_id[1];
    //    // alert (row_id);

    //     $.ajax({
    //         url: 'data/data_supplier.php',
    //         data: {
    //             id: row_id
    //         },
    //         method: 'POST',
    //         dataType: 'json',
    //         beforeSend: function () {
    //             $('#table-data tbody #'+str_id+' #btn-row-edit').button('loading');
    //             NProgress.start();
    //         },
    //         error: function (e) {
    //             $.bootstrapGrowl('<h4>Error!</h4> <p>Error retrieving records data</p>', {
    //                 type: 'danger',
    //                 delay: 2500,
    //                 allow_dismiss: true
    //             });

    //             $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
    //             NProgress.done();
    //         },
    //         success: function (r) {
    //             if (!r.result) {
    //                 $.bootstrapGrowl('<h4>Error!</h4> <p>'+r.message+'</p>', {
    //                     type: 'danger',
    //                     delay: 2500,
    //                     allow_dismiss: true
    //                 });
    //             }else{
    //                 $('#id').val(r.data[0].id);
    //                 $('#supname').val(r.data[0].system);
    //                 $('#supfname').val(r.data[0].module);
    //                 $('#supsname').val(r.data[0].company);
    //                 $('#address1').val(r.data[0].quotamount);
    //                 $('#address2').val(r.data[0].agreedamount);
    //                 $('#country').val(r.data[0].received);
    //                 $('#contactperson').val(r.data[0].balance);          
    //                 $('#contact').val(r.data[0].date);
    //                 $('#fax').val(r.data[0].proposal_no);
    //                 $('#email').val(r.data[0].proposal_no); 
    //                 $('#shipment').val(r.data[0].proposal_no); 
    //                 $('#payment_terms').val(r.data[0].proposal_no);   

    //                 $('#status').val(r.data[0].stage);  
    //                 $('#status').trigger("chosen:updated");                          
    //             }

    //             $('#table-data tbody #'+str_id+' #btn-row-edit').button('reset');
    //             NProgress.done();
    //         }
    //     });
    // });
    /*********** Table Control End ***********/

    /*********** Form Validation and Submission ***********/
	$('#form-main').on('submit', function (e){
		e.preventDefault();
		
		var id = $('#id').val();
		var op = (id == 0) ? "insert" : "update";
		
		var formdata = $('#form-main').serializeArray();
		formdata.push({'name':'operation','value':op});
		
		$.ajax({
			url: 'supplier_crud.php',
			data: formdata,
			success: function(r){
				var msg_typ = "info";
                var msg_txt = "";

                if (r.result){
                    msg_typ = 'success';
                    msg_txt = '<h4>Success!</h4> <p>Supplier saved</p>';

                    $('#form-main').trigger('reset');
                }else{
                    msg_typ = 'danger';
                    msg_txt = '<h4>Error!</h4> <p>'+r.message+'</p>';
                }

                $.bootstrapGrowl(msg_txt, {
                    type: msg_typ,
                    delay: 2500,
                    allow_dismiss: true
                });

                dt.ajax.reload();
                dt.draw();
			}
		});
	});

    $('#form-main').on('reset', function (e){
        $('#id').val("0");
        $('#supcode').val("");
        $('#supname').val("");
        $('#billname').val("");
        $('#shortname').val("");
        $('#sourcecountry').val("");
        $('#country').val("");
        $('#address1').val("");
        $('#address2').val("");
        $('#city').val("");
        $('#province').val("");
        $('#postcode').val("");
        $('#supnote').val("");
        $('#contact1_name').val("");
        $('#contact1_jobrole').val("");
        $('#contact1_tele').val("");
        $('#contact1_fax').val("");
        $('#contact1_email').val("");
        $('#contact2_name').val("");
        $('#contact2_jobrole').val("");
        $('#contact2_tele').val("");
        $('#contact2_fax').val("");
        $('#contact2_email').val("");
        $('#contact3_name').val("");
        $('#contact3_jobrole').val("");
        $('#contact3_tele').val("");
        $('#contact3_fax').val("");
        $('#contact3_email').val("");
        $('#resigned').val("");
        $('#status').val(1);
        $('#status').trigger("chosen:updated");
        $('#search').val("");
        $('#search').trigger("chosen:updated");
        $('#country').val("");
        $('#country').trigger("chosen:updated");

        
    });
	</script>
	<?php mysqli_close($con_main); ?>