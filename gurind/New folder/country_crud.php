<?php
	header('Content-Type: application/json');
	
	session_start();
	
	require_once ('../config.php');
	
	$op = $_REQUEST['operation']; 
	$id = $_REQUEST['id'];  
	$supcode = $_REQUEST['supcode']; 
	$colorcode = $_REQUEST['colorcode']; 
	$solartrans = $_REQUEST['solartrans']; 
	$uvalue = $_REQUEST['uvalue']; 
	$externalref = $_REQUEST['externalref'];
    $internalref = $_REQUEST['internalref'];
    $file_main = $_REQUEST['file_main'];
    $user = $_SESSION['USER_CODE'];
    
	$query = "";
	$success = true;
	$message = "";
	$responce = array();
	
	if ($op == "insert"){

$query = "INSERT INTO `mas_supplier_product` (
	`SupplierID`,
	`ColorID`,
	`SolarTrans`,
	`Uvalue`,
	`ExternalRef`,
	`InternalRef`,
	`File`,
	`EnteredBy`,
	`EnteredDate`
)
VALUES
	(
		'$supcode',
		'$colorcode',
		'$solartrans',
		'$uvalue',
		'$externalref',
		'$internalref',
		'$file_main',
		'$user',
		NOW()
	);";
	}
	else if ($op == "update"){
		$query = "UPDATE `mas_supplier_product`
					SET `SupplierID` = '$supcode',
					 `ColorID` = '$colorcode',
					 `SolarTrans` = '$solartrans',
					 `Uvalue` = '$uvalue',
					 `ExternalRef` = '$externalref',
					 `InternalRef` = '$internalref',
					 `File` = '$file_main',
					 `EnteredBy` = '$user',
					 `EnteredDate` = NOW()
					WHERE
						(`ID` = '$id');";
	}
	
	$sql = mysqli_query ($con_main, $query);
	
	$id = ($op == "insert") ? mysqli_insert_id($con_main) : $id;
	
	if ($sql){
		$success = true;
		$message = "Success";
	}else{
		$success = false;
		$message = "Error SQL: (".mysqli_errno($con_main).") ".mysqli_error($con_main);
	}
	
	$responce['operation'] = $op;
	$responce['result'] = $success;
	$responce['id'] = $id;
	$responce['message'] = $message;
	$responce['debug'] = $query;

	
	echo (json_encode($responce));

	
	mysqli_close($con_main);
?>