<?php
	header('Content-Type: application/json');
	
	session_start();
	
	require_once ('../config.php');
	
	$op = $_REQUEST['operation']; 
	$id = $_REQUEST['id'];  
	$locid = $_REQUEST['locid']; 
	$rackcode = $_REQUEST['rackcode']; 
	$rackname = $_REQUEST['rackname']; 
	$status = $_REQUEST['status'];
	$user = $_SESSION['USER_CODE']; 
	
	$query = "";
	$success = true;
	$message = "";
	$responce = array();
	
	if ($op == "insert"){

		$query = "INSERT INTO `mas_rack` (
										`loc_id`,
										`rack_code`,
										`rack_name`,
										`status`,
										`entered_by`,
										`entered_date`
									)
					VALUES
						($locid, '$rackcode', '$rackname', $status,$user,NOW());";
	}

	else if ($op == "update"){

		$query =   "UPDATE `mas_rack`

					SET  `loc_id` = $locid,
						 `rack_code` = '$rackcode',
						 `rack_name` = '$rackname',
						 `status` = $status,
						 `entered_by` = $user,
						 `entered_date` = NOW()

					WHERE
						(`id` = $id);";
	}
	
	$sql = mysqli_query ($con_main, $query);
	
	$id = ($op == "insert") ? mysqli_insert_id($con_main) : $id;
	
	if ($sql){
		$success = true;
		$message = "Success";
	}else{
		$success = false;
		$message = "Error SQL: (".mysqli_errno($con_main).") ".mysqli_error($con_main);
	}
	
	$responce['operation'] = $op;
	$responce['result'] = $success;
	$responce['id'] = $id;
	$responce['message'] = $message;
	$responce['debug'] = $query;

	
	echo (json_encode($responce));

	
	mysqli_close($con_main);
?>