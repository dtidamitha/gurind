

<?php
	session_start();
	
	if (empty($_SESSION['ACCESS_CODE']) || $_SESSION['ACCESS_CODE'] == NULL){
		header ('Location: login.php');
		exit;
	}
	
	$folder_depth = "";
	$prefix = "";
	
	$folder_depth = substr_count($_SERVER["PHP_SELF"] , "/");
	$folder_depth = ($folder_depth == false) ? 2 : (int)$folder_depth;
	
    $prefix = str_repeat("../", $folder_depth - 2);
    
    $title_suffix = " Customer Master";
?>
<?php include $prefix.'config.php'; ?>
<?php include $prefix.'menu.php'; ?>
<?php include $prefix.'template_start.php'; ?>
<?php include $prefix.'page_head.php'; ?>


<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-podium"></i>Customers Master<br><small>Create, Update or Delete Master</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="../home.php">Home</a></li>
        <li>Master Files</li>
        <li>Customer Master</li>
    </ul>
    <!-- END Blank Header -->
    
    <div class="row">
        <div class="col-md-12">
             <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group">
                       <label class="col-md-2 control-label" for="search">Search</label>
                            <div class="col-md-9">
                                 <select name="search" id="search" style="width:100%;">
                                     <option value="" selected disabled>Enter Customer, Name or Code</option>
                                 </select>
                            </div>   
                    </div>
                </div>
            </div>
        </div>
    </div>
	
    <div class="row">
        <div class="col-md-12">
            <!-- Basic Form Elements Block -->
            <div class="block">
                <!-- Basic Form Elements Title -->
                <div class="block-title">
					<h2>Customer</h2>
				</div>
                <!-- END Form Elements Title -->
                <iframe id="ifrmTarget" name="Target" style="display:none;"></iframe>
                <!-- Basic Form Elements Content -->
                <form id="form-main" name="form-main" enctype="multipart/form-data" action="customer_crud.php" method="post"  class="form-horizontal form-bordered" target="Target">  <!---->

                <div class="form-group">                      
                        <label class="col-md-2 control-label" for="code">Code</label>
                        <div class="col-md-4">
                            <table style="width:100%;">
                                <tr>
                                    <td style="width:100%;">
                                        <input type="text" id="code" name="code" class="form-control" placeholder="Enter customer code" maxlength="10" required="true" onchange="//ValidateCustomerCode();"  onblur="ValidateCustomerCode();"  onkeyup="//ValidateCustomerCode();"/>     
                                    </td>
                                    <td style="max-width: 25px;">
                                        <img id="imgCustomerCode_Required" src="../images/ErrorIcon.jpg" class="Error imgCustomerCode" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Customer Code is Required!"/>
                                        <img id="imgCustomerCode_Duplicating" src="../images/ErrorIcon.jpg" class="Error imgCustomerCode" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Customer Code is Duplicating!"/>
                                    </td>
                                </tr>
                            </table>
                            
                        </div> 
                        

                </div>
                    
                <div class="form-group">                      

                        <label class="col-md-2 control-label" for="name">Name</label>
                        <div class="col-md-10">
                            <table style="width:100%;">
                                <tr>
                                    <td style="width:100%;">
                                        <input type="text" id="name" name="name" class="form-control OnlyAlphabetic" placeholder="Enter customer name" maxlength="150" required="true"  onchange="ValidateCustomerName();"  onkeyup="ValidateCustomerName();" onblur="ValidateCustomerName();"> 
                                    </td>
                                    <td style="max-width: 25px;">
                                        <img id="imgCustomerName_Required" src="../images/ErrorIcon.jpg" class="Error imgCustomerName" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Customer Name is Required!"/>
                                        <img id="imgCustomerName_Minimum2" src="../images/ErrorIcon.jpg" class="Error imgCustomerName" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Customer Name should be atleast 2 characters  long!"/>
                                    </td>
                                </tr>
                            </table>
                            
                        </div>  


                </div>
                    
                <div class="form-group">                      

                    <label class="col-md-2 control-label" for="billingName">Billing Name</label>
                    <div class="col-md-10">
                        <table style="width:100%;">
                                <tr>
                                    <td style="width:100%;">
                                        <input type="text" id="billingName" name="billingName" class="form-control OnlyAlphabetic" placeholder="Enter billing name" maxlength="150" required="true" onchange="//ValidateBillingName();"  onblur="ValidateBillingName();"  onkeyup="//ValidateBillingName();"> 
                                    </td>
                                    <td style="max-width: 25px;">
                                        <img id="imgBillingName_Required" src="../images/ErrorIcon.jpg" class="Error imgBillingName" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Billing Name is Required!" />
                                        <img id="imgBillingName_Minimum2" src="../images/ErrorIcon.jpg" class="Error imgBillingName" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Billing Name should be atleast 2 characters  long!"/>
                                    </td>
                                </tr>
                            </table>
                        
                    </div> 

                </div>
                    
                <div class="form-group">                      
                     
                        <label class="col-md-2 control-label" for="address">Address</label>
                        <div class="col-md-4">
                            <textarea id="address" name="address" class="form-control" placeholder="Enter address" maxlength="500" cols="100" rows="5" style="resize:none;"></textarea>
                        </div> 
                        
                        <div class="col-md-6">
                            <div class="row">
                                <label class="col-md-2 control-label" for="city">City</label>
                                <div class="col-md-10">
                                    <input type="text" id="city" name="city" class="form-control" placeholder="Enter city" maxlength="100"> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 disapearApear" >
                                    &nbsp;
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-md-2 control-label" for="country">Country</label>
                                <div class="col-md-10">
                                    <select id="country" name="country" class="select-chosen" >
                                        <?php
                                            $query="SELECT * FROM mas_countries ORDER BY countryname";
                                            $queryResult = mysqli_query ($con_main, $query);
                                            while($row =  mysqli_fetch_array($queryResult))
                                            {
                                                echo "<option value='".$row["ID"]."'".($row["CountryCode"]=="SL"?"' selected='selected'>":"'>").$row["CountryCode"]."-".$row["CountryName"]."</option>";
                                            }
                                        ?>
                                    </select> 
                                </div>
                            </div>
                            
                        </div>
                            
                        
                </div>
   
                    <div class="form-group">
                    	<label class="col-md-2 control-label" for="genLineNos">General Line No.</label>
                    	<div class="col-md-10">
                            <input type="text" id="genLineNos" name="genLineNos" class="form-control" placeholder="Enter general line numbers" maxlength="100">
                        </div>
                    </div>
                    
                    <div class="form-group">
                    	<label class="col-md-2 control-label" for="businessRegistrationNo">Business Reg. No.</label>
                    	 <div class="col-md-4">
                            <input type="text" id="businessRegistrationNo" name="businessRegistrationNo" class="form-control" placeholder="Enter business reg. no." maxlength="100">
                        </div>
                        <label class="col-md-2 control-label" for="businessRegistrationCertificate">Certificate</label>
                    	<div class="col-md-2" style="min-width:188px;overflow-x: hidden;">
                            <input type="file" id="businessRegistrationCertificate" name="businessRegistrationCertificate"  placeholder="Enter business reg. cert.">
                            <a id="businessRegistrationCertificateDownloadLink"></a>
                        </div>
                        <div class="col-md-2 deleteUploaded" style="max-width:132px;">
                            <input type="checkbox" id="deleteBusinessRegistrationCertificate" name="deleteBusinessRegistrationCertificate">&nbsp;&nbsp;Delete even if not uploading
                        </div>
                    </div>
                    
                    <div class="form-group">
                    	<label class="col-md-2 control-label" for="VATRegistrationNo">VAT Reg. No.</label>
                    	 <div class="col-md-4">
                            <input type="text" id="VATRegistrationNo" name="VATRegistrationNo" class="form-control" placeholder="Enter VAT reg. no." maxlength="100">
                        </div>
                        <label class="col-md-2 control-label" for="VATRegistrationCertificate">Certificate</label>
                    	<div class="col-md-2" style="min-width:188px;overflow-x: hidden;">
                            <input type="file" id="VATRegistrationCertificate" name="VATRegistrationCertificate"  placeholder="Enter VAT reg. cert.">
                            <a id="VATRegistrationCertificateDownloadLink" target="_blank"></a>
                        </div>
                        <div class="col-md-2 deleteUploaded" style="max-width:132px;">
                            <input type="checkbox" id="deleteVATRegistrationCertificate" name="deleteVATRegistrationCertificate">&nbsp;&nbsp;Delete even if not uploading
                        </div>
                    </div>
                    
                    <div class="form-group">
                    	<label class="col-md-2 control-label" for="SVATRegistrationNo">SVAT Reg. No.</label>
                    	<div class="col-md-4">
                            <input type="text" id="SVATRegistrationNo" name="SVATRegistrationNo" class="form-control" placeholder="Enter SVAT reg. no." maxlength="100">
                        </div>
                        <label class="col-md-2 control-label" for="SVATRegistrationCertificate">Certificate</label>
                    	 <div class="col-md-2" style="min-width:188px;overflow-x: hidden;">
                            <input type="file" id="SVATRegistrationCertificate" name="SVATRegistrationCertificate"  placeholder="Enter SVAT reg. cert.">
                            <a id="SVATRegistrationCertificateDownloadLink" target="_blank"></a>
                        </div>
                        <div class="col-md-2 deleteUploaded" style="max-width:132px;">
                            <input type="checkbox" id="deleteSVATRegistrationCertificate" name="deleteSVATRegistrationCertificate">&nbsp;&nbsp;Delete even if not uploading
                        </div>
                    </div>
                    
                    <div class="form-group">
                    	<label class="col-md-2 control-label" for="BOIRegistrationNo">BOI Reg. No.</label>
                    	 <div class="col-md-4">
                            <input type="text" id="BOIRegistrationNo" name="BOIRegistrationNo" class="form-control" placeholder="Enter BOI reg. no." maxlength="100">
                        </div>
                        <label class="col-md-2 control-label" for="BOIRegistrationCertificate">Certificate</label>
                        <div class="col-md-2" style="min-width:188px;overflow-x: hidden;">
                            <input type="file" id="BOIRegistrationCertificate" name="BOIRegistrationCertificate"  placeholder="Enter BOI reg. cert.">
                            <a id="BOIRegistrationCertificateDownloadLink" target="_blank"></a>
                        </div>
                        <div class="col-md-2 deleteUploaded" style="max-width:132px;">
                            <input type="checkbox" id="deleteBOIRegistrationCertificate" name="deleteBOIRegistrationCertificate">&nbsp;&nbsp;Delete even if not uploading
                        </div>
                    </div>
                    
                    <fieldset>
                        <legend>Cordination</legend>
                        <h5>&nbsp;&nbsp;&nbsp;Contact Person #1</h5>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="cordinationContact1Name">Name</label>
                            <div class="col-md-4">
                                <input type="text" id="cordinationContact1Name" name="cordinationContact1Name" class="form-control OnlyAlphabetic" placeholder="Enter cordination contact #1 name" maxlength="150">
                            </div>
                            <label class="col-md-2 control-label" for="cordinationContact1Designation">Designation</label>
                            <div class="col-md-4">
                                <input type="text" id="cordinationContact1Designation" name="cordinationContact1Designation" class="form-control OnlyAlphabetic" placeholder="Enter cordination contact #1 designation" maxlength="150">
                            </div>
                            <label class="col-md-2 control-label" for="cordinationContact1Name">Mobile</label>
                            <div class="col-md-4">
                                <input type="text" id="cordinationContact1Mobile" name="cordinationContact1Mobile" class="form-control OnlyNumber" placeholder="Enter cordination contact #1 mobile" maxlength="10">
                            </div>
                            <label class="col-md-2 control-label" for="cordinationContact1DesignationEmail">E-mail</label>
                            <div class="col-md-4">
                                <table style="width:100%;">
                                <tr>
                                    <td style="width:100%;">
                                        <input type="email" id="cordinationContact1Email" name="cordinationContact1Email" class="form-control" placeholder="Enter cordination contact #1 e-mail" maxlength="150" onchange="//ValidateCordinationContact1Email();" onblur="ValidateCordinationContact1Email();" onkeyup="//ValidateCordinationContact1Email();">
                                    </td>
                                    <td style="max-width: 25px;">
                                        <img id="imgCordinationContact1Email_Required" src="../images/ErrorIcon.jpg" class="Error imgCordinationContact1Email" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Cordination Contact #1 Email is Required!"/>
                                        <img id="imgCordinationContact1Email_InValidFormat" src="../images/ErrorIcon.jpg" class="Error imgCordinationContact1Email" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Cordination Contact #1 Email Format Invalid!"/>
                                    </td>
                                </tr>
                            </table>
                                
                            </div>
                        </div>
                        <h5>&nbsp;&nbsp;&nbsp;Contact Person #2</h5>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="cordinationContact2Name">Name</label>
                            <div class="col-md-4">
                                <input type="text" id="cordinationContact2Name" name="cordinationContact2Name" class="form-control OnlyAlphabetic" placeholder="Enter cordination contact #1 name" maxlength="150">
                            </div>
                            <label class="col-md-2 control-label" for="cordinationContact2Designation">Designation</label>
                            <div class="col-md-4">
                                <input type="text" id="cordinationContact2Designation" name="cordinationContact2Designation" class="form-control OnlyAlphabetic" placeholder="Enter cordination contact #1 designation" maxlength="150">
                            </div>
                            <label class="col-md-2 control-label" for="cordinationContact2Name">Mobile</label>
                            <div class="col-md-4">
                                <input type="text" id="cordinationContact2Mobile" name="cordinationContact2Mobile" class="form-control OnlyNumber" placeholder="Enter cordination contact #1 mobile" maxlength="10">
                            </div>
                            <label class="col-md-2 control-label" for="cordinationContact2Email">E-mail</label>
                            <div class="col-md-4">
                                <table style="width:100%;">
                                <tr>
                                    <td style="width:100%;">
                                        <input type="email" id="cordinationContact2Email" name="cordinationContact2Email" class="form-control" placeholder="Enter cordination contact #2 e-mail" maxlength="250" onchange="//ValidateCordinationContact2Email();" onblur="ValidateCordinationContact2Email();" onkeyup="//ValidateCordinationContact2Email();">
                                    </td>
                                    <td style="max-width: 25px;">
                                        <img id="imgCordinationContact2Email_Required" src="../images/ErrorIcon.jpg" class="Error imgCordinationContact2Email" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Cordination Contact #2 Email is Required!"/>
                                        <img id="imgCordinationContact2Email_InValidFormat" src="../images/ErrorIcon.jpg" class="Error imgCordinationContact2Email" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Cordination Contact #2 Email Format Invalid!"/>
                                    </td>
                                </tr>
                            </table>

                            </div>
                        </div>
                    </fieldset>
                    
                    <fieldset>
                        <legend>Payments</legend>
                        <h5>&nbsp;&nbsp;&nbsp;Contact Person #1</h5>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="paymentsContact1Name">Name</label>
                            <div class="col-md-4">
                                <input type="text" id="paymentsContact1Name" name="paymentsContact1Name" class="form-control OnlyAlphabetic" placeholder="Enter payments contact #1 name" maxlength="150">
                            </div>
                            <label class="col-md-2 control-label" for="paymentsContact1Designation">Designation</label>
                            <div class="col-md-4">
                                <input type="text" id="paymentsContact1Designation" name="paymentsContact1Designation" class="form-control OnlyAlphabetic" placeholder="Enter payments contact #1 designation" maxlength="150">
                            </div>
                            <label class="col-md-2 control-label" for="paymentsContact1Name">Mobile</label>
                            <div class="col-md-4">
                                <input type="text" id="paymentsContact1Mobile" name="paymentsContact1Mobile" class="form-control OnlyNumber" placeholder="Enter payments contact #1 mobile" maxlength="10">
                            </div>
                            <label class="col-md-2 control-label" for="paymentsContact1Email">E-mail</label>
                            <div class="col-md-4">
                                <table style="width:100%;">
                                <tr>
                                    <td style="width:100%;">
                                        <input type="email" id="paymentsContact1Email" name="paymentsContact1Email" class="form-control" placeholder="Enter payments contact #1 e-mail" maxlength="150" onchange="//ValidatePaymentsContact1Email();" onblur="ValidatePaymentsContact1Email();" onkeyup="//ValidatePaymentsContact1Email();">
                                    </td>
                                    <td style="max-width: 25px;">
                                        <img id="imgPaymentsContact1Email_Required" src="../images/ErrorIcon.jpg" class="Error imgPaymentsContact1Email" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Payments Contact #1 Email is Required!"/>
                                        <img id="imgPaymentsContact1Email_InValidFormat" src="../images/ErrorIcon.jpg" class="Error imgPaymentsContact1Email" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Payments Contact #1 Email Format Invalid!"/>
                                    </td>
                                </tr>
                            </table>
                                
                            </div>
                        </div>
                        <h5>&nbsp;&nbsp;&nbsp;Contact Person #2 <strong>( Managerial )</strong></h5>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="paymentsContact2Name">Name</label>
                            <div class="col-md-4">
                                <input type="text" id="paymentsContact2Name" name="paymentsContact2Name" class="form-control OnlyAlphabetic" placeholder="Enter payments contact #1 name" maxlength="150">
                            </div>
                            <label class="col-md-2 control-label" for="paymentsContact2Designation">Designation</label>
                            <div class="col-md-4">
                                <input type="text" id="paymentsContact2Designation" name="paymentsContact2Designation" class="form-control OnlyAlphabetic" placeholder="Enter payments contact #1 designation" maxlength="150">
                            </div>
                            <label class="col-md-2 control-label" for="paymentsContact2Mobile">Mobile</label>
                            <div class="col-md-4">
                                <input type="text" id="paymentsContact2Mobile" name="paymentsContact2Mobile" class="form-control OnlyNumber" placeholder="Enter payments contact #1 mobile" maxlength="10">
                            </div>
                            <label class="col-md-2 control-label" for="paymentsContact2Email">E-mail</label>
                            <div class="col-md-4">
                                <table style="width:100%;">
                                <tr>
                                    <td style="width:100%;">
                                        <input type="email" id="paymentsContact2Email" name="paymentsContact2Email" class="form-control" placeholder="Enter payments contact #2 e-mail" maxlength="250" onchange="//ValidatePaymentsContact2Email();" onblur="ValidatePaymentsContact2Email();" onkeyup="//ValidatePaymentsContact2Email();">
                                    </td>
                                    <td style="max-width: 25px;">
                                        <img id="imgPaymentsContact2Email_Required" src="../images/ErrorIcon.jpg" class="Error imgPaymentsContact2Email" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Payments Contact #2 Email is Required!"/>
                                        <img id="imgPaymentsContact2Email_InValidFormat" src="../images/ErrorIcon.jpg" class="Error imgPaymentsContact2Email" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Payments Contact #2 Email Format Invalid!"/>
                                    </td>
                                </tr>
                            </table>
                                
                            </div>
                        </div>
                    </fieldset>
                    
                    <fieldset>
                        <legend>Logistics/BOI/Cust-Dec Handling</legend>
                        <h5>&nbsp;&nbsp;&nbsp;Contact Person #1</h5>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="logisticsBoiCusDecContact1Name">Name</label>
                            <div class="col-md-4">
                                <input type="text" id="logisticsBoiCusDecContact1Name" name="logisticsBoiCusDecContact1Name" class="form-control OnlyAlphabetic" placeholder="Enter Logistics/Boi/CusDec contact #1 name" maxlength="150">
                            </div>
                            <label class="col-md-2 control-label" for="logisticsBoiCusDecContact1Designation">Designation</label>
                            <div class="col-md-4">
                                <input type="text" id="logisticsBoiCusDecContact1Designation" name="logisticsBoiCusDecContact1Designation" class="form-control OnlyAlphabetic" placeholder="Enter Logistics/Boi/CusDec contact #1 designation" maxlength="150">
                            </div>
                            <label class="col-md-2 control-label" for="logisticsBoiCusDecContact1Mobile">Mobile</label>
                            <div class="col-md-4">
                                <input type="text" id="logisticsBoiCusDecContact1Mobile" name="logisticsBoiCusDecContact1Mobile" class="form-control OnlyNumber" placeholder="Enter Logistics/Boi/CusDec contact #1 mobile" maxlength="10">
                            </div>
                            <label class="col-md-2 control-label" for="logisticsBoiCusDecContact1Email">E-mail</label>
                            <div class="col-md-4">
                                <table style="width:100%;">
                                <tr>
                                    <td style="width:100%;">
                                        <input type="email" id="logisticsBoiCusDecContact1Email" name="logisticsBoiCusDecContact1Email" class="form-control" placeholder="Enter Logistics/Boi/CusDec contact #1 e-mail" maxlength="150"  onchange="//ValidateLogisticsContact1Email();" onblur="ValidateLogisticsContact1Email();" onkeyup="//ValidateLogisticsContact1Email();">
                                    </td>
                                    <td style="max-width: 33px;">
                                        <img id="imgLogisticsContact1Email_Required" src="../images/ErrorIcon.jpg" class="Error imgLogisticsContact1Email" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Logistics Contact #1 Email is Required!"/>
                                        <img id="imgLogisticsContact1Email_InValidFormat" src="../images/ErrorIcon.jpg" class="Error imgLogisticsContact1Email" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Logistics Contact #1 Email Format Invalid!"/>
                                    </td>
                                </tr>
                            </table>
                                
                            </div>
                        </div>
                    </fieldset>
                    
                    <fieldset>
                        <legend>Proprietor/Director</legend>
                        <h5>&nbsp;&nbsp;&nbsp;Contact details</h5>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="proprietorName">Name</label>
                            <div class="col-md-4">
                                <input type="text" id="proprietorName" name="proprietorName" class="form-control OnlyAlphabetic" placeholder="Enter logisticsBoiCusDec contact #1 name" maxlength="150">
                            </div>
                            <label class="col-md-2 control-label" for="proprietorDesignation">Designation</label>
                            <div class="col-md-4">
                                <input type="text" id="proprietorDesignation" name="proprietorDesignation" class="form-control OnlyAlphabetic" placeholder="Enter logisticsBoiCusDec contact #1 designation" maxlength="150">
                            </div>
                            <label class="col-md-2 control-label" for="proprietorMobile">Mobile</label>
                            <div class="col-md-4">
                                <input type="text" id="proprietorMobile" name="proprietorMobile" class="form-control OnlyNumber" placeholder="Enter logisticsBoiCusDec contact #1 mobile" maxlength="10">
                            </div>
                            <label class="col-md-2 control-label" for="proprietorEmail">E-mail</label>
                            <div class="col-md-4">
                                <table style="width:100%;">
                                <tr>
                                    <td style="width:100%;">
                                        <input type="email" id="proprietorEmail" name="proprietorEmail" class="form-control" placeholder="Enter logisticsBoiCusDec contact #1 e-mail" maxlength="150"  onchange="//ValidateProprietorEmail();" onblur="ValidateProprietorEmail();" onkeyup="//ValidateProprietorEmail();">
                                    </td>
                                    <td style="max-width: 25px;">
                                        <img id="imgProprietorEmail_Required" src="../images/ErrorIcon.jpg" class="Error imgProprietorEmail" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Proprietor Email is Required!"/>
                                        <img id="imgProprietorEmail_InValidFormat" src="../images/ErrorIcon.jpg" class="Error imgProprietorEmail" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Proprietor Email Format Invalid!"/>
                                    </td>
                                </tr>
                            </table>
                                
                            </div>
                        </div>
                    </fieldset>
                    
                    <fieldset>
                        <legend>Bank details</legend>
                        <h5>&nbsp;&nbsp;&nbsp;Bank #1</h5>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="row">
                                    <label class="col-md-2 control-label" for="Bank1Name">Name</label>
                                    <div class="col-md-10">
                                        <input type="text" id="Bank1Name" name="Bank1Name" class="form-control OnlyAlphabetic" placeholder="Enter bank #1 name" maxlength="150">
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-2 control-label" for="Bank1Branch">Branch</label>
                                    <div class="col-md-10">
                                        <input type="text" id="Bank1Branch" name="Bank1Branch" class="form-control OnlyAlphabetic" placeholder="Enter bank #1 branch" maxlength="150">
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <label class="col-md-12 control-label" for="Bank1LkrFcbuBoth"></label>
                                </div>
                                <div class="row" style="text-align: center;">
                                    <table style="display: inline-block;">
                                        <tr>
                                            <td>
                                                <input type="radio" id="radioBank1Lkr" name="Bank1LkrFcbuBoth" class="form-control" value="1"  checked="checked"/>
                                            </td>
                                            <td>
                                                &nbsp;LKR
                                            </td>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;
                                            </td>
                                            <td>
                                                <input type="radio" id="radioBank1Fcbu" name="Bank1LkrFcbuBoth" class="form-control" value="2" />
                                            </td>
                                            <td>
                                                &nbsp;FCBU
                                            </td>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;
                                            </td>
                                            <td>
                                                <input type="radio" id="radioBank1Both" name="Bank1LkrFcbuBoth" class="form-control" value="3"/>
                                            </td>
                                            <td>
                                                &nbsp;Both
                                            </td>
                                        </tr>
                                    </table>
                                    
                                </div>
                                
                            </div>
                        </div>
                        <h5>&nbsp;&nbsp;&nbsp;Bank #2</h5>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="row">
                                    <label class="col-md-2 control-label" for="Bank2Name">Name</label>
                                    <div class="col-md-10">
                                        <input type="text" id="Bank2Name" name="Bank2Name" class="form-control OnlyAlphabetic" placeholder="Enter bank #2 name" maxlength="150">
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-2 control-label" for="Bank2Branch">Branch</label>
                                    <div class="col-md-10">
                                        <input type="text" id="Bank2Branch" name="Bank2Branch" class="form-control OnlyAlphabetic" placeholder="Enter bank #2 branch" maxlength="150">
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <label class="col-md-12 control-label" for="Bank2LkrFcbuBoth"></label>
                                </div>
                                <div class="row" style="text-align: center;">
                                    <table style="display: inline-block;">
                                        <tr>
                                            <td>
                                                <input type="radio" id="radioBank2Lkr" name="Bank2LkrFcbuBoth" class="form-control"  value="1" checked="checked"/>
                                            </td>
                                            <td>
                                                &nbsp;LKR
                                            </td>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;
                                            </td>
                                            <td>
                                                <input type="radio" id="radioBank2Fcbu" name="Bank2LkrFcbuBoth" class="form-control" value="2" />
                                            </td>
                                            <td>
                                                &nbsp;FCBU
                                            </td>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;
                                            </td>
                                            <td>
                                                <input type="radio" id="radioBank2Both" name="Bank2LkrFcbuBoth" class="form-control" value="3"/>
                                            </td>
                                            <td>
                                                &nbsp;Both
                                            </td>
                                        </tr>
                                    </table>
                                    
                                </div>
                                
                            </div>
                            
                            
                        </div>
                    </fieldset>
                    
                    <div class="form-group">
                        <div class="col-md-6 leftside" style="text-align: right;">
                            <label class="control-label" style="display:inline-block;">Customer Locality</label>
                        </div>
                    	<div class="col-md-6 rightside" style="text-align: left;">
                            <table style="display: inline-block;margin-top:5px;">
                                <tr>
                                    <td>
                                        <input type="radio" id="radioLocal" name="locality"  checked="checked" value="1"/>
                                    </td>
                                    <td>
                                        &nbsp;&nbsp;Local
                                    </td>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <input type="radio" id="radioForeign" name="locality" value="0"/>
                                    </td>
                                    <td>
                                        &nbsp;&nbsp;Foreign
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-md-6 leftside" style="text-align: right;">
                            <label class="control-label" style="display:inline-block;">Registered / Walk In</label>
                        </div>
                    	<div class="col-md-6 rightside" style="text-align: left;">
                            <table style="display: inline-block;margin-top:5px;">
                                <tr>
                                    <td>
                                        <input type="radio" id="radioRegistered" checked="checked" name="registered" value="1"/>
                                    </td>
                                    <td>
                                        &nbsp;&nbsp;Registered
                                    </td>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <input type="radio" id="radioWalkIn" name="registered" value="0"/>
                                    </td>
                                    <td>
                                        &nbsp;&nbsp;Walk-In
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    
                    <div class="form-group">
                    	<label class="col-md-2 control-label" for="status">Customer Type</label>
                    	<div class="col-md-4">
                            <select id="customerType" name="customerType" class="select-chosen"> 
                                <?php
                                    $query="SELECT * FROM mas_customer_type ORDER BY customertype";
                                    $queryResult = mysqli_query ($con_main, $query);
                                    while($row =  mysqli_fetch_array($queryResult))
                                    {
                                        echo "<option value='".$row["ID"].($row["CustomerType"]=="Client"?"' selected='selected'>":"'>").$row["CustomerType"]."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <fieldset>
                        <legend>Settlement Terms</legend>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="status">Bank Guarantor</label>
                            <div class="col-md-4">
                                <label class="switch switch-primary" data-toggle="tooltip" title="Bank Guarantor">
                                    <input type="checkbox" id="settlementTermsBankGuarantor" name="settlementTermsBankGuarantor" checked onchange="hasBankGuarantorChanged();">
                                    <span></span>
                                </label>
                            </div>
                            <label class="col-md-2 control-label" for="settlementTerms_Attachment" class="checkbox">Attachment</label>
                            <div class="col-md-2" >
                                <table style="width:100%;">
                                    <tr>
                                        <td style="width:100%;max-width:150px;overflow-x: hidden;">
                                            <input type="file" id="settlementTerms_Attachment" name="settlementTerms_Attachment" onchange="ValidateSettlementTerms_Attachment();">
                                            <a id="settlementTerms_AttachmentDownloadLink" target="_blank"></a>
                                        </td>
                                        <td style="max-width: 25px;">
                                            <img id="imgSettlementTerms_Attachment_Required" src="../images/ErrorIcon.jpg" class="Error imgSettlementTerms_Attachment" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Settlement Terms Attachment is Required! if there is a Bank Guarantee" />

                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-2 deleteUploaded">
                                <input type="checkbox" id="deleteSettlementTerms_Attachment" name="deleteSettlementTerms_Attachment">&nbsp;&nbsp;Delete even if not uploading
                            </div>
                        </div>
                        <div class="form-group bankGuarantor">
                            <label class="col-md-2 control-label bankGuarantor" for="settlementTermsBankGuaranteeCurrency" class="checkbox">Currency</label>
                            <div class="col-md-4">
                                <select id="settlementTermsBankGuaranteeCurrency" name="settlementTermsBankGuaranteeCurrency" class="select-chosen">
                                   <?php
                                        $query="SELECT * FROM mas_currency ORDER BY currencyname";
                                        $queryResult = mysqli_query ($con_main, $query);
                                        while($row =  mysqli_fetch_array($queryResult))
                                        {
                                            echo "<option value='".$row["ID"].($row["CurrencyCode"]=="LKR"?"' selected='selected'>":"'>").$row["CurrencyName"]."</option>";
                                        }
                                    ?> 
                                </select>
                            </div>
                            <label class="col-md-2 control-label bankGuarantor" for="settlementTerms_BankGuarantee_Amount" class="checkbox">Amount</label>
                            <div class="col-md-4">
                                <table style="width:100%;">
                                <tr>
                                    <td style="width:100%;">
                                        <input type="text" id="settlementTerms_BankGuarantee_Amount" name="settlementTerms_BankGuarantee_Amount" class="form-control OnlyNumber" maxlength="10" onchange="//ValidateBankGuaranteeAmount();"  onkeyup="//ValidateBankGuaranteeAmount();" onblur="ValidateBankGuaranteeAmount();">
                                    </td>
                                    <td style="max-width: 25px;">
                                        <img id="imgSettlementTerms_BankGuarantee_Amount_Required" src="../images/ErrorIcon.jpg" class="Error imgSettlementTerms_BankGuarantee_Amount" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Bank Guarantee Amount is Required! if there is a Bank Guarantee" />
                                        <img id="imgSettlementTerms_BankGuarantee_Amount_Range" src="../images/ErrorIcon.jpg" class="Error imgSettlementTerms_BankGuarantee_Amount" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Bank Guarantee Amount should be between 500/= and 1,000,000,000/="/>
                                    </td>
                                </tr>
                            </table>
                                
                            </div>
                            
                        </div>
                        <div class="form-group bankGuarantor">
                            <label class="col-md-2 control-label bankGuarantor" for="settlementTerms_BankGuarantee_Expiry" class="checkbox">Expiry</label>
                            <div class="col-md-4">
                                <table style="width:100%;">
                                <tr>
                                    <td style="width:100%;">
                                        <input type="text" readonly="readonly" id="settlementTerms_BankGuarantee_Expiry" name="settlementTerms_BankGuarantee_Expiry" class="form-control bankGuarantor" placeholder="yyyy-mm-dd" maxlength="10" onblur="ValidateSettlementTermsBankGuaranteeExpiry();" onfocus="SettlementTermsBankGuaranteeExpiryFocussed=true;" onchange="if(SettlementTermsBankGuaranteeExpiryFocussed)ValidateSettlementTermsBankGuaranteeExpiry();">
                                    </td>
                                    <td style="max-width: 25px;">
                                        <img id="imgSettlementTerms_BankGuarantee_Expiry_Required" src="../images/ErrorIcon.jpg" class="Error imgSettlementTerms_BankGuarantee_Expiry" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Bank Guarantoee Expiry Date is Required! if there is a Bank Guarantee" />
                                        
                                    </td>
                                </tr>
                                </table>
                                
                            </div>
                            <label class="col-md-2 control-label bankGuarantor" for="settlementTerms_Bank" class="checkbox">Bank</label>
                            <div class="col-md-4">
                                <table style="width:100%;">
                                <tr>
                                    <td style="width:100%;">
                                        <input type="text" id="settlementTerms_Bank" name="settlementTerms_Bank" class="form-control bankGuarantor OnlyAlphabetic" maxlength="100" onchange="//ValidateSettlementTerms_GuarantorBank();" onkeyup="//ValidateSettlementTerms_GuarantorBank();" onblur="OnBlurSettlementTerms_GuarantorBank();">
                                    </td>
                                    <td style="max-width: 25px;">
                                        <img id="imgSettlementTerms_Bank_Required" src="../images/ErrorIcon.jpg" class="Error imgSettlementTerms_Bank" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Bank is Required! if there is a Bank Guarantee" />
                                        
                                    </td>
                                </tr>
                                </table>
                                
                            </div>
                            
                        </div>
                        <div class="form-group bankGuarantor">
                            <label class="col-md-2 control-label bankGuarantor" for="settlementTerms_BankBranch" class="checkbox">Bank Branch</label>
                            <div class="col-md-4">
                                <table style="width:100%;">
                                <tr>
                                    <td style="width:100%;">
                                        <input type="text" id="settlementTerms_BankBranch" name="settlementTerms_BankBranch" class="form-control bankGuarantor OnlyAlphabetic" maxlength="100"  onchange="//ValidateSettlementTerms_GuarantorBankBranch();" onkeyup="//ValidateSettlementTerms_GuarantorBankBranch();" onblur="OnBlurSettlementTerms_GuarantorBankBranch();">
                                    </td>
                                    <td style="max-width: 25px;">
                                        <img id="imgSettlementTerms_BankBranch_Required" src="../images/ErrorIcon.jpg" class="Error imgSettlementTerms_BankBranch" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Bank Branch is Required! if there is a Bank Guarantee" />
                                        
                                    </td>
                                </tr>
                                </table>
                                
                            </div>
                            <label class="col-md-2 control-label bankGuarantor" for="settlementTerms_GuarantorNo" class="checkbox">Guarantor No.</label>
                            <div class="col-md-4">
                                <table style="width:100%;">
                                <tr>
                                    <td style="width:100%;">
                                        <input type="text" id="settlementTerms_GuarantorNo" name="settlementTerms_GuarantorNo" class="form-control bankGuarantor" maxlength="40"  onchange="//ValidateSettlementTerms_GuarantorNo();" onkeyup="//ValidateSettlementTerms_GuarantorNo();" onblur="OnBlurSettlementTerms_GuarantorNo();">
                                    </td>
                                    <td style="max-width: 25px;">
                                        <img id="imgSettlementTerms_GuarantorNo_Required" src="../images/ErrorIcon.jpg" class="Error imgSettlementTerms_GuarantorNo" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Guarantor No is Required! if there is a Bank Guarantee" />
                                        
                                    </td>
                                </tr>
                                </table>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="status">Credit</label>
                            <div class="col-md-4">
                                <label class="switch switch-primary" data-toggle="tooltip" title="Credit">
                                    <input type="checkbox" id="settlementTerms_Credit" name="settlementTerms_Credit" checked onchange="CreditAllowedChanged();">
                                    <span></span>
                                </label>
                            </div>
                            <div class="col-md-1 disapearApear">
                                &nbsp;
                            </div>
                            <div class="col-md-5">
                                <table>
                                    <tr>
                                        <td>
                                            <label class="col-md-5 control-label credit" for="settlementTerms_Credit_PdcRequired" style="white-space: nowrap;min-width:167px;">P.D.C. Required</label>
                                        </td>
                                        <td>
                                            <input type="checkbox" id="settlementTerms_Credit_PdcRequired" name="settlementTerms_Credit_PdcRequired" class="form-control credit" style="display:inline-block;"/>
                                        </td>
                                    </tr>
                                </table>
                                                                
                            </div>
                            
                        </div>
                        <div class="form-group credit">
                            <label class="col-md-2 control-label" for="settlementTerms_CreditPeriod" class="checkbox">Credit Period</label>
                            <div class="col-md-4">
                                <table style="width:100%;">
                                <tr>
                                    <td style="width:100%;">
                                        <input type="text" id="settlementTerms_CreditPeriod" name="settlementTerms_CreditPeriod" class="form-control OnlyNumber" maxlength="3" placeholder="Credit Period in Number of Days"  onchange="//ValidateSettlementTerms_CreditPeriod();" onkeyup="//ValidateSettlementTerms_CreditPeriod();" onblur="OnBlurSettlementTerms_CreditPeriod();"/>
                                    </td>
                                    <td style="max-width: 25px;">
                                        <img id="imgSettlementTerms_CreditPeriod_Required" src="../images/ErrorIcon.jpg" class="Error imgSettlementTerms_CreditPeriod" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Credit Period is Required! if allowing credit" />
                                        
                                    </td>
                                </tr>
                                </table>
                                
                            </div>
                            <label class="col-md-2 control-label" for="settlementTerms_CreditPeriodFrom" class="checkbox">Credit Period From</label>
                            <div class="col-md-4">
                                <select id="settlementTerms_CreditPeriodFrom" name="settlementTerms_CreditPeriodFrom" class="select-chosen" >
                                    <option value="1">Invoiced Date</option>
                                    <option value="2">Dispatch Date</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group credit">
                            <label class="col-md-2 control-label" for="settlementTerms_CreditLimit" class="checkbox">Credit Limit</label>
                            <div class="col-md-10">
                                <table style="width:100%;">
                                <tr>
                                    <td style="width:100%;">
                                        <input type="text" id="settlementTerms_CreditLimit" name="settlementTerms_CreditLimit" class="form-control OnlyNumber" maxlength="10" placeholder="Credit Limit"  onchange="//ValidateSettlementTerms_CreditLimit();" onkeyup="//ValidateSettlementTerms_CreditLimit();" onblur="ValidateSettlementTerms_CreditLimit();">
                                    </td>
                                    <td style="max-width: 25px;">
                                        <img id="imgSettlementTerms_CreditLimit_Required" src="../images/ErrorIcon.jpg" class="Error imgSettlementTerms_CreditLimit" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Credit Limit is Required! if allowing credit" />
                                        
                                    </td>
                                </tr>
                                </table>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="status">Advance</label>
                            <div class="col-md-4">
                                <label class="switch switch-primary" data-toggle="tooltip" title="Advance">
                                    <input type="checkbox" id="settlementTerms_Advance" name="settlementTerms_Advance" checked onchange="mustPayAdvanceChanged();">
                                    <span></span>
                                </label>
                            </div>
                            <label class="col-md-2 control-label advance" for="settlementTerms_AdvancePercentage" class="checkbox">Advance Percentage</label>
                            <div class="col-md-4">
                                <table style="width:100%;">
                                <tr>
                                    <td style="width:100%;">
                                        <input type="text" id="settlementTerms_AdvancePercentage" name="settlementTerms_AdvancePercentage" class="form-control advance OnlyNumber" maxlength="3" placeholder="Advance Percentage"  onchange="//ValidateSettlementTerms_AdvancePercentage();" onkeyup="//ValidateSettlementTerms_AdvancePercentage();" onblur="ValidateSettlementTerms_AdvancePercentage();">
                                    </td>
                                    <td style="max-width: 25px;">
                                        <img id="imgSettlementTerms_AdvancePercentage_Required" src="../images/ErrorIcon.jpg" class="Error imgSettlementTerms_AdvancePercentage" style="width:33px;display:none;" data-toggle="tooltip" data-placement="top" title="Advance Percentage is Required! if an advance is set" />
                                        
                                    </td>
                                </tr>
                                </table>
                                
                            </div>
                        </div>
                        <div class="form-group advance">
                            <label class="col-md-2 control-label" for="settlementTerms_AdvancePaymentType" class="checkbox">Payment Type</label>
                            <div class="col-md-10" style="text-align: right;">
                                <div class="col-md-3 advance">
                                    <table>
                                        <tr>
                                            <td>
                                                <input type="radio" checked="checked" id="settlementTerms_AdvancePaymentType_Cash" value="1" name="settlementTerms_AdvancePaymentType" class="form-control advance">
                                            </td>
                                            <td>
                                                &nbsp;&nbsp;Cash / Cash Deposit
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-3">
                                    <table>
                                        <tr>
                                            <td>
                                                <input type="radio" id="settlementTerms_AdvancePaymentType_Check"  value="2"  name="settlementTerms_AdvancePaymentType" class="form-control advance">
                                            </td>
                                            <td>
                                                &nbsp;&nbsp;Cash / Current Check
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-3">
                                    <table>
                                        <tr>
                                            <td>
                                                <input type="radio" id="settlementTerms_PaymentType_Pdc" value="3"  name="settlementTerms_AdvancePaymentType" class="form-control advance">
                                            </td>
                                            <td style="white-space: nowrap;">
                                                &nbsp;&nbsp;Cash / Current Check / P.D.C.
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                
                            </div>
                            
                        </div>
                        
                    </fieldset>
                    
                    <div class="form-group">
                            <label class="col-md-2 control-label" for="shippingTerms" class="checkbox">Shipping Terms</label>
                            <div class="col-md-4">
                                <select id="shippingTerms" name="shippingTerms" class="select-chosen">
                                   <?php
                                        $query="SELECT * FROM mas_customer_shippingterms ORDER BY shippingterms";
                                        $queryResult = mysqli_query ($con_main, $query);
                                        while($row =  mysqli_fetch_array($queryResult))
                                        {
                                            echo "<option value='".$row["ID"]."'".(($row["ShippingTerms"]=="Delivery to site")?" selected='selected'":" ")." >".$row["ShippingTerms"]."</option>";
                                        }
                                    ?> 
                                </select>
                            </div>
                            <label class="col-md-2 control-label bankGuarantor" for="pricingPolicy" class="checkbox">Pricing Policy</label>
                            <div class="col-md-4">
                                <select id="pricingPolicy" name="pricingPolicy" class="select-chosen">
                                   <?php
                                        $query="SELECT * FROM mas_pricingpolicy ORDER BY PricingPolicy";
                                        $queryResult = mysqli_query ($con_main, $query);
                                        while($row =  mysqli_fetch_array($queryResult))
                                        {
                                            echo "<option value='".$row["ID"]."' ".(($row["PricingPolicy"]=="Standard Price")?" selected='selected'":" ").">".$row["PricingPolicy"]."</option>";
                                        }
                                    ?> 
                                </select>
                            </div>
                        </div>
                    <fieldset>
                        <legend>Customer Status</legend>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="status">Status</label>
                            <div class="col-md-4">
                                <select id="status" name="status" class="select-chosen"> 
                                    <option value="1" selected>Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                
                    <div class="form-group form-actions">
                        <input type="hidden" name="id" id="id" value="0" />
                        <input type="button" value="Get Customer Details" onclick="CustomerIDChangedRetrieveDetails();" class="btn btn-success primary-btn pull-left" style="display: none;"/>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success primary-btn pull-right" onclick="return ValidateEntireFormBeforeSubmitting();"><i class="fa fa-angle-right"></i> Submit</button>
                            <button type="button" id="btnReset" class="btn btn-warning" onclick="OnClickFormReset();"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Block -->
		    </div>
            <!-- END Example Content -->
        </div>
        <!-- END Example Block -->
    </div>

    <!-- Table Block 
    <div class="block full">
        
        <div class="block-title">
            <h2>Colors</h2><small>Colors currently exist in the system</small>
        </div>
        

        
        <div class="table-responsive"><table id="table-data" class="table table-condensed table-striped table-hover"></table></div>
        
    </div>
    Table Content -->
    <!-- END Table Block -->
<!-- END Page Content -->
</div>

<?php include $prefix.'page_footer.php'; ?>
<?php include $prefix.'template_scripts.php'; ?>
<?php include $prefix.'template_end.php'; ?>


<script src="/Gurind/js/jquery.alphanum-master/jquery.alphanum-master/jquery.alphanum.js" type="text/javascript"></script>


<script type="text/javascript">

    // $( "#aamount" ).keyup(function() {
    //   var aamount = $('#aamount').val();
    //   var received = $('#received').val();

    //   alert (aamount - received);

    // });
    
    var SettlementTermsBankGuaranteeExpiryFocussed=false;
    
    Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
    }
    
    function hasBankGuarantorChanged(){
        var hasBankGuarantor=$("#settlementTermsBankGuarantor").is(":checked");
        if(hasBankGuarantor)
        {
            $(".bankGuarantor").css("display","inline-block");
            $(".form-group.bankGuarantor").css("display","block");
            $("#settlementTermsBankGuaranteeCurrency_chosen").css("display","inline-block");
            $("#settlementTerms_BankGuarantee_Amount").css("display","inline-block");
            
            $("#settlementTerms_BankGuarantee_Expiry").css("display","inline-block");
        }
        else
        {
            $(".bankGuarantor").css("display","none");
            
            $("#settlementTermsBankGuaranteeCurrency_chosen").css("display","none");
            $("#settlementTerms_BankGuarantee_Amount").css("display","none");
            $("#settlementTerms_BankGuarantee_Expiry").css("display","none");
        }
        ValidateSettlementTerms_Attachment();
        ValidateBankGuaranteeAmount();
        ValidateSettlementTerms_GuarantorBank();
        ValidateSettlementTerms_GuarantorBankBranch();
        ValidateSettlementTermsBankGuaranteeExpiry();
        ValidateSettlementTerms_GuarantorNo();
    }
    
    function ValidateBankGuaranteeAmount()
    {
        var msg = null;
        var Amount=$("#settlementTerms_BankGuarantee_Amount").val();
        var BankGuarantee=$("#settlementTermsBankGuarantor").is(":checked");
        $("img.Error.imgSettlementTerms_BankGuarantee_Amount").css("display","none");
        if(Amount==""&&BankGuarantee)
        {
            $("img#imgSettlementTerms_BankGuarantee_Amount_Required").css("display","inline-block");
            msg = new SpeechSynthesisUtterance($("img#imgSettlementTerms_BankGuarantee_Amount_Required").attr("title"));
            window.speechSynthesis.speak(msg);
        }
        else if(Amount<500&&BankGuarantee||Amount>100000000&&BankGuarantee)
        {
            $("img#imgSettlementTerms_BankGuarantee_Amount_Range").css("display","inline-block");
            msg = new SpeechSynthesisUtterance($("img#imgSettlementTerms_BankGuarantee_Amount_Range").attr("title"));
            window.speechSynthesis.speak(msg);
        }
    }
    
    function ValidateSettlementTerms_Attachment()
    {
        var msg = null;
        var FileUpload=$("#settlementTerms_Attachment").val();
        var Uploaded=$("#settlementTerms_AttachmentDownloadLink").html();
        var DeleteAttachment=$("#deleteSettlementTerms_Attachment").is(":checked");
        var BankGuarantee=$("#settlementTermsBankGuarantor").is(":checked");
        
        $("img.Error.imgSettlementTerms_Attachment").css("display","none");
        if(FileUpload==""&&Uploaded==""&&BankGuarantee||FileUpload==""&&DeleteAttachment&&BankGuarantee)
        {
            $("img#imgSettlementTerms_Attachment_Required").css("display","inline-block");
            msg = new SpeechSynthesisUtterance($("img#imgSettlementTerms_Attachment_Required").attr("title"));
            window.speechSynthesis.speak(msg);
        }
    }
    
    function ValidateSettlementTerms_GuarantorBank()
    {
        var msg = null;
        var Bank=$("#settlementTerms_Bank").val();
        Bank=$.trim(Bank);
        var BankGuarantee=$("#settlementTermsBankGuarantor").is(":checked");
        
        $("img.Error.imgSettlementTerms_Bank").css("display","none");
        if(Bank==""&&BankGuarantee)
        {
            $("img#imgSettlementTerms_Bank_Required").css("display","inline-block");
            msg = new SpeechSynthesisUtterance($("img#imgSettlementTerms_Bank_Required").attr("title"));
            window.speechSynthesis.speak(msg);
        }
    }
    
    function OnBlurSettlementTerms_GuarantorBank()
    {
        var Bank=$("#settlementTerms_Bank").val();
        Bank=$.trim(Bank);
        $("#settlementTerms_Bank").val(Bank);
    }
    
    function ValidateSettlementTerms_GuarantorBankBranch()
    {
        var msg = null;
        var BankBranch=$("#settlementTerms_BankBranch").val();
        BankBranch=$.trim(BankBranch);
        var BankGuarantee=$("#settlementTermsBankGuarantor").is(":checked");
        
        $("img.Error.imgSettlementTerms_BankBranch").css("display","none");
        if(BankBranch==""&&BankGuarantee)
        {
            $("img#imgSettlementTerms_BankBranch_Required").css("display","inline-block");
            msg = new SpeechSynthesisUtterance($("img#imgSettlementTerms_BankBranch_Required").attr("title"));
            window.speechSynthesis.speak(msg);
        }
    }
    
    function OnBlurSettlementTerms_GuarantorBankBranch()
    {
        var msg = null;
        var BankBranch=$("#settlementTerms_BankBranch").val();
        BankBranch=$.trim(BankBranch);
        $("#settlementTerms_BankBranch").val(BankBranch);
        
        var BankGuarantee=$("#settlementTermsBankGuarantor").is(":checked");
        
        $("img.Error.imgSettlementTerms_BankBranch").css("display","none");
        if(BankBranch==""&&BankGuarantee)
        {
            $("img#imgSettlementTerms_BankBranch_Required").css("display","inline-block");
            msg = new SpeechSynthesisUtterance($("img#imgSettlementTerms_BankBranch_Required").attr("title"));
            window.speechSynthesis.speak(msg);
        }
    }
    
    function CreditAllowedChanged()
    {

        var creditAllowed=$("#settlementTerms_Credit").is(":checked");
        if(creditAllowed)
        {
            $(".credit").css("display","inline-block");
            $(".form-group.credit").css("display","block");

        }
        else
        {
            $(".credit").css("display","none");
            $(".form-group.credit").css("display","none");
        }
        ValidateSettlementTerms_CreditPeriod();
        ValidateSettlementTerms_CreditLimit();
        
    }
    
    function ValidateSettlementTerms_CreditPeriod()
    {
        var msg = null;
        var creditPeriod=$("#settlementTerms_CreditPeriod").val();
        creditPeriod=$.trim(creditPeriod);
        
        var creditAllowed=$("#settlementTerms_Credit").is(":checked");
        
        $("img.Error.imgSettlementTerms_CreditPeriod").css("display","none");
        if((creditPeriod==""||creditPeriod<=0)&&creditAllowed)
        {
            $("img#imgSettlementTerms_CreditPeriod_Required").css("display","inline-block");
            msg = new SpeechSynthesisUtterance($("img#imgSettlementTerms_CreditPeriod_Required").attr("title"));
            window.speechSynthesis.speak(msg);
        }
    }
    
    function OnBlurSettlementTerms_CreditLimit()
    {
        var msg = null;
        var creditLimit=$("#settlementTerms_CreditLimit").val();
        creditLimit=$.trim(creditLimit);
        $("#settlementTerms_CreditLimit").val(creditLimit);
        var creditAllowed=$("#settlementTerms_Credit").is(":checked");
        
        $("img.Error.imgSettlementTerms_CreditLimit").css("display","none");
        if((creditLimit==""||creditLimit<=0)&&creditAllowed)
        {
            $("img#imgSettlementTerms_CreditLimit_Required").css("display","inline-block");
            msg = new SpeechSynthesisUtterance($("img#imgSettlementTerms_CreditLimit_Required").attr("title"));
            window.speechSynthesis.speak(msg);
        }
    }
    
    function ValidateSettlementTerms_CreditLimit()
    {
        var msg = null;
        var creditLimit=$("#settlementTerms_CreditLimit").val();
        creditLimit=$.trim(creditLimit);
        
        var creditAllowed=$("#settlementTerms_Credit").is(":checked");
        
        $("img.Error.imgSettlementTerms_CreditLimit").css("display","none");
        if((creditLimit==""||creditLimit<=0)&&creditAllowed)
        {
            $("img#imgSettlementTerms_CreditLimit_Required").css("display","inline-block");
            msg = new SpeechSynthesisUtterance($("img#imgSettlementTerms_CreditLimit_Required").attr("title"));
            window.speechSynthesis.speak(msg);
        }
    }
    
    function OnBlurSettlementTerms_CreditPeriod()
    {
        var msg = null;
        var creditPeriod=$("#settlementTerms_CreditPeriod").val();
        creditPeriod=$.trim(creditPeriod);
        $("#settlementTerms_CreditPeriod").val(creditPeriod);
        var creditAllowed=$("#settlementTerms_Credit").is(":checked");
        
        $("img.Error.imgSettlementTerms_CreditPeriod").css("display","none");
        if((creditPeriod==""||creditPeriod<=7)&&creditAllowed)
        {
            $("img#imgSettlementTerms_CreditPeriod_Required").css("display","inline-block");
            msg = new SpeechSynthesisUtterance($("img#imgSettlementTerms_CreditPeriod_Required").attr("title"));
            window.speechSynthesis.speak(msg);
        }
    }
    
    function ValidateSettlementTerms_GuarantorNo()
    {
        var msg = null;
        var GuarantorNo=$("#settlementTerms_GuarantorNo").val();
        GuarantorNo=$.trim(GuarantorNo);
        
        var hasBankGuarantor=$("#settlementTermsBankGuarantor").is(":checked");
        
        $("img.Error.imgSettlementTerms_GuarantorNo").css("display","none");
        if(GuarantorNo==""&&hasBankGuarantor)
        {
            $("img#imgSettlementTerms_GuarantorNo_Required").css("display","inline-block");
            msg = new SpeechSynthesisUtterance($("img#imgSettlementTerms_GuarantorNo_Required").attr("title"));
            window.speechSynthesis.speak(msg);
        }
    }
    
    function OnBlurSettlementTerms_GuarantorNo()
    {
        var msg = null;
        var GuarantorNo=$("#settlementTerms_GuarantorNo").val();
        GuarantorNo=$.trim(GuarantorNo);
        $("#settlementTerms_GuarantorNo").val(GuarantorNo);
        var CreditIsAllowed=$("#settlementTerms_Credit").is(":checked");
        
        var hasBankGuarantor=$("#settlementTermsBankGuarantor").is(":checked");
        
        $("img.Error.imgSettlementTerms_GuarantorNo").css("display","none");
        if(GuarantorNo==""&&hasBankGuarantor)
        {
            $("img#imgSettlementTerms_GuarantorNo_Required").css("display","inline-block");
            msg = new SpeechSynthesisUtterance($("img#imgSettlementTerms_GuarantorNo_Required").attr("title"));
            window.speechSynthesis.speak(msg);
        }
    }
    
    $(document).ready(function(){
        window.onresize=function(){
            setTimeout(function(){
                LayoutAdjustments();
            },1);
        };
        $('#search').select2({
            minimumInputLength:2,
            ajax: {
                url: 'data/customer_select.php',
                dataType: 'json',
                delay: 100,
                data: function (term) {
                    return term;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });
        
        $('#search').on('select2:open', function (e){
            $('#search').val('');
            $("#id").val("");
            $("span#select2-search-container").html("Enter Customer, Name or Code");
        });
        
        $('#search').on('change', function (e2){
            $("#id").val($('#search').select2('val'));
            CustomerIDChangedRetrieveDetails();
        });
        
        UpdateForDeleteUploaded();
        
        $('#btnReset').on('click', function (e){
            $("#id").val("");
            $("input[type=text]").val("");
            $("select").val("");
            $("input[type=checkbox]").prop("checked",false).trigger("change");
            $("input[type=radio]#radioBank2Lkr").prop("checked",true);
            $("input[type=radio]#settlementTerms_AdvancePaymentType_Cash").prop("checked",true);
            $("img.Error").css("display","none");
        });
        
        $('[data-toggle="tooltip"]').tooltip();
        
        $(".OnlyNumber").numeric({
            allowMinus   : false,
            allowThouSep : false,
            allowDecSep : true
        });
        
        $(".OnlyAlphabetic").alpha({
            allowNumeric : false
        });
        
        var fiveYearsFromNow=(new Date()).addDays(365*5);
        
        $("#settlementTerms_BankGuarantee_Expiry").datepicker({
           startDate:new Date() , endDate:fiveYearsFromNow, format:"yyyy-mm-dd"
        });
        
        $(".OnlyAlphabetic").blur(function(eventData){
            var CustomerName=$(this).val();
            CustomerName=$.trim(CustomerName);
            if(CustomerName!="")
            {
                while(CustomerName.indexOf("  ")>-1)
                {
                   CustomerName=CustomerName.replace("  "," ");
                }

                var Length=CustomerName.length;
                var Index=0;
                var NewCustomerName="";
                var PrevChar="";

                for(Index=0;Index<Length;Index++)
                {
                    if(PrevChar==""||PrevChar==" ")
                    {
                        NewCustomerName+=CustomerName.charAt(Index).toUpperCase();
                    }
                    else
                    {
                        NewCustomerName+=CustomerName.charAt(Index).toLowerCase();
                    }
                    PrevChar=CustomerName.charAt(Index);
                }
                $(this).val(NewCustomerName);
                if(this.id=="name")
                {
                    if($("#billingName").val()=="")
                    {
                        $("#billingName").val($("#name").val());
                        $("#billingName").trigger("change");
                    }
                }
                
            }
        });
        
    });
    
    function ValidateSettlementTermsBankGuaranteeExpiry(){
        
        var msg =null;
        
        var BankGuaranteeExpiry=$("#settlementTerms_BankGuarantee_Expiry").val();
        var BankGuarantee=$("#settlementTermsBankGuarantor").is(":checked");
        
        $("img.Error.imgSettlementTerms_BankGuarantee_Expiry").css("display","none");
        if(BankGuaranteeExpiry==""&&BankGuarantee)
        {
            $("img#imgSettlementTerms_BankGuarantee_Expiry_Required").css("display","inline-block");
            msg = new SpeechSynthesisUtterance($("img#imgSettlementTerms_BankGuarantee_Expiry_Required").attr("title"));
            window.speechSynthesis.speak(msg);
        }
    }
    
    function LayoutAdjustments()
    {
        var browserWinWidth=window.innerWidth;
        if(browserWinWidth>767)
        {
            $("div.disapearApear").html("&nbsp;");
            $("div.col-md-6.leftside").css("textAlign","right");
            $("div.col-md-6.rightside").css("textAlign","left");
        }
        else
        {
            $("div.disapearApear").html("");
            $("div.col-md-6").css("textAlign","center");
        }
    }
    
    function ServerResponseReceived(Operation,Outcome,Id,Messege){
        
        var msg=null;
        var msg_typ ="";
        var msg_txt="";
        
        if (Outcome){
            msg_typ = 'success';
            
            if(Operation=="insert")
            {
               msg_txt = '<h4>Success!</h4> <p>Customer was saved<br>Customer ID is '+Id+'.</p>';
            }
            else
            {
               msg_txt = '<h4>Success!</h4> <p>Customer was updated<br>Customer ID was '+Id+'.</p>';
            }
            CustomerIDChangedRetrieveDetails();

        }
        else
        {
             msg_typ = 'danger';
             msg_txt = '<h4>Error!</h4> <p>'+Messege+'</p>';

        }  
        
        $.bootstrapGrowl(msg_txt, {
            type: msg_typ,
            delay: 2500,
            allow_dismiss: true
        });

        msg = new SpeechSynthesisUtterance(msg_txt);
        //window.speechSynthesis.speak(msg);

//        setTimeout(function(){ 
//            window.location="/Gurind/Gurind/customer.php";
//        },3500);
    }
    
    function CustomerIDChangedRetrieveDetails()
    {
        var customerid=$("#id").val();
        if(customerid>0)
        {
            $.ajax({
                url:"/Gurind/gurind/data/get_customer_by_id.php",
                method:"post",
                cache:false,
                async:true,
                dataType:"json",
                data:{
                 "id":customerid   
                },
                success:function(data, textStatus, jqXHR){
                    
                    if(data)
                    {
                        $("#code").val(data.CustomerCode);$("#name").val(data.CustomerName);$("#billingName").val(data.BillingName);
                        $("#address").val(data.CustomerAddress);$("#city").val(data.City);
                        $("#country").val(data.Country).trigger("chosen:updated");
                        
                        $("#genLineNos").val(data.GeneralLineNo);
                        
                        $("#businessRegistrationNo").val(data.BusinessRegistrationNo);
                        if(data.BusinessRegistrationCertificateFileName!="" && data.BusinessRegistrationCertificateFileName!=null)
                        {
                            $("a#businessRegistrationCertificateDownloadLink").attr("href","/Gurind/CustomerBusinessRegistrationAttachments/"+ customerid + data.BusinessRegistrationCertificateFileName);
                            $("a#businessRegistrationCertificateDownloadLink").html("Business Registration Certificate");
                            $($("#deleteBusinessRegistrationCertificate").parent()).css("visibility","visible");
                        }
                        else
                        {
                            $("a#businessRegistrationCertificateDownloadLink").attr("href","");
                            $("a#businessRegistrationCertificateDownloadLink").html("");
                        }
                        $("#VATRegistrationNo").val(data.VATRegNo);
                        if(data.VATRegCertificateFileName!=""&&data.VATRegCertificateFileName!=null)
                        {
                            $("#VATRegistrationCertificateDownloadLink").attr("href","/Gurind/CustomerVatRegistrationAttachments/"+ customerid  +  data.VATRegCertificateFileName);
                            $("#VATRegistrationCertificateDownloadLink").html("VAT Certificate");
                            $($("#deleteVATRegistrationCertificate").parent()).css("visibility","visible");
                        }
                        else
                        {
                            $("#VATRegistrationCertificateDownloadLink").attr("href","");
                            $("#VATRegistrationCertificateDownloadLink").html("");
                        }
                        $("#SVATRegistrationNo").val(data.SVATRegNo);
                        if(data.SVATRegCertificateFileName!=""&&data.SVATRegCertificateFileName!=null)
                        {
                            $("#SVATRegistrationCertificateDownloadLink").attr("href","/Gurind/CustomerSvatRegistrationAttachments/"+ customerid  +  data.SVATRegCertificateFileName);
                            $("#SVATRegistrationCertificateDownloadLink").html("SVAT Certificate");
                            $($("#deleteSVATRegistrationCertificate").parent()).css("visibility","visible");
                        }
                        else
                        {
                            $("#SVATRegistrationCertificateDownloadLink").attr("href","");
                            $("#SVATRegistrationCertificateDownloadLink").html("");
                        }
                        $("#BOIRegistrationNo").val(data.BOIRegNo);
                        if(data.BOIRegCertificateFileName!=""&&data.BOIRegCertificateFileName!=null)
                        {
                            $("#BOIRegistrationCertificateDownloadLink").attr("href","/Gurind/CustomerBoiRegistrationAttachments/"+ customerid  +  data.BOIRegCertificateFileName);
                            $("#BOIRegistrationCertificateDownloadLink").html("BOI Registration Certificate");
                            $($("#deleteBOIRegistrationCertificate").parent()).css("visibility","visible");
                        }
                        else
                        {
                            $("#BOIRegistrationCertificateDownloadLink").attr("href","");
                            $("#BOIRegistrationCertificateDownloadLink").html("");
                        }
                        
                        $("#cordinationContact1Name").val(data.CordinationContact1Name);
                        $("#cordinationContact1Designation").val(data.CordinationContact1Designation);
                        $("#cordinationContact1Mobile").val(data.CordinationContact1Mobile);
                        $("#cordinationContact1Email").val(data.CordinationContact1Email);
                        $("#cordinationContact2Name").val(data.CordinationContact2Name);
                        $("#cordinationContact2Designation").val(data.CordinationContact2Designation);
                        $("#cordinationContact2Mobile").val(data.CordinationContact2Mobile);
                        $("#cordinationContact2Email").val(data.CordinationContact2Email);
                        
                        $("#paymentsContact1Name").val(data.PaymentsContact1Name);
                        $("#paymentsContact1Designation").val(data.PaymentsContact1Designation);
                        $("#paymentsContact1Mobile").val(data.PaymentsContact1Mobile);
                        $("#paymentsContact1Email").val(data.PaymentsContact1Email);
                        
                        $("#paymentsContact2Name").val(data.PaymentsManagerName);
                        $("#paymentsContact2Designation").val(data.PaymentsManagerDesignation);
                        $("#paymentsContact2Mobile").val(data.PaymentsManagerMobile);
                        $("#paymentsContact2Email").val(data.PaymentsManagerEmail);
                        
                        $("#logisticsBoiCusDecContact1Name").val(data.LogisticsBoiCusDecContactName);
                        $("#logisticsBoiCusDecContact1Designation").val(data.LogisticsBoiCusDecContactDesignation);
                        $("#logisticsBoiCusDecContact1Mobile").val(data.LogisticsBoiCusDecContactMobile);
                        $("#logisticsBoiCusDecContact1Email").val(data.LogisticsBoiCusDecContactEmail);
                        
                        $("#proprietorName").val(data.ProprietorName);
                        $("#proprietorDesignation").val(data.ProprietorDesignation);
                        $("#proprietorMobile").val(data.ProprietorMobile);
                        $("#proprietorEmail").val(data.ProprietorEmail);
                        
                        $("#Bank1Name").val(data.Bank1Name);$("#Bank1Branch").val(data.Bank1Branch);
                        switch(1*data.Bank1LkrFcbuBoth)
                        {
                            case 1:
                                $("#radioBank1Lkr").prop("checked",true);
                                break;
                            case 2:
                                $("#radioBank1Fcbu").prop("checked",true);
                                break;
                            case 3:
                                $("#radioBank1Both").prop("checked",true);
                                break;
                        }
                        
                        
                        $("#Bank2Name").val(data.Bank2Name);$("#Bank2Branch").val(data.Bank2Branch);
                        switch(1*data.Bank2LkrFcbuBoth)
                        {
                            case 1:
                                $("#radioBank2Lkr").prop("checked",true);
                                break;
                            case 2:
                                $("#radioBank2Fcbu").prop("checked",true);
                                break;
                            case 3:
                                $("#radioBank2Both").prop("checked",true);
                                break;
                        }
                        
                        $("#settlementTermsBankGuarantor").prop("checked",data.SettlementTerms_BankGuarantee_Flag==0?false:true);
                        hasBankGuarantorChanged();
                        $("#settlementTermsBankGuarantor").trigger("change");
                        $("#settlementTermsBankGuaranteeCurrency").val(data.SettlementTerms_BankGuaranteeCurrencyID).trigger("chosen:updated");
                        $("#settlementTerms_BankGuarantee_Amount").val(data.SettlementTerms_BankGuarantee_Amount);
                        if(data.SettlementTerms_BankGuarantee_Expiry!=null)
                        {
                           $("input[name=settlementTerms_BankGuarantee_Expiry]").val(data.SettlementTerms_BankGuarantee_Expiry);
                        }
                        
                        $("#settlementTerms_Bank").val(data.SettlementTerms_Bank);
                        $("#settlementTerms_BankBranch").val(data.SettlementTerms_BankBranch);
                        $("#settlementTerms_GuarantorNo").val(data.SettlementTerms_GuarantorNo);
                        if(data.SettlementTerms_Attachment!=""&&data.SettlementTerms_Attachment!=null)
                        {
                            $("#settlementTerms_AttachmentDownloadLink").attr("href","/Gurind/CustomerSettlementAttachments/"+ customerid + data.SettlementTerms_Attachment);
                            $("#settlementTerms_AttachmentDownloadLink").html("Settlement Terms Attchment");
                            $($("#deleteSettlementTerms_Attachment").parent()).css("visibility","visible");
                        }
                        else
                        {
                            $("#settlementTerms_AttachmentDownloadLink").attr("href","");
                            $("#settlementTerms_AttachmentDownloadLink").html("");
                        }
                        

                        $("#settlementTerms_Advance").prop("checked",data.SettlementTerms_AdvanceFlag==0?false:true).trigger("change"); 
                        $("#settlementTerms_Advance").trigger("change");
                        $("#settlementTerms_AdvancePercentage").val(data.SettlementTerms_AdvancePercentage);

                        if(data.SettlementTerms_AdvancePaymentType==1)
                        {
                            $("#settlementTerms_AdvancePaymentType_Cash").prop("checked", true);
                        }
                        else if(data.SettlementTerms_AdvancePaymentType==2)
                        {
                            $("#settlementTerms_AdvancePaymentType_Check").prop("checked", true);
                        }
                        else if(data.SettlementTerms_AdvancePaymentType==3)
                        {
                            $("#settlementTerms_AdvancePaymentType_Pdc").prop("checked", true);
                        }
                        
                        $("#settlementTerms_Credit").prop("checked", data.SettlementTerms_Credit==0?false:true).trigger("change");
                        $("#settlementTerms_Credit_PdcRequired").prop("checked", data.SettlementTerms_CreditPdcRequired);
                        $("#settlementTerms_GuarantorNo").val(data.SettlementTerms_GuarantorNo);
                        
                        $("#settlementTerms_CreditPeriodFrom").val(data.SettlementTerms_CreditPeriodBasis).trigger("chosen:updated");
                        $("#settlementTerms_CreditLimit").val(data.SettlementTerms_CreditLimit);
                        $("#shippingTerms").val(data.ShippingTermsID).trigger("chosen:updated");
                        $("#pricingPolicy").val(data.PricingPolicyID).trigger("chosen:updated");
                        $("#status").val(data.Status).trigger("chosen:updated");
                        
                        switch(1*data.CustomerLocality)
                        {
                            case 0:
                                $("#radioForeign").prop("checked",true);
                                break;
                            case 1:
                                $("#radioLocal").prop("checked",true);
                                break;
                        }
                        
                        switch(1*data.IsWalkInCustomer)
                        {
                            case 0:
                                $("#radioRegistered").prop("checked",true);
                                break;
                            case 1:
                                $("#radioWalkIn").prop("checked",true);
                                break;
                        }
                        
                        UpdateForDeleteUploaded();
                        
                        $("img.Error").css("display","none");
                        
                        var msg = new SpeechSynthesisUtterance("Successfully Retrieved Customer Details.");
                        window.speechSynthesis.speak(msg);

                    }
                }
            });
        }
    }


    function validateEmail(email) {
        var msg = null;
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var result=re.test(String(email).toLowerCase());
        if(!result)
        {
            msg = new SpeechSynthesisUtterance("Invalid E-mail Format");
            //window.speechSynthesis.speak(msg);
        }
        return result;
    }
    
    function ValidateCordinationContact1Name()
    {
        
    }
    
    function ValidateCordinationContact1Designation()
    {
        
    }
    
    function ValidateCordinationContact1Email()
    {
        $("img.Error.imgCordinationContact1Email").css("display","none");
        var Email=$("#cordinationContact1Email").val();
        Email=$.trim(Email);
        if(Email!="")
        {
            if(!validateEmail(Email))
            {
                $("#imgCordinationContact1Email_InValidFormat").css("display","inline-block");
            }
            else
            {
                $("img.Error.imgCordinationContact1Email").css("display","none");
            }
        }
    }
    
    function ValidateCordinationContact2Name()
    {
        
    }
    
    function ValidateCordinationContact2Designation()
    {
        
    }
    
    function ValidateCordinationContact2Email()
    {
        $("img.Error.imgCordinationContact2Email").css("display","none");
        var Email=$("#cordinationContact2Email").val();
        Email=$.trim(Email);
        if(Email!="")
        {
            if(!validateEmail(Email))
            {
                $("#imgCordinationContact2Email_InValidFormat").css("display","inline-block");
            }
            else
            {
                $("img.Error.imgCordinationContact2Email").css("display","none");
                
            }
        }
    }
    
    function ValidatePaymentsContact1Name()
    {
        
    }
    
    function ValidatePaymentsContact1Designation()
    {
        
    }
    
    function ValidatePaymentsContact1Email()
    {
        $("img.Error.imgPaymentsContact1Email").css("display","none");
        var Email=$("#cordinationContact1Email").val();
        Email=$.trim(Email);
        if(Email!="")
        {
            if(!validateEmail(Email))
            {
                $("#imgPaymentsContact1Email_InValidFormat").css("display","inline-block");
            }
            else
            {
                $("img.Error.imgPaymentsContact1Email").css("display","none");
            }
        }
    }
    
    function ValidatePaymentsContact2Name()
    {
        
    }
    
    function ValidatePaymentsContact2Designation()
    {
        
    }
    
    function ValidatePaymentsContact2Email()
    {
        $("img.Error.imgPaymentsContact2Email").css("display","none");
        var Email=$("#cordinationContact2Email").val();
        Email=$.trim(Email);
        if(Email!="")
        {
            if(!validateEmail(Email))
            {
                $("#imgPaymentsContact2Email_InValidFormat").css("display","inline-block");
            }
            else
            {
                $("img.Error.imgPaymentsContact2Email").css("display","none");
            }
        }
    }
    
    function ValidateLogisticsContact1Name()
    {
        
    }
    
    function ValidateLogisticsContact1Designation()
    {
        
    }
    
    function ValidateLogisticsContact1Email()
    {
        $("img.Error.imgLogisticsContact1Email").css("display","none");
        var Email=$("#logisticsBoiCusDecContact1Email").val();
        Email=$.trim(Email);
        if(Email!="")
        {
            if(!validateEmail(Email))
            {
                $("#imgLogisticsContact1Email_InValidFormat").css("display","inline-block");
            }
            else
            {
                $("img.Error.imgLogisticsContact1Email").css("display","none");
            }
        }
    }
    
    function ValidateProprietorName()
    {
        
    }
    
    function ValidateProprietorDesignation()
    {
        
    }
    
    function ValidateProprietorEmail()
    {
        $("img.Error.imgProprietorEmail").css("display","none");
        var Email=$("#proprietorEmail").val();
        Email=$.trim(Email);
        if(Email!="")
        {
            if(!validateEmail(Email))
            {
                $("#imgProprietorEmail_InValidFormat").css("display","inline-block");
            }
            else
            {
                $("img.Error.imgProprietorEmail").css("display","none");
                
            }
        }
    }
        
    function ValidateCustomerCode()
    {
        var msg = null;
        var CustomerCode=$("#code").val();
        
        CustomerCode=$.trim(CustomerCode);
        
        $("#img.Error.imgCustomerCode").css("display","none");
        
        if(CustomerCode==null||CustomerCode=="")
        {
           $("#imgCustomerCode_Duplicating").css("display","none");
           $("#imgCustomerCode_Required").css("display","inline-block");
           msg = new SpeechSynthesisUtterance($("#imgCustomerCode_Required").attr("title"));
           //window.speechSynthesis.speak(msg);
        }
        else 
        {
           $("#imgCustomerCode_Required").css("display","none");
           $.ajax({
               url:"data/customer_code_duplicating.php",
               type:"get",
               cache:false,
               async:true,
               dataType:"json",
               data:{
                 id:$("#id").val(),
                 code:CustomerCode
               },
               success:function(data, textStatus, jqXHR)
               {
                   $("#img.Error.imgCustomerCode").css("display","none");
                   $("#imgCustomerCode_Required").css("display","none");
                   data=""+data;
                   data=$.trim(data);
                   //$("#name").val(data);
                   
                   if(data=="true")
                   {
                       $("#imgCustomerCode_Duplicating").css("display","inline-block");
                       msg = new SpeechSynthesisUtterance($("#imgCustomerCode_Duplicating").attr("title"));
                       //window.speechSynthesis.speak(msg);
                   }
                   else
                   {
                       $("#imgCustomerCode_Duplicating").css("display","none");
                   }
               }
           });
        }
    }
    
    function ValidateCustomerName()
    {
        var CustomerName=$("#name").val();
        
        CustomerName=$.trim(CustomerName);
        
        $("img.Error.imgCustomerName").css("display","none");
        
        if(CustomerName==null||CustomerName=="")
        {
           $("#imgCustomerName_Required").css("display","inline-block");
           $("#imgCustomerName_Minimum2").css("display","none");
        }
        else if(CustomerName.length<=2)
        {
           $("#imgCustomerName_Minimum2").css("display","inline-block");
           $("#imgCustomerName_Required").css("display","none");
        }
        else
        {
            $("#imgCustomerName_Required").css("display","none");
            $("#imgCustomerName_Minimum2").css("display","none");
        }
    }
    
    function OnBlurCustomerName()
    {
        ValidateCustomerName();
        var CustomerName=$("#name").val();
        CustomerName=$.trim(CustomerName);
        if(CustomerName!="")
        {
            while(CustomerName.indexOf("  ")>-1)
            {
               CustomerName=CustomerName.replace("  "," ");
            }
            
            var Length=CustomerName.length;
            var Index=0;
            var NewCustomerName="";
            var PrevChar="";
            
            for(Index=0;Index<Length;Index++)
            {
                if(PrevChar==""||PrevChar==" ")
                {
                    NewCustomerName+=CustomerName.charAt(Index).toUpperCase();
                }
                else
                {
                    NewCustomerName+=CustomerName.charAt(Index).toLowerCase();
                }
                PrevChar=CustomerName.charAt(Index);
            }
            $("#name").val(NewCustomerName);
            if($("#billingName").val()=="")
            {
                $("#billingName").val($("#name").val());
            }
        }
    }
    
    function ValidateBillingName()
    {
        var BillingName=$("#billingName").val();
        
        BillingName=$.trim(BillingName);
        
        $("img.Error.imgBillingName").css("display","none");
        
        if(BillingName==null||BillingName=="")
        {
           $("#imgBillingName_Required").css("display","inline-block");
           $("#imgBillingName_Minimum2").css("display","none");
        }
        else if(BillingName.length<=2)
        {
           $("#imgBillingName_Minimum2").css("display","inline-block");
           $("#imgBillingName_Required").css("display","none");
        }
        else
        {
           $("#imgBillingName_Required").css("display","none");
           $("#imgBillingName_Minimum2").css("display","none");
        }
    }
    
    function OnBlurBillingName()
    {
        ValidateBillingName();
        var CustomerBillingName=$("#billingName").val();
        CustomerBillingName=$.trim(CustomerBillingName);
        if(CustomerBillingName!="")
        {
            while(CustomerBillingName.indexOf("  ")>-1)
            {
               CustomerBillingName=CustomerBillingName.replace("  "," ");
            }
            
            var Length=CustomerBillingName.length;
            var Index=0;
            var NewCustomerBillingName="";
            var PrevChar="";
            
            for(Index=0;Index<Length;Index++)
            {
                if(PrevChar==""||PrevChar==" ")
                {
                    NewCustomerBillingName+=CustomerBillingName.charAt(Index).toUpperCase();
                }
                else
                {
                    NewCustomerBillingName+=CustomerBillingName.charAt(Index).toLowerCase();
                }
                PrevChar=CustomerBillingName.charAt(Index);
            }
            $("#billingName").val(NewCustomerBillingName);
        }
    }
    
    function ValidateEntireFormBeforeSubmitting()
    {
        var msg = null;

        $("#img.Error").css("display","none");
        
        ValidateCustomerCode();
        ValidateCustomerName();
        ValidateBillingName();
        hasBankGuarantorChanged();
        ValidateSettlementTerms_Attachment();
        ValidateBankGuaranteeAmount();
        ValidateSettlementTerms_GuarantorBank();
        ValidateSettlementTerms_GuarantorBankBranch();
        ValidateSettlementTerms_GuarantorNo();
        ValidateSettlementTermsBankGuaranteeExpiry();
        ValidateSettlementTerms_CreditPeriod();
        ValidateSettlementTerms_CreditLimit();
        ValidateSettlementTerms_AdvancePercentage();
        
        var InValid=false;
        
        var Icons=$("img.Error");
        $.each(Icons,function(k,v){
            if($(v).css("display")!="none")
            {
                InValid=true;
            }
            else
            {
                msg = new SpeechSynthesisUtterance($(v).attr("title"));
                window.speechSynthesis.speak(msg);
            }
        });
        
        if(!InValid)
        {
            return true;
        }
        else
        {
            
            $.bootstrapGrowl("<h4><strong>Error!</strong></h4><p>Did not submit. Please correct the errors and try again.</p>", {
                type: "danger",
                delay: 2500,
                allow_dismiss: true
            });
            
            msg = new SpeechSynthesisUtterance("Did not submit. Please correct the errors and try again.");
            window.speechSynthesis.speak(msg);
                
            return false;
        }
    }
    
    function UpdateForDeleteUploaded()
    {
        if($("#id").val()==null||$("#id").val()==""||$("#id").val()=="0")
        {
            $("div.deleteUploaded").css("visibility","hidden");
        }
        else
        {
            //$("div.deleteUploaded").css("visibility","visible");
            
        }
    }
    
    function mustPayAdvanceChanged()
    {
        var mustPayAdvance=$("#settlementTerms_Advance").is(":checked");
        if(mustPayAdvance)
        {
            $(".advance").css("display","inline-block");
            $(".form-group.advance").css("display","block");
            $("#settlementTerms_AdvancePercentage").css("display","inline-block");
        }
        else
        {
            $(".advance").css("display","none");
            $("#settlementTerms_AdvancePercentage").css("display","none");
        }
        ValidateSettlementTerms_AdvancePercentage();
    }
    
    function ValidateSettlementTerms_AdvancePercentage()
    {
        var mustPayAdvance=$("#settlementTerms_Advance").is(":checked");
        var advancePercentage=$("#settlementTerms_AdvancePercentage").val();
        advancePercentage=$.trim(advancePercentage);
        $("img.Error.imgSettlementTerms_AdvancePercentage").css("display","none");
        if(advancePercentage==""&&mustPayAdvance)
        {
           $("img#imgSettlementTerms_AdvancePercentage_Required").css("display","inline-block");
        }
    }
   
</script>
	
<?php mysqli_close($con_main); ?>

        