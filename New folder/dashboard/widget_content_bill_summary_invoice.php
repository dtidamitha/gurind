<table class="table table-striped table-vcenter table-condensed">
<thead>
    <tr>
        <th class="text-center">Invoice Date</th>
        <th class="text-center">Total Due</th>
        <th class="text-center">Comp Payable</th>
        <th class="text-center">Emp Payable</th>
    </tr>
</thead>

<tbody>
<?php
    $where = "";
    
    if ($sp == 1){

        if (!empty($yr)){
            $where .= (empty($where)) ? " WHERE " : " AND ";
            $where .= " YEAR(DH.BILL_DATE_TO) = '$yr' ";
        }
    
        if (!empty($mn)){
            $where .= (empty($where)) ? " WHERE " : " AND ";
            $where .= " MONTH(DH.BILL_DATE_TO) = '$mn' ";
        }

        $query_record_id = "SELECT
        DH.RECORD_ID,
        DH.INVOICE_NO,
        DH.BILL_DATE_TO
        FROM
        dialog_header AS DH".$where."
        GROUP BY
        DH.INVOICE_NO";

        $sql_record_id = mysqli_query($con_main,$query_record_id);

        while ($record = mysqli_fetch_assoc($sql_record_id)){
            $record_id = $record['RECORD_ID'];
            $invoice_no = $record['INVOICE_NO'];
            $invoice_date = $record['BILL_DATE_TO'];

            $query = "SELECT
            MU.EMP_NO,
            CONCAT_WS(' ',MU.FIRST_NAME,MU.LAST_NAME) AS EMP_NAME,
            MD.DEPARTMENT,
            DDS.MOBILE_NO,
            MSP.SHORT_NAME AS SERVICE_PROVIDER,
            MCT.CON_TYPE AS CONNECTION_TYPE,
            MN.`LIMIT`,
            IF(MN.UNLIMITED=1,'YES','NO') AS UNLIMITED,
            DDS.MONTHLY_RENTAL,
            DDS.TOTAL_USAGE_CHARGES,
            DDS.TOTAL_TAX,
            DDS.TOTAL_BILL_AMOUNT,
            DDS.TOTAL_DUE
            FROM
            dialog_detail_summary AS DDS
            LEFT JOIN mobi_number AS MN ON DDS.MOBILE_NO = MN.NUMBER
            LEFT JOIN mas_user AS MU ON MN.USER_ID = MU.USER_CODE
            LEFT JOIN mobi_con_type AS MCT ON MN.CON_TYPE = MCT.CON_TYPE_ID
            LEFT JOIN mobi_service_provider AS MSP ON MN.SERVICE_PROVIDER = MSP.SP_ID
            LEFT JOIN mas_department AS MD ON MU.DEPARTMENT = MD.DEP_CODE
            WHERE DDS.RECORD_ID=$record_id AND
            MN.NUMBER <> '0.00'";

            $sql = mysqli_query ($con_main, $query);

            $total_company_payable = 0;
            $total_employee_payable = 0;
            $total_total_due = 0;

            while ($row = mysqli_fetch_array ($sql)){
                $company_payable = 0;
                $company_payable_balance = 0;
                $employee_payable = 0;
                $total_due = (double)$row['TOTAL_DUE'];
                $total_tax = (double)$row['TOTAL_TAX'];
                $limit = (double)$row['LIMIT'];
                $monthly_rental = (double)$row['MONTHLY_RENTAL'];
        
                if ($row['UNLIMITED'] == 0){
                    $company_payable = $monthly_rental + $total_tax;
                    $company_payable_balance = $total_due - $company_payable;
                    $employee_payable = $company_payable_balance - $limit;
                    $employee_payable = ($employee_payable < 0) ? 0 : $employee_payable;
                }else{
                    $company_payable = $total_due;
                    $employee_payable = 0;
                }

                $total_total_due = $total_total_due + $total_due;
                $total_company_payable = $total_company_payable + $company_payable;
                $total_employee_payable = $total_employee_payable + $employee_payable;
            }

            echo ('<tr>');
            echo ('<td align="left">'.$invoice_date.' ('.$invoice_no.')</td>');
            echo ('<td align="right">'.number_format($total_total_due,2).'</td>');
            echo ('<td align="right">'.number_format($total_company_payable,2).'</td>');
            echo ('<td align="right">'.number_format($total_employee_payable,2).'</td>');
            echo ('</tr>');
        }

    }else if($sp == 2){
    
        if (!empty($yr)){
            $where .= (empty($where)) ? " WHERE " : " AND ";
            $where .= " YEAR(MH.DUE_DATE) = '$yr' ";
        }
    
        if (!empty($mn)){
            $where .= (empty($where)) ? " WHERE " : " AND ";
            $where .= " MONTH(MH.DUE_DATE) = '$mn' ";
        }

        $query_record_id = "SELECT
        MH.RECORD_ID,
        MH.BILL_NO,
        MH.DUE_DATE
        FROM
        mobitel_header AS MH".$where."
        GROUP BY
        MH.BILL_NO";

        $sql_record_id = mysqli_query($con_main,$query_record_id);

        while ($record = mysqli_fetch_assoc($sql_record_id)){
            $record_id = $record['RECORD_ID'];
            $invoice_no = $record['BILL_NO'];
            $invoice_date = $record['DUE_DATE'];
    
            $query = "SELECT
            MU.EMP_NO,
            CONCAT_WS(' ',MU.FIRST_NAME,MU.LAST_NAME) AS EMP_NAME,
            MD.DEPARTMENT,
            MDS.MOBILE_NO,
            MS.SHORT_NAME AS SERVICE_PROVIDER,
            MC.CON_TYPE AS CONNECTION_TYPE,
            MN.`LIMIT`,
            IF(MN.UNLIMITED=1,'YES','NO') AS UNLIMITED,
            MDS.MONTHLY_RENTAL,
            MDS.TOTAL_USAGE_CHARGES,
            MDS.TOTAL_TAX,
            MDS.TOTAL_DUE AS TOTAL_BILL_AMOUNT,
            MDS.TOTAL_DUE
            FROM
            mobitel_detail_summary AS MDS
            LEFT JOIN mobi_number AS MN ON MDS.MOBILE_NO = MN.NUMBER
            LEFT JOIN mas_user AS MU ON MN.USER_ID = MU.USER_CODE
            LEFT JOIN mas_department AS MD ON MU.DEPARTMENT = MD.DEP_CODE
            LEFT JOIN mobi_service_provider AS MS ON MN.SERVICE_PROVIDER = MS.SP_ID
            LEFT JOIN mobi_con_type AS MC ON MN.CON_TYPE = MC.CON_TYPE_ID
            WHERE MDS.RECORD_ID=$record_id";

            $sql = mysqli_query ($con_main, $query);

            $total_company_payable = 0;
            $total_employee_payable = 0;
            $total_total_due = 0;

            while ($row = mysqli_fetch_array ($sql)){
                $company_payable = 0;
                $company_payable_balance = 0;
                $employee_payable = 0;
                $total_due = (double)$row['TOTAL_DUE'];
                $total_tax = (double)$row['TOTAL_TAX'];
                $limit = (double)$row['LIMIT'];
                $monthly_rental = (double)$row['MONTHLY_RENTAL'];

                if ($row['UNLIMITED'] == 0){
                    $company_payable = $monthly_rental + $total_tax;
                    $company_payable_balance = $total_due - $company_payable;
                    $employee_payable = $company_payable_balance - $limit;
                    $employee_payable = ($employee_payable < 0) ? 0 : $employee_payable;
                }else{
                    $company_payable = $total_due;
                    $employee_payable = 0;
                }

                $total_total_due = $total_total_due + $total_due;
                $total_company_payable = $total_company_payable + $company_payable;
                $total_employee_payable = $total_employee_payable + $employee_payable;
            }

            echo ('<tr>');
            echo ('<td align="left">'.$invoice_date.' ('.$invoice_no.')</td>');
            echo ('<td align="right">'.number_format($total_total_due,2).'</td>');
            echo ('<td align="right">'.number_format($total_company_payable,2).'</td>');
            echo ('<td align="right">'.number_format($total_employee_payable,2).'</td>');
            echo ('</tr>');
        }
    }
?>
</tbody>
</table>