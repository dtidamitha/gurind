<table class="table table-striped table-vcenter table-condensed">
<thead>
    <tr>
        <th class="text-center">Mobile No</th>
        <th class="text-center">Name</th>
        <th class="text-center">Total Usage</th>
    </tr>
</thead>

<tbody>
<?php
    $where = "";

    if ($sp == 1){

        if (!empty($yr)){
            $where .= (empty($where)) ? " WHERE " : " AND ";
            $where .= " YEAR(DH.BILL_DATE_TO) = '$yr' ";
        }
    
        if (!empty($mn)){
            $where .= (empty($where)) ? " WHERE " : " AND ";
            $where .= " MONTH(DH.BILL_DATE_TO) = '$mn' ";
        }

        $query_record_id = "SELECT
        DH.RECORD_ID,
        DH.INVOICE_NO,
        DH.BILL_DATE_TO
        FROM
        dialog_header AS DH".$where."
        GROUP BY
        DH.INVOICE_NO";

        $sql_record_id = mysqli_query($con_main,$query_record_id);

        while ($record = mysqli_fetch_assoc($sql_record_id)){
            $record_id = $record['RECORD_ID'];

            $query = "SELECT
            DDS.MOBILE_NO,
            mas_user.EMP_NO,
            mas_user.FIRST_NAME,
            mas_user.LAST_NAME,
            DDS.TOTAL_DUE
            FROM
            dialog_detail_summary AS DDS
            INNER JOIN mobi_number ON DDS.MOBILE_NO = mobi_number.NUMBER
            INNER JOIN mas_user ON mobi_number.USER_ID = mas_user.USER_CODE
            WHERE
            DDS.RECORD_ID = $record_id AND
            mobi_number.CON_TYPE = $ct
            ORDER BY
            DDS.TOTAL_DUE DESC
            LIMIT 0, 10";

            $sql = mysqli_query ($con_main, $query);

            while ($row = mysqli_fetch_array ($sql)){
                $mobile_no = $row['MOBILE_NO'];
                $name = $row['FIRST_NAME']." ".$row['LAST_NAME'];
                $emp_no = $row['EMP_NO'];
                $name_text = $name." [".$emp_no."]";
                $total_usage = (double)$row['TOTAL_DUE'];
        
                echo ('<tr>');
                echo ('<td align="center">'.$mobile_no.'</td>');
                echo ('<td align="left">'.$name_text.'</td>');
                echo ('<td align="right">'.number_format($total_usage,2).'</td>');
                echo ('</tr>');
            }
        }

    }else if($sp == 2){
    
        if (!empty($yr)){
            $where .= (empty($where)) ? " WHERE " : " AND ";
            $where .= " YEAR(MH.DUE_DATE) = '$yr' ";
        }
    
        if (!empty($mn)){
            $where .= (empty($where)) ? " WHERE " : " AND ";
            $where .= " MONTH(MH.DUE_DATE) = '$mn' ";
        }

        $query_record_id = "SELECT
        MH.RECORD_ID,
        MH.BILL_NO,
        MH.DUE_DATE
        FROM
        mobitel_header AS MH".$where."
        GROUP BY
        MH.BILL_NO";

        $sql_record_id = mysqli_query($con_main,$query_record_id);

        while ($record = mysqli_fetch_assoc($sql_record_id)){
            $record_id = $record['RECORD_ID'];
    
            $query = "SELECT
            MDS.MOBILE_NO,
            MU.EMP_NO,
            MU.FIRST_NAME,
            MU.LAST_NAME,
            MDS.TOTAL_DUE
            FROM
            mobitel_detail_summary AS MDS
            INNER JOIN mobi_number AS MN ON MDS.MOBILE_NO = MN.NUMBER
            INNER JOIN mas_user AS MU ON MN.USER_ID = MU.USER_CODE
            WHERE
            MDS.RECORD_ID = $record_id AND
            MN.CON_TYPE = $ct
            ORDER BY
            MDS.TOTAL_DUE DESC
            LIMIT 0, 10";

            $sql = mysqli_query ($con_main, $query);

            while ($row = mysqli_fetch_array ($sql)){
                $mobile_no = $row['MOBILE_NO'];
                $name = $row['FIRST_NAME']." ".$row['LAST_NAME'];
                $emp_no = $row['EMP_NO'];
                $name_text = $name." [".$emp_no."]";
                $total_usage = (double)$row['TOTAL_DUE'];
        
                echo ('<tr>');
                echo ('<td align="center">'.$mobile_no.'</td>');
                echo ('<td align="left">'.$name_text.'</td>');
                echo ('<td align="right">'.number_format($total_usage,2).'</td>');
                echo ('</tr>');
            }
        }
    }
?>
</tbody>
</table>