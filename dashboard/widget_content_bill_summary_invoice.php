<?php 
  $from = (isset($_REQUEST['from'])) ? $_REQUEST['from'] : "";
  $to = (isset($_REQUEST['to'])) ? $_REQUEST['to'] : "";

  $where = "";

  if (!empty($from) && !empty($to)){
    $where .= (empty($where)) ? " WHERE " : " AND ";
    $where .= (!empty($from) && !empty($to)) ? "collection_adds.add_collect_date BETWEEN '$from' AND '$to' " : "";
  }
 ?>

<table class="table table-striped table-vcenter table-condensed">
<thead>
    <tr>
        <th class="text-center">Team Name</th>
        <th class="text-center">Team Code</th>
        <th class="text-center">Total Advertisements</th>
        
    </tr>
</thead>

<tbody>
<?php
  $add_count = "SELECT
                         collection_team.team_code,
                         collection_team.team_name,
                         Count(collection_adds.team_id) AS COUNT
                FROM
                         collection_adds 
              INNER JOIN collection_team ON collection_adds.team_id = collection_team.id ".$where."
              GROUP BY
                         collection_team.team_code,
                         collection_team.team_name,
                         collection_adds.team_id ";

 $add_count_result = mysqli_query($con_main,$add_count);

 while($row = mysqli_fetch_assoc($add_count_result)){

   ?>
   <tr>
       <td class="text-center"><?php echo ($row['team_name']);?></td>
       <td class="text-center"><?php echo ($row['team_code']);?></td>
       <td class="text-center"><?php echo ($row['COUNT']); ?></td>
   </tr>
<?php
 } 
?>
</tbody>
</table>



