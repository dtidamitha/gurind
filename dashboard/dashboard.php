<?php
	session_start();
	
	if (empty($_SESSION['ACCESS_CODE']) || $_SESSION['ACCESS_CODE'] == NULL){
		header ('Location: login.php');
		exit;
	}
	
	$folder_depth = "";
	$prefix = "";
	
	$folder_depth = substr_count($_SERVER["PHP_SELF"] , "/");
	$folder_depth = ($folder_depth == false) ? 2 : (int)$folder_depth;
	
    $prefix = str_repeat("../", $folder_depth - 2);
    
    $title_suffix = " Dashboard";

    $sp = (isset($_REQUEST['service_provider']) && !empty($_REQUEST['service_provider'])) ? $_REQUEST['service_provider'] : 1;
    $ct = (isset($_REQUEST['con_type']) && !empty($_REQUEST['con_type'])) ? $_REQUEST['con_type'] : 1;
    $yr = (isset($_REQUEST['year']) && !empty($_REQUEST['year'])) ? $_REQUEST['year'] : date('Y', strtotime('today'));
    $mn = (isset($_REQUEST['month']) && !empty($_REQUEST['month'])) ? $_REQUEST['month'] : date('n', strtotime('last month'));
?>
<?php include $prefix.'config.php'; ?>
<?php include $prefix.'menu.php'; ?>
<?php include $prefix.'template_start.php'; ?>
<?php include $prefix.'page_head.php'; ?>

<!-- Page content -->
<div id="page-content">
    <!-- Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-dashboard"></i>Dashboard<br><small>&nbsp;</small>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="../home.php">Home</a></li>
        <li>Dashboard</li>
    </ul>
    <!-- END Header -->

    <!-- Filter Block -->
    <div class="block">
        <!-- Filter Title -->
        <div class="block-title">
            <!-- Interactive block controls (initialized in js/app.js -> interactiveBlocks()) -->
            <div class="block-options pull-right">
                <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-toggle-content"><i class="fa fa-arrows-v"></i></a>
            </div>

            <h2>Heading 2</h2>
        </div>
        <!-- END Filter Title -->

        <!-- Filter Content -->
        <div class="block-content">
            <form id="form-main" name="form-main" action="dashboard.php" method="post" class="form-horizontal form-bordered">
                <div class="form-group">
                    <label class="col-md-2 control-label" for="from">From</label>
                <div class="col-md-4">
                       <input type="text" id="from" name="from" class="form-control" placeholder="yyyy-mm-dd">
                </div>


                    <label class="col-md-2 control-label" for="to">To</label>
                    <div class="col-md-4">
                       <input type="text" id="to" name="to" class="form-control" placeholder="yyyy-mm-dd">
                    </div>
                </div>

                <div class="form-group form-actions">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success btn-sm primary-btn pull-right"><i class="fa fa-angle-right"></i> Submit</button>
                        <button type="reset" class="btn btn-warning btn-sm"><i class="fa fa-repeat"></i> Reset</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- END Filter Content -->
    </div>
    <!-- END Filter Block -->

    <div class="row draggable-blocks">
        <div class="col-md-6 column">

            
            <div class="block">
                <div class="block-title">
                    <!-- Interactive block controls -->
                    <div class="block-options pull-right">
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-toggle-content"><i class="fa fa-arrows-v"></i></a>
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-toggle-fullscreen"><i class="fa fa-desktop"></i></a>
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-hide"><i class="fa fa-times"></i></a>
                    </div>

                    <h2>Heading 2</h2>
                </div>

                <div class="block-content">
                    <!-- <?php include('widget_content_bill_summary_invoice.php'); ?> -->
                </div>
            </div>

            <div class="block">
                <div class="block-title">
                    <!-- Interactive block controls -->
                    <div class="block-options pull-right">
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-toggle-content"><i class="fa fa-arrows-v"></i></a>
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-toggle-fullscreen"><i class="fa fa-desktop"></i></a>
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-hide"><i class="fa fa-times"></i></a>
                    </div>

                    <h2>Heading 2</h2>
                </div>
                
                <div class="block-content">
                    <div id="chart-classic" class="chart"></div>
                </div>
            </div>
        </div>

        <div class="col-md-6 column">
            
            <div class="block">
                <div class="block-title">
                    <!-- Interactive block controls -->
                    <div class="block-options pull-right">
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-toggle-content"><i class="fa fa-arrows-v"></i></a>
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-toggle-fullscreen"><i class="fa fa-desktop"></i></a>
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-hide"><i class="fa fa-times"></i></a>
                    </div>

                    <h2>Top Agents</h2>
                </div>
                
                <div class="block-content">
                    <!-- <?php include('widget_top_usage.php'); ?> -->
                </div>
            </div>   

        </div>
    </div>
</div>
<!-- END Page Content -->

<?php include $prefix.'page_footer.php'; ?>
<?php include $prefix.'template_scripts.php'; ?>
<?php include $prefix.'template_end.php'; ?>

<script type="text/javascript">

    /*$('#form-main').on('submit', function(e){
        e.preventDefault();
        var form_data = $('#form-main').serializeArray();
        form_data.push({name: 'request', value : 'data_load'});

        $.ajax({
            url : 'widget_top_usage.php',
            method : 'post',
            data : form_data,
            error : function(e){
                alert (e);
            },
            success : function(r){
                alert (r);
            }
        });
    });*/

   
   $('#from').datepicker({
            format: 'yyyy-mm-dd',
            //startDate: '-3d'
        });
                    $('#to').datepicker({
            format: 'yyyy-mm-dd',
            //startDate: '-3d'
        });

    $('.draggable-blocks').sortable({
        connectWith: '.block',
        items: '.block',
        opacity: 0.75,
        handle: '.block-title',
        placeholder: 'draggable-placeholder',
        tolerance: 'pointer',
        start: function(e, ui){
            ui.placeholder.css('height', ui.item.outerHeight());
        }
    });

    // Get the elements where we will attach the charts
    var chartClassic = $('#chart-classic');
    //var data_json_array = <?php include('data_month_overview_con_type.php'); ?>;

    // Classic Chart
    $.plot(chartClassic,data_json_array.coords,
        {
            colors: ['#3498db', '#333333'],
            legend: {show: true, position: 'nw', margin: [15, 10]},
            grid: {borderWidth: 0, hoverable: true, clickable: true},
            yaxis: {ticks: 4, tickColor: '#eeeeee'},
            xaxis: {ticks: data_json_array.x_axis, tickColor: '#ffffff'}
        }
    );

    // Creating and attaching a tooltip to the classic chart
    var previousPoint = null, ttlabel = null;
    chartClassic.bind('plothover', function(event, pos, item) {

        if (item) {
            if (previousPoint !== item.dataIndex) {
                previousPoint = item.dataIndex;

                $('#chart-tooltip').remove();
                var x = item.datapoint[0], y = item.datapoint[1];

                if (item.seriesIndex === 1) {
                    ttlabel = 'Rs <strong>' + y + '</strong>';
                } else {
                    ttlabel = 'Rs <strong>' + y + '</strong>';
                }

                $('<div id="chart-tooltip" class="chart-tooltip">' + ttlabel + '</div>')
                    .css({top: item.pageY - 45, left: item.pageX + 5}).appendTo("body").show();
            }
        }
        else {
            $('#chart-tooltip').remove();
            previousPoint = null;
        }
    });
</script>